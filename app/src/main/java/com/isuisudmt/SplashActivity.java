package com.isuisudmt;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.utility.ConnectionDetector;
import com.isuisudmt.utils.ForceUpdateAsync;

import java.util.ArrayList;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.content.ContentValues.TAG;
import static com.isuisudmt.utils.Constants.APP_UNIQUE_ID;

public class SplashActivity extends AppCompatActivity {

    Boolean navigationflag = false;
    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String ADMIN_NAME = "adminNameKey";
    public static final String CREATED_BY = "createdKey";
    public static final String MULTI_ADMINS = "multiAdminKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        //throw new RuntimeException("This is a crash test");

        setContentView(R.layout.activity_splash);
        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectionDetector cd = new ConnectionDetector(
                SplashActivity.this);
        if (cd.isConnectingToInternet()) {
            forceUpdate();
        } else {
            Toast.makeText(SplashActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    private void checkParams() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("AdminNameManagement").document(getPackageName());

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                Log.e(TAG, "onComplete: executed");
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        Log.e(TAG, "onComplete: has data status " + task.getResult().getData());
                        Map<String, Object> packageMap = task.getResult().getData();
                        String adminName = (String) packageMap.get("AdminName");
                        String createdBy = (String) packageMap.get("CreatedBy");

                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString(ADMIN_NAME, adminName);
                        editor.putString(CREATED_BY, createdBy);
                        editor.apply();

                        String appName = (String) packageMap.get("AppName");
                        String mainApp = (String) packageMap.get("MainApp");
                        String appCode = (String) packageMap.get("AppCode");

                        String codeString = getString(R.string.app_code);

                        if (packageMap.containsKey("MultiAdmins")) {
                            ArrayList<String> names = (ArrayList<String>) packageMap.get("MultiAdmins");
                            sp.edit().putString(MULTI_ADMINS, String.valueOf(names)).apply();
                        }

                        if (!codeString.equalsIgnoreCase(appCode)) {

                            if (!checkPermission()){
                                //  Intent intent = new Intent(SplashActivity.this, StoreListActivity.class);
                                Intent intent = new Intent(SplashActivity.this, PermissionActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            }else{
                                //  Intent intent = new Intent(SplashActivity.this, StoreListActivity.class);
                                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                intent.putExtra("appName", appName);
                                intent.putExtra("mainApp", mainApp);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }


                        } else {
                            if (packageMap.containsKey("UniqueId")) {

                                String value = (String) packageMap.get("UniqueId");

                                if (value.equalsIgnoreCase(APP_UNIQUE_ID)) {

                                    if (navigationflag == false) {

                                        if (!checkPermission()){
                                            //  Intent intent = new Intent(SplashActivity.this, StoreListActivity.class);
                                            Intent intent = new Intent(SplashActivity.this, PermissionActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                            finish();

                                        }else{
                                            //  Intent intent = new Intent(SplashActivity.this, StoreListActivity.class);
                                            Intent in = new Intent(SplashActivity.this, LoginActivity.class);
                                            startActivity(in);
                                            finish();
                                            navigationflag = true;
                                        }


                                    }


                                } else {
                                    showStatusFailure("Unauthorized, please call or email helpdesk to continue service.");
                                }
                            }

                        }

                    }
                } else {
                    Log.e(TAG, "onComplete: error fetching data");
                    showStatusFailure("Failed to connect. Please try after sometimes.");
                }
            }
        });

    }


    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;

        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String currentVersion = packageInfo.versionName;

        new ForceUpdateAsync(currentVersion, SplashActivity.this, new ForceUpdateAsync.AsyncListener() {
            @Override
            public void doStuff(String latestVersion, String currentVersion) {
                if (latestVersion != null) {
                    if (!currentVersion.equalsIgnoreCase(latestVersion)) {

                        showForceUpdateDialog(latestVersion);

                    } else {
                        checkParams();
                    }
                } else {
                    checkParams();
                }
            }
        }).execute();
    }


    private void showStatusFailure(final String statusDesc) {
        try {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(SplashActivity.this);
            builder1.setMessage(statusDesc);
            builder1.setTitle("Alert");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } catch (Exception e) {

        }

    }

    public void showForceUpdateDialog(final String latestVersion) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    AlertDialog.Builder alertbuilderupdate;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        alertbuilderupdate = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                    } else {
                        alertbuilderupdate = new AlertDialog.Builder(SplashActivity.this);
                    }
                    alertbuilderupdate.setCancelable(false);
                    String message = "A new version of this app is available. Please update to version " + latestVersion + " " + getResources().getString(R.string.youAreNotUpdatedMessage1);
                    alertbuilderupdate.setTitle(getResources().getString(R.string.youAreNotUpdatedTitle))
                            .setMessage(message)
                            .setPositiveButton(getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert11 = alertbuilderupdate.create();
                    alert11.show();
                } catch (Exception e) {

                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }
}
