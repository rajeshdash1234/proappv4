package com.isuisudmt.dth;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface DthApi {

    @POST()
    Call<DthResponse> getDth(@Header("Authorization") String token, @Body DthRequest dthRequest, @Url String url);

}
