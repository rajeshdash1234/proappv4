package com.isuisudmt;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.isuisudmt.utility.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePasswordFragment extends Fragment {

    TextInputEditText confirm_pwd, otp, new_pwd, old_pwd;
    TextInputLayout otpLayout, newPwdLayout, oldPwdLayout, confirmPwdLayout;
    Button sendOtp, changePwdBtn;
    String newPassword, oldPassword;
    int userOtp = 0;
    ProgressBar progressBar;
    TextView resendotp;
    String tokenStr = "";
    SessionManager session;

    public static ChangePasswordFragment newInstance(String param1, String param2) {
        ChangePasswordFragment fragment = new ChangePasswordFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        new CustomThemes(getActivity());
        View rootview =  inflater.inflate(R.layout.activity_change_password, container, false);
        
        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        confirm_pwd = getActivity().findViewById(R.id.confirm_pwd);
        otp = getActivity().findViewById(R.id.otp);
        new_pwd = getActivity().findViewById(R.id.new_pwd);
        old_pwd = getActivity().findViewById(R.id.old_pwd);
        otpLayout = getActivity().findViewById(R.id.otpLayout);
        newPwdLayout = getActivity().findViewById(R.id.newPwdLayout);
        oldPwdLayout = getActivity().findViewById(R.id.oldPwdLayout);
        confirmPwdLayout = getActivity().findViewById(R.id.confirmPwdLayout);
        sendOtp = getActivity().findViewById(R.id.sendOtp);
        changePwdBtn = getActivity().findViewById(R.id.changePwdBtn);
        progressBar = getActivity().findViewById(R.id.progressBar);
        resendotp = getActivity().findViewById(R.id.resendotp);
        //otpLayout, newPwdLayout, oldPwdLayout, confirmPwdLayout;

        /*Added by RAshmi RAnjan*/
        otpLayout.setBoxStrokeColorStateList(AppCompatResources.getColorStateList(getActivity(), R.color.text_input_layout_stroke_color));
        newPwdLayout.setBoxStrokeColorStateList(AppCompatResources.getColorStateList(getActivity(), R.color.text_input_layout_stroke_color));
        oldPwdLayout.setBoxStrokeColorStateList(AppCompatResources.getColorStateList(getActivity(), R.color.text_input_layout_stroke_color));
        confirmPwdLayout.setBoxStrokeColorStateList(AppCompatResources.getColorStateList(getActivity(), R.color.text_input_layout_stroke_color));

        if (old_pwd.hasFocus()) {
            old_pwd.setCursorVisible(true);
        }

        old_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    oldPwdLayout.setError("Enter the exact old Password");
                } else {
                    oldPwdLayout.setError(null);
                }
            }
        });

        new_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


             /*   if (!isValidPassword(new_pwd.getText().toString())) {
                    newPwdLayout.setError(getResources().getString(R.string.changePasswordText));
                } else {
                    newPwdLayout.setError(null);
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    newPwdLayout.setError("Enter the maximum 6 characters");
                }
                else {
                    newPwdLayout.setError(null);
                }

            }
        });

        confirm_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


              /*  if (!isValidPassword(confirm_pwd.getText().toString())) {
                    confirmPwdLayout.setError(getResources().getString(R.string.changePasswordText));
                } else {
                    confirmPwdLayout.setError(null);
                }*/

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 1) {
                    confirmPwdLayout.setError("Enter the maximum 6 characters");
                }
                else {
                    confirmPwdLayout.setError(null);
                }

            }
        });

        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPassword = old_pwd.getText().toString().trim();
                newPassword = new_pwd.getText().toString().trim();
                String con_pwd = confirm_pwd.getText().toString().trim();


                if (oldPassword.length() != 0) {

                    if (newPassword.length() != 0) {

                        if (con_pwd.length() != 0) {
                            if (!oldPassword.equalsIgnoreCase(newPassword)) {

                                if (newPassword.equalsIgnoreCase(con_pwd) && isValidPassword(confirm_pwd.getText().toString())) {
                                    ConnectionDetector cd = new ConnectionDetector(
                                            getActivity());
                                    if (cd.isConnectingToInternet()) {

                                        progressBar.setVisibility(View.VISIBLE);
                                        getEncodeUrlCode();


                                    } else {
                                        Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Confirm password should match with new password", Toast.LENGTH_LONG).show();

                                }

                            } else {
                                Toast.makeText(getActivity(), "Old and new password can't be same .", Toast.LENGTH_LONG).show();

                            }
                        } else {
                            Toast.makeText(getActivity(), "Enter Confirm Password.", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Toast.makeText(getActivity(), "Enter New Password.", Toast.LENGTH_LONG).show();
                    }


                } else {
                    Toast.makeText(getActivity(), "Enter Old Password.", Toast.LENGTH_LONG).show();

                }

            }
        });

        resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /*Call api to send the otp to the registered mobile number*/
                ConnectionDetector cd = new ConnectionDetector(
                        getActivity());
                if (cd.isConnectingToInternet()) {

                    if (oldPassword.length() != 0 && newPassword.length() != 0) {
                        progressBar.setVisibility(View.VISIBLE);
                        getEncodeUrlCode();


                    } else {
                        Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        changePwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                /*Call api to send the otp to the registered mobile number*/
                ConnectionDetector cd = new ConnectionDetector(
                        getActivity());
                if (cd.isConnectingToInternet()) {

                    if(otp.getText().toString().equalsIgnoreCase("null") || otp.getText().toString().equalsIgnoreCase("")){

                        Toast.makeText(getActivity(), "Enter the OTP to Change Password", Toast.LENGTH_LONG).show();



                    }else{
                        oldPassword = old_pwd.getText().toString().trim();
                        newPassword = new_pwd.getText().toString().trim();
                        String otpvalue = otp.getText().toString().trim();
                        userOtp = Integer.parseInt(otpvalue);

                        if (oldPassword.length() != 0 && newPassword.length() != 0 && userOtp != 0) {
                            progressBar.setVisibility(View.VISIBLE);
                            getEncodeUrlCodeforPwd();


                        } else {
                            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_LONG).show();
                        }
                    }


                }
            }
        });
        return rootview;
    }

    private void getEncodeUrlCode() {

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v97")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            sendOtp(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                    }

                });
    }

    private void sendOtp(String base64) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("oldPassword", oldPassword);
            obj.put("newPassword", newPassword);
            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                if (status.equals("0")) {
                                    changePwdBtn.setVisibility(View.VISIBLE);
                                    otpLayout.setVisibility(View.VISIBLE);
                                    oldPwdLayout.setVisibility(View.GONE);
                                    newPwdLayout.setVisibility(View.GONE);
                                    confirmPwdLayout.setVisibility(View.GONE);
                                    resendotp.setVisibility(View.VISIBLE);
                                    sendOtp.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), obj.getString("statusDesc").toString(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.GONE);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(anError.getErrorBody().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Toast.makeText(getActivity(), obj.getString("statusDesc").toString(), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getEncodeUrlCodeforPwd() {

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v96")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            ChangePassword(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                    }

                });
    }

    private void ChangePassword(String base64) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("oldPassword", oldPassword);
            obj.put("password", newPassword);
            obj.put("otp", userOtp);

            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                String msg = obj.getString("statusDesc");
                                if (status.equals("0")) {
                                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getActivity(), MainActivity.class);
                                    startActivity(i);
                                    getActivity().finish();
                                } else {
                                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.GONE);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(anError.getErrorBody().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Toast.makeText(getActivity(), obj.getString("statusDesc").toString(), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isValidPassword(String password) {
        Matcher matcher = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{4,20})").matcher(password);
        return matcher.matches();
    }

   }