package com.isuisudmt.dmt;

public class VerifyBeneRequest {

    public VerifyBeneRequest(String coustomerId, String toAccount, String bankName, String beneName, String ifscCode, String pipeNo) {
        this.coustomerId = coustomerId;
        this.toAccount = toAccount;
        this.bankName = bankName;
        this.beneName = beneName;
        this.ifscCode = ifscCode;
        this.pipeNo = pipeNo;
    }

    private String coustomerId;

    private String toAccount;

    private String bankName;

    private String beneName;

    private String ifscCode;

    private String pipeNo;

    public String getpipeNo ()
    {
        return pipeNo;
    }

    public void setpipeNo (String coustomerId)
    {
        this.pipeNo = pipeNo;
    }

    public String getCoustomerId ()
    {
        return coustomerId;
    }

    public void setCoustomerId (String coustomerId)
    {
        this.coustomerId = coustomerId;
    }

    public String getToAccount ()
    {
        return toAccount;
    }

    public void setToAccount (String toAccount)
    {
        this.toAccount = toAccount;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getBeneName ()
    {
        return beneName;
    }

    public void setBeneName (String beneName)
    {
        this.beneName = beneName;
    }

    public String getIfscCode ()
    {
        return ifscCode;
    }

    public void setIfscCode (String ifscCode)
    {
        this.ifscCode = ifscCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [coustomerId = "+coustomerId+", toAccount = "+toAccount+", bankName = "+bankName+", beneName = "+beneName+", ifscCode = "+ifscCode+"]";
    }

}
