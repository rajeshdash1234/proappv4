package com.isuisudmt.dmt;

public class ResendOtpResponse {

    private String status;

    private String statusDesc;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getStatusDesc ()
    {
        return statusDesc;
    }

    public void setStatusDesc (String statusDesc)
    {
        this.statusDesc = statusDesc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", statusDesc = "+statusDesc+"]";
    }

}
