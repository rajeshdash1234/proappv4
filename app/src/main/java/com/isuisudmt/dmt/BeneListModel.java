package com.isuisudmt.dmt;

public class BeneListModel {

    private String id;

    @Override
    public String toString() {
        return "BeneListModel{" +
                "accountNo='" + accountNo + '\'' +
                ", beneName='" + beneName + '\'' +
                '}';
    }

    private String status;

    private String beneMobileNo;

    private String customerNumber;

    private String bankName;

    private String accountNo;

    private String beneName;

    private String ifscCode;

    private String createdDate;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getBeneMobileNo ()
    {
        return beneMobileNo;
    }

    public void setBeneMobileNo (String beneMobileNo)
    {
        this.beneMobileNo = beneMobileNo;
    }

    public String getCustomerNumber ()
    {
        return customerNumber;
    }

    public void setCustomerNumber (String customerNumber)
    {
        this.customerNumber = customerNumber;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getAccountNo ()
    {
        return accountNo;
    }

    public void setAccountNo (String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getBeneName() {
        return beneName;
    }

    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }

    public String getIfscCode ()
    {
        return ifscCode;
    }

    public void setIfscCode (String ifscCode)
    {
        this.ifscCode = ifscCode;
    }

    public String getCreatedDate ()
    {
        return createdDate;
    }

    public void setCreatedDate (String createdDate)
    {
        this.createdDate = createdDate;
    }


    /*@Override
    public String toString() {
        return "BeneListModel{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", beneMobileNo='" + beneMobileNo + '\'' +
                ", customerNumber='" + customerNumber + '\'' +
                ", bankName='" + bankName + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", beneName='" + beneName + '\'' +
                ", ifscCode='" + ifscCode + '\'' +
                ", createdDate='" + createdDate + '\'' +
                '}';
    }*/
}
