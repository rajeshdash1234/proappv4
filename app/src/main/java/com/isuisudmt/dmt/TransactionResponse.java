package com.isuisudmt.dmt;

public class TransactionResponse {

        private String clientUniqueId;



    private String status;

        private String beneName;

        private String txnId;

        private String statusDesc;

        public String getClientUniqueId ()
        {
            return clientUniqueId;
        }

        public void setClientUniqueId (String clientUniqueId)
        {
            this.clientUniqueId = clientUniqueId;
        }

        public String getStatus ()
        {
            return status;
        }

        public void setStatus (String status)
        {
            this.status = status;
        }

        public String getBeneName ()
        {
            return beneName;
        }

        public void setBeneName (String beneName)
        {
            this.beneName = beneName;
        }

        public String getTxnId ()
        {
            return txnId;
        }

        public void setTxnId (String txnId)
        {
            this.txnId = txnId;
        }

        public String getStatusDesc ()
        {
            return statusDesc;
        }

        public void setStatusDesc (String statusDesc)
        {
            this.statusDesc = statusDesc;
        }

    }
