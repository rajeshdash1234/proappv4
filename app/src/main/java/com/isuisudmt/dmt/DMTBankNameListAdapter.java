package com.isuisudmt.dmt;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 6/27/2018.
 */

public class DMTBankNameListAdapter extends RecyclerView.Adapter<DMTBankNameListAdapter.BankViewHolder> implements Filterable {
    private int lastSelectedPosition = -1;

    public List<String> bankNameModelList,filterList;
    RecyclerViewClickListener recyclerViewClickListener;
    Activity activity;

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }
    public class BankViewHolder extends RecyclerView.ViewHolder {
        public RadioButton bankName;
        public TextView bank_name;
        public ImageView logoImg;

        public BankViewHolder(View view) {
            super ( view );
            bankName = view.findViewById ( R.id.bankName );
            bank_name = view.findViewById(R.id.bank_name);
            logoImg = view.findViewById(R.id.logoImg);

            bankName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    recyclerViewClickListener.recyclerViewListClicked(v,lastSelectedPosition);

                }
            });

            bank_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    recyclerViewClickListener.recyclerViewListClicked(v,lastSelectedPosition);

                }
            });


        }
    }

    public String getItem(int position) {
        return filterList.get(position);
    }
    public DMTBankNameListAdapter(Activity activity, List<String> bankNameModelList, RecyclerViewClickListener recyclerViewClickListener) {
        this.bankNameModelList = bankNameModelList;
        this.filterList = bankNameModelList;
        this.recyclerViewClickListener = recyclerViewClickListener;
        this.activity = activity;
    }

    @Override
    public BankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dmt_bank_row_list_item, parent, false);
        return new BankViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BankViewHolder holder, int position) {
        //BankListModel bankNameModel = filterList.get(position);
        holder.bank_name.setText(filterList.get(position));
        String imgtemp = filterList.get(position).replaceAll("[^a-zA-Z0-9]", "");
        imgtemp = imgtemp.replaceAll(" ","").toLowerCase();

      /*  String imageStr = "https://firebasestorage.googleapis.com/v0/b/fir-banklogostore.appspot.com/o/BankList%2F"+imgtemp+".png?alt=media&token=6b39d09d-145b-41af-bcbd-dba2f7676688";

        Picasso.with(activity)
                .load(imageStr)
                .placeholder(R.drawable.ic_bank)
                .error(R.drawable.ic_bank)
                .into(holder.logoImg);*/

        holder.bankName.setChecked(lastSelectedPosition == position);
    }

    @Override
    public int getItemCount() {
        return filterList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filterList = bankNameModelList;
                } else {
                    List<String> filteredList = new ArrayList<>();
                    for (String row : bankNameModelList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match

                        if (row.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filterList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filterList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterList = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}

