package com.isuisudmt.dmt;

public class AddNPayRequest {

    public AddNPayRequest(String amount, String recipientMobileNo, String transactionType, String recipientName, String accIfsc, String bankName, String accountNo, String customerMobileNo, String pipeNo) {
        this.amount = amount;
        this.recipientMobileNo = recipientMobileNo;
        this.transactionType = transactionType;
        this.recipientName = recipientName;
        this.accIfsc = accIfsc;
        this.bankName = bankName;
        this.accountNo = accountNo;
        this.customerMobileNo = customerMobileNo;
        this.pipeNo = pipeNo;
    }

    private String amount;

    private String recipientMobileNo;

    @Override
    public String toString() {
        return "AddNPayRequest{" +
                "amount='" + amount + '\'' +
                ", recipientMobileNo='" + recipientMobileNo + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", recipientName='" + recipientName + '\'' +
                ", accIfsc='" + accIfsc + '\'' +
                ", bankName='" + bankName + '\'' +
                ", accountNo='" + accountNo + '\'' +
                ", customerMobileNo='" + customerMobileNo + '\'' +
                ", pipeNo='" + pipeNo + '\'' +
                '}';
    }

    private String transactionType;

    private String recipientName;

    private String accIfsc;

    private String bankName;

    private String accountNo;

    private String customerMobileNo;

    private String pipeNo;

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getRecipientMobileNo ()
    {
        return recipientMobileNo;
    }

    public void setRecipientMobileNo (String recipientMobileNo)
    {
        this.recipientMobileNo = recipientMobileNo;
    }

    public String getTransactionType ()
    {
        return transactionType;
    }

    public void setTransactionType (String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getRecipientName ()
    {
        return recipientName;
    }

    public void setRecipientName (String recipientName)
    {
        this.recipientName = recipientName;
    }

    public String getAccIfsc ()
    {
        return accIfsc;
    }

    public void setAccIfsc (String accIfsc)
    {
        this.accIfsc = accIfsc;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getAccountNo ()
    {
        return accountNo;
    }

    public void setAccountNo (String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getCustomerMobileNo ()
    {
        return customerMobileNo;
    }

    public void setCustomerMobileNo (String customerMobileNo)
    {
        this.customerMobileNo = customerMobileNo;
    }

    public String getPipeNo ()
    {
        return pipeNo;
    }

    public void setPipeNo (String pipeNo)
    {
        this.pipeNo = pipeNo;
    }

}
