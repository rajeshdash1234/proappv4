package com.isuisudmt.dmt;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface AddBeneAPI {
    @POST()
    Call<AddBeneResponse> getAddBene(@Header("Authorization") String token, @Body AddBeneRequest addBeneRequestBody, @Url String url);
}
