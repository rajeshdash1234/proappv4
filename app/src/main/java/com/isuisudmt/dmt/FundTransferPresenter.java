package com.isuisudmt.dmt;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.isuisudmt.utils.APIFundTransferService;
import com.matm.matmsdk.aepsmodule.balanceenquiry.BalanceEnquiryResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.isuisudmt.BuildConfig.GET_LOAD_WALLET_URL;


public class FundTransferPresenter implements FundTransferContract.UserActionsListener {

    private FundTransferContract.View walletView;
    private APIFundTransferService apiFundTransferService;

    public FundTransferPresenter(FundTransferContract.View walletView) {
        this.walletView = walletView;

    }



    @Override
    public void loadWallet(final String token, final String mob) {
        if (token != null) {

            walletView.showLoader();

            JSONObject obj = new JSONObject();
            try {
                obj.put("number", mob);

                AndroidNetworking.post(GET_LOAD_WALLET_URL)
                        .setPriority(Priority.HIGH)
                        .addJSONObjectBody(obj)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    JSONObject obj = new JSONObject(response.toString());
                                    String key = obj.getString("hello");
                                    System.out.println(">>>>-----" + key);
                                    byte[] data = Base64.decode(key, Base64.DEFAULT);
                                    String encodedUrl = new String(data, "UTF-8");

                                    if (encodedUrl.contains("https://nginx.iserveu.tech")) {
                                        encodedUrl = encodedUrl.replace("nginx", "fino1");
                                    }

                                    System.out.println(">>>>-----" + encodedUrl);
                                    encodedLoadWallet(walletView, apiFundTransferService, token, encodedUrl);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                walletView.hideLoader();
                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();
                walletView.hideLoader();
            }
        }
    }

    public void encodedLoadWallet(final FundTransferContract.View walletVieww, APIFundTransferService apiFundTransferService, String token, String encodedUrl) {
        if (apiFundTransferService == null) {
            apiFundTransferService = new APIFundTransferService();
        }

        FundTransferAPI fundTransferAPI = this.apiFundTransferService.getClient().create(FundTransferAPI.class);
        Call<FundTransferResponse> respuesta = fundTransferAPI.getStatus(token, encodedUrl);
        respuesta.enqueue(new Callback<FundTransferResponse>() {
            @Override
            public void onResponse(Call<FundTransferResponse> call, Response<FundTransferResponse> response) {
                if (response.isSuccessful()) {

                    String status = response.body().getStatus();

                    String customerId = response.body().getCustomerId();

                    String messageStatus = response.body().getStatusDesc();

                    walletVieww.status();


                    if (status.equals("0")) {

                        String gateway = String.valueOf(response.body().getGateWayList());

                        for (int i = 0; i < response.body().getGateWayList().size(); i++) {
                            walletVieww.showWallet(status, customerId, response.body().getGateWayList().get(i).getId(),
                                    response.body().getGateWayList().get(i).isPipeFlag(),
                                    response.body().getGateWayList().get(i).getBalance(),
                                    response.body().getGateWayList().get(i).getChannelList().isImps(),
                                    response.body().getGateWayList().get(i).getChannelList().isNeft(),
                                    response.body().getGateWayList().get(i).isVerificationFlag());
//                                    response.body().getGateWayList().get(i).isSplitService());
                            walletVieww.hideLoader();
                            walletVieww.showMessage(0);

                        }

                        BeneListModel[] benilist = response.body().getBeneList();

                        if (benilist.length == 0) {
                            BeneListModel abc = new BeneListModel();
                            walletVieww.showBeniName(abc);
                        } else {
                            for (int i = 0; i < benilist.length; i++) {
                                walletVieww.showBeniName(benilist[i]);
                            }
                        }
                    } else if (status.equals("11")) {
                        walletVieww.hideLoader();
                        int message = 11;
                        walletVieww.showMessage(message);
                    } else if (status.equals("12")) {
                        walletVieww.hideLoader();
                        int message = 12;
                        walletVieww.showMessage(message);
                    } else {
                        walletVieww.hideLoader();
                        int message = 1;
                        walletVieww.showMessage(message);
                    }
                } else {
                    try {
                        if (response.errorBody() != null) {
                            JsonParser parser = new JsonParser();
                            JsonElement mJson = null;
                            try {
                                mJson = parser.parse(response.errorBody().string());
                                Gson gson = new Gson();
                                BalanceEnquiryResponse errorResponse = gson.fromJson(mJson, BalanceEnquiryResponse.class);
                                JSONObject obj = new JSONObject(mJson.toString());
                                String statusCode = errorResponse.getStatus();


                            } catch (IOException | JSONException ex) {
                                ex.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }


            @Override
            public void onFailure(Call<FundTransferResponse> call, Throwable t) {
                Log.d("STATUS", "noStatus9999" + t);
            }
        });
    }
}
