package com.isuisudmt.dmt;

public class FundTransferContract {

    public interface View {
        // void showBeniName(String beneName,String accountNo,String bankName,String ifscCode);
        void showWallet(String status, String customerId, long id, Boolean flag, Double balance, Boolean imps, Boolean neft, Boolean verificationFlag);
        void showMessage(int message);
        void showBeniName(BeneListModel beneListModel);
        void showLoader();
        void hideLoader();
        void status();
    }

    interface UserActionsListener {
        void loadWallet(String token, String mob);
    }
}
