package com.isuisudmt.dmt;

public class BeneModel {

    public String beneId;
    public String beneName;
    String customerNumber;
    String beneMobileNo;
    String bebeAccNo;
    String bankName;
    String accountNo;
    public String ifscCode;
    String beneStatus;

    public String getBeneId() {
        return beneId;
    }

    public void setBeneId(String beneId) {
        this.beneId = beneId;
    }

    public String getBeneName() {
        return beneName;
    }

    public void setBeneName(String beneName) {
        this.beneName = beneName;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getBeneMobileNo() {
        return beneMobileNo;
    }

    public void setBeneMobileNo(String beneMobileNo) {
        this.beneMobileNo = beneMobileNo;
    }

    public String getBebeAccNo() {
        return bebeAccNo;
    }

    public void setBebeAccNo(String bebeAccNo) {
        this.bebeAccNo = bebeAccNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBeneStatus() {
        return beneStatus;
    }

    public void setBeneStatus(String beneStatus) {
        this.beneStatus = beneStatus;
    }
}
