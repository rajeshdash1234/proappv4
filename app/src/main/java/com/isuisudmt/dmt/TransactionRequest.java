package com.isuisudmt.dmt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionRequest {

    public TransactionRequest(String amount, String coustomerId, String pipeNo, String beneId, String transactionMode) {
        this.amount = amount;
        this.coustomerId = coustomerId;
        this.pipeNo = pipeNo;
        this.beneId = beneId;
        this.transactionMode = transactionMode;
    }

    @SerializedName("amount")
    @Expose
    private String amount;



//    public TransactionRequest(String beneId, String coustomerId, String amount, String transactionMode, String pipeNo) {
//
//    }

    @Override
    public String toString() {
        return "TransactionRequest{" +
                "amount='" + amount + '\'' +
                ", coustomerId='" + coustomerId + '\'' +
                ", pipeNo='" + pipeNo + '\'' +
                ", beneId='" + beneId + '\'' +
                ", transactionMode='" + transactionMode + '\'' +
                '}';
    }
    @SerializedName("coustomerId")
    @Expose
    private String coustomerId;
    @SerializedName("pipeNo")
    @Expose
    private String pipeNo;
    @SerializedName("beneId")
    @Expose
    private String beneId;
    @SerializedName("transactionMode")
    @Expose
    private String transactionMode;

//    public TransactionRequest() {
//    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getCoustomerId ()
    {
        return coustomerId;
    }

    public void setCoustomerId (String coustomerId)
    {
        this.coustomerId = coustomerId;
    }

    public String getPipeNo ()
    {
        return pipeNo;
    }

    public void setPipeNo (String pipeNo)
    {
        this.pipeNo = pipeNo;
    }

    public String getBeneId ()
    {
        return beneId;
    }

    public void setBeneId (String beneId)
    {
        this.beneId = beneId;
    }

    public String getTransactionMode ()
    {
        return transactionMode;
    }

    public void setTransactionMode (String transactionMode)
    {
        this.transactionMode = transactionMode;
    }


}
