package com.isuisudmt.dmt;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FinoTransactionReports {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("toAccount")
    @Expose
    private String toAccount;
    @SerializedName("apiTid")
    @Expose
    private String apiTid;
    @SerializedName("apiComment")
    @Expose
    private String apiComment;
    @SerializedName("previousAmount")
    @Expose
    private Double previousAmount;
    @SerializedName("amountTransacted")
    @Expose
    private Integer amountTransacted;
    @SerializedName("balanceAmount")
    @Expose
    private Double balanceAmount;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("beniMobile")
    @Expose
    private String beniMobile;
    @SerializedName("benificiaryName")
    @Expose
    private String benificiaryName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("transactionMode")
    @Expose
    private String transactionMode;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;
    @SerializedName("masterName")
    @Expose
    private String masterName;
    @SerializedName("createdDate")
    @Expose
    private long createdDate;
    @SerializedName("updatedDate")
    @Expose
    private long updatedDate;
    @SerializedName("API")
    @Expose
    private String aPI;
    @SerializedName("routeID")
    @Expose
    private String routeID;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getApiTid() {
        return apiTid;
    }

    public void setApiTid(String apiTid) {
        this.apiTid = apiTid;
    }

    public String getApiComment() {
        return apiComment;
    }

    public void setApiComment(String apiComment) {
        this.apiComment = apiComment;
    }

    public Double getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(Double previousAmount) {
        this.previousAmount = previousAmount;
    }

    public Integer getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(Integer amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBeniMobile() {
        return beniMobile;
    }

    public void setBeniMobile(String beniMobile) {
        this.beniMobile = beniMobile;
    }

    public String getBenificiaryName() {
        return benificiaryName;
    }

    public void setBenificiaryName(String benificiaryName) {
        this.benificiaryName = benificiaryName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(long updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getAPI() {
        return aPI;
    }

    public void setAPI(String aPI) {
        this.aPI = aPI;
    }

    public String getRouteID() {
        return routeID;
    }

    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }
}
