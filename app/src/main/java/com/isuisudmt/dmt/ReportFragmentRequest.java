package com.isuisudmt.dmt;

public class ReportFragmentRequest {
    private String transactionType;

    public ReportFragmentRequest(String transactionType, String fromDate, String toDate) {
        this.transactionType = transactionType;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    private String fromDate;

    private String toDate;

    public String getTransactionType ()
    {
        return transactionType;
    }

    public void setTransactionType (String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getFromDate ()
    {
        return fromDate;
    }

    public void setFromDate (String fromDate)
    {
        this.fromDate = fromDate;
    }

    public String getToDate ()
    {
        return toDate;
    }

    public void setToDate (String toDate)
    {
        this.toDate = toDate;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [transactionType = "+transactionType+", fromDate = "+fromDate+", toDate = "+toDate+"]";
    }
}
