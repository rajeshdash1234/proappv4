package com.isuisudmt.dmt;

public class AddBeneRequest {
    public AddBeneRequest(String beneMobileNo, String customerNumber, String bankName, String accountNo, String beneName, String ifscCode) {
        this.beneMobileNo = beneMobileNo;
        this.customerNumber = customerNumber;
        this.bankName = bankName;
        this.accountNo = accountNo;
        this.beneName = beneName;
        this.ifscCode = ifscCode;
    }

    private String beneMobileNo;

    private String customerNumber;

    private String bankName;

    private String accountNo;

    private String beneName;

    private String ifscCode;

    public String getBeneMobileNo ()
    {
        return beneMobileNo;
    }

    public void setBeneMobileNo (String beneMobileNo)
    {
        this.beneMobileNo = beneMobileNo;
    }

    public String getCustomerNumber ()
    {
        return customerNumber;
    }

    public void setCustomerNumber (String customerNumber)
    {
        this.customerNumber = customerNumber;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getAccountNo ()
    {
        return accountNo;
    }

    public void setAccountNo (String accountNo)
    {
        this.accountNo = accountNo;
    }

    public String getBeneName ()
    {
        return beneName;
    }

    public void setBeneName (String beneName)
    {
        this.beneName = beneName;
    }

    public String getIfscCode ()
    {
        return ifscCode;
    }

    public void setIfscCode (String ifscCode)
    {
        this.ifscCode = ifscCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [beneMobileNo = "+beneMobileNo+", customerNumber = "+customerNumber+", bankName = "+bankName+", accountNo = "+accountNo+", beneName = "+beneName+", ifscCode = "+ifscCode+"]";
    }
}
