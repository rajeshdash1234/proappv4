package com.isuisudmt.dmt;

public class PaymentChannel {

    private boolean imps;
    private boolean neft;

    public boolean isImps() {
        return imps;
    }

    public void setImps(boolean imps) {
        this.imps = imps;
    }

    public boolean isNeft() {
        return neft;
    }

    public void setNeft(boolean neft) {
        this.neft = neft;
    }

    @Override
    public String toString() {
        return "PaymentChannel{" +
                "imps=" + imps +
                ", neft=" + neft +
                '}';
    }

}
