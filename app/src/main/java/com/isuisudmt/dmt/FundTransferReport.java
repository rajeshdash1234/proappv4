package com.isuisudmt.dmt;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class FundTransferReport extends AppCompatActivity {

    //LoadingView loadingView;

  /*  TextView textView;
    TextView noData;

    private RecyclerView reportRecyclerView;
   CustomReportAdapter mAdapter;
  //  RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    String transactionType = "ISU_FT";
//String transactionType = "RECHARGE";
    private AEPSAPIService apiService;
   // Session session;
    AutoCompleteTextView transaction_spinner;
    ReportFragmentRequest reportFragmentRequest;
    ArrayList<FinoTransactionReports> finoTransactionReports;
    Toolbar toolbar;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_fund_transfer_report);

        // hide notification bar
//        getSupportActionBar().hide();

        //session = new Session(FundTransferReport.this);
      /*  toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        textView = findViewById(R.id.text_date_picker);
        transaction_spinner = findViewById(R.id.transaction_spinner);
        reportRecyclerView = findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(FundTransferReport.this);

        reportRecyclerView.setLayoutManager(layoutManager);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(FundTransferReport.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(),"jitu");
                dpd.setMaxDate ( Calendar.getInstance () );
                dpd.setAutoHighlight ( true );

            }
        });
        noData = findViewById(R.id.noData);

        transaction_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
              // mAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                mAdapter.getFilter().filter(editable.toString());
                if(mAdapter.getItemCount()==0){
                    noData.setVisibility(View.VISIBLE);
                }else{
                    noData.setVisibility(View.GONE);
                }
            }
        });




    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        String todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd);
        String api_todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd+1);

        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+(dayOfMonthEnd)+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        textView.setText ( date );
        Log.d("Report", "fromdate: "+fromdate);
        Log.d("Report", "fromdate todate: "+todate);

        *//*ReportFragmentRequest*//* reportFragmentRequest= new ReportFragmentRequest(transactionType,fromdate,api_todate);

        Date from_date=null;
        Date to_date=null;
       SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            from_date = formatter.parse(fromdate);
            to_date = formatter.parse(todate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(from_date.before(to_date) || from_date.equals(to_date)){
            reportCall(fromdate,api_todate,session.getUserToken());
            showLoader();
        }else{
            Toast.makeText(this, "From date should be less than To date.", Toast.LENGTH_LONG).show();
        }

    }

    private void reportCall(String fromdate,String todate,String token) {
        if (this.apiService == null) {
            this.apiService = new AEPSAPIService();
        }
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                           // encriptedReportCall(encodedUrl);
                            encryptedReport(fromdate,todate,token,encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(FundTransferReport.this);
        }
        loadingView.show();

    }

    private void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }
    //Edited Rajesh
   private void  encriptedReportCall(String encodedUrl){
       final ReportFragmentApi reportFragmentApi = this.apiService.getClient().create(ReportFragmentApi.class);
       reportFragmentApi.getReport(session.getUserToken(),reportFragmentRequest, encodedUrl).enqueue(new Callback<ReportFragmentResponse>() {
           @Override
           public void onResponse(Call<ReportFragmentResponse> call, Response<ReportFragmentResponse> response) {
               if (response.isSuccessful()){
                   Log.d("STATUS", "TransactionSuccessfulReport"+response.body());
                   finoTransactionReports = response.body().getFinoTransactionReports();
                  // for( int i=0;i<finoTransactionReports.size();i++) {

                       transaction_spinner.setVisibility(View.VISIBLE);
                       Collections.reverse(finoTransactionReports);
                       mAdapter = new CustomReportAdapter(FundTransferReport.this, finoTransactionReports);
                       reportRecyclerView.setAdapter(mAdapter);

                       hideLoader();
                 //  }

                   if(finoTransactionReports.size()<=0){
                       noData.setVisibility(View.VISIBLE);
                   }else{
                       noData.setVisibility(View.GONE);
                   }
               }else {
                   transaction_spinner.setVisibility(View.GONE);
                   Log.d("STATUS", "TransactionFailedReport"+response.body());
                   hideLoader();
                   if(finoTransactionReports.size()<=0){
                       noData.setVisibility(View.VISIBLE);
                   }else{
                       noData.setVisibility(View.GONE);
                   }
               }
           }

           @Override
           public void onFailure(Call<ReportFragmentResponse> call, Throwable t) {
               Log.d("STATUS", "TransactionFailedReport"+t);
               transaction_spinner.setVisibility(View.GONE);
               if(finoTransactionReports.size()<=0){
                   noData.setVisibility(View.VISIBLE);
               }else{
                   noData.setVisibility(View.GONE);
               }

           }
       });

    }

    public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType",transactionType);
            jsonObject.put("fromDate",fromDate);
            jsonObject.put("toDate",toDate);



            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                // Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                                JSONArray jsonArray = obj.getJSONArray("BQReport");

                                ReportFragmentResponse reportResponse =new ReportFragmentResponse() ;
                                ArrayList<FinoTransactionReports> reportModels = new ArrayList<>();
                                for(int i =0 ; i<jsonArray.length();i++){
                                    FinoTransactionReports reportModel = new FinoTransactionReports();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    reportModel.setId(jsonObject1.getInt("Id"));
                                    reportModel.setPreviousAmount(Double.valueOf(jsonObject1.getString("previousAmount")));
                                    reportModel.setBalanceAmount(Double.valueOf(jsonObject1.getString("balanceAmount")));
                                    reportModel.setAmountTransacted(jsonObject1.getInt("amountTransacted"));
                                    reportModel.setBeniMobile(jsonObject1.getString("beniMobile"));
                                    reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                    reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                    reportModel.setBankName(jsonObject1.getString("bankName"));
                                    reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                    reportModel.setStatus(jsonObject1.getString("status"));
                                    reportModel.setUserName(jsonObject1.getString("userName"));
                                    reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                    reportModel.setMasterName(jsonObject1.getString("masterName"));
                                    reportModel.setAPI(jsonObject1.getString("API"));
                                    reportModel.setBenificiaryName(jsonObject1.getString("benificiaryName"));
                                    reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                    reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));
                                    reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                    reportModel.setRouteID(jsonObject1.getString("routeID"));
                                    reportModel.setToAccount(jsonObject1.getString("toAccount"));

                                    reportModels.add(reportModel);
                                }
                                reportResponse.setFinoTransactionReports(reportModels);

                                finoTransactionReports = reportResponse.getFinoTransactionReports();
                                // for( int i=0;i<finoTransactionReports.size();i++) {

                                transaction_spinner.setVisibility(View.VISIBLE);
                                Collections.reverse(finoTransactionReports);
                                mAdapter = new CustomReportAdapter(FundTransferReport.this, finoTransactionReports);
                                reportRecyclerView.setAdapter(mAdapter);

                                hideLoader();
                                //  }

                                if(finoTransactionReports.size()<=0){
                                    noData.setVisibility(View.VISIBLE);
                                }else{
                                    noData.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                transaction_spinner.setVisibility(View.GONE);
                                hideLoader();
                                if(finoTransactionReports.size()<=0){
                                    noData.setVisibility(View.VISIBLE);
                                }else{
                                    noData.setVisibility(View.GONE);
                                }
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            transaction_spinner.setVisibility(View.GONE);
                            if(finoTransactionReports.size()<=0){
                                noData.setVisibility(View.VISIBLE);
                            }else{
                                noData.setVisibility(View.GONE);
                            }
                        }
                    });








        }catch (Exception e){
            e.printStackTrace();
        }

    }*/

    }

}
