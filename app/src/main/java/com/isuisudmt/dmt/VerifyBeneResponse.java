package com.isuisudmt.dmt;

public class VerifyBeneResponse {

    private String status;

    private String beneName;

    private String txnId;

    private String statusDesc;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getBeneName ()
    {
        return beneName;
    }

    public void setBeneName (String beneName)
    {
        this.beneName = beneName;
    }

    public String getTxnId ()
    {
        return txnId;
    }

    public void setTxnId (String txnId)
    {
        this.txnId = txnId;
    }

    public String getStatusDesc ()
    {
        return statusDesc;
    }

    public void setStatusDesc (String statusDesc)
    {
        this.statusDesc = statusDesc;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", beneName = "+beneName+", txnId = "+txnId+", statusDesc = "+statusDesc+"]";
    }
}
