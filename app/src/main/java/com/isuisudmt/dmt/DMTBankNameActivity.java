package com.isuisudmt.dmt;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.Constants;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;

import java.util.ArrayList;

public class DMTBankNameActivity extends AppCompatActivity {

    private RecyclerView bankNameRecyclerView;
    private DMTBankNameListAdapter bankNameListAdapter;
    SearchView searchView;
    ProgressDialog loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_bank_name_list);

        bankNameRecyclerView = (RecyclerView) findViewById(R.id.bankNameRecyclerView);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getApplicationContext());
        bankNameRecyclerView.setLayoutManager(layoutManager);
        bankNameRecyclerView.setItemAnimator(new DefaultItemAnimator());

        ArrayList<String> bankList = getIntent().getStringArrayListExtra("BANKDATA");

        searchView = findViewById(R.id.searchView);
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint(getResources().getString(R.string.search_hint));
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.clearFocus();
        searchView.requestFocusFromTouch();

        setToolbar();


        if (bankList != null && bankList.size() > 0) {
            bankNameListAdapter = new DMTBankNameListAdapter(DMTBankNameActivity.this, bankList, new DMTBankNameListAdapter.RecyclerViewClickListener() {
                @Override
                public void recyclerViewListClicked(View v, int position) {
                    Constants.BANK_FLAG = bankNameListAdapter.getItem(position);
                    finish();
                }
            });

            bankNameRecyclerView.setAdapter(bankNameListAdapter);
        }


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast.makeText(getBaseContext(), query, Toast.LENGTH_LONG).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                bankNameListAdapter.getFilter().filter(newText);

                //Toast.makeText(getBaseContext(), newText, Toast.LENGTH_LONG).show();
                return true;
            }
        });


    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.banklist));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
