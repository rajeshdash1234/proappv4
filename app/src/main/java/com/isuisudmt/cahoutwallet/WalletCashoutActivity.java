package com.isuisudmt.cahoutwallet;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class WalletCashoutActivity extends AppCompatActivity {
    Button submit;
    EditText amount_edt;
    TextView wal2_bal_edt, wal1_bal_edt;
    SessionManager session;
    String _token, _admin;
    ProgressDialog pd;
    double wallet2_bal = 0.0;
    ImageView back;
    Handler timer_handler;
    String _userName;
    boolean session_logout = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_wallet_cashout);
        wal2_bal_edt = findViewById(R.id.wal2_bal_edt);
        wal1_bal_edt = findViewById(R.id.wal1_bal_edt);
        amount_edt = findViewById(R.id.amount_edt);
        back = findViewById(R.id.back);
        submit = findViewById(R.id.submit);
        pd = new ProgressDialog(WalletCashoutActivity.this);
        session = new SessionManager(getApplicationContext());

        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);
        _admin = _user.get(SessionManager.KEY_ADMIN);
        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amount = amount_edt.getText().toString().trim();
                if (amount.length() == 0) {
                    amount_edt.setError("Enter amount");
                } else if (amount.length() != 0 && (Double.valueOf(amount) > wallet2_bal)) {
                    showErrorAlert();
                } else {
                    transferBalance(amount_edt.getText().toString().trim());
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getWallet2Balance();
    }


    public void transferBalance(final String amount) {
        try {
            showLoader();
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v73")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                transferBalance1(amount, encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void transferBalance1(String amount, String url) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("amount", amount);
            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(jsonObject)
                    .addHeaders("Authorization", _token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoader();
                            try {
                                JSONObject obj = new JSONObject(response.toString());

                                String status = obj.getString("status");
                                String msg = obj.getString("statusDesc");

                                if (status.equalsIgnoreCase("0")) {
                                    showAlert(WalletCashoutActivity.this, "Fund successfully transfer to wallet");
                                } else {
                                    showAlert(WalletCashoutActivity.this, "Transaction failed, Please try after sometimes");
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorBody();
                            hideLoader();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getWallet2Balance() {
        try {
            showLoader();
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v72")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                getWallet2Balance2(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getWallet2Balance2(String url) {

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        // hideLoader();
                        String wallet2_balance = response;
                        wallet2_bal = Double.valueOf(wallet2_balance);
                        wal2_bal_edt.setText("Balance : ₹ " + wallet2_balance);
                        getWallet1Balance();
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }

    public void getWallet1Balance() {
        try {
            //showLoader();
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v69")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                getWallet1Balance2(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getWallet1Balance2(String url) {

        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                //.addBodyParameter(map)
                .addHeaders("Authorization", _token)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        hideLoader();
                        String wallet1_balance = response;
                        wal1_bal_edt.setText("Balance : ₹ " + wallet1_balance);
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }

    public void showLoader() {
        pd.setMessage("please wait..");
        pd.setCancelable(false);
        pd.show();
    }

    public void hideLoader() {
        try {
            if (!isFinishing()) {
                if (pd != null) {
                    pd.dismiss();
                }
            }
        } catch (Exception e) {

        }
    }


    public void showAlert(Context context, String message) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            amount_edt.setText("");
                            getWallet2Balance();


                        }
                    })
                    .show();
        } catch (Exception e) {

        }
    }

    public void showErrorAlert() {
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Enter lower amount to cashout";
        alertbuilderupdate.setTitle("Insufficient Fund")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        amount_edt.setText("");
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

 /*   @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        if(_userName!=null) {
            checkSessionExistance(_userName,true);
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
        //startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
    }

    public void startTimer(){

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(WalletCashoutActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(WalletCashoutActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(WalletCashoutActivity.this, user_name, status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(WalletCashoutActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/


}
