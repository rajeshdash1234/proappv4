package com.isuisudmt.report.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelReportWallet2 {

    /* "Id": "786223191520452608",
            "previousBalance": 77,
            "amountTransacted": 0,

            "currentBalance": 77,
            "transactionType": "COMMISION",

            "status": "SUCCESS",
            "relationalId": 786223178006405100,

            "relationalOperation": "AEPS_MINI_STATEMENT",
            "createdDate": 1607520616180,
            "updatedDate": 1607520616223,


            "userName": "Snehasony",
            "distributerName": null,
            "masterName": null
}*/

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("previousBalance")
    @Expose
    private String previousBalance;
    @SerializedName("amountTransacted")
    @Expose
    private String amountTransacted;
    @SerializedName("currentBalance")
    @Expose
    private Float currentBalance;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("relationalId")
    @Expose
    private String relationalId;
    @SerializedName("relationalOperation")
    @Expose
    private String relationalOperation;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;


    @SerializedName("masterName")
    @Expose
    private String masterName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(String previousBalance) {
        this.previousBalance = previousBalance;
    }

    public String getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(String amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public Float getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Float currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRelationalId() {
        return relationalId;
    }

    public void setRelationalId(String relationalId) {
        this.relationalId = relationalId;
    }

    public String getRelationalOperation() {
        return relationalOperation;
    }

    public void setRelationalOperation(String relationalOperation) {
        this.relationalOperation = relationalOperation;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }
}
