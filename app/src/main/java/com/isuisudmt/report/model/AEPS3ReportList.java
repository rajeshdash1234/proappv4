package com.isuisudmt.report.model;

public class AEPS3ReportList {


    public String id;

    public String previousAmount;

    public String amountTransacted;

    public String balanceAmount;

    public String operationPerformed;

    public String apiTid;

    public String apiComment;

    public String transactionMode;

    public String status;

    public String userName;

    public String distributerName;

    public String masterName;

    public String createdDate;

    public String updatedDate;

    public String referenceNo;

    public AEPS3ReportList(String id, String previousAmount, String amountTransacted, String balanceAmount, String operationPerformed, String apiTid, String apiComment, String transactionMode, String status, String userName, String masterName, String createdDate, String updatedDate, String referenceNo) {
        this.id = id;
        this.previousAmount = previousAmount;
        this.amountTransacted = amountTransacted;
        this.balanceAmount = balanceAmount;
        this.operationPerformed = operationPerformed;
        this.apiTid = apiTid;
        this.apiComment = apiComment;
        this.transactionMode = transactionMode;
        this.status = status;
        this.userName = userName;
        this.masterName = masterName;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.referenceNo = referenceNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        this.previousAmount = previousAmount;
    }

    public String getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(String amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getOperationPerformed() {
        return operationPerformed;
    }

    public void setOperationPerformed(String operationPerformed) {
        this.operationPerformed = operationPerformed;
    }

    public String getApiTid() {
        return apiTid;
    }

    public void setApiTid(String apiTid) {
        this.apiTid = apiTid;
    }

    public String getApiComment() {
        return apiComment;
    }

    public void setApiComment(String apiComment) {
        this.apiComment = apiComment;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
