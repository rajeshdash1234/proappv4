package com.isuisudmt.report.model;

public class ModelReportBBPS {
    public String Id;
    public String previousAmount;
    public String amountTransacted;
    public String balanceAmount;
    public String apiTid;
    public String apiComment;
    public String operationPerformed;
    public String status;
    public String transactionMode;
    public String userName;
    public String masterName;
    public String createdDate;
    public String updatedDate;
    public String referenceNo;
    //public String mobileNumber;
    public String param_a;
    public String param_b;
    public String param_c;

    public ModelReportBBPS() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        this.previousAmount = previousAmount;
    }

    public String getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(String amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getApiTid() {
        return apiTid;
    }

    public void setApiTid(String apiTid) {
        this.apiTid = apiTid;
    }

    public String getApiComment() {
        return apiComment;
    }

    public void setApiComment(String apiComment) {
        this.apiComment = apiComment;
    }

    public String getOperationPerformed() {
        return operationPerformed;
    }

    public void setOperationPerformed(String operationPerformed) {
        this.operationPerformed = operationPerformed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getParam_a() {
        return param_a;
    }

    public void setParam_a(String param_a) {
        this.param_a = param_a;
    }

    public String getParam_b() {
        return param_b;
    }

    public void setParam_b(String param_b) {
        this.param_b = param_b;
    }

    public String getParam_c() {
        return param_c;
    }

    public void setParam_c(String param_c) {
        this.param_c = param_c;
    }

/*
    @Override
    public String toString() {
        return  blrName;
    }
*/

}