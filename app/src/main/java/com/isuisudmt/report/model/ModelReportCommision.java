package com.isuisudmt.report.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ModelReportCommision implements Serializable {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("RelationalAmount")
    @Expose
    private Double relationalAmount;
    @SerializedName("previousAmount")
    @Expose
    private Double previousAmount;
    @SerializedName("amountTransacted")
    @Expose
    private Double amountTransacted;
    @SerializedName("balanceAmount")
    @Expose
    private Double balanceAmount;

    @SerializedName("GST")
    @Expose
    private String gST;
    @SerializedName("TDS")
    @Expose
    private String tDS;

    @SerializedName("Taxable")
    @Expose
    private String taxable;
    @SerializedName("InvoiceValue")
    @Expose
    private String invoiceValue;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("creditedUser")
    @Expose
    private String creditedUser;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;
    @SerializedName("masterName")
    @Expose
    private String masterName;
    @SerializedName("transactingUser")
    @Expose
    private String transactingUser;
    @SerializedName("RelationalOperation")
    @Expose
    private String relationalOperation;
    @SerializedName("API")
    @Expose
    private String aPI;
    @SerializedName("createdDate")
    @Expose
    private Long createdDate;
    @SerializedName("updatedDate")
    @Expose
    private Long updatedDate;


    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getgST() {
        return gST;
    }

    public void setgST(String gST) {
        this.gST = gST;
    }

    public String gettDS() {
        return tDS;
    }

    public void settDS(String tDS) {
        this.tDS = tDS;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getRelationalAmount() {
        return relationalAmount;
    }

    public void setRelationalAmount(Double relationalAmount) {
        this.relationalAmount = relationalAmount;
    }

    public Double getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(Double previousAmount) {
        this.previousAmount = previousAmount;
    }

    public Double getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(Double amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public String getTaxable() {
        return taxable;
    }

    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }

    public String getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(String invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreditedUser() {
        return creditedUser;
    }

    public void setCreditedUser(String creditedUser) {
        this.creditedUser = creditedUser;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getTransactingUser() {
        return transactingUser;
    }

    public void setTransactingUser(String transactingUser) {
        this.transactingUser = transactingUser;
    }

    public String getRelationalOperation() {
        return relationalOperation;
    }

    public void setRelationalOperation(String relationalOperation) {
        this.relationalOperation = relationalOperation;
    }

    public String getaPI() {
        return aPI;
    }

    public void setaPI(String aPI) {
        this.aPI = aPI;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Long updatedDate) {
        this.updatedDate = updatedDate;
    }
}
