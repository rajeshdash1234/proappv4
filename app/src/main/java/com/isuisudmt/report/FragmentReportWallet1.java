package com.isuisudmt.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.matm.MicroReportModel;
import com.isuisudmt.matm.TransactionStatusModel;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportMATM;
import com.isuisudmt.report.adapter.AdapterReportWallet;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;
import com.isuisudmt.wallet.WalletReportContract;
import com.isuisudmt.wallet.WalletReportPresenter;

import java.util.ArrayList;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;

public class FragmentReportWallet1 extends Fragment implements WalletReportContract.View {

    private RecyclerView reportRecyclerView;
    private WalletReportPresenter mActionsListener;
    ArrayList<MicroReportModel> reportResponseArrayList = new ArrayList<>();
    LinearLayout dateLayout, detailsLayout;
    private AdapterReportWallet microReportRecyclerViewAdapter;
    TextView noData, totalreport, amount;
    TextView chooseDateRange;
    String fromdate = "";
    String todate = "";
    String clientId = "";
    TransactionStatusModel transactionStatusModel;
    private boolean ascending = true;
    SessionManager session;
    String tokenStr = "", trans_type = "";
    ProgressBar progressBar;
    Context context;
    Boolean calenderFlag = false;
    public static Boolean refreshmAtmReport = false;
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    public FragmentReportWallet1() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wallet, container, false);
        pref = getActivity().getSharedPreferences("DEFUALT", 0);
        editor = pref.edit();

        AdapterReportMATM.transType = "";

        initView(rootView);

        trans_type = "WALLET";//should be send in Api
        if (calenderFlag) {
            if (reportResponseArrayList != null) {
                reportResponseArrayList.clear();
            }
            if (microReportRecyclerViewAdapter != null) {
                microReportRecyclerViewAdapter.notifyDataSetChanged();
            }

            callReportApi(trans_type);
        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }


    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                //Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                try {
                    if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {
                        // filter recycler view when text is changed
                        microReportRecyclerViewAdapter.getFilter().filter(s);
                        microReportRecyclerViewAdapter.notifyDataSetChanged();

                        /*amount.setText(getResources().getString(R.string.toatlamountacitvity) + microReportRecyclerViewAdapter.getTotalAmount());
                        totalreport.setText("Entries: " + microReportRecyclerViewAdapter.getItems().size());*/

                    } else {
                        if (calenderFlag == false)
                            Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {

                }
                return false;
            }
        });

    }


    private void initView(View rootView) {
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("Wallet 1 Report");
        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressBar = rootView.findViewById(R.id.progressV);

        //session = new Session(MicroAtmReportActivity.this);
        noData = rootView.findViewById(R.id.noData);
        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        chooseDateRange = rootView.findViewById(R.id.chooseDateRange);
        dateLayout = rootView.findViewById(R.id.dateLayout);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mActionsListener = new WalletReportPresenter(this);
        /*wallet1 = rootView.findViewById(R.id.wallet1);
        wallet2 = rootView.findViewById(R.id.wallet2);


        wallet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        wallet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trans_type="WALLET2";//should be send in Api
                wallet2.setTextColor(getResources().getColor(R.color.colorBluee));
                wallet1.setTextColor(getResources().getColor(R.color.colorBlack));
                if(calenderFlag){
                    wallet2.setTextColor(getResources().getColor(R.color.colorBluee));
                    wallet1.setTextColor(getResources().getColor(R.color.colorBlack));
                    if(reportResponseArrayList!=null) {
                        reportResponseArrayList.clear();
                    }
                    if(microReportRecyclerViewAdapter!=null) {
                        microReportRecyclerViewAdapter.notifyDataSetChanged();
                    }

                    callReportApi(trans_type);
                }else{
                    Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
                }
            }
        });*/


    }

    @Override
    public void onResume() {
        super.onResume();


        if (fromdate.length() != 0) {
            mActionsListener.loadReports(fromdate, Util.getNextDate(todate), tokenStr, "WALLET");
        }
    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void reportsReady(ArrayList<MicroReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        detailsLayout.setVisibility(View.VISIBLE);
        amount.setText("Amt Tnx: ₹" + totalAmount);
    }


    @Override
    public void showReports() {

        if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);

            microReportRecyclerViewAdapter = new AdapterReportWallet(getActivity(), reportResponseArrayList, trans_type, new AdapterReportWallet.IMethodCaller() {
                @Override
                public void refreshMethod(MicroReportModel microReportModel) {

                }
            }, amount, totalreport);
            reportRecyclerView.setAdapter(microReportRecyclerViewAdapter);
            microReportRecyclerViewAdapter.notifyDataSetChanged();
            totalreport.setText("Entries: " + reportResponseArrayList.size());
        } else {
            reportRecyclerView.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
            totalreport.setText("Entries: " + 0);

        }

    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
    }

    @Override
    public void checkAmount(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.amountrefersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_mode_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_type_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {

    }

    @Override
    public void emptyRefreshData(String status) {

    }


    //================
    public void callReportApi(String trans_tpye) {
        noData.setVisibility(View.GONE);
        reportRecyclerView.setVisibility(View.VISIBLE);
        mActionsListener.loadReports(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, trans_tpye);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate + " to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                callReportApi(trans_type);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
