package com.isuisudmt.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.matm.MicroReportModel;
import com.isuisudmt.matm.MicroReportResponse;
import com.isuisudmt.matm.TransactionStatusModel;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportMATM;
import com.isuisudmt.report.adapter.AdapterReportWallet2;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;
import com.isuisudmt.wallet.WalletReportContract;
import com.isuisudmt.wallet.WalletReportPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;
import static com.isuisudmt.utils.Constants.URL_WALLET2;

public class FragmentReportWallet2 extends Fragment implements WalletReportContract.View {

    private RecyclerView reportRecyclerView;
    private WalletReportPresenter mActionsListener;
    static ArrayList<MicroReportModel> reportResponseArrayList = new ArrayList<MicroReportModel>();
    LinearLayout dateLayout, detailsLayout;
    private AdapterReportWallet2 microReportRecyclerViewAdapter;
    TextView noData, totalreport, amount;
    TextView chooseDateRange;
    String fromdate = "";
    String todate = "";
    String clientId = "";
    TransactionStatusModel transactionStatusModel;
    private boolean ascending = true;
    SessionManager session;
    String tokenStr = "", trans_type = "";
    ProgressBar progressBar;
    Context context;
    //Button wallet1,wallet2;
    Boolean calenderFlag = false;
    public static Boolean refreshmAtmReport = false;

    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";


    public FragmentReportWallet2() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wallet, container, false);
        pref = getActivity().getSharedPreferences("DEFUALT", 0);
        editor = pref.edit();

        AdapterReportMATM.transType = "";

        initView(rootView);

        trans_type = "WALLET2";//should be send in Api
        if (calenderFlag) {
            if (reportResponseArrayList != null) {
                reportResponseArrayList.clear();
            }
            if (microReportRecyclerViewAdapter != null) {
                microReportRecyclerViewAdapter.notifyDataSetChanged();
            }

            callReportApi(trans_type);
        } else {
            callCalenderFunction();

            //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
        }


        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }


    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                //Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                try {
                    if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {
                        // filter recycler view when text is changed
                        microReportRecyclerViewAdapter.getFilter().filter(s);
                        microReportRecyclerViewAdapter.notifyDataSetChanged();

                        /*amount.setText(getResources().getString(R.string.toatlamountacitvity) + microReportRecyclerViewAdapter.getTotalAmount());
                        totalreport.setText("Entries: " + microReportRecyclerViewAdapter.getItems().size());*/

                    } else {
                        if(calenderFlag == false)
                        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {

                }
                return false;
            }
        });

    }


    private void initView(View rootView) {
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("Wallet 2 Report");
        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressBar = rootView.findViewById(R.id.progressV);

        //session = new Session(MicroAtmReportActivity.this);
        noData = rootView.findViewById(R.id.noData);
        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        chooseDateRange = rootView.findViewById(R.id.chooseDateRange);
        dateLayout = rootView.findViewById(R.id.dateLayout);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mActionsListener = new WalletReportPresenter(this);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void reportsReady(ArrayList<MicroReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText("Amt Tnx: ₹" + totalAmount);
    }


    @Override
    public void showReports() {

        if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);

            microReportRecyclerViewAdapter = new AdapterReportWallet2(getActivity(), reportResponseArrayList, trans_type, new AdapterReportWallet2.IMethodCaller() {
                @Override
                public void refreshMethod(MicroReportModel microReportModel) {

                }
            }, amount, totalreport);
            reportRecyclerView.setAdapter(microReportRecyclerViewAdapter);
            microReportRecyclerViewAdapter.notifyDataSetChanged();
            totalreport.setText("Entries: " + reportResponseArrayList.size());
        } else {
            reportRecyclerView.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
            totalreport.setText("Entries: " + 0);

        }

    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
    }

    @Override
    public void checkAmount(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.amountrefersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_mode_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_type_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {

    }

    @Override
    public void emptyRefreshData(String status) {

    }

    public void callReportApi(String trans_tpye) {
        getReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, trans_tpye);
    }


    private void getReport(final String fromDate, final String toDate, final String token, final String transactionType) {

        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType", transactionType);
            jsonObject.put("startDate", fromDate);
            jsonObject.put("endDate", toDate);
            jsonObject.put("userName", sp.getString(USER_NAME, ""));


            AndroidNetworking.post(URL_WALLET2)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                dialog.cancel();

                                reportResponseArrayList.clear();
                                noData.setVisibility(View.GONE);
                                reportRecyclerView.setVisibility(View.VISIBLE);


                                JSONObject obj = new JSONObject(response.toString());
                                String message = obj.getString("message");
                                String length = obj.getString("length");
                                String status = obj.getString("status");

                                if (length.equals("0")) {
                                    noData.setVisibility(View.VISIBLE);
                                    detailsLayout.setVisibility(View.GONE);
                                    reportRecyclerView.setVisibility(View.GONE);
                                    //noData.setText(message);
                                } else {
                                    JSONArray jsonArray = obj.getJSONArray("BQReport");

                                    MicroReportResponse reportResponse = new MicroReportResponse();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        MicroReportModel reportModel = new MicroReportModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);


                                        if (transactionType.equalsIgnoreCase("WALLET2")) {
                                            reportModel.setId(jsonObject1.getString("Id"));

                                            String previousAmount = jsonObject1.getString("previousBalance");
                                            if (!previousAmount.equalsIgnoreCase("null")) {
                                                reportModel.setPreviousAmount(previousAmount);
                                            } else {
                                                reportModel.setPreviousAmount("N/A");
                                            }

                                            String balanceAmount = jsonObject1.getString("currentBalance");
                                            if (!balanceAmount.equalsIgnoreCase("null")) {
                                                reportModel.setBalanceAmount(balanceAmount);
                                            } else {
                                                reportModel.setBalanceAmount("N/A");
                                            }

                                            String amountTransacted = jsonObject1.getString("amountTransacted");
                                            if (!amountTransacted.equalsIgnoreCase("null")) {
                                                reportModel.setAmountTransacted(amountTransacted);
                                            } else {
                                                reportModel.setAmountTransacted("N/A");
                                            }
                                            reportModel.setStatus(jsonObject1.getString("status"));
                                            reportModel.setTransactionMode(jsonObject1.getString("transactionType"));


                                            reportModel.setUserName(jsonObject1.getString("userName"));
                                            reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                            reportModel.setMasterName(jsonObject1.getString("masterName"));

                                            reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                            reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                            reportResponseArrayList.add(reportModel);
                                        }


                                    }

                                    if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {

                                        reportRecyclerView.setVisibility(View.VISIBLE);
                                        noData.setVisibility(View.GONE);

                                        microReportRecyclerViewAdapter = new AdapterReportWallet2(getActivity(), reportResponseArrayList, trans_type, new AdapterReportWallet2.IMethodCaller() {
                                            @Override
                                            public void refreshMethod(MicroReportModel microReportModel) {

                                            }
                                        }, amount, totalreport);
                                        reportRecyclerView.setAdapter(microReportRecyclerViewAdapter);
                                        microReportRecyclerViewAdapter.notifyDataSetChanged();

                                        detailsLayout.setVisibility(View.VISIBLE);
                                        totalreport.setText("Entries: " + reportResponseArrayList.size());
                                        double totalAmount = 0;
                                        for (int i = 0; i < reportResponseArrayList.size(); i++) {
                                            if (!reportResponseArrayList.get(i).getAmountTransacted().equals("null")) {
                                                totalAmount += Double.parseDouble(String.valueOf(reportResponseArrayList.get(i).getAmountTransacted()));
                                            }
                                        }
                                        amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);


                                    } else {
                                        reportRecyclerView.setVisibility(View.GONE);
                                        detailsLayout.setVisibility(View.GONE);
                                        noData.setVisibility(View.VISIBLE);
                                        totalreport.setText("Entries: " + 0);

                                    }
                                }


                            } catch (JSONException e) {
                                dialog.cancel();
                                e.printStackTrace();

                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            dialog.cancel();
                        }
                    });


        } catch (Exception e) {
            dialog.cancel();
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                callReportApi(trans_type);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
