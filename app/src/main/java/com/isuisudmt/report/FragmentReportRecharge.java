package com.isuisudmt.report;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.PrepaidBBPS;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterPrepaid;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static com.isuisudmt.bbps.utils.Const.URL_GET_REPORT_PREPAID;
import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;


public class FragmentReportRecharge extends Fragment {

    TextView noData;
    Spinner spinnerOperationPerformed, spinnerStatus;
    LinearLayout ll_main;
    private RecyclerView reportRecyclerView;
    Button btnGetReport;
    AdapterPrepaid mAdapter;
    RecyclerView.LayoutManager layoutManager;
    SessionManager session;
    String tokenStr = "";
    String _userName;
    ProgressBar progressV;
    Context context;

    String selectedStatus = "All", fromdate = "", todate = "", api_todate = "";
    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    JSONArray jArrayOpsPerformed, jArrayTransactionType;
    ArrayList<PrepaidBBPS> data = new ArrayList<>();
    Boolean calenderFlag = false;

    TextView totalreport, amount;
    LinearLayout detailsLayout;

    public FragmentReportRecharge() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_report_recharge, container, false);

        initView(rootView);

        if (calenderFlag) {
            //Do nothing

        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    private void initView(View rootView) {

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setNestedScrollingEnabled(false);
        ll_main = rootView.findViewById(R.id.ll_main_bbps_report);
        spinnerOperationPerformed = rootView.findViewById(R.id.spinner_operation_performed);
        spinnerStatus = rootView.findViewById(R.id.spinner_status);
        btnGetReport = rootView.findViewById(R.id.btn_get_report);

        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);


        btnGetReport.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        jArrayOpsPerformed = new JSONArray();
        jArrayTransactionType = new JSONArray();

        jArrayOpsPerformed.put("Prepaid Recharge");
        jArrayTransactionType.put("Recharge");

    }

    private void getReport(String from_date, String to_date) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {

            obj.put("start_date", from_date);
            obj.put("end_date", to_date);
            obj.put("userName", _userName);
            obj.put("transaction_type", jArrayTransactionType);
            obj.put("operationPerformed", jArrayOpsPerformed);
            obj.put("status", selectedStatus);

        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(URL_GET_REPORT_PREPAID)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.cancel();

                        handleResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(anError.getErrorBody().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Toast.makeText(getActivity(), obj.getString("statusDescription").toString(), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                });

    }

    private void handleResponse(String response) {
        try {
            data.clear();
            noData.setVisibility(View.GONE);
            reportRecyclerView.setVisibility(View.VISIBLE);

            JSONObject obj = new JSONObject(response);
            String status = obj.getString("status");
            String message = obj.getString("message");
            String results = obj.getString("results");
            String length = obj.getString("length");

            if (length.equals("0")) {
                noData.setVisibility(View.VISIBLE);
                detailsLayout.setVisibility(View.GONE);
                reportRecyclerView.setVisibility(View.GONE);
            } else {
                if (status.equals("200")) {
                    JSONObject objCommonClass = new JSONObject(results);
                    String BQReport = objCommonClass.getString("BQReport");
                    JSONArray jsonArray = new JSONArray(BQReport);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        PrepaidBBPS pojoReportBBPS = new PrepaidBBPS();

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        pojoReportBBPS.Id = jsonObject.getString("Id");
                        pojoReportBBPS.previousAmount = jsonObject.getString("previousAmount");
                        pojoReportBBPS.amountTransacted = jsonObject.getString("amountTransacted");
                        pojoReportBBPS.balanceAmount = jsonObject.getString("balanceAmount");
                        pojoReportBBPS.apiTid = jsonObject.getString("apiTid");
                        pojoReportBBPS.apiComment = jsonObject.getString("apiComment");
                        pojoReportBBPS.operationPerformed = jsonObject.getString("operationPerformed");
                        pojoReportBBPS.status = jsonObject.getString("status");
                        pojoReportBBPS.transactionMode = jsonObject.getString("transactionMode");
                        pojoReportBBPS.userName = jsonObject.getString("userName");
                        pojoReportBBPS.masterName = jsonObject.getString("masterName");
                        pojoReportBBPS.createdDate = jsonObject.getString("createdDate");
                        pojoReportBBPS.updatedDate = jsonObject.getString("updatedDate");
                        pojoReportBBPS.mobileNumber = jsonObject.getString("mobileNumber");
                        pojoReportBBPS.operatorDescription = jsonObject.getString("operatorDescription");
                        pojoReportBBPS.param_a = jsonObject.getString("param_a");
                        pojoReportBBPS.param_b = jsonObject.getString("param_b");
                        pojoReportBBPS.param_c = jsonObject.getString("param_c");

                        data.add(pojoReportBBPS);
                    }

                    mAdapter = new AdapterPrepaid(getActivity(), data, amount, totalreport);
                    reportRecyclerView.setAdapter(mAdapter);
                    Collections.reverse(data);
                    detailsLayout.setVisibility(View.VISIBLE);
                    totalreport.setText("Entries: " + data.size());
                    double totalAmount = 0;
                    for (int i = 0; i < data.size(); i++) {
                        if (!data.get(i).getAmountTransacted().equals("null")) {
                            totalAmount += Double.parseDouble(data.get(i).getAmountTransacted());
                        }
                    }
                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);


                } else {
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try {
                    if (data != null && data.size() > 0) {
                        mAdapter.getFilter().filter(s);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if (calenderFlag == false)
                            Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {

                }
                return false;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                getReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate));
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}