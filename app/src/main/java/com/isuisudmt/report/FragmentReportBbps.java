package com.isuisudmt.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportBbps;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.model.ModelReportBBPS;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static com.isuisudmt.bbps.utils.Const.URL_GET_REPORT_BBPS;
import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;


public class FragmentReportBbps extends Fragment {

    TextView noData;
    Spinner spinnerOperationPerformed, spinnerStatus;
    LinearLayout ll_main;
    private RecyclerView reportRecyclerView;
    Button btnGetReport;
    AdapterReportBbps adapterReportBbps;
    RecyclerView.LayoutManager layoutManager;
    SessionManager session;
    String tokenStr = "";
    ProgressBar progressV;
    Context context;
    String[] arr_operation_performed = {"All","BBPS_Mobile Postpaid", "BBPS_DTH", "BBPS_Electricity", "BBPS_Broadband Postpaid",
            "BBPS_Water", "BBPS_Landline Postpaid", "BBPS_Gas", "BBPS_Education Fees",
            "BBPS_Cable TV", "BBPS_LPG Gas", "BBPS_Fastag", "BBPS_Life Insurance",
            "BBPS_Loan Repayment", "BBPS_Health Insurance",};
    String[] arr_status = { "INITIATED", "PENDING", "SUCCESS", "FAILED"};

    String selectedStatus = "All";
    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    JSONArray jArrayOpsPerformed, jArrayTransactionType;
    ArrayList<ModelReportBBPS> bbpsReportList = new ArrayList<>();
    Boolean calenderFlag = false;

    TextView totalreport,amount;
    LinearLayout detailsLayout;

    public FragmentReportBbps() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bbps, container, false);

        initView(rootView);

        if (calenderFlag) {
            //Do Nothing
        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    private void initView(View rootView) {

        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("Pay Bill Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);

        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setNestedScrollingEnabled(false);
        ll_main = rootView.findViewById(R.id.ll_main_bbps_report);
        spinnerOperationPerformed = rootView.findViewById(R.id.spinner_operation_performed);
        spinnerStatus = rootView.findViewById(R.id.spinner_status);
        btnGetReport = rootView.findViewById(R.id.btn_get_report);
        btnGetReport.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        amount = rootView.findViewById ( R.id.amount );
        totalreport = rootView.findViewById ( R.id.totalreport );
        detailsLayout = rootView.findViewById ( R.id.detailsLayout );

        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        jArrayOpsPerformed = new JSONArray();
        for (int i = 1; i < arr_operation_performed.length; i++) {
            jArrayOpsPerformed.put(arr_operation_performed[i]);
        }

        jArrayTransactionType = new JSONArray();
        jArrayTransactionType.put("BBPS");

    }

    private void getReport(String result_from_date, String nextDate) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {

            obj.put("start_date", result_from_date);
            obj.put("end_date", nextDate);
            obj.put("userName", sp.getString(USER_NAME, ""));
            obj.put("transaction_type", jArrayTransactionType);
            obj.put("operationPerformed", jArrayOpsPerformed);
            obj.put("status", selectedStatus);

        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(URL_GET_REPORT_BBPS)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            dialog.cancel();
                            bbpsReportList.clear();
                            noData.setVisibility(View.GONE);

                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.optString("status");
                            String message = obj.optString("message");
                            String results = obj.optString("results");
                            String length = obj.optString("length");

                            if (length.equals("0")) {
                                noData.setVisibility(View.VISIBLE);
                                detailsLayout.setVisibility(View.GONE);
                                reportRecyclerView.setVisibility(View.GONE);
                            } else {
                                reportRecyclerView.setVisibility(View.VISIBLE);
                                if (status.equals("200")) {
                                    JSONObject objCommonClass = new JSONObject(results);
                                    String BQReport = objCommonClass.getString("BQReport");

                                    JSONArray jsonArray = new JSONArray(BQReport);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ModelReportBBPS modelReportBBPS = new ModelReportBBPS();

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        modelReportBBPS.Id = jsonObject.getString("Id");
                                        modelReportBBPS.previousAmount = jsonObject.getString("previousAmount");
                                        modelReportBBPS.amountTransacted = jsonObject.getString("amountTransacted");
                                        modelReportBBPS.balanceAmount = jsonObject.getString("balanceAmount");
                                        modelReportBBPS.apiTid = jsonObject.getString("apiTid");
                                        modelReportBBPS.apiComment = jsonObject.getString("apiComment");
                                        modelReportBBPS.operationPerformed = jsonObject.getString("operationPerformed");
                                        modelReportBBPS.status = jsonObject.getString("status");
                                        modelReportBBPS.transactionMode = jsonObject.getString("transactionMode");
                                        modelReportBBPS.userName = jsonObject.getString("userName");
                                        modelReportBBPS.masterName = jsonObject.getString("masterName");
                                        modelReportBBPS.createdDate = jsonObject.getString("createdDate");
                                        modelReportBBPS.updatedDate = jsonObject.getString("updatedDate");
                                        modelReportBBPS.referenceNo = jsonObject.getString("referenceNo");
                                        //pojoReportBBPS.mobileNumber = jsonObject.getString("mobileNumber");
                                        modelReportBBPS.param_a = jsonObject.getString("param_a");
                                        modelReportBBPS.param_b = jsonObject.getString("param_b");
                                        modelReportBBPS.param_c = jsonObject.getString("param_c");

                                        bbpsReportList.add(modelReportBBPS);
                                    }

                                    adapterReportBbps = new AdapterReportBbps(getActivity(), bbpsReportList, amount, totalreport);
                                    reportRecyclerView.setAdapter(adapterReportBbps);
                                    Collections.reverse(bbpsReportList);

                                    totalreport.setText("Entries: " + bbpsReportList.size());
                                    double totalAmount = 0;
                                    for (int i = 0; i < bbpsReportList.size(); i++) {
                                        if (!bbpsReportList.get(i).getAmountTransacted().equalsIgnoreCase("null")) {
                                            totalAmount += Double.parseDouble(bbpsReportList.get(i).getAmountTransacted());
                                        }
                                    }
                                    detailsLayout.setVisibility(View.VISIBLE);
                                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);

                                    dialog.cancel();

                                } else {
                                    dialog.cancel();
                                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                                }
                            }

                            dialog.cancel();
                        } catch (JSONException e) {
                            dialog.cancel();

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(anError.getErrorBody().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Toast.makeText(getActivity(), obj.getString("statusDescription").toString(), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (bbpsReportList != null && bbpsReportList.size() > 0) {
                    // filter recycler view when text is changed
                    adapterReportBbps.getFilter().filter(s);
                    adapterReportBbps.notifyDataSetChanged();

                    /*amount.setText(getResources().getString(R.string.toatlamountacitvity) + adapterReportBbps.getTotalAmount());
                    totalreport.setText("Entries: " + adapterReportBbps.getItems().size());*/

                } else {
                    if(calenderFlag == false)
                    Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if(resultCode == Activity.RESULT_OK){
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                getReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate));

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
