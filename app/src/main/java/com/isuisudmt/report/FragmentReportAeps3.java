package com.isuisudmt.report;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.tabs.TabLayout;
import com.isuisudmt.Constants;
import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportAEPS3;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.model.AEPS3ReportList;
import com.isuisudmt.report.util.ConstantsReport;
import com.isuisudmt.utility.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;


public class FragmentReportAeps3 extends Fragment {


    LinearLayout recyclerView_container, dateLayout;
    RecyclerView statement_list;
    AdapterReportAEPS3 adapterReportAEPS;
    TextView tv_nodata;
    Session session;
    ProgressDialog pd;
    static ArrayList<AEPS3ReportList> listOfAEPS3 = new ArrayList<>();
    private TabLayout tablayout;
    TextView totalreport;
    TextView amount;
    LinearLayout detailsLayout;
    String SELECTED_TAB = "all", AePS3ReportResponse = "";
    Boolean calenderFlag = false;


    public FragmentReportAeps3() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.activity_report_aeps3, container, false);

        init(rootview);

        callTablistener();

        if (calenderFlag) {
            ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectFromDate + " to " + ConstantsReport.selectToDate);
            try {
                callReportAPI(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), SELECTED_TAB);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ShowCalendar();
        }


        return rootview;
    }

    private void init(View rootview) {
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("AePS 3 Report");

        pd = new ProgressDialog(getActivity());

        statement_list = rootview.findViewById(R.id.statement_list);
        tv_nodata = rootview.findViewById(R.id.tv_nodata);
        recyclerView_container = rootview.findViewById(R.id.recyclerView_container);
        dateLayout = rootview.findViewById(R.id.dateLayout);
        tablayout = rootview.findViewById(R.id.tablayout);
        amount = rootview.findViewById(R.id.amount);
        totalreport = rootview.findViewById(R.id.totalreport);
        detailsLayout = rootview.findViewById(R.id.detailsLayout);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        statement_list.setLayoutManager(linearLayoutManager);
        statement_list.setAdapter(adapterReportAEPS);
        session = new Session(getActivity());

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }


    private void callTablistener() {
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getActivity().invalidateOptionsMenu();
                if (calenderFlag) {
                    if (tab.getPosition() == 0) {
                        SELECTED_TAB = "all";
                        if (calenderFlag) {
                            if (!AePS3ReportResponse.equals("")) {
                                filterResponse(AePS3ReportResponse, SELECTED_TAB);
                            }
                        } else {
                            ShowCalendar();
                        }
                    } else if (tab.getPosition() == 1) {
                        SELECTED_TAB = "AEPS_CASH_WITHDRAWAL";
                        if (calenderFlag) {
                            if (!AePS3ReportResponse.equals("")) {
                                filterResponse(AePS3ReportResponse, SELECTED_TAB);
                            }
                        } else {
                            ShowCalendar();
                        }
                    } else if (tab.getPosition() == 2) {
                        SELECTED_TAB = "AEPS_BALANCE_ENQUIRY";
                        if (calenderFlag) {
                            if (!AePS3ReportResponse.equals("")) {
                                filterResponse(AePS3ReportResponse, SELECTED_TAB);
                            }
                        } else {
                            ShowCalendar();
                        }
                    } else if (tab.getPosition() == 3) {
                        SELECTED_TAB = "AEPS_MINI_STATEMENT";
                        if (calenderFlag) {
                            if (!AePS3ReportResponse.equals("")) {
                                filterResponse(AePS3ReportResponse, SELECTED_TAB);
                            }
                        } else {
                            ShowCalendar();
                        }
                    } else if (tab.getPosition() == 4) {
                        SELECTED_TAB = "AADHAAR_PAY";
                        if (calenderFlag) {
                            if (!AePS3ReportResponse.equals("")) {
                                filterResponse(AePS3ReportResponse, SELECTED_TAB);
                            }
                        } else {
                            ShowCalendar();
                        }
                    }
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void filterResponse(String aePS3ReportResponse, String type) {
        try {
            listOfAEPS3.clear();

            /* statement_list.clearFocus();*/
            JSONObject obj = new JSONObject(aePS3ReportResponse);
            String status = obj.getString("status");
            String message = obj.optString("message");
            String results = obj.getString("results");
            String length = obj.getString("length");

            if (length.equals("0")) {
                statement_list.setVisibility(View.GONE);
                tv_nodata.setVisibility(View.VISIBLE);
            } else {
                if (status.equals("200")) {
                    statement_list.setVisibility(View.VISIBLE);
                    tv_nodata.setVisibility(View.GONE);
                    JSONObject objCommonClass = new JSONObject(results);
                    String BQReport = objCommonClass.getString("BQReport");

                    JSONArray jsonArray = new JSONArray(BQReport);
                    Log.e("TAG", "onResponse: " + jsonArray);

                    if (jsonArray.length() == 0) {
                        statement_list.setVisibility(View.GONE);
                        tv_nodata.setVisibility(View.VISIBLE);
                    } else {
                        if (type.equals("all")) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                String id = jsonObject1.getString("Id");
                                String previousAmount = jsonObject1.getString("previousAmount");
                                String amountTransacted = jsonObject1.getString("amountTransacted");
                                String balanceAmount = jsonObject1.getString("balanceAmount");
                                String apiTid = jsonObject1.getString("apiTid");
                                String apiComment = jsonObject1.getString("apiComment");
                                String operationPerformed = jsonObject1.getString("operationPerformed");
                                String status2 = jsonObject1.getString("status");

                                String transactionMode = jsonObject1.getString("transactionMode");
                                String userName = jsonObject1.getString("userName");
                                String masterName = jsonObject1.getString("masterName");
                                String createdDate = jsonObject1.getString("createdDate");
                                String updatedDate = jsonObject1.getString("updatedDate");
                                String referenceNo = jsonObject1.getString("referenceNo");

                                listOfAEPS3.add(new AEPS3ReportList(id, previousAmount, amountTransacted, balanceAmount, operationPerformed, apiTid, apiComment, transactionMode, status2, userName, masterName, createdDate, updatedDate, referenceNo));

                            }
                        } else {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                if (jsonObject1.getString("operationPerformed").equals(type)) {
                                    String id = jsonObject1.getString("Id");
                                    String previousAmount = jsonObject1.getString("previousAmount");
                                    String amountTransacted = jsonObject1.getString("amountTransacted");
                                    String balanceAmount = jsonObject1.getString("balanceAmount");
                                    String apiTid = jsonObject1.getString("apiTid");
                                    String apiComment = jsonObject1.getString("apiComment");
                                    String operationPerformed = jsonObject1.getString("operationPerformed");
                                    String status2 = jsonObject1.getString("status");

                                    String transactionMode = jsonObject1.getString("transactionMode");
                                    String userName = jsonObject1.getString("userName");
                                    String masterName = jsonObject1.getString("masterName");
                                    String createdDate = jsonObject1.getString("createdDate");
                                    String updatedDate = jsonObject1.getString("updatedDate");
                                    String referenceNo = jsonObject1.getString("referenceNo");

                                    listOfAEPS3.add(new AEPS3ReportList(id, previousAmount, amountTransacted, balanceAmount, operationPerformed, apiTid, apiComment, transactionMode, status2, userName, masterName, createdDate, updatedDate, referenceNo));
                                }
                            }
                        }
                    }

                    adapterReportAEPS = new AdapterReportAEPS3(listOfAEPS3, getActivity(), totalreport);
                    statement_list.setAdapter(adapterReportAEPS);
                    Collections.reverse(listOfAEPS3);

                    totalreport.setText("Entries: " + listOfAEPS3.size());
                    detailsLayout.setVisibility(View.VISIBLE);

                } else {
                    statement_list.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.VISIBLE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void ShowCalendar() {
        getActivity().invalidateOptionsMenu();
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    private void showAlert(String title, String message, Context context) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(message);
        builder1.setTitle(title);
        builder1.setIcon(R.drawable.ic_warning);
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void callReportAPI(String fromdate, String todate, String selectedTab_) throws JSONException {
        showLoader();

        List<String> list2 = new ArrayList<String>();
        list2.add("AEPS3");
        JSONArray jsonArray = new JSONArray(list2);

        List<String> list = new ArrayList<String>();
        list.add("AEPS_CASH_WITHDRAWAL");
        list.add("AEPS_BALANCE_ENQUIRY");
        list.add("AEPS_MINI_STATEMENT");
        list.add("AADHAAR_PAY");
        JSONArray array = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            array.put(list.get(i));
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("transaction_type", jsonArray);
        jsonObject.put("start_date", fromdate);
        jsonObject.put("end_date", Util.getNextDate(todate));
        jsonObject.put("userName", Constants.USER_NAME);
        jsonObject.put("operationPerformed", array);
        jsonObject.put("status", "All");

        Log.e("TAG", "CallReportApi: " + jsonObject);

        AndroidNetworking.post("https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/all_transaction_report")
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", session.getUserToken())
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideLoader();
                        AePS3ReportResponse = response.toString();
                        filterResponse(AePS3ReportResponse, SELECTED_TAB);

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        statement_list.setVisibility(View.GONE);
                        tv_nodata.setVisibility(View.VISIBLE);
                        Log.e("TAG", "onError: " + anError.getErrorBody().toString());
                        showAlert("Alert!!", "Something went wrong, Please try again latter.", getActivity());
                    }
                });
    }

    public void showLoader() {
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();

    }

    public void hideLoader() {
        pd.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            ShowCalendar();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    SearchView searchView;

    private void callFilterFunction(MenuItem item) {
        try {
            searchView = (SearchView) item.getActionView();

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if (!searchView.isIconified()) {
                        searchView.setIconified(true);
                    }
                    item.collapseActionView();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {

                    if (listOfAEPS3 != null && listOfAEPS3.size() > 0) {
                        adapterReportAEPS.getFilter().filter(newText);
                        adapterReportAEPS.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                    }
                    return false;
                }
            });
        } catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;
                //Make API call
                try {

                    callReportAPI(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), SELECTED_TAB);

                 } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
