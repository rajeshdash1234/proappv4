package com.isuisudmt.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.aeps.AEPS2ReportModel;
import com.isuisudmt.aeps.ReportActivity;
import com.isuisudmt.aeps.ReportContract;
import com.isuisudmt.aeps.ReportModel;
import com.isuisudmt.aeps.ReportPresenter;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportAEPS;
import com.isuisudmt.report.adapter.AdapterReportAEPS2;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;

import java.util.ArrayList;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;


public class FragmentReportAeps1 extends Fragment implements ReportContract.View {



    private RecyclerView reportRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ReportPresenter mActionsListener;
    ArrayList<ReportModel> reportResponseArrayList = new ArrayList<ReportModel>();
    ArrayList<AEPS2ReportModel> aeps2reportResponseArrayList = new ArrayList<AEPS2ReportModel>();
    LinearLayout dateLayout, detailsLayout;
    private AdapterReportAEPS reportRecyclerviewAdapterReportAEPS;
    private AdapterReportAEPS2 aeps2ReportRecyclerviewAdapterReportAEPS2;

    TextView noData;
    TextView totalreport;
    TextView amount;
    TextView chooseDateRange;
    private static final String TAG = ReportActivity.class.getSimpleName();
    SessionManager session;
    String tokenStr = "";
    ProgressBar progressV;
    Context context;

    //Button aeps1,aeps2;
//    String fromdate = "", todate = "";
    Boolean calenderFlag = false;
    String providerType = "AEPS";



    public FragmentReportAeps1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_aeps, container, false);

        initView(rootview);

        if (calenderFlag) {
            try {
                if (!reportResponseArrayList.isEmpty()) {
                    reportResponseArrayList.clear();
                    if (reportRecyclerviewAdapterReportAEPS != null) {
                        reportRecyclerviewAdapterReportAEPS.notifyDataSetChanged();
                    }
                }
                providerType = "AEPS";
                CallReportApi(providerType);

            } catch (Exception e) {

            }

        } else {
            callCalenderFunction();
        }

        return rootview;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                //Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                try {
                    if (providerType.equalsIgnoreCase("AEPS")) {
                        if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {
                            // filter recycler view when text is changed
                            reportRecyclerviewAdapterReportAEPS.getFilter().filter(s);
                            reportRecyclerviewAdapterReportAEPS.notifyDataSetChanged();

                            //Get Total Amount Filtered
                            detailsLayout.setVisibility(View.VISIBLE);

                        } else {
                            if(calenderFlag == false)
                            Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (aeps2reportResponseArrayList != null && aeps2reportResponseArrayList.size() > 0) {
                            // filter recycler view when text is changed
                            aeps2ReportRecyclerviewAdapterReportAEPS2.getFilter().filter(s);
                            aeps2ReportRecyclerviewAdapterReportAEPS2.notifyDataSetChanged();
                        } else {
                            if(calenderFlag == false)
                            Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {

                }


                return false;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void initView(View view) {
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("AePS 1 Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressV = view.findViewById(R.id.progressV);
        noData = view.findViewById(R.id.noData);
        amount = view.findViewById(R.id.amount);
        totalreport = view.findViewById(R.id.totalreport);
        detailsLayout = view.findViewById(R.id.detailsLayout);
        chooseDateRange = view.findViewById(R.id.chooseDateRange);
        dateLayout = view.findViewById(R.id.dateLayout);
        reportRecyclerView = view.findViewById(R.id.reportRecyclerView);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(mLayoutManager);

        mActionsListener = new ReportPresenter(FragmentReportAeps1.this);

    }

    @Override
    public void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount) {
        try {
            reportResponseArrayList = reportModelArrayList;
            amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        } catch (Exception e) {
        }
    }

    @Override
    public void AEPS2reportsReady(ArrayList<AEPS2ReportModel> reportModelArrayList, String totalAmount) {
        try {
            aeps2reportResponseArrayList = reportModelArrayList;
            amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        } catch (Exception e) {
        }
    }

    @Override
    public void showReports() {

        try {
            if (providerType.equalsIgnoreCase("AEPS")) {

                if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {
                    reportRecyclerView.setVisibility(View.VISIBLE);
                    detailsLayout.setVisibility(View.VISIBLE);
                    noData.setVisibility(View.GONE);
                    reportRecyclerviewAdapterReportAEPS = new AdapterReportAEPS(reportResponseArrayList, context, amount, totalreport);
                    reportRecyclerView.setAdapter(reportRecyclerviewAdapterReportAEPS);
                    totalreport.setText("Entries: " + reportResponseArrayList.size());
                } else {
                    reportRecyclerView.setVisibility(View.GONE);
                    detailsLayout.setVisibility(View.GONE);
                    noData.setVisibility(View.VISIBLE);
                    totalreport.setText("Entries: " + 0);
                }

            } else {
                if (aeps2reportResponseArrayList != null && aeps2reportResponseArrayList.size() > 0) {
                    reportRecyclerView.setVisibility(View.VISIBLE);
                    detailsLayout.setVisibility(View.VISIBLE);
                    noData.setVisibility(View.GONE);
                    try {

                        aeps2ReportRecyclerviewAdapterReportAEPS2 = new AdapterReportAEPS2(aeps2reportResponseArrayList, context, amount, totalreport);
                        reportRecyclerView.setAdapter(aeps2ReportRecyclerviewAdapterReportAEPS2);
                        totalreport.setText("Entries: " + aeps2reportResponseArrayList.size());

                    } catch (Exception e) {

                    }

                } else {
                    reportRecyclerView.setVisibility(View.GONE);
                    detailsLayout.setVisibility(View.GONE);
                    noData.setVisibility(View.VISIBLE);
                    totalreport.setText("Entries: " + 0);
                }
            }

        } catch (Exception e) {

        }
    }

    @Override
    public void showLoader() {
        progressV.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoader() {
        progressV.setVisibility(View.GONE);
    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();

    }

    public void CallReportApi(String providerType) {
        if (calenderFlag) {
            mActionsListener.loadReports(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, providerType);
        } else {
            Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if(resultCode == Activity.RESULT_OK){
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                CallReportApi(providerType);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
