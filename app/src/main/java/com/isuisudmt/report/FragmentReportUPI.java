package com.isuisudmt.report;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportUpi;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.model.ModelReportUPI;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;
import static com.isuisudmt.utils.Constants.URL_UPI;


public class FragmentReportUPI extends Fragment {

    private RecyclerView reportRecyclerView;
    AdapterReportUpi mAdapter;
    RecyclerView.LayoutManager layoutManager;
    AutoCompleteTextView transaction_spinner;
    ArrayList<ModelReportUPI> finoTransactionReports = new ArrayList<ModelReportUPI>();
    SessionManager session;
    String tokenStr = "", _userName;
    ProgressBar progressV;
    Context context;
    Boolean calenderFlag = false;
    TextView totalreport, amount, noData;
    LinearLayout detailsLayout;

    DecimalFormat decim;

    public FragmentReportUPI() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_upi, container, false);
        initView(rootView);

        if (calenderFlag) {
            //Do nothing
        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    private void initView(View rootView) {

        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("UPI Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        _userName = user.get(SessionManager.userName);
        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        transaction_spinner = rootView.findViewById(R.id.transaction_spinner);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);

        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);

        decim = new DecimalFormat("0.00");

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        transaction_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // mAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (mAdapter != null) {
                        mAdapter.getFilter().filter(editable.toString());
                        if (mAdapter.getItemCount() == 0) {
                            noData.setVisibility(View.VISIBLE);
                            detailsLayout.setVisibility(View.GONE);
                        } else {
                            noData.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try {
                    if (finoTransactionReports != null && finoTransactionReports.size() > 0) {
                        mAdapter.getFilter().filter(s);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        if(calenderFlag == false)
                            Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e){

                }

                return false;
            }
        });

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    public void encryptedReport(String fromDate, String toDate, String token) {
        try {
            JSONObject jsonObject = new JSONObject();
            JSONArray arr = new JSONArray();
            arr.put(0, "UPI");

            JSONArray oper_arr = new JSONArray();
            oper_arr.put(0, "QR_COLLECT");
            oper_arr.put(1, "UPI_COLLECT");
            oper_arr.put(2, "QR_STATIC");


            jsonObject.put("start_date", fromDate);
            jsonObject.put("end_date", toDate);
            jsonObject.put("userName", _userName);
            jsonObject.put("transaction_type", arr);
            jsonObject.put("operationPerformed", oper_arr);
            jsonObject.put("status", "All");


            AndroidNetworking.post(URL_UPI)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            hideLoader();

                            try {
                                finoTransactionReports.clear();
                                JSONObject obj = new JSONObject(response.toString());
                                // Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                                String length = obj.optString("length");
                                JSONObject obj_result = obj.getJSONObject("results");
                                JSONArray jsonArray = obj_result.getJSONArray("BQReport");

                                if (length.equals("0")) {
                                    noData.setVisibility(View.VISIBLE);
                                    detailsLayout.setVisibility(View.GONE);
                                    reportRecyclerView.setVisibility(View.GONE);
                                    //noData.setText(message);
                                } else {
                                    noData.setVisibility(View.GONE);

                                    finoTransactionReports.clear();
                                    reportRecyclerView.setVisibility(View.VISIBLE);

                                    ArrayList<ModelReportUPI> reportModels = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ModelReportUPI reportModel = new ModelReportUPI();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        reportModel.setId(jsonObject1.getString("Id"));
                                        reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                                        reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                        reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                        reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                        reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                        reportModel.setStatus(jsonObject1.getString("status"));
                                        reportModel.setUserName(jsonObject1.getString("userName"));
                                        reportModel.setMasterName(jsonObject1.getString("masterName"));
                                        reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                        reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));
                                        reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                        reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                        reportModel.setParamA(jsonObject1.getString("param_a"));
                                        reportModel.setParamB(jsonObject1.getString("param_b"));
                                        reportModel.setParamC(jsonObject1.getString("param_c"));

                                        finoTransactionReports.add(reportModel);
                                    }


                                    mAdapter = new AdapterReportUpi(getActivity(), finoTransactionReports, amount, totalreport);
                                    reportRecyclerView.setAdapter(mAdapter);
                                    Collections.reverse(finoTransactionReports);

                                    totalreport.setText("Entries: " + finoTransactionReports.size());
                                    double totalAmount = 0;
                                    for (int i = 0; i < finoTransactionReports.size(); i++) {
                                        if (!finoTransactionReports.get(i).getAmountTransacted().equalsIgnoreCase("null")) {
                                            totalAmount += Double.parseDouble(finoTransactionReports.get(i).getAmountTransacted());
                                        }
                                    }
                                    detailsLayout.setVisibility(View.VISIBLE);
                                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + decim.format(totalAmount));

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                noData.setVisibility(View.VISIBLE);
                                detailsLayout.setVisibility(View.GONE);

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            transaction_spinner.setVisibility(View.GONE);

                            if (finoTransactionReports.size() <= 0) {
                                noData.setVisibility(View.VISIBLE);
                                detailsLayout.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showLoader() {
        progressV.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        progressV.setVisibility(View.GONE);


    }

    private void showErrorPopup() {
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(getActivity());
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Please choose the date within 10 days from the current date";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        callCalenderFunction();

                        dialog.dismiss();
                    }
                });
        AlertDialog alert11 = alertbuilderupdate.create();
        alert11.show();
    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                showLoader();
                encryptedReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
