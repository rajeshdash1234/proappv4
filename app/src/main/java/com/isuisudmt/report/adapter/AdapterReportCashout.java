package com.isuisudmt.report.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.cashoutReport.CashoutReportModel;

import java.util.ArrayList;
import java.util.List;

public class AdapterReportCashout extends RecyclerView.Adapter<AdapterReportCashout.ReportViewhOlder> implements Filterable {

    private List<CashoutReportModel> reportModelListFiltered;
    private List<CashoutReportModel> reportModels;

    private IMethodCaller listener;
    private int lastSelectedPosition = -1;
    Context context;

    TextView fragAmount, fragTotal;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    reportModelListFiltered = reportModels;
                } else {
                    List<CashoutReportModel> filteredList = new ArrayList<>();
                    for (CashoutReportModel row : reportModels) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (
                                row.getUserName().toLowerCase().contains(charString.toLowerCase()) ||
                                        row.getStatus().toLowerCase().contains(charString.toLowerCase()) ||
                                        row.getId().contains(charString.toLowerCase()) ||
                                        row.getOperationPerformed().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    reportModelListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = reportModelListFiltered;

                getTotalAmount();


                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                reportModelListFiltered = (ArrayList<CashoutReportModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ReportViewhOlder extends RecyclerView.ViewHolder {
        public TextView openingBal, closingBal, account_no, statusTextView, tnxId, userName, createdDate, operationPerformed, flag, amount;
        LinearLayout mainView, topLayout, buttomlayout;


        public ReportViewhOlder(View view) {
            super(view);

            topLayout = view.findViewById(R.id.topLayout);
            buttomlayout = view.findViewById(R.id.buttomlayout);

            createdDate = view.findViewById(R.id.dateTime);
            statusTextView = view.findViewById(R.id.statusTextView);
            tnxId = view.findViewById(R.id.tnxId);
            amount = view.findViewById(R.id.amount);
            operationPerformed = view.findViewById(R.id.tnxType);


            userName = view.findViewById(R.id.userName);
            account_no = view.findViewById(R.id.account_no);
            openingBal = view.findViewById(R.id.openingBal);
            closingBal = view.findViewById(R.id.closingBal);


            flag = view.findViewById(R.id.flag);
            mainView = view.findViewById(R.id.mainView);


            topLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lastSelectedPosition != getAdapterPosition()) {
                        lastSelectedPosition = getAdapterPosition();
                    } else {
                        lastSelectedPosition = -1;
                    }
                    notifyDataSetChanged();
                }
            });


        }
    }

    public AdapterReportCashout(Context context, List<CashoutReportModel> reportModels, IMethodCaller listener, TextView fragAmount, TextView fragTotal) {
        this.reportModels = reportModels;
        this.reportModelListFiltered = reportModels;
        this.listener = listener;
        this.context = context;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;

    }

    public ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cashout_report_row, parent, false);

        return new ReportViewhOlder(itemView);
    }

    public void onBindViewHolder(ReportViewhOlder holder, int position) {
        CashoutReportModel reportModel = reportModelListFiltered.get(position);

        if (position == lastSelectedPosition) {
            holder.buttomlayout.setVisibility(View.VISIBLE);
            holder.mainView.setBackgroundColor(Color.parseColor("#DCE0E0"));
        } else {
            holder.mainView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
            holder.buttomlayout.setVisibility(View.GONE);
        }

        //Date
        if (reportModel.getUpdatedDate() != null && !reportModel.getUpdatedDate().matches("")) {
            holder.createdDate.setVisibility(View.VISIBLE);
            holder.createdDate.setText("Date: " + Util.getDateTime(String.valueOf(reportModel.getUpdatedDate())));
        } else {
            holder.createdDate.setVisibility(View.GONE);
        }

        //Status
        if (reportModel.getStatus() != null && !reportModel.getStatus().matches("")) {
            holder.statusTextView.setVisibility(View.VISIBLE);
            if (reportModel.getStatus().equalsIgnoreCase("SUCCESS")) {
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
            } else {
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            holder.statusTextView.setText(reportModel.getStatus());
        } else {
            holder.statusTextView.setVisibility(View.GONE);
        }

        //TnxID
        if (reportModel.getId() != null) {
            holder.tnxId.setVisibility(View.VISIBLE);
            holder.tnxId.setText("Txn ID: " + reportModel.getId());
        } else {
            holder.tnxId.setVisibility(View.GONE);
        }

        //Amount
        if (reportModel.getAmountTransacted().equals("null") || reportModel.getAmountTransacted().equals(null)) {
            holder.amount.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        } else {
            holder.amount.setText(context.getResources().getString(R.string.toatlamountreport) + reportModel.getAmountTransacted());
        }

        //Operation Performed
        if (reportModel.getOperationPerformed() != null && !reportModel.getOperationPerformed().matches("")) {
            holder.operationPerformed.setVisibility(View.VISIBLE);
            holder.operationPerformed.setText("Type: "+reportModel.getOperationPerformed());
        } else {
            holder.operationPerformed.setVisibility(View.GONE);
        }


        if (reportModel.getUserName().equals("null") || reportModel.getUserName().equals(null)) {
            holder.userName.setText("N/A");
        } else {
            holder.userName.setText(reportModel.getUserName());
        }

        if (reportModel.getToAccount().equals("null") || reportModel.getToAccount().equals(null)) {
            holder.account_no.setText("N/A");
        } else {
            holder.account_no.setText(reportModel.getToAccount());
        }



        if (reportModel.getBalanceAmount().equals("null") || reportModel.getBalanceAmount().equals(null)) {
            holder.openingBal.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        } else {
            holder.openingBal.setText(context.getResources().getString(R.string.toatlamountreport) + reportModel.getBalanceAmount());
        }
        if (reportModel.getPreviousAmount().equals("null") || reportModel.getPreviousAmount().equals(null)) {
            holder.closingBal.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        } else {
            holder.closingBal.setText(context.getResources().getString(R.string.toatlamountreport) + reportModel.getPreviousAmount());
        }
    }

    public int getItemCount() {
        return reportModelListFiltered.size();
    }

    public interface IMethodCaller {
        void refreshMethod(CashoutReportModel microReportModel);
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < reportModelListFiltered.size(); i++) {
            if (reportModelListFiltered.get(i).getAmountTransacted().equals("N/A") || reportModelListFiltered.get(i).getAmountTransacted().equals("null") ||
                    reportModelListFiltered.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(reportModelListFiltered.get(i).getAmountTransacted()));
            }
        }

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        fragTotal.setText("Entries: " + reportModelListFiltered.size());

        return totalAmount;
    }

}
