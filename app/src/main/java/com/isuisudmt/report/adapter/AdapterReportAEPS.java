package com.isuisudmt.report.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.aeps.ReportModel;

import java.util.ArrayList;
import java.util.List;

public class AdapterReportAEPS extends RecyclerView.Adapter<AdapterReportAEPS.ReportViewhOlder> implements Filterable {

    private List<ReportModel> reportModelListFiltered;
    private List<ReportModel> reportModels;
    Context context;
    private int lastSelectedPosition = -1;

    TextView fragAmount, fragTotal;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    reportModelListFiltered = reportModels;
                } else {
                    List<ReportModel> filteredList = new ArrayList<>();
                    for (ReportModel row : reportModels) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getApiComment().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getApiTid().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getBankName().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getId().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getReferenceNo().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getTransactionMode().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getUserTrackId().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getUserName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    reportModelListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = reportModelListFiltered;

                getTotalAmount();

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                reportModelListFiltered = (ArrayList<ReportModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public AdapterReportAEPS(List<ReportModel> reportModels, Context context, TextView fragAmount, TextView fragTotal) {
        this.reportModels = reportModels;
        this.reportModelListFiltered = reportModels;
        this.context = context;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;
    }

    public class ReportViewhOlder extends RecyclerView.ViewHolder {

        TextView dateTime, rrnTextView, statusTextView, bankName, amount, tnxType, userName, tranID, trackID, openingBal, closingBal, flag;
        LinearLayout topLayout, buttomlayout, mainView;

        public ReportViewhOlder(View view) {
            super(view);
            dateTime = view.findViewById(R.id.dateTime);
            rrnTextView = view.findViewById(R.id.rrnTextView);
            statusTextView = view.findViewById(R.id.statusTextView);
            // bankName = view.findViewById(R.id.bankName);
            amount = view.findViewById(R.id.amount);
            tnxType = view.findViewById(R.id.tnxType);
            userName = view.findViewById(R.id.userName);
            //createdDate = view.findViewById(R.id.createdDate);
            tranID = view.findViewById(R.id.tranID);
            trackID = view.findViewById(R.id.trackID);
            openingBal = view.findViewById(R.id.openingBal);
            closingBal = view.findViewById(R.id.closingBal);
            flag = view.findViewById(R.id.flag);

            topLayout = view.findViewById(R.id.topLayout);
            buttomlayout = view.findViewById(R.id.buttomlayout);
            mainView = view.findViewById(R.id.mainView);

            topLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lastSelectedPosition != getAdapterPosition()) {
                        lastSelectedPosition = getAdapterPosition();
                    } else {
                        lastSelectedPosition = -1;
                    }
                    notifyDataSetChanged();
                }
            });

        }


    }

    public ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_row, parent, false);

        return new ReportViewhOlder(itemView);
    }

    public void onBindViewHolder(ReportViewhOlder holder, int position) {
        ReportModel reportModel = reportModelListFiltered.get(position);

        if (position == lastSelectedPosition) {
            holder.buttomlayout.setVisibility(View.VISIBLE);
            holder.mainView.setBackgroundColor(Color.parseColor("#DCE0E0"));
            //flag.setText("true");
        } else {
            holder.mainView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
            holder.buttomlayout.setVisibility(View.GONE);
            //flag.setText("false");
        }

        //  holder.tokenAndDate.setText( reportModels.get(position).getDateReport ()+""+ reportModels.get(position).getTokenDate ());
        holder.trackID.setText(reportModel.getUserTrackId());
        // holder.bankName.setText("Txn Id : " + reportModel.getId ().toString());
        holder.trackID.setText(reportModel.getApiTid());
        holder.tnxType.setText("Txn Type : " + reportModel.getTransactionMode());
        holder.rrnTextView.setText(reportModel.getReferenceNo());

        // holder.transactionModeTextView.setText("Transaction Mode : "+ reportModel.getTransactionMode ());

        holder.tranID.setText("" + reportModel.getId());

        holder.userName.setText(reportModel.getUserName());
        //holder.operationPerformed.setText("Operation Performed : "+reportModel.getOperationPerformed());

        if (reportModel.getPreviousAmount().equals("null") || reportModel.getPreviousAmount() == null)
            holder.openingBal.setText("N/A");
        else
            holder.openingBal.setText(reportModel.getPreviousAmount().toString());

        if (reportModel.getBalanceAmount().equals("null") || reportModel.getBalanceAmount() == null)
            holder.closingBal.setText("N/A");
        else
            holder.closingBal.setText(reportModel.getBalanceAmount().toString());


        if (reportModel.getAmountTransacted() == null)
            holder.amount.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        else
            holder.amount.setText(context.getResources().getString(R.string.toatlamountreport) + reportModel.getAmountTransacted());
        // holder.createdDate.setText("Date :"+Util.getDateFromTime(reportModel.getCreatedDate()));
        // holder.dateTime.setText("Date: "+reportModel.getApiComment());
        // holder.statusTextView.setText(reportModel.getStatus().toUpperCase());

        //holder.dateTime.setText("Date: " + Util.getDateFromTime(reportModel.getUpdatedDate()));
        holder.dateTime.setText("Date: " + Util.getDateTime(String.valueOf(reportModel.getUpdatedDate())));


        if (reportModel.getStatus() != null && !reportModel.getStatus().matches("")) {
            holder.statusTextView.setVisibility(View.VISIBLE);
            if (reportModel.getStatus().equalsIgnoreCase("SUCCESS")) {
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
            } else {
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            holder.statusTextView.setText(reportModel.getStatus().toUpperCase());
        } else {
            holder.statusTextView.setVisibility(View.GONE);
        }

//             holder.statusTextView.setText(reportModel.getStatus().toUpperCase());

    }


    public int getItemCount() {
        return reportModelListFiltered.size();
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < reportModelListFiltered.size(); i++) {
            if (reportModelListFiltered.get(i).getAmountTransacted().equals("N/A") || reportModelListFiltered.get(i).getAmountTransacted().equals("null") ||
                    reportModelListFiltered.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(reportModelListFiltered.get(i).getAmountTransacted()));
            }
        }


        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        fragTotal.setText("Entries: " + reportModelListFiltered.size());

        return totalAmount;
    }

}