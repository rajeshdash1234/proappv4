package com.isuisudmt.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.report.model.ModelReportBBPS;
import com.paxsz.easylink.model.AppSelectResponse;

import java.util.ArrayList;
import java.util.List;

public class AdapterReportBbps extends RecyclerView.Adapter<AdapterReportBbps.ViewHolder> implements Filterable {
    private int lastSelectedPosition = -1;
    private Context context;
    /* access modifiers changed from: private */
    public List<ModelReportBBPS> modelReportBBPS;
    public List<ModelReportBBPS> modelReportBBPSFilter;
    /* access modifiers changed from: private */

    TextView fragAmount, fragTotal;

    public AdapterReportBbps(Context context2, ArrayList<ModelReportBBPS> modelReportBBPS, TextView fragAmount, TextView fragTotal) {
        this.context = context2;
        this.modelReportBBPS = modelReportBBPS;
        this.modelReportBBPSFilter = modelReportBBPS;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report_bbps, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelReportBBPS current = modelReportBBPSFilter.get(position);

        if (current.getAmountTransacted().equals("null") || current.getAmountTransacted().equals(null))
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountacitvity) + "N/A");
        else            
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountacitvity) + current.getAmountTransacted());


        holder.updated_date.setText(new StringBuilder().append("Date: ").append(Util.getDateTime(String.valueOf(current.getUpdatedDate()))).toString());
        holder.transaction_id_recharge.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());

        holder.status.setText(current.getStatus());

        holder.operator_performed.setText("Operation Performed: " + current.getOperationPerformed());

        if (current.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }

        //Have hide it because Mobile Number field was not coming from API
        /*if (current.getMobileNumber() == "null" || current.getMobileNumber() == null)
            holder.mobile_number.setText("Mobile No: N/A");
        else
            holder.mobile_number.setText(new StringBuilder().append("Mobile No:").append(current.getMobileNumber()).toString());
*/

        //QA asked to hide it on 4/12/2020
        //holder.transaction_mode.setText("Transaction Mode: " + current.transactionMode);

    }

    public int getItemCount() {
        return this.modelReportBBPSFilter.size();
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    AdapterReportBbps adapterReportBBPS = AdapterReportBbps.this;
                    adapterReportBBPS.modelReportBBPSFilter = adapterReportBBPS.modelReportBBPS;
                } else {
                    List<ModelReportBBPS> filteredList = new ArrayList<>();
                    for (ModelReportBBPS row : AdapterReportBbps.this.modelReportBBPS) {

                        if (row.getId().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getStatus().toLowerCase().contains(charSequence) ||
                                row.getOperationPerformed().toLowerCase().contains(charSequence) ) {
                            filteredList.add(row);
                        }
                    }
                    AdapterReportBbps.this.modelReportBBPSFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = AdapterReportBbps.this.modelReportBBPSFilter;

                getTotalAmount();

                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                modelReportBBPSFilter = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView operator_performed;
        public TextView status;
        public TextView transaction_id_recharge;
        public TextView transaction_mode;
        public TextView updated_date;

        public ViewHolder(View itemView) {
            super(itemView);

            updated_date = itemView.findViewById(R.id.updated_date);
            status = itemView.findViewById(R.id.status);
            transaction_id_recharge = itemView.findViewById(R.id.transaction_id_recharge);
            amount_transacted_recharge = itemView.findViewById(R.id.amount_transacted);
            operator_performed = itemView.findViewById(R.id.operation_performed);
            transaction_mode = itemView.findViewById(R.id.transactionMode);
        }
    }

/*
    public List<ModelReportBBPS> getItems() {
        return modelReportBBPSFilter;
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < modelReportBBPSFilter.size(); i++) {
            if (modelReportBBPSFilter.get(i).getAmountTransacted().equals("N/A") || modelReportBBPSFilter.get(i).getAmountTransacted().equals("null") ||
                    modelReportBBPSFilter.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(modelReportBBPSFilter.get(i).getAmountTransacted()));
            }
        }

        return totalAmount;
    }
*/

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < modelReportBBPSFilter.size(); i++) {
            if (modelReportBBPSFilter.get(i).getAmountTransacted().equals("N/A") || modelReportBBPSFilter.get(i).getAmountTransacted().equals("null") ||
                    modelReportBBPSFilter.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(modelReportBBPSFilter.get(i).getAmountTransacted()));
            }
        }

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        fragTotal.setText("Entries: " + modelReportBBPSFilter.size());

        return totalAmount;
    }
}
