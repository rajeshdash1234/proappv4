package com.isuisudmt.report.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.report.model.ModelReportUPI;
import com.paxsz.easylink.model.AppSelectResponse;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AdapterReportUpi extends RecyclerView.Adapter<AdapterReportUpi.ViewHolder> implements Filterable {

    Context context;
    private List<ModelReportUPI> finoTransactionReports;
    private List<ModelReportUPI> finoTransactionReportsFiltered;

    TextView fragAmount, fragTotal;

    public AdapterReportUpi(Context context, List <ModelReportUPI>finoTransactionReports, TextView fragAmount, TextView fragTotal) {
        this.context = context;
        this.finoTransactionReports = finoTransactionReports;
        this.finoTransactionReportsFiltered = finoTransactionReports;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.upi_report_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelReportUPI current = finoTransactionReportsFiltered.get(position);


        if (current.getAmountTransacted().equals("null") || current.getAmountTransacted() == null)
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
        else
            holder.amount_transacted_recharge.setText(context.getResources().getString(R.string.toatlamountreport) + current.getAmountTransacted());

        holder.transactionId.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());
        holder.updated_date.setText(new StringBuilder().append("Date: ").append(Util.getDateTime(String.valueOf(current.getUpdatedDate()))).toString());
        holder.status.setText(current.getStatus());
        holder.operator_performed.setText("Operation Performed: " + current.getOperationPerformed());
        //holder.refNo.setText("Reference No: "+current.getReferenceNo());

        if (current.getReferenceNo().equals("null") || current.getReferenceNo() == null)
            holder.upiId.setText("BHIM UPI ID: N/A");
        else
            holder.upiId.setText(new StringBuilder().append("BHIM UPI ID: ").append(current.getReferenceNo()).toString());

        if (current.getParamC().equals("null") || current.getParamC() == null)
            holder.upiTransID.setText("UPI Transaction ID: N/A");
        else
            holder.upiTransID.setText("UPI Transaction ID: " + current.getParamC());

        if (current.getApiTid().equals("null") || current.getApiTid() == null)
            holder.apiTid.setText("API TID: N/A");
        else
            holder.apiTid.setText("API TID: " + current.getApiTid());

        if (current.getApiComment().equals("null") || current.getApiComment() == null)
            holder.apiComment.setText("API Comment: N/A");
        else
            holder.apiComment.setText("API Comment: " + current.getApiComment());


        if (current.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }
    }

    @Override
    public int getItemCount() {
        return finoTransactionReportsFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView upiId;
        public TextView operator_performed;
        public TextView status;
        public TextView transactionId;
        //public TextView refNo;
        public TextView updated_date;
        public TextView apiTid, apiComment, upiTransID;

        public ViewHolder(View itemView) {
            super(itemView);
            updated_date = itemView.findViewById(R.id.updated_date);
            status = itemView.findViewById(R.id.status);
            transactionId = itemView.findViewById(R.id.transactionId);
            amount_transacted_recharge = itemView.findViewById(R.id.amount_transacted);
            upiId = itemView.findViewById(R.id.upiId);
            operator_performed = itemView.findViewById(R.id.operation_performed);
            //refNo =  itemView.findViewById(R.id.refNo);
            upiTransID = itemView.findViewById(R.id.upiTransID);
            apiTid = itemView.findViewById(R.id.apiTid);
            apiComment = itemView.findViewById(R.id.apiComment);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    finoTransactionReportsFiltered = finoTransactionReports;
                } else {
                    List<ModelReportUPI> filteredList = new ArrayList<>();
                    for (ModelReportUPI row : finoTransactionReports) {

                        if (row.getApiComment().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getApiTid().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getId().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getReferenceNo().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getUserName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }

                      /*  if (String.valueOf(row.getApiTid()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getApiTid()).contains(charSequence)) {
                        if (String.valueOf(row.getApiTid()).toLowerCase().contains(charString.toLowerCase()) || row.getApiTid().contains(charSequence)) {
                            filteredList.add(row);
                            Log.e("dhfklsdhfisdhilhflsdk",filteredList.toString());
                        }
                        if (String.valueOf(row.getReferenceNo()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getReferenceNo()).contains(charSequence)) {
                            filteredList.add(row);
                        }

                        if (String.valueOf(row.getId()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getId()).contains(charSequence)) {
                            filteredList.add(row);
                        }*/
                    }
                    finoTransactionReportsFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = finoTransactionReportsFiltered;

                getTotalAmount();


                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                finoTransactionReportsFiltered = (ArrayList<ModelReportUPI>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }






    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < finoTransactionReportsFiltered.size(); i++) {
            if (finoTransactionReportsFiltered.get(i).getAmountTransacted().equals("N/A") || finoTransactionReportsFiltered.get(i).getAmountTransacted().equals("null") ||
                    finoTransactionReportsFiltered.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(finoTransactionReportsFiltered.get(i).getAmountTransacted()));
            }
        }

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + new DecimalFormat("0.00").format(totalAmount));
        fragTotal.setText("Entries: " + finoTransactionReportsFiltered.size());

        return totalAmount;
    }

}
