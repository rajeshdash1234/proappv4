package com.isuisudmt.report.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.report.model.AEPS3ReportList;

import java.util.ArrayList;
import java.util.List;

public class AdapterReportAEPS3 extends RecyclerView.Adapter<AdapterReportAEPS3.RecyclerViewHolder> implements Filterable {

    private static int lastSelectedPosition = -1;

    public List<AEPS3ReportList> reportModelListFiltered;
    public List<AEPS3ReportList> reportModels;
    private Context context;
    TextView fragTotal;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                String charString = constraint.toString();
                if (charString.isEmpty()) {
                    Log.e("TAG", "performFiltering: if charstring is empty:" + charString);
                    reportModelListFiltered = reportModels;
                } else {
                    List<AEPS3ReportList> filteredList = new ArrayList<>();
                    for (AEPS3ReportList row : reportModels) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match

                        Log.e("TAG", "performFiltering: to lowercase: " + row.operationPerformed.toLowerCase() + "," + row.apiTid.toLowerCase().contains(charString.toLowerCase()) + "," + row.id.toLowerCase().contains(charString.toLowerCase()));
                        if (row.operationPerformed.toLowerCase().contains(charString.toLowerCase()) ||
                                row.apiTid.toLowerCase().contains(charString.toLowerCase()) ||
                                row.id.toLowerCase().contains(charString.toLowerCase()) ||
                                row.status.toLowerCase().contains(charString.toLowerCase()) ||
                                row.amountTransacted.toLowerCase().contains(charString.toLowerCase()) ||
                                row.referenceNo.toLowerCase().contains(charString.toLowerCase()) ||
                                row.transactionMode.toLowerCase().contains(charString.toLowerCase()) ||
                                row.userName.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);

                        }
                        Log.e("TAG", "performFiltering: filteredList:" + filteredList);
                    }
                    reportModelListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = reportModelListFiltered;

                fragTotal.setText("Entries: " + reportModelListFiltered.size());

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                reportModelListFiltered = (ArrayList<AEPS3ReportList>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public AdapterReportAEPS3(ArrayList<AEPS3ReportList> list, Context context, TextView fragTotal) {
        this.reportModelListFiltered = list;
        this.reportModels = list;
        this.context = context;
        this.fragTotal = fragTotal;
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView dateTime, statusTextView, bankName, rrnTextView, amount, tnxType, userName, createdDate, tranID, trackID, openingBal, closingBal, flag;
        LinearLayout topLayout, buttomlayout, mainView;

        public RecyclerViewHolder(View view) {
            super(view);
            dateTime = view.findViewById(R.id.dateTime);
            statusTextView = view.findViewById(R.id.statusTextView);
            // bankName = view.findViewById(R.id.bankName);
            amount = view.findViewById(R.id.amount);
            tnxType = view.findViewById(R.id.tnxType);
            userName = view.findViewById(R.id.userName);
            rrnTextView = view.findViewById(R.id.rrnTextView);
            tranID = view.findViewById(R.id.tranID);
            trackID = view.findViewById(R.id.trackID);
            openingBal = view.findViewById(R.id.openingBal);
            closingBal = view.findViewById(R.id.closingBal);
            flag = view.findViewById(R.id.flag);
            topLayout = view.findViewById(R.id.topLayout);
            buttomlayout = view.findViewById(R.id.buttomlayout);
            mainView = view.findViewById(R.id.mainView);

            topLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lastSelectedPosition != getAdapterPosition()) {
                        lastSelectedPosition = getAdapterPosition();
                    } else {
                        lastSelectedPosition = -1;
                    }
                    notifyDataSetChanged();
                }
            });

        }

    }


    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                 int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_row, parent, false);

        return new RecyclerViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder,
                                 int position) {
        try {
            if (position == lastSelectedPosition) {
                holder.buttomlayout.setVisibility(View.VISIBLE);
                holder.mainView.setBackgroundColor(Color.parseColor("#DCE0E0"));
                //flag.setText("true");
            } else {
                holder.mainView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                holder.buttomlayout.setVisibility(View.GONE);
                //flag.setText("false");
            }


            if (reportModelListFiltered.get(position).operationPerformed.contains("AEPS_CASH_WITHDRAWAL")) {
                holder.tnxType.setText("Txn Type : " + "cash withdrawal");
            } else if (reportModelListFiltered.get(position).operationPerformed.contains("AEPS_BALANCE_ENQUIRY")) {
                holder.tnxType.setText("Txn Type : " + " Balance Enquiry");
            } else if (reportModelListFiltered.get(position).operationPerformed.contains("AEPS_MINI_STATEMENT")) {
                holder.tnxType.setText("Txn Type : " + "Mini statement");
            } else {
                holder.tnxType.setText("Txn Type : " + "Aadhaar Pay");
            }

            holder.trackID.setText(reportModelListFiltered.get(position).apiTid);

            holder.tranID.setText("" + reportModelListFiltered.get(position).id);
            holder.userName.setText(reportModelListFiltered.get(position).userName);
            if (reportModelListFiltered.get(position).previousAmount == "null" || reportModelListFiltered.get(position).previousAmount == null) {
                holder.openingBal.setText("N/A");
            } else {
                holder.openingBal.setText(reportModelListFiltered.get(position).previousAmount);
            }

            if (reportModelListFiltered.get(position).previousAmount == "null" || reportModelListFiltered.get(position).previousAmount == null) {
                holder.closingBal.setText("N/A");
            } else {
                holder.closingBal.setText(reportModelListFiltered.get(position).balanceAmount);
            }

            if (reportModelListFiltered.get(position).previousAmount == "null" || reportModelListFiltered.get(position).previousAmount == null) {
                holder.amount.setText("N/A");
            } else {
                holder.amount.setText(context.getResources().getString(R.string.toatlamountreport) + reportModelListFiltered.get(position).amountTransacted);
            }

            if (reportModelListFiltered.get(position).referenceNo == "null" || reportModelListFiltered.get(position).referenceNo == null) {
                holder.rrnTextView.setText("N/A");
            } else {
                holder.rrnTextView.setText(reportModelListFiltered.get(position).referenceNo);
            }
            holder.dateTime.setText("Date: " + Util.getDateTime(String.valueOf(reportModelListFiltered.get(position).updatedDate)));

            if (reportModelListFiltered.get(position).status != null && !reportModelListFiltered.get(position).status.matches("")) {
                holder.statusTextView.setVisibility(View.VISIBLE);
                if (reportModelListFiltered.get(position).status.equalsIgnoreCase("SUCCESS")) {
                    holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
                } else {
                    holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.red));
                }
                holder.statusTextView.setText(reportModelListFiltered.get(position).status.toUpperCase());
            } else {
                holder.statusTextView.setVisibility(View.GONE);
            }

        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return reportModelListFiltered.size();
    }

}
