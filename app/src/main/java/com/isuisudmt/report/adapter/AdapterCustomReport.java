package com.isuisudmt.report.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.dmt.FinoTransactionReports;

import java.util.ArrayList;
import java.util.List;

public class AdapterCustomReport extends RecyclerView.Adapter<AdapterCustomReport.ViewHolder> implements Filterable {
    private final static String TAG = AdapterCustomReport.class.getSimpleName();

    private Context context;
    private List<FinoTransactionReports> finoTransactionReports;
    private List<FinoTransactionReports> finoTransactionReportsFiltered;

    TextView fragAmount, fragTotal;

    public AdapterCustomReport(Context context, List finoTransactionReports, TextView fragAmount, TextView fragTotal) {
        this.context = context;
        this.finoTransactionReports = finoTransactionReports;
        this.finoTransactionReportsFiltered = finoTransactionReports;
        this.fragTotal = fragTotal;
        this.fragAmount = fragAmount;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dmt_report_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            holder.itemView.setTag(finoTransactionReportsFiltered.get(position));

            FinoTransactionReports pu = finoTransactionReportsFiltered.get(position);

            holder.accNo.setText("Account No: " + pu.getToAccount());
            // holder.previousAmount.setText(String.valueOf(pu.getPreviousAmount()));
            // holder.amountTransacted.setText(String.valueOf(pu.getAmountTransacted()));
            if (pu.getAmountTransacted().toString().equals("null") || pu.getAmountTransacted().toString().equals(null))
                holder.amount.setText(context.getResources().getString(R.string.toatlamountreport) + "N/A");
            else
                holder.amount.setText(String.valueOf(context.getResources().getString(R.string.toatlamountreport) + pu.getAmountTransacted()));

            holder.bankName.setText("Bank Name: " + pu.getBankName());
            //holder.beniMobile.setText(pu.getBeniMobile());
            //holder.benificiaryName.setText(pu.getBenificiaryName());
            //holder.transactionMode.setText(pu.getTransactionMode());
            holder.tnxType.setText("Txn ID: " + pu.getApiTid() + "  " + "( " + pu.getTransactionMode() + " )");
            holder.dateTime.setText("Date: " + Util.getDateTime(String.valueOf(pu.getCreatedDate())));
            holder.route.setText("Route: " + pu.getRouteID());
            if (pu.getStatus().equalsIgnoreCase("Success")) {
                holder.statusTextView.setText(pu.getStatus());
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
            } else {
                holder.statusTextView.setText(pu.getStatus());
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_red));

            }

            if (pu.getApiComment() != null) {
                holder.api_comment.setText("" + pu.getApiComment());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return finoTransactionReportsFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dateTime, statusTextView, bankName, amount, accNo, tnxType, route, api_comment;


        public ViewHolder(View itemView) {
            super(itemView);

            dateTime = itemView.findViewById(R.id.dateTime);
            statusTextView = (TextView) itemView.findViewById(R.id.statusTextView);
            bankName = (TextView) itemView.findViewById(R.id.bankName);
            amount = (TextView) itemView.findViewById(R.id.amount);
            accNo = (TextView) itemView.findViewById(R.id.accNo);
            tnxType = (TextView) itemView.findViewById(R.id.tnxType);
            route = (TextView) itemView.findViewById(R.id.route);
            api_comment = itemView.findViewById(R.id.api_comment);

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    finoTransactionReportsFiltered = finoTransactionReports;
                } else {
                    List<FinoTransactionReports> filteredList = new ArrayList<>();
                    for (FinoTransactionReports row : finoTransactionReports) {

                        if (row.getApiTid().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getBankName().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getToAccount().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getId().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }

                    }

                    finoTransactionReportsFiltered = filteredList;


                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = finoTransactionReportsFiltered;

                getTotalAmount();

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                finoTransactionReportsFiltered = (ArrayList<FinoTransactionReports>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public double getTotalAmount() {
        double totalAmount = 0;
        for (int i = 0; i < finoTransactionReportsFiltered.size(); i++) {
            Log.e(TAG, "get Individual amount: "+finoTransactionReportsFiltered.get(i).getAmountTransacted() );
            if (finoTransactionReportsFiltered.get(i).getAmountTransacted().equals("N/A") || finoTransactionReportsFiltered.get(i).getAmountTransacted().equals("null") ||
                    finoTransactionReportsFiltered.get(i).getAmountTransacted().toString().equals(null)) {
                totalAmount += 0;
            } else {
                totalAmount += Double.parseDouble(String.valueOf(finoTransactionReportsFiltered.get(i).getAmountTransacted()));
            }
        }

        Log.e(TAG, "getTotalAmount: "+totalAmount );

        fragAmount.setText(context.getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        fragTotal.setText("Entries: " + finoTransactionReportsFiltered.size());

        return totalAmount;
    }

}