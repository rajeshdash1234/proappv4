package com.isuisudmt.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.cashoutReport.CashoutReportContract;
import com.isuisudmt.cashoutReport.CashoutReportModel;
import com.isuisudmt.cashoutReport.CashoutReportPresenter;
import com.isuisudmt.matm.RefreshModel;
import com.isuisudmt.matm.TransactionStatusModel;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportCashout;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;

import java.util.ArrayList;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;

public class FragmentReportCashout2 extends Fragment implements CashoutReportContract.View {

    private RecyclerView reportRecyclerView;
    private CashoutReportPresenter mActionsListener;
    ArrayList<CashoutReportModel> reportResponseArrayList = new ArrayList<>();
    LinearLayout dateLayout, detailsLayout;
    private AdapterReportCashout adapterReportCashout;
    TextView noData, totalreport, amount;
    TextView chooseDateRange;
    String fromdate = "";
    String todate = "";
    String clientId = "";
    TransactionStatusModel transactionStatusModel;
    private boolean ascending = true;
    SessionManager session;
    String tokenStr = "";
    ProgressBar progressBar;
    Context context;
    //Button wallet1,wallet2;
    Boolean calenderFlag = false;
    String transaction_type = "WALLET2CASHOUT";
    String  _userName;

    public FragmentReportCashout2() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cashout, container, false);

        initView(rootView);

        if (calenderFlag) {
            //Do Nothing
        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {
                    adapterReportCashout.getFilter().filter(s);
                    adapterReportCashout.notifyDataSetChanged();
                } else {
                    if(calenderFlag == false)
                    Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }


    private void initView(View rootView) {
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("Cashout 2 Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();

        _userName = user.get(SessionManager.userName);
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressBar = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        chooseDateRange = rootView.findViewById(R.id.chooseDateRange);
        dateLayout = rootView.findViewById(R.id.dateLayout);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mActionsListener = new CashoutReportPresenter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void reportsReady(ArrayList<CashoutReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        detailsLayout.setVisibility(View.VISIBLE);
        amount.setText("Amt Tnx: ₹" + totalAmount);
    }

    @Override
    public void refreshDone(RefreshModel refreshModel) {

    }

    @Override
    public void showReports() {

        if (reportResponseArrayList != null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);
            adapterReportCashout = new AdapterReportCashout(getActivity(), reportResponseArrayList, new AdapterReportCashout.IMethodCaller() {
                @Override
                public void refreshMethod(CashoutReportModel microReportModel) {

                }
            }, amount, totalreport);
            adapterReportCashout.notifyDataSetChanged();
            reportRecyclerView.setAdapter(adapterReportCashout);
            totalreport.setText("Entries: " + reportResponseArrayList.size());
        } else {
            totalreport.setText("Entries: 0");
            reportRecyclerView.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();

    }

    @Override
    public void checkAmount(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.amountrefersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_mode_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if (status != null && status.matches("1")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.transaction_type_refersh), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {

    }

    @Override
    public void emptyRefreshData(String status) {

    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                mActionsListener.loadReports(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, transaction_type, _userName);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
