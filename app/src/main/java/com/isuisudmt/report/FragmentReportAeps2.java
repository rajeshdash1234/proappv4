package com.isuisudmt.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.tabs.TabLayout;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.aeps.AEPS2ReportModel;
import com.isuisudmt.aeps.ReportContract;
import com.isuisudmt.aeps.ReportModel;
import com.isuisudmt.aeps.ReportPresenter;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportAEPS2;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;

public class FragmentReportAeps2 extends Fragment implements ReportContract.View {


    public static RecyclerView reportRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ReportPresenter mActionsListener;
    ArrayList<ReportModel> reportResponseArrayList = new ArrayList<ReportModel>();
    static ArrayList<AEPS2ReportModel> aeps2reportResponseArrayList = new ArrayList<AEPS2ReportModel>();
    LinearLayout dateLayout, detailsLayout;
    public static AdapterReportAEPS2 aeps2ReportRecyclerviewAdapterReportAEPS2;
    TextView noData, totalreport, amount;
    TextView chooseDateRange;
    SessionManager session;
    String tokenStr = "";
    ProgressBar progressV;
    static Context context;
    String providerType = "AEPS2";
    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    JSONArray jArrayTransactionType;
    JSONArray jArrayOpsPerformed;
    String[] arr_operation_performed = {"AEPS_CASH_WITHDRAWAL", "AEPS_BALANCE_ENQUIRY", "AEPS_MINI_STATEMENT"};
    String[] arr_operation_performed_filter = {"AEPS_CASH_WITHDRAWAL", "AEPS_BALANCE_ENQUIRY", "AEPS_MINI_STATEMENT"};

    String selectedStatus = "All";
    ProgressDialog dialog;
    private TabLayout tablayout;
    Boolean calenderFlag = false;
    String AePS2ReportResponse = "";
    String SELECTED_TYPE = "all";

    ActivityReportDashboardNew activityReportDashboardNew;

    public FragmentReportAeps2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_report_aeps_two, container, false);

        initView(rootview);

        callTablistener();

        if (calenderFlag) {
            ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectFromDate + " to " + ConstantsReport.selectToDate);
            getReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, providerType);
            //filterResponse(ConstantsReport.loadJson(getActivity(), "report_responses_aeps2.json"), SELECTED_TYPE);
        } else {
            callCalenderFunction();
        }

        return rootview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    SearchView searchView;
    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try {
                    if (aeps2reportResponseArrayList != null && aeps2reportResponseArrayList.size() > 0) {
                        aeps2ReportRecyclerviewAdapterReportAEPS2.getFilter().filter(s);
                        aeps2ReportRecyclerviewAdapterReportAEPS2.notifyDataSetChanged();
                    } else {
                        if (calenderFlag == false)
                            Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {

                }
                return false;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void initView(View view) {
        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("AePS 2 Report");
        activityReportDashboardNew = new ActivityReportDashboardNew();

        dialog = new ProgressDialog(getActivity());

        jArrayTransactionType = new JSONArray();
        jArrayTransactionType.put("AEPS2");

        jArrayOpsPerformed = new JSONArray();
        for (int i = 0; i < arr_operation_performed.length; i++) {
            jArrayOpsPerformed.put(arr_operation_performed[i]);
        }

        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);

        tablayout = view.findViewById(R.id.tablayout);
        progressV = view.findViewById(R.id.progressV);
        noData = view.findViewById(R.id.noData);
        amount = view.findViewById(R.id.amount);
        totalreport = view.findViewById(R.id.totalreport);
        detailsLayout = view.findViewById(R.id.detailsLayout);
        chooseDateRange = view.findViewById(R.id.chooseDateRange);
        dateLayout = view.findViewById(R.id.dateLayout);
        reportRecyclerView = view.findViewById(R.id.reportRecyclerView);


        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(mLayoutManager);

        mActionsListener = new ReportPresenter(FragmentReportAeps2.this);

    }

    @Override
    public void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount) {
        try {
            reportResponseArrayList = reportModelArrayList;
            amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        } catch (Exception e) {
        }
    }

    @Override
    public void AEPS2reportsReady(ArrayList<AEPS2ReportModel> reportModelArrayList, String totalAmount) {
        try {
            aeps2reportResponseArrayList = reportModelArrayList;
            amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);
        } catch (Exception e) {
        }
    }

    @Override
    public void showReports() {

        try {

            if (aeps2reportResponseArrayList != null && aeps2reportResponseArrayList.size() > 0) {
                reportRecyclerView.setVisibility(View.VISIBLE);
                detailsLayout.setVisibility(View.VISIBLE);
                noData.setVisibility(View.GONE);
                try {

                    aeps2ReportRecyclerviewAdapterReportAEPS2 = new AdapterReportAEPS2(aeps2reportResponseArrayList, context, amount, totalreport);
                    reportRecyclerView.setAdapter(aeps2ReportRecyclerviewAdapterReportAEPS2);
                    totalreport.setText("Entries: " + aeps2reportResponseArrayList.size());

                } catch (Exception e) {

                }

            } else {
                reportRecyclerView.setVisibility(View.GONE);
                detailsLayout.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                totalreport.setText("Entries: " + 0);

            }

        } catch (Exception e) {

        }
    }

    @Override
    public void showLoader() {
        progressV.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoader() {
        progressV.setVisibility(View.GONE);
    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();

    }

    private void getReport(final String fromDate, final String toDate, final String token, String providerType) {
        String URL = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/all_transaction_report";

        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {

            obj.put("start_date", fromDate);
            obj.put("end_date", toDate);
            obj.put("userName", sp.getString(USER_NAME, ""));
            obj.put("transaction_type", jArrayTransactionType);
            obj.put("operationPerformed", jArrayOpsPerformed);
            obj.put("status", selectedStatus);

        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(URL)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.cancel();

                        AePS2ReportResponse = response.toString();
                        filterResponse(AePS2ReportResponse, SELECTED_TYPE);

                        /*try {


                            aeps2reportResponseArrayList.clear();
                            noData.setVisibility(View.GONE);

                            JSONObject obj = new JSONObject(response.toString());

                            String status = obj.getString("status");
                            String message = obj.optString("message");
                            String results = obj.getString("results");
                            String length = obj.getString("length");

                            if (length.equals("0")) {
                                detailsLayout.setVisibility(View.GONE);
                                noData.setVisibility(View.VISIBLE);
                                reportRecyclerView.setVisibility(View.GONE);
                            } else {
                                reportRecyclerView.setVisibility(View.VISIBLE);
                                noData.setVisibility(View.GONE);

                                if (status.equals("200")) {
                                    JSONObject objCommonClass = new JSONObject(results);
                                    String BQReport = objCommonClass.getString("BQReport");

                                    JSONArray jsonArray = new JSONArray(BQReport);

                                    AEPS2ReportResponse reportResponse = new AEPS2ReportResponse();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AEPS2ReportModel reportModel = new AEPS2ReportModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        reportModel.setId(jsonObject1.getString("Id"));
                                        reportModel.setPreviousAmount(jsonObject1.getString("previousAmount"));
                                        reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                                        reportModel.setBalanceAmount(jsonObject1.getString("balanceAmount"));
                                        reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                        reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                        reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                        reportModel.setStatus(jsonObject1.getString("status"));
                                        reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                        reportModel.setUserName(jsonObject1.getString("userName"));
                                        reportModel.setMasterName(jsonObject1.getString("masterName"));
                                        reportModel.setCreatedDate(jsonObject1.getString("createdDate"));
                                        reportModel.setUpdatedDate(jsonObject1.getString("updatedDate"));
                                        reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                        aeps2reportResponseArrayList.add(reportModel);
                                    }

                                    aeps2ReportRecyclerviewAdapterReportAEPS2 = new AdapterReportAEPS2(aeps2reportResponseArrayList, context, amount, totalreport);
                                    reportRecyclerView.setAdapter(aeps2ReportRecyclerviewAdapterReportAEPS2);

                                    totalreport.setText("Entries: " + aeps2reportResponseArrayList.size());
                                    double totalAmount = 0;
                                    for (int i = 0; i < aeps2reportResponseArrayList.size(); i++) {
                                        if (!aeps2reportResponseArrayList.get(i).getAmountTransacted().equalsIgnoreCase("null")) {
                                            totalAmount += Double.parseDouble(aeps2reportResponseArrayList.get(i).getAmountTransacted());
                                        }
                                    }
                                    detailsLayout.setVisibility(View.VISIBLE);
                                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);

                                    dialog.cancel();


                                } else {
                                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                                    dialog.cancel();
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.cancel();
                        }*/
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(anError.getErrorBody().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Toast.makeText(getActivity(), obj.getString("statusDescription").toString(), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                });

    }

    private void callCalenderFunction() {
        getActivity().invalidateOptionsMenu();
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    private void callTablistener() {
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getActivity().invalidateOptionsMenu();
                if (tab.getPosition() == 0) {
                    SELECTED_TYPE = "all";

                    if (calenderFlag) {
                        if (!AePS2ReportResponse.equals("")) {
                            filterResponse(AePS2ReportResponse, "all");
                        }
                    } else {
                        callCalenderFunction();
                    }

                } else if (tab.getPosition() == 1) {
                    SELECTED_TYPE = "AEPS_CASH_WITHDRAWAL";

                    if (calenderFlag) {
                        if (!AePS2ReportResponse.equals("")) {
                            filterResponse(AePS2ReportResponse, "AEPS_CASH_WITHDRAWAL");
                        }
                    } else {
                        callCalenderFunction();
                    }

                } else if (tab.getPosition() == 2) {
                    SELECTED_TYPE = "AEPS_BALANCE_ENQUIRY";

                    if (calenderFlag) {
                        if (!AePS2ReportResponse.equals("")) {
                            filterResponse(AePS2ReportResponse, "AEPS_BALANCE_ENQUIRY");
                        }
                    } else {
                        callCalenderFunction();
                    }

                } else if (tab.getPosition() == 3) {
                    SELECTED_TYPE = "AEPS_MINI_STATEMENT";
                    if (calenderFlag) {
                        if (!AePS2ReportResponse.equals("")) {
                            filterResponse(AePS2ReportResponse, "AEPS_MINI_STATEMENT");
                        }
                    } else {
                        callCalenderFunction();
                    }

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void filterResponse(String aePS2ReportResponse, String Type) {
        try {

            aeps2reportResponseArrayList.clear();
            noData.setVisibility(View.GONE);

            JSONObject obj = new JSONObject(aePS2ReportResponse);

            String status = obj.getString("status");
            String message = obj.optString("message");
            String results = obj.getString("results");
            String length = obj.getString("length");

            if (length.equals("0")) {
                detailsLayout.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                reportRecyclerView.setVisibility(View.GONE);
            } else {
                reportRecyclerView.setVisibility(View.VISIBLE);
                noData.setVisibility(View.GONE);

                if (status.equals("200")) {
                    JSONObject objCommonClass = new JSONObject(results);
                    String BQReport = objCommonClass.getString("BQReport");

                    JSONArray jsonArray = new JSONArray(BQReport);

                    if (Type.equals("all")) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            AEPS2ReportModel reportModel = new AEPS2ReportModel();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            reportModel.setId(jsonObject1.getString("Id"));
                            reportModel.setPreviousAmount(jsonObject1.getString("previousAmount"));
                            reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                            reportModel.setBalanceAmount(jsonObject1.getString("balanceAmount"));
                            reportModel.setApiTid(jsonObject1.getString("apiTid"));
                            reportModel.setApiComment(jsonObject1.getString("apiComment"));
                            reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                            reportModel.setStatus(jsonObject1.getString("status"));
                            reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                            reportModel.setUserName(jsonObject1.getString("userName"));
                            reportModel.setMasterName(jsonObject1.getString("masterName"));
                            reportModel.setCreatedDate(jsonObject1.getString("createdDate"));
                            reportModel.setUpdatedDate(jsonObject1.getString("updatedDate"));
                            reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                            aeps2reportResponseArrayList.add(reportModel);
                        }
                    } else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            AEPS2ReportModel reportModel = new AEPS2ReportModel();
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            if (jsonObject1.getString("operationPerformed").equals(Type)) {
                                reportModel.setId(jsonObject1.getString("Id"));
                                reportModel.setPreviousAmount(jsonObject1.getString("previousAmount"));
                                reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                                reportModel.setBalanceAmount(jsonObject1.getString("balanceAmount"));
                                reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                reportModel.setStatus(jsonObject1.getString("status"));
                                reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                reportModel.setUserName(jsonObject1.getString("userName"));
                                reportModel.setMasterName(jsonObject1.getString("masterName"));
                                reportModel.setCreatedDate(jsonObject1.getString("createdDate"));
                                reportModel.setUpdatedDate(jsonObject1.getString("updatedDate"));
                                reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                aeps2reportResponseArrayList.add(reportModel);
                            }
                        }
                    }


                    aeps2ReportRecyclerviewAdapterReportAEPS2 = new AdapterReportAEPS2(aeps2reportResponseArrayList, context, amount, totalreport);
                    reportRecyclerView.setAdapter(aeps2ReportRecyclerviewAdapterReportAEPS2);

                    totalreport.setText("Entries: " + aeps2reportResponseArrayList.size());
                    double totalAmount = 0;
                    for (int i = 0; i < aeps2reportResponseArrayList.size(); i++) {
                        if (!aeps2reportResponseArrayList.get(i).getAmountTransacted().equalsIgnoreCase("null")) {
                            totalAmount += Double.parseDouble(aeps2reportResponseArrayList.get(i).getAmountTransacted());
                        }
                    }
                    detailsLayout.setVisibility(View.VISIBLE);
                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);

                    dialog.cancel();


                } else {
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    dialog.cancel();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            dialog.cancel();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                getReport(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, providerType);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}