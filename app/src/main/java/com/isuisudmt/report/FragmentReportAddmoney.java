package com.isuisudmt.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.FetchraisedRequestModel;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportAddMoneyRequestTime;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;

public class FragmentReportAddmoney extends Fragment {

    private RecyclerView reportRecyclerView;
    AdapterReportAddMoneyRequestTime mAdapter;
    RecyclerView.LayoutManager layoutManager;
    AutoCompleteTextView transaction_spinner;
    ArrayList<FetchraisedRequestModel> finoTransactionReports = new ArrayList<>();
    SessionManager session;
    String tokenStr = "", _userName;
    ProgressBar progressV;
    Context context;
    Boolean calenderFlag = false;
    TextView totalreport, amount, noData;
    LinearLayout detailsLayout;


    public FragmentReportAddmoney() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_addmoney_report, container, false);
        initView(rootView);

        if (calenderFlag) {
            //Do nothing
        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    private void initView(View rootView) {

        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("Request Time Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        _userName = user.get(SessionManager.userName);
        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        transaction_spinner = rootView.findViewById(R.id.transaction_spinner);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);

        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);


        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        transaction_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // mAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (mAdapter != null) {
                        mAdapter.getFilter().filter(editable.toString());
                        if (mAdapter.getItemCount() == 0) {
                            noData.setVisibility(View.VISIBLE);
                        } else {
                            noData.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (finoTransactionReports != null && finoTransactionReports.size() > 0) {
                    mAdapter.getFilter().filter(s);
                    mAdapter.notifyDataSetChanged();
                }else{
                    if(calenderFlag == false)
                    Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callRequestData(String fromDate, String toDate, String token) {
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v98")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            System.out.println(">>>>-----" + encodedUrl);
                            encryptedReport(encodedUrl, fromDate, toDate, token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressV.setVisibility(View.GONE);
                    }
                });
    }

    public void encryptedReport(String encodedUrl, String fromDate, String toDate, String token) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fromDate", toDate);
            jsonObject.put("toDate", fromDate);
            jsonObject.put("requestedOrApproveDeclineTime", "Request");

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                finoTransactionReports.clear();
                                noData.setVisibility(View.GONE);
                                reportRecyclerView.setVisibility(View.VISIBLE);

                                JSONObject obj = new JSONObject(response.toString());
                                String length = obj.getString("statusDesc");
                                String status = obj.getString("status");
                                JSONArray jsonArray = obj.getJSONArray("fetchraisedRequests");

                                if (status.equalsIgnoreCase("0")) {
                                    ArrayList<FetchraisedRequestModel> reportModels = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        FetchraisedRequestModel reportModel = new FetchraisedRequestModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        reportModel.setId(jsonObject1.getString("id"));
                                        reportModel.setAmount(jsonObject1.getString("amount"));
                                        reportModel.setTransferType(jsonObject1.getString("transferType"));
                                        reportModel.setBankRefId(jsonObject1.getString("bankRefId"));
                                        reportModel.setRemarks(jsonObject1.getString("remarks"));
                                        reportModel.setRequestFrom(jsonObject1.getString("requestFrom"));
                                        reportModel.setStatus(jsonObject1.getString("status"));
                                        reportModel.setRequestTo(jsonObject1.getString("requestTo"));
                                        reportModel.setDepositedDate(jsonObject1.getLong("depositedDate"));
                                        reportModel.setRequestedTime(jsonObject1.getLong("requestedTime"));
                                        reportModel.setApproveOrDeclineTime(jsonObject1.getString("approveOrDecline_Time"));
                                        reportModel.setDepositedBankName(jsonObject1.getString("depositedBankName"));

                                        finoTransactionReports.add(reportModel);
                                    }

                                    mAdapter = new AdapterReportAddMoneyRequestTime(getActivity(), finoTransactionReports, amount, totalreport);
                                    reportRecyclerView.setAdapter(mAdapter);

                                    totalreport.setText("Entries: " + finoTransactionReports.size());
                                    double totalAmount = 0;
                                    for (int i = 0; i < finoTransactionReports.size(); i++) {
                                        if (!finoTransactionReports.get(i).getAmount().equalsIgnoreCase("null")) {
                                            totalAmount += Double.parseDouble(finoTransactionReports.get(i).getAmount());
                                        }
                                    }
                                    detailsLayout.setVisibility(View.VISIBLE);
                                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);
                                } else {
                                    noData.setVisibility(View.VISIBLE);
                                    detailsLayout.setVisibility(View.GONE);
                                    reportRecyclerView.setVisibility(View.GONE);

                                }

                                hideLoader();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                noData.setVisibility(View.VISIBLE);
                                detailsLayout.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            transaction_spinner.setVisibility(View.GONE);
                            if (finoTransactionReports.size() <= 0) {
                                noData.setVisibility(View.VISIBLE);
                                detailsLayout.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showLoader() {
        progressV.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        progressV.setVisibility(View.GONE);


    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                showLoader();
                callRequestData(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}