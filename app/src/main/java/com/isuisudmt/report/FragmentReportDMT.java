package com.isuisudmt.report;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.dmt.FinoTransactionReports;
import com.isuisudmt.dmt.ReportFragmentResponse;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterCustomReport;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;


public class FragmentReportDMT extends Fragment {

    private RecyclerView reportRecyclerView;
    AdapterCustomReport adapterCustomReport;
    RecyclerView.LayoutManager layoutManager;
    String transactionType = "ISU_FT";
    private AEPSAPIService apiService;
    AutoCompleteTextView transaction_spinner;
    ArrayList<FinoTransactionReports> finoTransactionReports = new ArrayList<>();
    LinearLayout detailsLayout;
    TextView noData;
    public static TextView totalreport;
    TextView amount;
    SessionManager session;
    String tokenStr = "";
    ProgressBar progressV;
    Context context;
    Boolean calenderFlag = false;

    public FragmentReportDMT() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dmt, container, false);

        initView(rootView);

        if (calenderFlag) {

        } else {
            callCalenderFunction();
        }

        return rootView;
    }

    private void initView(View rootView) {

        ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setTitle("DMT Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        amount = rootView.findViewById(R.id.amount);
        totalreport = rootView.findViewById(R.id.totalreport);
        detailsLayout = rootView.findViewById(R.id.detailsLayout);


        transaction_spinner = rootView.findViewById(R.id.transaction_spinner);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        transaction_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // mAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (adapterCustomReport != null) {
                        adapterCustomReport.getFilter().filter(editable.toString());
                        if (adapterCustomReport.getItemCount() == 0) {
                            noData.setVisibility(View.VISIBLE);
                        } else {
                            noData.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                if (finoTransactionReports != null && finoTransactionReports.size() > 0) {
                    // filter recycler view when text is changed
                    adapterCustomReport.getFilter().filter(s);
                    adapterCustomReport.notifyDataSetChanged();

                } else {
                    if (calenderFlag == false)
                        Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    private void reportCall(String fromdate, String todate, String token) {
        if (this.apiService == null) {
            this.apiService = new AEPSAPIService();
        }
        showLoader();

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            // encriptedReportCall(encodedUrl);
                            encryptedReport(fromdate, todate, token, encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType", transactionType);
            jsonObject.put("fromDate", fromDate);
            jsonObject.put("toDate", toDate);

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                hideLoader();

                                finoTransactionReports.clear();
                                noData.setVisibility(View.GONE);

                                JSONObject obj = new JSONObject(response.toString());

                                reportRecyclerView.setVisibility(View.VISIBLE);
                                JSONArray jsonArray = obj.getJSONArray("BQReport");


                                if (jsonArray.length() == 0) {
                                    noData.setVisibility(View.VISIBLE);
                                    detailsLayout.setVisibility(View.GONE);
                                    reportRecyclerView.setVisibility(View.GONE);

                                } else {
                                    ReportFragmentResponse reportResponse = new ReportFragmentResponse();
                                    ArrayList<FinoTransactionReports> reportModels = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        FinoTransactionReports reportModel = new FinoTransactionReports();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        reportModel.setId(jsonObject1.getString("Id"));
                                        reportModel.setPreviousAmount(Double.valueOf(jsonObject1.getString("previousAmount")));
                                        reportModel.setBalanceAmount(Double.valueOf(jsonObject1.getString("balanceAmount")));
                                        reportModel.setAmountTransacted(jsonObject1.getInt("amountTransacted"));
                                        reportModel.setBeniMobile(jsonObject1.getString("beniMobile"));
                                        reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                        reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                        reportModel.setBankName(jsonObject1.getString("bankName"));
                                        reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                        reportModel.setStatus(jsonObject1.getString("status"));
                                        reportModel.setUserName(jsonObject1.getString("userName"));
                                        reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                        reportModel.setMasterName(jsonObject1.getString("masterName"));
                                        reportModel.setAPI(jsonObject1.getString("API"));
                                        reportModel.setBenificiaryName(jsonObject1.getString("benificiaryName"));
                                        reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                        reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));
                                        reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                        reportModel.setRouteID(jsonObject1.getString("routeID"));
                                        reportModel.setToAccount(jsonObject1.getString("toAccount"));

                                        reportModels.add(reportModel);
                                    }

                                    reportResponse.setFinoTransactionReports(reportModels);

                                    finoTransactionReports = reportResponse.getFinoTransactionReports();

                                    transaction_spinner.setVisibility(View.GONE);
                                    Collections.reverse(finoTransactionReports);
                                    adapterCustomReport = new AdapterCustomReport(getActivity(), finoTransactionReports, amount, totalreport);
                                    reportRecyclerView.setAdapter(adapterCustomReport);

                                    noData.setVisibility(View.GONE);
                                    detailsLayout.setVisibility(View.VISIBLE);
                                    double totalAmount = 0;
                                    for (int i = 0; i < reportModels.size(); i++) {
                                        if (reportModels.get(i).getAmountTransacted().equals("N/A") || finoTransactionReports.get(i).getAmountTransacted().equals("null")) {
                                            totalAmount += 0;
                                        } else {
                                            totalAmount += Double.parseDouble(String.valueOf(reportModels.get(i).getAmountTransacted()));
                                        }
                                    }

                                    amount.setText(getResources().getString(R.string.toatlamountacitvity) + totalAmount);
                                    totalreport.setText("Entries: " + reportModels.size());

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                transaction_spinner.setVisibility(View.GONE);
                                hideLoader();
                                if (finoTransactionReports.size() <= 0) {
                                    detailsLayout.setVisibility(View.GONE);
                                    noData.setVisibility(View.VISIBLE);
                                } else {
                                    detailsLayout.setVisibility(View.VISIBLE);
                                    noData.setVisibility(View.GONE);
                                }
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            transaction_spinner.setVisibility(View.GONE);
                            hideLoader();
                            if (finoTransactionReports.size() <= 0) {
                                noData.setVisibility(View.VISIBLE);
                            } else {
                                noData.setVisibility(View.GONE);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showLoader() {
        progressV.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        progressV.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate + " to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                reportCall(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
