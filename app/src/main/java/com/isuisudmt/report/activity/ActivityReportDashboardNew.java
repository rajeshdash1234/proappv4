

package com.isuisudmt.report.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.Constants;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.MainActivity;
import com.isuisudmt.QRWeb.QRWebActivity;
import com.isuisudmt.R;
import com.isuisudmt.SelectCashoutWalletOptionActivity;
import com.isuisudmt.SessionManager;
import com.isuisudmt.TabsPagerAdapter;
import com.isuisudmt.Util;
import com.isuisudmt.bbps.utils.Const;
import com.isuisudmt.bluetooth.SharePreferenceClass;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.password.ChangePasswordActivity;
import com.isuisudmt.report.FragmentReportAddMoneyActive;
import com.isuisudmt.report.FragmentReportAddmoney;
import com.isuisudmt.report.FragmentReportAeps1;
import com.isuisudmt.report.FragmentReportAeps2;
import com.isuisudmt.report.FragmentReportAeps3;
import com.isuisudmt.report.FragmentReportBbps;
import com.isuisudmt.report.FragmentReportCashout1;
import com.isuisudmt.report.FragmentReportCashout2;
import com.isuisudmt.report.FragmentReportCommision1;
import com.isuisudmt.report.FragmentReportCommision2;
import com.isuisudmt.report.FragmentReportDMT;
import com.isuisudmt.report.FragmentReportInsurance;
import com.isuisudmt.report.FragmentReportMatm1;
import com.isuisudmt.report.FragmentReportMatm2;
import com.isuisudmt.report.FragmentReportRecharge;
import com.isuisudmt.report.FragmentReportUPI;
import com.isuisudmt.report.FragmentReportWallet1;
import com.isuisudmt.report.FragmentReportWallet2;
import com.isuisudmt.settings.SettingsNewActivity;
import com.matm.matmsdk.Utils.SdkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS1;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS2;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS3;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_BBPS;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_CASHOUT;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_CASHOUT_WALLET1;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_CASHOUT_WALLET2;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_DMT;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_MATM;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_MATM1;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_MATM2;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_RECHARGE;
import static com.isuisudmt.bbps.utils.Const.isAEPS1enabled;
import static com.isuisudmt.bbps.utils.Const.isAEPS2enabled;
import static com.isuisudmt.bbps.utils.Const.isAEPS3enabled;
import static com.isuisudmt.bbps.utils.Const.isBBPSenabled;
import static com.isuisudmt.bbps.utils.Const.isCashoutWallet1enabled;
import static com.isuisudmt.bbps.utils.Const.isCashoutWallet2enabled;
import static com.isuisudmt.bbps.utils.Const.isDMTenabled;
import static com.isuisudmt.bbps.utils.Const.isMATM1enabled;
import static com.isuisudmt.bbps.utils.Const.isMATM2enabled;
import static com.isuisudmt.bbps.utils.Const.isRechargeEnabled;
import static com.isuisudmt.utils.Constants.BBPS_PREF;


public class ActivityReportDashboardNew extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private LinearLayout fragment_container;
    private TabLayout tablayout;

    DrawerLayout drawer;
    NavigationView navigationView;
    TextView app_name_txt, das_user_name, nav_user_name, das_user_balance,
            das_user_name2, das_user_balance2, tv_showqr, tv_cashout, tv_upi_settlement, tv_settings,
            tv_language, tv_privacy, tv_change_password, tv_logout, tv_nav_add_money1, tv_nav_add_money2;
    RelativeLayout rlMainViewPager, rl_nav_aeps, rl_nav_matm, rl_nav_cashout, rl_nav_wallet,
            rl_nav_commission, rl_nav_Theme, rl_nav_add_money, rl_nav_view_more;

    // RelativeLayout rlMainViewPager, rl_nav_aeps, rl_nav_matm, rl_nav_cashout, rl_nav_wallet, rl_nav_commission, rl_nav_add_money;
    LinearLayout nav_insurance, nav_cms, nav_wallet, ll_nav_home, ll_nav_dmt, ll_nav_recharge, ll_nav_bbps, ll_nav_upi, ll_nav_insurance, fragment_layout, collpase_toolbar, bottomSheet;
    ImageView iv_nav_AEPS_dropdown, iv_nav_mATM_dropdown, iv_nav_cash_out_dropdown, iv_nav_wallet_dropdown, iv_nab_commission_dropdown, iv_nav_view_more;
    CardView _nav_view;
    ImageView iv_nav_aeps_dropdown, iv_nav_matm_dropdown, iv_nav_cashout_dropdown, iv_nav_commision_dropdown, iv_nav_add_money_dropdown;

    TextView tv_nav_aeps1, tv_nav_aeps2, tv_nav_aeps3, tv_nav_matm1, tv_nav_matm2,
            tv_nav_cashout1, tv_nav_cashout2, tv_nav_wallet1, tv_nav_wallet2,
            tv_nav_commision1, tv_nav_commision2, tv_viewmore_viewless;

    SharePreferenceClass sharePreferenceClass;
    TextView tv_nav_ThemeDefault, tv_nav_ThemeRed, tv_nav_ThemeYellow, tv_nav_ThemeGreen;
    RelativeLayout rl_theme_default, rl_theme_yellow, rl_theme_green, rl_theme_red, rl_PermanentGeraniumLake;
    ImageView iv_nav_theme_dropdown;
    ViewPager vpPager;
    TabsPagerAdapter myAdapter;
    Boolean isExpandedAEPS = false;
    Boolean isExpandedMATM = false;
    Boolean isExpandedCASHOUT = false;
    Boolean isExpandedWALLET = false;
    Boolean isExpandedCOMMISION = false;
    Boolean isExpandedTheme = false;
    Boolean isExpandedaddMoney = false;

    Boolean isViewMore = false;
    Handler timer_handler;
    private FirebaseAnalytics firebaseAnalytics;
    String userName = "", tokenStr = "";
    SessionManager session;
    ProgressDialog pd;
    String _token = "", _admin = "";
    String _is_upi_settle1, _userRequest;
    SharedPreferences settings, settings2;
    SharedPreferences.Editor editor;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    SharedPreferences sp, spBbps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.onActivityCreateSetTheme(this);
        new CustomThemes(this);

        setContentView(R.layout.activity_report_dashboard_new);

        setUpToolBar();
        initView();
        loadEvent();
    }

    public void setUpToolBar() {
        // Inflate the layout for this fragment
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle("");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.START);
                //What to do on back clicked
            }
        });

        //mToolbar.collapseActionView();
    }

    private void initView() {
        pd = new ProgressDialog(ActivityReportDashboardNew.this);
        sharePreferenceClass = new SharePreferenceClass(ActivityReportDashboardNew.this);

        settings = getSharedPreferences("YOUR_PREF_NAME", 0);
        editor = settings.edit();
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        spBbps = getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE);
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);
        tokenStr = user.get(SessionManager.KEY_TOKEN);
        userName = Constants.USER_NAME;
        tablayout = findViewById(R.id.tablayout);
        fragment_container = findViewById(R.id.fragment_container);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);

        nav_user_name = headerview.findViewById(R.id.nav_user_name);
        nav_user_name.setText(Constants.FULL_NAME);
        ll_nav_home = headerview.findViewById(R.id.ll_nav_home);
        ll_nav_dmt = headerview.findViewById(R.id.ll_nav_dmt);
        ll_nav_recharge = headerview.findViewById(R.id.ll_nav_recharge);
        ll_nav_bbps = headerview.findViewById(R.id.ll_nav_bbps);
        ll_nav_upi = headerview.findViewById(R.id.ll_nav_upi);
        tv_upi_settlement = headerview.findViewById(R.id.tv_upi_settlement);
        ll_nav_insurance = headerview.findViewById(R.id.ll_nav_insurance);
        rl_nav_aeps = headerview.findViewById(R.id.rl_nav_aeps);
        rl_nav_matm = headerview.findViewById(R.id.rl_nav_matm);
        rl_nav_cashout = headerview.findViewById(R.id.rl_nav_cashout);
        rl_nav_wallet = headerview.findViewById(R.id.rl_nav_wallet);
        rl_nav_commission = headerview.findViewById(R.id.rl_nav_commission);
        rl_nav_Theme = headerview.findViewById(R.id.rl_nav_theme);
        iv_nav_AEPS_dropdown = headerview.findViewById(R.id.iv_nav_aeps_dropdown);
        iv_nav_mATM_dropdown = headerview.findViewById(R.id.iv_nav_matm_dropdown);
        iv_nav_cash_out_dropdown = headerview.findViewById(R.id.iv_nav_cashout_dropdown);
        rl_nav_add_money = headerview.findViewById(R.id.rl_nav_add_money);
        rl_nav_view_more = headerview.findViewById(R.id.rl_nav_view_more);
        iv_nav_aeps_dropdown = headerview.findViewById(R.id.iv_nav_aeps_dropdown);
        iv_nav_matm_dropdown = headerview.findViewById(R.id.iv_nav_matm_dropdown);
        iv_nav_cashout_dropdown = headerview.findViewById(R.id.iv_nav_cashout_dropdown);
        iv_nav_wallet_dropdown = headerview.findViewById(R.id.iv_nav_wallet_dropdown);
        iv_nav_commision_dropdown = headerview.findViewById(R.id.iv_nav_commision_dropdown);
        iv_nav_add_money_dropdown = headerview.findViewById(R.id.iv_nav_add_money_dropdown);

        iv_nab_commission_dropdown = headerview.findViewById(R.id.iv_nav_commision_dropdown);
        iv_nav_view_more = headerview.findViewById(R.id.iv_nav_view_more);
        iv_nav_theme_dropdown = headerview.findViewById(R.id.iv_nav_theme_dropdown);
        tv_nav_aeps1 = headerview.findViewById(R.id.tv_nav_aeps1);
        tv_nav_aeps2 = headerview.findViewById(R.id.tv_nav_aeps2);
        tv_nav_aeps3 = headerview.findViewById(R.id.tv_nav_aeps3);
        tv_nav_matm1 = headerview.findViewById(R.id.tv_nav_matm1);
        tv_nav_matm2 = headerview.findViewById(R.id.tv_nav_matm2);
        tv_nav_cashout1 = headerview.findViewById(R.id.tv_nav_cashout1);
        tv_nav_cashout2 = headerview.findViewById(R.id.tv_nav_cashout2);
        tv_nav_wallet1 = headerview.findViewById(R.id.tv_nav_wallet1);
        tv_nav_wallet2 = headerview.findViewById(R.id.tv_nav_wallet2);
        tv_nav_commision1 = headerview.findViewById(R.id.tv_nav_commision1);
        tv_nav_commision2 = headerview.findViewById(R.id.tv_nav_commision2);
        tv_nav_ThemeDefault = headerview.findViewById(R.id.tv_nav_ThemeDefault);
        tv_nav_ThemeGreen = headerview.findViewById(R.id.tv_nav_ThemeGreen);
        tv_nav_ThemeYellow = headerview.findViewById(R.id.tv_nav_ThemeYellow);
        tv_nav_ThemeRed = headerview.findViewById(R.id.tv_nav_ThemeRed);
        tv_nav_add_money1 = headerview.findViewById(R.id.tv_nav_add_money1);
        tv_nav_add_money2 = headerview.findViewById(R.id.tv_nav_add_money2);

        tv_viewmore_viewless = headerview.findViewById(R.id.tv_viewmore_viewless);
        tv_cashout = headerview.findViewById(R.id.tv_cashout);
        tv_showqr = headerview.findViewById(R.id.tv_showqr);
        tv_settings = headerview.findViewById(R.id.tv_settings);
        tv_language = headerview.findViewById(R.id.tv_language);
        tv_privacy = headerview.findViewById(R.id.tv_privacy);
        tv_logout = headerview.findViewById(R.id.tv_logout);
        tv_change_password = headerview.findViewById(R.id.tv_change_password);
        rl_theme_default = headerview.findViewById(R.id.rl_theme_default);
        rl_theme_yellow = headerview.findViewById(R.id.rl_theme_yellow);
        rl_theme_green = headerview.findViewById(R.id.rl_theme_green);
        rl_theme_red = headerview.findViewById(R.id.rl_theme_red);
        rl_PermanentGeraniumLake = headerview.findViewById(R.id.rl_PermanentGeraniumLake);

        nav_user_name.setText(Constants.FULL_NAME);
    }

    @SuppressLint({"WrongConstant", "SetTextI18n", "ResourceAsColor", "CommitPrefEdits"})
    private void loadEvent() {

        if (Constants.REPORT_MODULE.equals("aeps1")) {
            replaceFragment(new FragmentReportAeps1());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("AePS1 Report");
        } else if (Constants.REPORT_MODULE.equals("aeps2")) {
            replaceFragment(new FragmentReportAeps2());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("AePS2 Report");
        } else if (Constants.REPORT_MODULE.equals("aeps3")) {
            replaceFragment(new FragmentReportAeps3());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("AePS3 Report");
        } else if (Constants.REPORT_MODULE.equals("matm1")) {
            replaceFragment(new FragmentReportMatm1());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("mATM1 Report");
        } else if (Constants.REPORT_MODULE.equals("matm2")) {
            replaceFragment(new FragmentReportMatm2());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("mATM2 Report");
        } else if (Constants.REPORT_MODULE.equals("dmt")) {
            replaceFragment(new FragmentReportDMT());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("DMT Report");
        } else if (Constants.REPORT_MODULE.equals("cashout1")) {
            replaceFragment(new FragmentReportCashout1());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Cashout 1 Report");
        } else if (Constants.REPORT_MODULE.equals("cashout2")) {
            replaceFragment(new FragmentReportCashout2());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Cashout 2 Report");
        } else if (Constants.REPORT_MODULE.equals("bbps")) {
            replaceFragment(new FragmentReportBbps());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("BBPS Report");
        } else if (Constants.REPORT_MODULE.equals("upi")) {
            replaceFragment(new FragmentReportUPI());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("UPI Report");
        } else if (Constants.REPORT_MODULE.equals("recharge")) {
            replaceFragment(new FragmentReportRecharge());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Recharge Report");
        } else if (Constants.REPORT_MODULE.equals("commision1")) {
            replaceFragment(new FragmentReportCommision1());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Commision Report");
        } else if (Constants.REPORT_MODULE.equals("commision2")) {
            replaceFragment(new FragmentReportCommision2());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Commision Report");
        } else if (Constants.REPORT_MODULE.equals("wallet1")) {
            replaceFragment(new FragmentReportWallet1());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Wallet 1 Report");
        } else if (Constants.REPORT_MODULE.equals("wallet2")) {
            replaceFragment(new FragmentReportWallet2());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Wallet 2 Report");
        } else if (Constants.REPORT_MODULE.equals("insurance")) {
            replaceFragment(new FragmentReportInsurance());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Insurance Report");
        } else if (Constants.REPORT_MODULE.equals("addMoney1")) {
            replaceFragment(new FragmentReportAddmoney());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Request Time Report");
        } else if (Constants.REPORT_MODULE.equals("addMoney2")) {
            replaceFragment(new FragmentReportAddMoneyActive());
            getSupportActionBar().setSubtitle("");
            getSupportActionBar().setTitle("Active/Decline Report");
        }


        rl_nav_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isViewMore == false) {
                    isViewMore = true;
                    tv_viewmore_viewless.setText("View Less");
                    iv_nav_view_more.setImageResource(R.drawable.ic_group_296__1_);
                    ll_nav_recharge.setVisibility(View.VISIBLE);
                    ll_nav_bbps.setVisibility(View.VISIBLE);
                    ll_nav_upi.setVisibility(View.VISIBLE);
                    rl_nav_commission.setVisibility(View.VISIBLE);
                    rl_nav_cashout.setVisibility(View.VISIBLE);
                    ll_nav_insurance.setVisibility(View.VISIBLE);
                    rl_nav_add_money.setVisibility(View.VISIBLE);

                    resetNavigationDropdownIcons();
                } else {
                    isViewMore = false;
                    tv_viewmore_viewless.setText("View More");
                    iv_nav_view_more.setImageResource(R.drawable.ic_group_296);
                    ll_nav_recharge.setVisibility(View.GONE);
                    ll_nav_bbps.setVisibility(View.GONE);
                    ll_nav_upi.setVisibility(View.GONE);
                    rl_nav_commission.setVisibility(View.GONE);
                    rl_nav_cashout.setVisibility(View.GONE);
                    ll_nav_insurance.setVisibility(View.GONE);
                    rl_nav_add_money.setVisibility(View.GONE);
                    resetNavigationDropdownIcons();
                }
            }
        });


        ll_nav_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityReportDashboardNew.this, MainActivity.class));
                drawer.closeDrawers();
            }
        });

        rl_nav_aeps.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Setting up Report Visibility
                if (isAEPS1enabled == false && isAEPS2enabled == false && isAEPS3enabled == false) {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_AEPS, Toast.LENGTH_SHORT).show();
                } else {
                    //TransitionManager.beginDelayedTransition(ll_parent);

                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    if (!isExpandedAEPS) {
                        isExpandedAEPS = true;
                        isExpandedMATM = false;
                        isExpandedCASHOUT = false;
                        isExpandedWALLET = false;
                        isExpandedCOMMISION = false;
                        isExpandedTheme = false;

                        iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

                        tv_nav_aeps1.setVisibility(View.VISIBLE);
                        tv_nav_aeps2.setVisibility(View.VISIBLE);
                        tv_nav_aeps3.setVisibility(View.VISIBLE);

                        rl_theme_default.setVisibility(View.GONE);
                        rl_theme_yellow.setVisibility(View.GONE);
                        rl_theme_green.setVisibility(View.GONE);
                        rl_theme_red.setVisibility(View.GONE);
                        rl_PermanentGeraniumLake.setVisibility(View.GONE);

                        tv_nav_matm1.setVisibility(View.GONE);
                        tv_nav_matm2.setVisibility(View.GONE);
                        tv_nav_cashout1.setVisibility(View.GONE);
                        tv_nav_cashout2.setVisibility(View.GONE);
                        tv_nav_wallet1.setVisibility(View.GONE);
                        tv_nav_wallet2.setVisibility(View.GONE);
                        tv_nav_commision1.setVisibility(View.GONE);
                        tv_nav_commision2.setVisibility(View.GONE);
                        tv_nav_add_money1.setVisibility(View.GONE);
                        tv_nav_add_money2.setVisibility(View.GONE);


                    } else {
                        resetNavigationDropdownIcons();
                    }
                }

            }
        });
        tv_nav_aeps1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAEPS1enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    Constants.REPORT_MODULE = "aeps1";
                    setFBAnalytics("MAIN_NAV_AEPS1_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_AEPS1, Toast.LENGTH_SHORT).show();
                }


            }

        });
        tv_nav_aeps2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAEPS2enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "aeps2";
                    setFBAnalytics("MAIN_NAV_AEPS2_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_AEPS2, Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_nav_aeps3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAEPS3enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "aeps3";
                    setFBAnalytics("MAIN_NAV_AEPS3_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_AEPS3, Toast.LENGTH_SHORT).show();
                }
            }
        });

        rl_nav_matm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isMATM1enabled == false && isMATM2enabled == false) {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_MATM, Toast.LENGTH_SHORT).show();
                } else {

                    //TransitionManager.beginDelayedTransition(ll_parent);

                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    if (!isExpandedMATM) {
                        isExpandedMATM = true;
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        tv_nav_matm1.setVisibility(View.VISIBLE);
                        tv_nav_matm2.setVisibility(View.VISIBLE);

                        isExpandedAEPS = false;
                        isExpandedMATM = true;
                        isExpandedCASHOUT = false;
                        isExpandedWALLET = false;
                        isExpandedCOMMISION = false;
                        isExpandedTheme = false;

                        iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

                        rl_theme_default.setVisibility(View.GONE);
                        rl_theme_yellow.setVisibility(View.GONE);
                        rl_theme_green.setVisibility(View.GONE);
                        rl_theme_red.setVisibility(View.GONE);
                        rl_PermanentGeraniumLake.setVisibility(View.GONE);

                        tv_nav_aeps1.setVisibility(View.GONE);
                        tv_nav_aeps2.setVisibility(View.GONE);
                        tv_nav_aeps3.setVisibility(View.GONE);
                        tv_nav_matm1.setVisibility(View.VISIBLE);
                        tv_nav_matm2.setVisibility(View.VISIBLE);
                        tv_nav_cashout1.setVisibility(View.GONE);
                        tv_nav_cashout2.setVisibility(View.GONE);
                        tv_nav_wallet1.setVisibility(View.GONE);
                        tv_nav_wallet2.setVisibility(View.GONE);
                        tv_nav_commision1.setVisibility(View.GONE);
                        tv_nav_commision2.setVisibility(View.GONE);
                        tv_nav_add_money1.setVisibility(View.GONE);
                        tv_nav_add_money2.setVisibility(View.GONE);


                    } else {
                        resetNavigationDropdownIcons();
                    }
                }

            }
        });
        tv_nav_matm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isMATM1enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "matm1";
                    setFBAnalytics("MAIN_NAV_MATM1_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_MATM1, Toast.LENGTH_SHORT).show();
                }
            }
        });
        tv_nav_matm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMATM2enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "matm2";
                    setFBAnalytics("MAIN_NAV_MATM2_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_MATM2, Toast.LENGTH_SHORT).show();
                }


            }
        });
        ll_nav_dmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDMTenabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "dmt";
                    setFBAnalytics("MAIN_NAV_DMT_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_DMT, Toast.LENGTH_SHORT).show();
                }
            }
        });


        rl_nav_cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCashoutWallet1enabled == false && isCashoutWallet2enabled == false) {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_CASHOUT, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    if (!isExpandedCASHOUT) {
                        isExpandedAEPS = false;
                        isExpandedMATM = false;
                        isExpandedCASHOUT = true;
                        isExpandedWALLET = false;
                        isExpandedCOMMISION = false;
                        isExpandedTheme = false;

                        iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

           /*     tv_nav_ThemeYellow.setVisibility(View.GONE);
                tv_nav_ThemeDefault.setVisibility(View.GONE);
             //   iv_theme_icon.setVisibility(View.GONE);
                tv_nav_ThemeGreen.setVisibility(View.GONE);
                tv_nav_ThemeRed.setVisibility(View.GONE);*/

                        rl_theme_default.setVisibility(View.GONE);
                        rl_theme_yellow.setVisibility(View.GONE);
                        rl_theme_green.setVisibility(View.GONE);
                        rl_theme_red.setVisibility(View.GONE);
                        rl_PermanentGeraniumLake.setVisibility(View.GONE);

                        rl_PermanentGeraniumLake.setVisibility(View.GONE);


                        tv_nav_aeps1.setVisibility(View.GONE);
                        tv_nav_aeps2.setVisibility(View.GONE);
                        tv_nav_aeps3.setVisibility(View.GONE);
                        tv_nav_matm1.setVisibility(View.GONE);
                        tv_nav_matm2.setVisibility(View.GONE);
                        tv_nav_cashout1.setVisibility(View.VISIBLE);
                        tv_nav_cashout2.setVisibility(View.VISIBLE);
                        tv_nav_wallet1.setVisibility(View.GONE);
                        tv_nav_wallet2.setVisibility(View.GONE);
                        tv_nav_commision1.setVisibility(View.GONE);
                        tv_nav_commision2.setVisibility(View.GONE);

                    } else {
                        resetNavigationDropdownIcons();
                    }
                }
            }
        });

        tv_nav_cashout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCashoutWallet1enabled) {

                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "cashout1";

                    setFBAnalytics("MAIN_NAV_CASHOUT1_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);

                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_CASHOUT_WALLET1, Toast.LENGTH_SHORT).show();
                }
            }

        });
        tv_nav_cashout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCashoutWallet2enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "cashout2";

                    setFBAnalytics("MAIN_NAV_CASHOUT2_CLICK", userName);

                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);

                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_CASHOUT_WALLET2, Toast.LENGTH_SHORT).show();
                }
            }

        });

        ll_nav_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                if (isRechargeEnabled) {
                    Constants.REPORT_MODULE = "recharge";
                    setFBAnalytics("MAIN_NAV_RECHARGE_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    intent.putExtra("report_type", "recharge");
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_RECHARGE, Toast.LENGTH_SHORT).show();
                }
            }

        });
        ll_nav_bbps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isBBPSenabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "bbps";
                    setFBAnalytics("MAIN_NAV_BBPS_CLICK", userName);
                    Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(ActivityReportDashboardNew.this, ERROR_MESSAGE_DISABLED_BBPS, Toast.LENGTH_SHORT).show();
                }
            }

        });
        ll_nav_upi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "upi";

                setFBAnalytics("MAIN_NAV_UPI_CLICK", userName);
                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();
            }
        });

        tv_upi_settlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                FetchSettlement();

                //drawer.closeDrawers();
            }
        });

        rl_nav_commission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isExpandedCOMMISION == false) {
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = false;
                    isExpandedCOMMISION = true;
                    isExpandedaddMoney = false;
                    isExpandedTheme = false;

                    iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_up);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_down);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.GONE);
                    tv_nav_wallet2.setVisibility(View.GONE);
                    tv_nav_commision1.setVisibility(View.VISIBLE);
                    tv_nav_commision2.setVisibility(View.VISIBLE);
                    tv_nav_add_money1.setVisibility(View.GONE);
                    tv_nav_add_money2.setVisibility(View.GONE);

                } else {
                    resetNavigationDropdownIcons();
                }
            }
        });

        tv_nav_commision1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "commision1";

                setFBAnalytics("MAIN_NAV_COMMISSION1_CLICK", userName);
                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);
                drawer.closeDrawers();
            }
        });
        tv_nav_commision2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "commision2";

                setFBAnalytics("MAIN_NAV_COMMISSION2_CLICK", userName);
                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();
            }
        });

        rl_nav_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isExpandedWALLET == false) {
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = true;
                    isExpandedCOMMISION = false;
                    isExpandedaddMoney = false;
                    isExpandedTheme = false;
                    iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_up);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_down);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.VISIBLE);
                    tv_nav_wallet2.setVisibility(View.VISIBLE);
                    tv_nav_commision1.setVisibility(View.GONE);
                    tv_nav_commision2.setVisibility(View.GONE);
                    tv_nav_add_money1.setVisibility(View.GONE);
                    tv_nav_add_money2.setVisibility(View.GONE);


                } else {
                    resetNavigationDropdownIcons();
                }
            }
        });

        tv_nav_wallet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Constants.REPORT_MODULE = "wallet1";

                setFBAnalytics("MAIN_NAV_WALLET1_CLICK", userName);

                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });
        tv_nav_wallet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "wallet2";

                setFBAnalytics("MAIN_NAV_WALLET2_CLICK", userName);

                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });

        /*@Author - RashmiRanjan
         * */
        rl_nav_Theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (!isExpandedTheme) {
                    isExpandedTheme = true;
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = false;
                    isExpandedCOMMISION = false;

                    iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_up);
                    iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);

                    rl_theme_default.setVisibility(View.VISIBLE);
                    rl_theme_yellow.setVisibility(View.VISIBLE);
                    rl_theme_green.setVisibility(View.VISIBLE);
                    rl_theme_red.setVisibility(View.VISIBLE);
                    rl_PermanentGeraniumLake.setVisibility(View.VISIBLE);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.GONE);
                    tv_nav_wallet2.setVisibility(View.GONE);
                    tv_nav_commision1.setVisibility(View.GONE);
                    tv_nav_commision2.setVisibility(View.GONE);

                } else {
                    resetNavigationDropdownIcons();
                }
            }
        });

        rl_theme_default.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(ActivityReportDashboardNew.this, Util.THEME_DEFAULT);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                settings.edit();
                SharedPreferences.Editor editor;
                editor = settings.edit();
                /**Saving the theme color name into the sharedPreference**/
                editor.putInt("SNOW_DENSITY", Util.THEME_DEFAULT);
                editor.putString("Theme", "THEME_DEFAULT");
                editor.apply();
                Util.putTheme(ActivityReportDashboardNew.this, "THEME_DEFAULT");
                drawer.closeDrawers();
            }
        });

        rl_PermanentGeraniumLake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(ActivityReportDashboardNew.this, Util.PermanentGeraniumLake);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.PermanentGeraniumLake);
                editor.putString("Theme", "PermanentGeraniumLake");
                editor.putInt("SNOW_DENSITY", Util.VioletBlue);
                editor.putString("Theme", "VioletBlue");
                editor.apply();
                Util.putTheme(ActivityReportDashboardNew.this, "VioletBlue");
                drawer.closeDrawers();
            }

        });//rl_PermanentGeraniumLake
        rl_PermanentGeraniumLake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(ActivityReportDashboardNew.this, Util.PermanentGeraniumLake);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.PermanentGeraniumLake);
                editor.putString("Theme", "PermanentGeraniumLake");
                editor.apply();
                Util.putTheme(ActivityReportDashboardNew.this, "PermanentGeraniumLake");
                drawer.closeDrawers();
            }

        });


        rl_theme_default.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(ActivityReportDashboardNew.this, Util.THEME_DEFAULT);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                settings.edit();
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.THEME_DEFAULT);
                editor.putString("Theme", "THEME_DEFAULT");
                editor.apply();
                Util.putTheme(ActivityReportDashboardNew.this, "PermanentGeraniumLake");
                drawer.closeDrawers();
            }

        });


        rl_theme_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(ActivityReportDashboardNew.this, Util.Bluetiful);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.Bluetiful);
                editor.putString("Theme", "Bluetiful");
                editor.apply();
                Util.putTheme(ActivityReportDashboardNew.this, "Bluetiful");
                drawer.closeDrawers();
            }

        });

        rl_theme_yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(ActivityReportDashboardNew.this, Util.MediumSlateBlue);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.MediumSlateBlue);
                editor.putString("Theme", "MediumSlateBlue");
                editor.apply();
                Util.putTheme(ActivityReportDashboardNew.this, "MediumSlateBlue");
                drawer.closeDrawers();
            }
        });

        rl_theme_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(ActivityReportDashboardNew.this, Util.VioletBlue);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.VioletBlue);
                editor.putString("Theme", "VioletBlue");
                editor.apply();
                Util.putTheme(ActivityReportDashboardNew.this, "VioletBlue");
                drawer.closeDrawers();
            }

        });


        ll_nav_insurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "insurance";

                setFBAnalytics("MAIN_NAV_INSURANCE_CLICK", userName);

                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });

        rl_nav_add_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isExpandedaddMoney == false) {
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = false;
                    isExpandedCOMMISION = false;
                    isExpandedaddMoney = true;

                    iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_up);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.GONE);
                    tv_nav_wallet2.setVisibility(View.GONE);
                    tv_nav_commision1.setVisibility(View.GONE);
                    tv_nav_commision2.setVisibility(View.GONE);
                    tv_nav_add_money1.setVisibility(View.VISIBLE);
                    tv_nav_add_money2.setVisibility(View.VISIBLE);

                } else {
                    resetNavigationDropdownIcons();
                }

            }
        });

        tv_nav_add_money1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "addMoney1";

                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });
        tv_nav_add_money2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "addMoney2";

                Intent intent = new Intent(ActivityReportDashboardNew.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });

        tv_cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                setFBAnalytics("MAIN_TV_CASHOUT_CLICK", userName);
                Intent __intent = new Intent(ActivityReportDashboardNew.this, SelectCashoutWalletOptionActivity.class);
                startActivity(__intent);
            }
        });
        tv_showqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                setFBAnalytics("MAIN_TV_QR_CLICK", userName);
                Intent __intent = new Intent(ActivityReportDashboardNew.this, QRWebActivity.class);
                startActivity(__intent);
            }
        });

        tv_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                setFBAnalytics("MAIN_TV_SETTINGS_CLICK", userName);
               /* startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                finish();*/
                Intent settingsIntent = new Intent(ActivityReportDashboardNew.this, SettingsNewActivity.class);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(settingsIntent);
                overridePendingTransition(0, 0);

                drawer.closeDrawers();

            }
        });

        tv_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("MAIN_TV_CHANGE_PASSWORD_CLICK", userName);
                Intent pwdIntent = new Intent(ActivityReportDashboardNew.this, ChangePasswordActivity.class);
                startActivity(pwdIntent);
            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogoutAlert();
            }
        });


    }

    public void showLogoutAlert() {
        Constants.selected_fingerPrint = null;
        Constants.selected_btdevice = null;
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            String message = "Do you want to logout from app";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                        /*if(_userName!=null && session_logout==false) {
                            //checkSessionExistance(_userName,false);
                        }*/
                            setFBAnalytics("MAIN_LOGOUT_YES", userName);
                            Const.viewRequiredInfoResponse = "";
                            Const.isUpdatedResponse = "";
                            Const.usernameForBBPS = "";
                            Const.Wallet2Amount = 0.0;

                            SdkConstants.LogOut = "0"; //For logout from SDK end

                            SdkConstants.BlueToothPairFlag = "0";
                            Constants.bluetoothDevice = null;
                            SdkConstants.bluetoothDevice = null;
                            Constants.selected_fingerPrint = null;
                            Constants.selected_btdevice = null;
                            sharePreferenceClass.clearData();


                            Intent i = new Intent(ActivityReportDashboardNew.this, LoginActivity.class);
                            // set the new task and clear flags
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            setFBAnalytics("MAIN_LOGOUT_NO", userName);
                            dialog.dismiss();

                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }

    }

    private void FetchSettlement() {

        if (pd != null) {
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();
        }

        //https://settlement-api-vn3k2k7q7q-uc.a.run.app/
        // String Fetch_url = "https://settlement-api-vn3k2k7q7q-uc.a.run.app/get_user_settlement_type";
//[1:35 PM] Satyam Rath
//
//
//https://settlementapi.iserveu.website/get_user_settlement_type
        //String Fetch_url = "https://settlement-api-vn3k2k7q7q-uc.a.run.app/get_user_settlement_type";

        String Fetch_url = "https://settlementapi.iserveu.website/get_user_settlement_type";

        //    String urlStr = String.valueOf(Uri.parse(Fetch_url));

        //  Log.d(TAG, "FetchSettlement:token : " + _token);

        AndroidNetworking.get(Fetch_url)
                .addHeaders("Authorization", _token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject jsonObject;
                        String resp = response.toString();
                        //  Log.d(TAG, "onResponse: " + resp);
                        if (response != null) {
                            try {
                                jsonObject = new JSONObject(String.valueOf(response));
                                JSONArray jsonArray = jsonObject.getJSONArray("userData");
                                JSONObject settle1 = (JSONObject) jsonArray.get(0);
                                _is_upi_settle1 = settle1.optString("is_upi_settle");
                                if (!_is_upi_settle1.equals("")) {
                                    pd.cancel();
                                    showAlertDialogSettleUPIdialog();
                                } else {
                                    pd.cancel();
                                    Toast.makeText(ActivityReportDashboardNew.this, "Something Went Wrong Please Try again latter", Toast.LENGTH_LONG).show();
                                }

                                // Log.d(TAG, "onResponse: " + _is_upi_settle1.toString());
                            } catch (JSONException e) {
                                pd.cancel();
                                e.printStackTrace();
                            }
                        } else {
                            pd.cancel();
                            Toast.makeText(ActivityReportDashboardNew.this, "Something Went Wrong Please Try again latter", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        pd.cancel();
                        String error_msg = anError.getErrorDetail();
                        int errorCode = anError.getErrorCode();
                        // Log.d(TAG, "onError: " + error_msg + ",ErrorCode:" + errorCode);
                        Toast.makeText(ActivityReportDashboardNew.this, error_msg + " ,Please Try again latter", Toast.LENGTH_LONG).show();

                    }
                });

    }


    @SuppressLint("NonConstantResourceId")
    private void showAlertDialogSettleUPIdialog() {
        final Dialog dialog = new Dialog(this);
        Window window = dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        RadioGroup rg_my_qrCode = dialog.findViewById(R.id.rg_choose_settlement);
        RadioButton rb_hourly_settlement = dialog.findViewById(R.id.rb_hr_settlement);
        RadioButton rb_daily_settlement = dialog.findViewById(R.id.rb_dly_settlement);
        Button btn_save_close = dialog.findViewById(R.id.btn_save_close);
        Button btn_close = dialog.findViewById(R.id.btn_cancle);

        if (_is_upi_settle1.equals("1")) {
            rg_my_qrCode.check(R.id.rb_dly_settlement);
        } else if (_is_upi_settle1.equals("0")) {
            rg_my_qrCode.check(R.id.rb_hr_settlement);
        } else {
            rb_daily_settlement.setChecked(false);
            rb_hourly_settlement.setChecked(false);
        }

        rg_my_qrCode.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rb_hr_settlement:
                    _userRequest = "0";
                    break;
                case R.id.rb_dly_settlement:
                    _userRequest = "1";
                    break;
            }
        });
        btn_save_close.setOnClickListener(v -> {
            changeSettlementAPI();
            dialog.dismiss();
        });
        btn_close.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void changeSettlementAPI() {
        if (pd != null) {
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userRequest", _userRequest);
            String url = "https://settlement-api-vn3k2k7q7q-uc.a.run.app/change_user_settlement_type";
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", _token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONObject jsonObject;
                            if (response != null) {
                                try {
                                    jsonObject = new JSONObject(String.valueOf(response));
                                    String msg = jsonObject.optString("message");
                                    if (!msg.equals("")) {
                                        Toast.makeText(ActivityReportDashboardNew.this, msg, Toast.LENGTH_LONG).show();
                                        pd.cancel();
                                    } else {
                                        Toast.makeText(ActivityReportDashboardNew.this, "something went Wrong Please try again Latter  !!!", Toast.LENGTH_LONG).show();
                                        pd.cancel();
                                    }
                                } catch (JSONException e) {
                                    pd.cancel();
                                    Toast.makeText(ActivityReportDashboardNew.this, "something went Wrong Please try again Latter !!!", Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            } else {
                                pd.cancel();
                                Toast.makeText(ActivityReportDashboardNew.this, "something went Wrong Please try again Latter !!!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            pd.cancel();
                            String error_msg = anError.getErrorDetail();
                            Toast.makeText(ActivityReportDashboardNew.this, error_msg + " ,Please try again !!!", Toast.LENGTH_LONG).show();
                        }
                    });

        } catch (JSONException e) {
            pd.cancel();
            e.printStackTrace();
        }
    }

    public void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }


    private void resetNavigationDropdownIcons() {
        isExpandedAEPS = false;
        isExpandedMATM = false;
        isExpandedCASHOUT = false;
        isExpandedWALLET = false;
        isExpandedCOMMISION = false;
        isExpandedaddMoney = false;
        isExpandedTheme = false;


        iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

        tv_nav_aeps1.setVisibility(View.GONE);
        tv_nav_aeps2.setVisibility(View.GONE);
        tv_nav_aeps3.setVisibility(View.GONE);
        tv_nav_matm1.setVisibility(View.GONE);
        tv_nav_matm2.setVisibility(View.GONE);
        tv_nav_cashout1.setVisibility(View.GONE);
        tv_nav_cashout2.setVisibility(View.GONE);
        tv_nav_wallet1.setVisibility(View.GONE);
        tv_nav_wallet2.setVisibility(View.GONE);
        tv_nav_commision1.setVisibility(View.GONE);
        tv_nav_commision2.setVisibility(View.GONE);

       /* tv_nav_ThemeYellow.setVisibility(View.GONE);
        tv_nav_ThemeGreen.setVisibility(View.GONE);
        tv_nav_ThemeDefault.setVisibility(View.GONE);
      //  iv_theme_icon.setVisibility(View.GONE);
        tv_nav_ThemeRed.setVisibility(View.GONE);*/
        rl_theme_default.setVisibility(View.GONE);
        rl_theme_yellow.setVisibility(View.GONE);
        rl_theme_green.setVisibility(View.GONE);
        rl_theme_red.setVisibility(View.GONE);
        rl_PermanentGeraniumLake.setVisibility(View.GONE);
        tv_nav_add_money1.setVisibility(View.GONE);
        tv_nav_add_money2.setVisibility(View.GONE);
    }

/*
    private void callTablistener() {
        replaceFragment(new AepsFragment());
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    replaceFragment(new AepsFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("AePS Report");
                } else if (tab.getPosition() == 1) {
                    replaceFragment(new MatmFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("mATM Report");
                }
                else if(tab.getPosition() == 2) {
                    replaceFragment(new DmtFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("DMT Report");
                }
                else if(tab.getPosition() == 3){
                    replaceFragment(new BbpsFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("BBPS");
                }
                else if(tab.getPosition() == 4){
                    replaceFragment(new MobilePrepaidFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("Mobile Prepaid");
                }
                else if(tab.getPosition() == 5){
                    replaceFragment(new CashOutFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("Cashout");
                }
                 else if (tab.getPosition() == 6) {
                    replaceFragment(new WalletFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle((CharSequence) "Wallet");
                } else if (tab.getPosition() == 7) {
                    replaceFragment(new InsuranceFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle((CharSequence) "Insurance Report");
                } else {
                    replaceFragment(new Recharge_Fragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle((CharSequence) "Recharge Report");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    */

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.navigation_home:


                break;
            case R.id.navigation_aeps:
              /*  try {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    if (Constants.user_feature_array.contains("27") || Constants.user_feature_array.contains("47")) {
//                    Intent intent = new Intent(ActivityReportDashboardNew.this, AEPSActivity.class);
//                    startActivity(intent);
                        home = "AEPS";

                        //  toolbar_layout.setVisibility(View.GONE);
                        fragmentManager = getSupportFragmentManager();
                        Fragment fragment = AepsbFragment.getInstance();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fragment_layout, fragment);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        showOption(R.id.add_money);
                        transaction.commit();
                        fragment_layout.setVisibility(View.VISIBLE);
                        collpase_toolbar.setVisibility(View.GONE);
                        scrollRange = 0;

                    }else {
                        Toast.makeText(this, "AEPS Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();
                    }


                }catch (Exception e){
                    e.printStackTrace();
                }*/
                break;

            case R.id.navigation_matm:

              /*  try {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    if (Constants.user_feature_array.contains("33") || Constants.user_feature_array.contains("48")) {
                        home = "MATM";
                        fragmentManager = getSupportFragmentManager();
                        Fragment fragment = MatmbFragment.getInstance();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fragment_layout, fragment);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        showOption(R.id.add_money);
                        transaction.commit();
                        fragment_layout.setVisibility(View.VISIBLE);
                        collpase_toolbar.setVisibility(View.GONE);
                        scrollRange = 0;


                    Intent microATM = new Intent(ActivityReportDashboardNew.this, MATMActivity.class);
                    startActivity(microATM);
                    } else {
                        Toast.makeText(this, "MATM Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();


                    }
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                break;
            case R.id.navigation_dmt:

              /*  try{
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    if (Constants.user_feature_array.contains("30")) {
                        home = "DMT";
                        fragmentManager = getSupportFragmentManager();
                        Fragment fragment = DmtbFragment.getInstance();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fragment_layout, fragment);
                        transaction.commit();
                        fragment_layout.setVisibility(View.VISIBLE);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        collpase_toolbar.setVisibility(View.GONE);
                        showOption(R.id.add_money);
                        scrollRange = 0;

//                    Intent navigation_dmt = new Intent(ActivityReportDashboardNew.this, TransferFragment.class);
//                    startActivity(navigation_dmt);
                    } else {
                        Toast.makeText(this, "DMT Feature not available, please try after sometimes", Toast.LENGTH_SHORT).show();
                    }


                }catch (Exception e){
                    e.printStackTrace();
                }*/

                break;

        }

        drawer.closeDrawers();

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        drawer.closeDrawers();
        startActivity(new Intent(ActivityReportDashboardNew.this, MainActivity.class));
    }

}
