package com.isuisudmt.report.calender.timesquare;

public enum RangeState {
    NONE, FIRST, MIDDLE, LAST
}
