package com.isuisudmt.report.calender.timesquare;

public interface DayViewAdapter {
  void makeCellView(CalendarCellView parent);
}
