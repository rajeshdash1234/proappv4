package com.isuisudmt.report;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.matm.MicroReportContract;
import com.isuisudmt.matm.MicroReportModel;
import com.isuisudmt.matm.MicroReportPresenter;
import com.isuisudmt.matm.RefreshModel;
import com.isuisudmt.matm.TransactionStatusModel;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.report.adapter.AdapterReportMATM;
import com.isuisudmt.report.calender.CalenderActivity;
import com.isuisudmt.report.util.ConstantsReport;

import java.util.ArrayList;
import java.util.HashMap;

import static com.isuisudmt.report.util.ConstantsReport.CALENDER_PICKER_CODE;


public class FragmentReportMatm1 extends Fragment implements MicroReportContract.View {

    private RecyclerView reportRecyclerView;
    private MicroReportPresenter mActionsListener;
    ArrayList<MicroReportModel> reportResponseArrayList ;
    LinearLayout dateLayout,detailsLayout;
    private AdapterReportMATM adapterReportMATM;
    TextView noData,totalreport,amount;
    TextView chooseDateRange;
    String fromdate = "";
    String todate = "";
    String clientId = "";
    TransactionStatusModel transactionStatusModel;
    private boolean ascending = true;
    SessionManager session;
    String tokenStr="",trans_type="";
    ProgressBar progressBar;
    Context context;
    //Button matm1,matm2;
    Boolean calenderFlag = false;
    public static Boolean refreshmAtmReport = false;

    public String matam_type="MATM_FUND_TRANSFER";

    public FragmentReportMatm1() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_matm, container, false);
        rootView.findViewById(R.id.tablayout).setVisibility(View.GONE);
        pref = getActivity().getSharedPreferences("DEFUALT", 0);
        editor = pref.edit();

        AdapterReportMATM.transType="";

        initView(rootView);

        matam_type="MATM_FUND_TRANSFER";
        trans_type="IATM_SETTLED";//should be send in Api
        if(calenderFlag){
            matam_type="MATM_FUND_TRANSFER";
            if(reportResponseArrayList!=null) {
                reportResponseArrayList.clear();
            }
            if(adapterReportMATM !=null) {
                adapterReportMATM.notifyDataSetChanged();
            }

            CallReportApi(trans_type);
        }else{
            callCalenderFunction();
        }

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        }
        else if(id == R.id.filterBar){
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                try{
                if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
                    adapterReportMATM.getFilter().filter(s);
                    adapterReportMATM.notifyDataSetChanged();

                }else{
                    if(calenderFlag == false)
                    Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                }
                }catch (Exception e){

                }
                return false;
            }
        });

    }

    private void initView(View rootView) {
        ((ActivityReportDashboardNew)getActivity()).getSupportActionBar().setTitle("mATM 1 Report");
        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressBar  = rootView.findViewById(R.id.progressBar);

        noData = rootView.findViewById ( R.id.noData );
        amount = rootView.findViewById ( R.id.amount );
        totalreport = rootView.findViewById ( R.id.totalreport );
        chooseDateRange = rootView.findViewById ( R.id.chooseDateRange );
        dateLayout = rootView.findViewById ( R.id.dateLayout );
        detailsLayout = rootView.findViewById ( R.id.detailsLayout );
        reportRecyclerView = rootView.findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize ( true );
        reportRecyclerView.setLayoutManager ( new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false ) );
        mActionsListener = new MicroReportPresenter ( this );

    }

    @Override
    public void onResume() {
        super.onResume();
        if (refreshmAtmReport){

            if (AdapterReportMATM.transType.equalsIgnoreCase("")){
                CallReportApi("IATM_SETTLED");
            }
            else {
                CallReportApi(AdapterReportMATM.transType);
            }

        }

        if(fromdate.length()!=0) {
            mActionsListener.loadReports(fromdate, Util.getNextDate(todate), tokenStr, "IATM_TRANSACTION", matam_type);
        }
    }

    private void callCalenderFunction() {
        Intent intent = new Intent(getActivity(), CalenderActivity.class);
        startActivityForResult(intent, CALENDER_PICKER_CODE);
    }

    @Override
    public void reportsReady(ArrayList<MicroReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText("Amt Tnx: ₹"+totalAmount);
    }

    @Override
    public void refreshDone(RefreshModel refreshModel) {

        try{
        editor.putBoolean("INTENT",true);
        editor.commit();

            boolean installed  =   appInstalledOrNot("com.matm.matmservice");
            if(installed) {
                Intent intent = new Intent(Intent.ACTION_DATE_CHANGED);
                PackageManager manager = getActivity().getPackageManager();
                intent = manager.getLaunchIntentForPackage("com.matm.matmservice");
                intent.putExtra("RequestData", refreshModel.getEncData());
                intent.putExtra("HeaderData", refreshModel.getAuthentication());
                intent.putExtra("ReturnTime", 5);
                intent.putExtra("IS_PAIR_DEVICE", false);
                intent.putExtra("Flag", "transaction");
                intent.putExtra("TransactionType", "refresh");
                intent.putExtra("client_id", clientId);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                startActivityForResult(intent, 1);
            }else {
                showAlert(getActivity());
                System.out.println("App is not installed on your phone");
            }
        }catch (Exception e){

        }
    }

    @Override
    public void showReports() {

        if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);

            adapterReportMATM = new AdapterReportMATM(getActivity(),reportResponseArrayList,trans_type, new AdapterReportMATM.IMethodCaller() {
                @Override
                public void refreshMethod(MicroReportModel microReportModel) {
                    boolean installed  =   appInstalledOrNot("com.matm.matmservice");
                    if(installed)
                    {
                        clientId = String.valueOf(microReportModel.getId());
                        mActionsListener.refreshReports(tokenStr,String.valueOf(microReportModel.getAmountTransacted()),"statusEnquiry","mobile",String.valueOf(microReportModel.getId()));

                    }else{
                        showAlert(getActivity());

                        System.out.println("App is not installed on your phone");
                    }
                }
            }, amount, totalreport);

            reportRecyclerView.setAdapter(adapterReportMATM);
            adapterReportMATM.notifyDataSetChanged();
            totalreport.setText("Entries: "+reportResponseArrayList.size());
        }else{
            reportRecyclerView.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
            totalreport.setText("Entries: "+ 0);

        }

    }
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    @Override
    public void showLoader() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();

    }

    @Override
    public void checkAmount(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.amountrefersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.transaction_mode_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(getActivity(),getResources().getString(R.string.transaction_type_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {

    }

    @Override
    public void emptyRefreshData(String status) {

    }

    public void showAlert(Context context){
        AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(context);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Please download the MATM service app from the playstore.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        redirectToPlayStore();
                    }
                })
                .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void redirectToPlayStore(){
        Uri uri = Uri.parse("market://details?id=com.matm.matmservice");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.matm.matmservice")));
        }
    }

    public void CallReportApi(String trans_tpye) {
        if (calenderFlag) {
            mActionsListener.loadReports(ConstantsReport.selectFromDate, Util.getNextDate(ConstantsReport.selectToDate), tokenStr, trans_tpye, matam_type);
        } else {
            Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CALENDER_PICKER_CODE) {
            if(resultCode == Activity.RESULT_OK){
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle(ConstantsReport.selectToolbarFromDate +" to " + ConstantsReport.selectToolbarToDate);
                calenderFlag = true;

                //Make API call
                CallReportApi(trans_type);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                ((ActivityReportDashboardNew) getActivity()).getSupportActionBar().setSubtitle("");
                calenderFlag = false;
                //Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}