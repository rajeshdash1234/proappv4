package com.isuisudmt;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.matm.matmsdk.aepsmodule.bankspinner.BankNameModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.isuisudmt.Util.hasPermissions;

public class AddMoneyActivity extends AppCompatActivity implements PermissionManager.OnRequestPermissionListener {
    private EditText cash_withdrawal_bankspinner, amountTxt, bankref_id, remark_txt;
    private TextView amoutplainTxt, dtTxt;
    Calendar myCalendar;
    private ImageView add_photo;
    private ProgressBar progressLoad;
    private NestedScrollView nestedScrollview;

    private static final int REQUEST_SELECT_PICTURE = 222;
    public static int CAMERA_CODE = 64;
    public static Uri mImageCaptureUri;

    PermissionManager manager;
    SessionManager sessionManager;
    Button requestBtn;
    ProgressBar progressV;

    String token;
    String date_new, from_date = "";
    Spinner transfertypeSpinner, cash_withdrawal_select_bank;
    String generatedFilePath;
    Task<Uri> downloadUri;
    FirebaseStorage storage;
    StorageReference storageReference;
    private FirebaseAnalytics firebaseAnalytics;
    String userName = "", adminName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_add_money);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        manager = new PermissionManager(AddMoneyActivity.this);
        sessionManager = new SessionManager(AddMoneyActivity.this);

        HashMap<String, String> user = sessionManager.getUserDetails();
        HashMap<String, String> userDetails = sessionManager.getUserSession();


        adminName = user.get(SessionManager.KEY_ADMIN);
        userName = Constants.USER_NAME;
        token = user.get(SessionManager.KEY_TOKEN);
       // userName = user.get(SessionManager.userName);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.add_money));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        progressV = findViewById(R.id.progressV);
        requestBtn = findViewById(R.id.requestBtn);
        amountTxt = findViewById(R.id.amountTxt);
       // amoutplainTxt = findViewById(R.id.amoutplainTxt);
        bankref_id = findViewById(R.id.bankref_id);
        dtTxt = findViewById(R.id.dtTxt);
        progressLoad = findViewById(R.id.progressLoad);
        transfertypeSpinner = findViewById(R.id.transfertypeSpinner);
        cash_withdrawal_select_bank = findViewById(R.id.cash_withdrawal_select_bank);
        remark_txt = findViewById(R.id.remark_txt);
        nestedScrollview = findViewById(R.id.nestedScrollview);
        nestedScrollview.setNestedScrollingEnabled(true);


        add_photo = findViewById(R.id.add_photo1);
        add_photo.setClickable(true);
        add_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] needed_permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
//                if (hasPermissions(AddMoneyActivity.this, needed_permissions)) {

                    // progressLoad.setVisibility(View.VISIBLE);
                    //  String[] needed_permissions=new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if(hasPermissions(AddMoneyActivity.this,needed_permissions)){
                        OpenCameraDialog();
                        setFBAnalytics("ADD_MONEY_ADD_PHOTO", userName);
                    } else {
                        //OpenCameraDialog();
                        manager.checkAndRequestPermissions(AddMoneyActivity.this, needed_permissions);
                    }
//                }
            }
        });


        dtTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.YEAR, year);
                        date_new = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        dtTxt.setText(date_new);
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date from_date = null, to_date = null;
                        try {
                            from_date = formatter.parse(date_new);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        // updateLabel(dayOfMonth, monthOfYear, year);
                    }
                };

                new DatePickerDialog(AddMoneyActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });



        storage = FirebaseStorage.getInstance("gs://iserveu_storage");
        storageReference = storage.getReference();

        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String amount = amountTxt.getText().toString().trim();
                String bank_ref_id = bankref_id.getText().toString().trim();
                String date = dtTxt.getText().toString().trim();
                String bank = cash_withdrawal_select_bank.getSelectedItem().toString().trim();
                String trans_type = transfertypeSpinner.getSelectedItem().toString().trim();

                if (amount.equals(""))
                    Toast.makeText(AddMoneyActivity.this, "Please Enter Amount", Toast.LENGTH_SHORT).show();
                else if (bank.equals("Select BANK")) {
                    Toast.makeText(AddMoneyActivity.this, "Please Enter Valid Bank", Toast.LENGTH_SHORT).show();
                } else if (trans_type.equals("Select Transfer Type")) {
                    Toast.makeText(AddMoneyActivity.this, "Please Enter Valid Transfer type", Toast.LENGTH_SHORT).show();
                } else if (bank_ref_id.equals(""))
                    Toast.makeText(AddMoneyActivity.this, "Please Enter Bank Ref Id", Toast.LENGTH_SHORT).show();
                else if (date.equals(""))
                    Toast.makeText(AddMoneyActivity.this, "Please Enter Date", Toast.LENGTH_SHORT).show();
                else {
                    callRequestData(amount, bank_ref_id);
                }
            }
        });

    }


    private void OpenCameraDialog() {
        setFBAnalytics("ADD_MONEY_OPEN_CAMERA_DIALOG", userName);
        final Dialog dialog = new Dialog(AddMoneyActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.open_camera_dialogrecharge);

        Button btnGallery = dialog.findViewById(R.id.btnGallery);
        Button btnCamera = dialog.findViewById(R.id.btnCamera);

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("ADD_MONEY_GALLARY_CLICK", userName);
                PickImage(dialog);
                dialog.dismiss();
            }
        });

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("ADD_MONEY_CAMERA_CLICK", userName);
                CaptureImage(dialog);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /*    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
            cash_withdrawal_bankspinner.setText(bankIINValue.getBankName());
           // balance_enquiry_nnid = bankIINValue.getIin();
            //cash_withdrawal_nnid = "";
            // checkBalanceEnquiryValidation();


        }
    }*/

    private void PickImage(final Dialog dl) {
        File folder = new File(Environment.getExternalStorageDirectory().toString() + "/Coreapp");
        folder.mkdirs();
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_SELECT_PICTURE);
    }

    public void CaptureImage(final Dialog dl) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            File folder = new File(Environment.getExternalStorageDirectory().toString() + "/Coreapp");
            folder.mkdirs();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File f = new File(Environment.getExternalStorageDirectory() + "/Coreapp", "image.jpg");
            mImageCaptureUri = Uri.fromFile(f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(intent, CAMERA_CODE);

        } else {
            File folder = new File(Environment.getExternalStorageDirectory().toString() + "/Coreapp");
            folder.mkdirs();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File f = new File(Environment.getExternalStorageDirectory() + "/Coreapp", "image.jpg");
            mImageCaptureUri = Uri.fromFile(f);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            startActivityForResult(intent, CAMERA_CODE);
        }


    }

    private void updateLabel(int dayOfMonth, int monthOfYear, int year) {
        String myFormat = "MM/dd/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        try {
            int age = Integer.parseInt(getAge(year, monthOfYear, dayOfMonth));

            if (age >= 18 && age <= 55) {


            } else {
                /*Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.valid_age), Snackbar.LENGTH_LONG);
                snackbar.show();*/

                /*age_validation.setVisibility(View.VISIBLE);
                pincode_message.setVisibility(View.GONE);
                pincode.setVisibility(View.GONE);
                pincode_layout.setVisibility(View.GONE);
                customer_details.setVisibility(View.GONE);
                customer_bank_details.setVisibility(View.GONE);*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        manager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onRequestGranted(int requestCode, String permission) {

    }

    @Override
    public void onRequestDeclined(int requestCode, String permission) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_SELECT_PICTURE) {
                final Uri filePath = data.getData();
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Uploading");
                progressDialog.show();


                try {

                    StorageReference riversRef = storageReference.child("Balance Request/" + adminName + "/" + userName + "/receipt" + new Date() + ".png");
                    riversRef.putFile(filePath)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    //if the upload is successfull
                                    //hiding the progress dialog
                                    progressDialog.dismiss();

                                    downloadUri = taskSnapshot.getStorage().getDownloadUrl();

                                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            generatedFilePath = uri.toString();
                                        }
                                    });

//                                    if (downloadUri.isSuccessful()) {
//                                        generatedFilePath = downloadUri.getResult().toString();
//                                        Log.e("TAG", "onSuccess: generatedFilePath "+generatedFilePath );
//                                        add_photo.setImageURI(downloadUri.getResult());
//                                    }
                                    //and displaying a success toast
                                    Toast.makeText(getApplicationContext(), "image upload successfully", Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    //if the upload is not successfull
                                    //hiding the progress dialog
                                    progressDialog.dismiss();

                                    //and displaying error message
                                    Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    //calculating progress percentage
                                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                    //displaying percentage in progress dialog
                                    progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                                }
                            });


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "ERROR Photo uploading", Toast.LENGTH_SHORT).show();

                }


               /* try {
                               final Uri selectedUri = data.getData();

                    InputStream imageStream = getContentResolver().openInputStream(selectedUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);


                    String encodedImage = Util.encodeImage(selectedImage);


                    Log.i("SELECT_PICTURE :: ",""+encodedImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"ERROR Photo uploading",Toast.LENGTH_SHORT).show();
                }*/


            } else if (requestCode == CAMERA_CODE) {

                final Uri filePath = mImageCaptureUri;

                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Uploading");
                progressDialog.show();


                try {
                    // InputStream imageStream = getContentResolver().openInputStream(filePath);
                    // final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    // String encodedImage = Util.encodeImage(selectedImage);
                    // Log.i("CAMERA_CODE :: ",""+encodedImage);

                    //'Balance Request/'+ $rootScope.AdminName +'/'+$rootScope.user + '/' + "receipt" + new Date() +".png"

                    StorageReference riversRef = storageReference.child("Balance Request/" + adminName + "/" + userName + "/receipt" + new Date() + ".png");

                    riversRef.putFile(filePath)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    //if the upload is successfull
                                    //hiding the progress dialog
                                    progressDialog.dismiss();


                                    Task<Uri> downloadUri = taskSnapshot.getStorage().getDownloadUrl();

                                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            generatedFilePath = uri.toString();
                                        }
                                    });


                                    //and dis
                                    // playing a success toast
                                    Toast.makeText(getApplicationContext(), "image upload successfully ", Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    //if the upload is not successfull
                                    //hiding the progress dialog
                                    progressDialog.dismiss();

                                    //and displaying error message
                                    Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    //calculating progress percentage
                                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                    //displaying percentage in progress dialog
                                    progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                                }
                            });


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "ERROR Photo uploading", Toast.LENGTH_SHORT).show();
                }

            } else {
                if (resultCode == RESULT_OK) {
                    BankNameModel bankIINValue = (BankNameModel) data.getSerializableExtra(Constants.IIN_KEY);
                    cash_withdrawal_bankspinner.setText(bankIINValue.getBankName());
                    // balance_enquiry_nnid = bankIINValue.getIin();
                    //cash_withdrawal_nnid = "";
                    // checkBalanceEnquiryValidation();


                }
            }
        }

    }

    private void loadPath(StorageReference storageRef, UploadTask.TaskSnapshot taskSnapshot) {
        storageRef.child("users/me/profile.png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Toast.makeText(AddMoneyActivity.this, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                Task<Uri> downloadUri = taskSnapshot.getStorage().getDownloadUrl();

                if (downloadUri.isSuccessful()) {
                    String generatedFilePath = downloadUri.getResult().toString();
                    System.out.println("## Stored path is " + generatedFilePath);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }

    /* private void uploadFile(Uri filePath) {
        //checking if file is available
       final ProgressDialog progressdl =  new ProgressDialog(AddMoneyActivity.this);
        progressdl.setMessage("Uploading...");
        progressdl.setCancelable(false);
        progressdl.show();

        try{


            StorageReference storageReference = FirebaseStorage.getInstance().getReference();
            if (filePath != null) {
                //FirebaseStorage storage = FirebaseStorage.getInstance();
                //StorageReference storageRef = storage.getReferenceFromUrl("gs://isuaepsdev.appspot.com/");
                FirebaseStorage storage = FirebaseStorage.getInstance("gs://iserveu_storage/Balance Request");
                StorageReference storageRef = storage.getReference();

                //gs://iserveu_storage/Balance Request/demoisu/itpl

                // Create a reference to "file"
                final StorageReference mountainsRef = storageRef.child());
                //adding the file to reference
                mountainsRef.putFile(filePath)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                //dismissing the progress dialog
                                progressdl.dismiss();
                                //displaying success toast
                                Toast.makeText(AddMoneyActivity.this, "File Uploaded ", Toast.LENGTH_LONG).show();

                                //creating the upload object to store uploaded image details

                                mountainsRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        //Log.d(TAG, "onSuccess: uri= "+ uri.toString());
                                        //Toast.makeText(getActivity(), "Url "+uri.toString(), Toast.LENGTH_LONG).show();
                                        Constants.VERIFIED_USER_VIDEO = uri.toString();

                                    }
                                });
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                progressdl.dismiss();
                                Toast.makeText(AddMoneyActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                //displaying the upload progress
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                progressdl.setMessage("Uploaded " + ((int) progress) + "%...");
                            }
                        });
            } else {
                //display an error if no file is selected
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getApplicationContext().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void callRequestData(String amount, String bankRefId) {
        progressV.setVisibility(View.VISIBLE);
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v59")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            System.out.println(">>>>-----" + encodedUrl);
                            callEncriptedRequestData(encodedUrl, amount, bankRefId, progressV);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressV.setVisibility(View.GONE);
                    }
                });
    }

    private void callEncriptedRequestData(String encodedUrl, String amount, String bankRefId, final ProgressBar progressV) {

        JSONObject obj = new JSONObject();

        try {
            obj.put("senderName", userName);
            obj.put("senderBankName", cash_withdrawal_select_bank.getSelectedItem().toString());
            obj.put("senderAccountNo", "");
            obj.put("depositDate", date_new);
            obj.put("amount", amount);
            obj.put("transferType", transfertypeSpinner.getSelectedItem().toString());
            obj.put("bankRefId", bankRefId);
            obj.put("remarks", remark_txt.getText().toString());
            obj.put("receiptDownloadUrl", generatedFilePath);
            Log.e("TAG", "callEncriptedRequestData: "+obj );


            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("authorization", token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressV.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("status");

                                if (statusCode.equalsIgnoreCase("0")) {
                                    String statusDescription = obj.getString("statusDesc");
                                    startActivity(new Intent(AddMoneyActivity.this, MainActivity.class));
                                    Toast.makeText(AddMoneyActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(AddMoneyActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(AddMoneyActivity.this, anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                            progressV.setVisibility(View.GONE);
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }
}

