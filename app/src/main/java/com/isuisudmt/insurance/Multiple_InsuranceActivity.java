package com.isuisudmt.insurance;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;

import java.util.HashMap;

public class Multiple_InsuranceActivity extends AppCompatActivity {
    String _admin="",_token;
    CardView hospicash_CV;
    CardView one_lac_term_CV;
    String pennydropStatus;
    CardView sampoorna_CV;
    SessionManager session;
    CardView two_lac_term_CV;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView((int) R.layout.multiple_insurance_layout);
        this.sampoorna_CV =  findViewById(R.id.sampoorna_CV);
        this.one_lac_term_CV =  findViewById(R.id.one_lac_term_CV);
        this.two_lac_term_CV =  findViewById(R.id.two_lac_term_CV);
        this.hospicash_CV =  findViewById(R.id.hospicash_CV);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.insurance));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Multiple_InsuranceActivity.this.onBackPressed();
            }
        });
        this.session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = this.session.getUserDetails();
        this._token = (String) user.get(SessionManager.KEY_TOKEN);
        this._admin = (String) user.get(SessionManager.KEY_ADMIN);
        this.sampoorna_CV.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Multiple_InsuranceActivity multiple_InsuranceActivity = Multiple_InsuranceActivity.this;
                multiple_InsuranceActivity.startActivity(new Intent(multiple_InsuranceActivity, Insurance.class));
            }
        });
        this.one_lac_term_CV.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Multiple_InsuranceActivity multiple_InsuranceActivity = Multiple_InsuranceActivity.this;
                multiple_InsuranceActivity.startActivity(new Intent(multiple_InsuranceActivity, OneLakhINR_insuranceActivity.class));
            }
        });
        this.two_lac_term_CV.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Multiple_InsuranceActivity multiple_InsuranceActivity = Multiple_InsuranceActivity.this;
                multiple_InsuranceActivity.startActivity(new Intent(multiple_InsuranceActivity, TwoLakhINR_insuranceActivity.class));
            }
        });
        this.hospicash_CV.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(Multiple_InsuranceActivity.this, "Not available !", Toast.LENGTH_LONG).show();
            }
        });
    }
}
