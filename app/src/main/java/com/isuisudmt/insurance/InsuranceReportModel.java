package com.isuisudmt.insurance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsuranceReportModel {
    @SerializedName("amountTransacted")
    @Expose
    private Integer amountTransacted;
    @SerializedName("apiComment")
    @Expose
    private String apiComment;
    @SerializedName("applicationId")
    @Expose
    private String applicationId;
    @SerializedName("balanceAmount")
    @Expose
    private Object balanceAmount;
    @SerializedName("cardDetail")
    @Expose
    private String cardDetail;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;
    @SerializedName("Id")
    @Expose

    /* renamed from: id */
    private String f317id;
    @SerializedName("masterName")
    @Expose
    private String masterName;
    @SerializedName("operationPerformed")
    @Expose
    private String operationPerformed;
    @SerializedName("previousAmount")
    @Expose
    private Object previousAmount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;
    @SerializedName("userName")
    @Expose
    private String userName;

    public String getId() {
        return this.f317id;
    }

    public void setId(String id) {
        this.f317id = id;
    }

    public Object getPreviousAmount() {
        return this.previousAmount;
    }

    public void setPreviousAmount(Object previousAmount2) {
        this.previousAmount = previousAmount2;
    }

    public Integer getAmountTransacted() {
        return this.amountTransacted;
    }

    public void setAmountTransacted(Integer amountTransacted2) {
        this.amountTransacted = amountTransacted2;
    }

    public Object getBalanceAmount() {
        return this.balanceAmount;
    }

    public void setBalanceAmount(Object balanceAmount2) {
        this.balanceAmount = balanceAmount2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(String transactionType2) {
        this.transactionType = transactionType2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public String getDistributerName() {
        return this.distributerName;
    }

    public void setDistributerName(String distributerName2) {
        this.distributerName = distributerName2;
    }

    public String getMasterName() {
        return this.masterName;
    }

    public void setMasterName(String masterName2) {
        this.masterName = masterName2;
    }

    public String getCardDetail() {
        return this.cardDetail;
    }

    public void setCardDetail(String cardDetail2) {
        this.cardDetail = cardDetail2;
    }

    public String getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(String createdDate2) {
        this.createdDate = createdDate2;
    }

    public String getUpdatedDate() {
        return this.updatedDate;
    }

    public void setUpdatedDate(String updatedDate2) {
        this.updatedDate = updatedDate2;
    }

    public String getOperationPerformed() {
        return this.operationPerformed;
    }

    public void setOperationPerformed(String operationPerformed2) {
        this.operationPerformed = operationPerformed2;
    }

    public String getApiComment() {
        return this.apiComment;
    }

    public void setApiComment(String apiComment2) {
        this.apiComment = apiComment2;
    }

    public String getApplicationId() {
        return this.applicationId;
    }

    public void setApplicationId(String applicationId2) {
        this.applicationId = applicationId2;
    }
}
