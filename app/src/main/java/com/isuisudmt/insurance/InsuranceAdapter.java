package com.isuisudmt.insurance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.isuisudmt.R;

import java.util.ArrayList;
import java.util.HashMap;

public class InsuranceAdapter extends ArrayAdapter {

    private Context context;
    private int resource, textViewResourceId;
    ArrayList<HashMap<String, String>> items, tempItems, suggestions;

    public InsuranceAdapter(Context context, int resource, int textViewResourceId, ArrayList<HashMap<String, String>> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;

        tempItems = new ArrayList<HashMap<String, String>>(items);
        suggestions = new ArrayList<HashMap<String, String>>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.searchlayout, parent, false);
        }

        HashMap<String, String> people = items.get(position);
        TextView name = view.findViewById(R.id.name);

        name.setText(people.get("district") + ", " + people.get("subdistrict") + ", " + people.get("village"));
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }


    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((HashMap<String, String>) resultValue).get("subdistrict");
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            if (constraint != null) {
                suggestions.clear();

                for (HashMap<String, String> search_area : tempItems) {
                    if (search_area.get("subdistrict").toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(search_area);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;

            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<HashMap<String, String>> filterList = (ArrayList<HashMap<String, String>>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (HashMap<String, String> search_area : filterList) {
                    add(search_area);
                    notifyDataSetChanged();
                }
            }
        }
    };
}