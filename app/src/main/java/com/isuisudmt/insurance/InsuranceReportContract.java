package com.isuisudmt.insurance;

import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.matm.RefreshModel;
import com.isuisudmt.report.adapter.AdapterCustomReport;

import java.util.ArrayList;

public class InsuranceReportContract {

    interface UserActionsListener {
        void loadReports(String str, String str2, String str3, String str4, String str5, ArrayList<InsuranceReportModel> arrayList);

        void refreshReports(String str, String str2, String str3, String str4, String str5);
    }

    public interface View {
        void emptyDates();

        void hideLoader();

        void refreshAdapter(RecyclerView recyclerView, AdapterCustomReport adapterCustomReport);

        void refreshDone(RefreshModel refreshModel);

        void reportsReady(ArrayList<InsuranceReportModel> arrayList, String str);

        void showLoader();

        void showReports();
    }
}
