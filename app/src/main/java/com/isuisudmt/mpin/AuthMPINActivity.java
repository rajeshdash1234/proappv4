package com.isuisudmt.mpin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.login.LoginActivity;
import com.matm.matmsdk.Utils.SdkConstants;

import org.json.JSONException;
import org.json.JSONObject;

import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;


public class AuthMPINActivity extends AppCompatActivity {
    private static final String TAG = AuthMPINActivity.class.getSimpleName();

    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    public static final String USER_MPIN = "mpinKey";
    boolean own, firstResend = true;
    int counter, allowed = 30;
    String userName, mpin;

    ProgressDialog dialog;
   // OtpTextView otpTextView;
    TextView generate;
    Button verify;
    TextInputLayout mpinLayout;
    EditText[] mpinET;
    String mPin;
    String token,mPinValue;
    TextView resend;
    private FirebaseAnalytics firebaseAnalytics;
    private OtpTextView otpTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_auth_mpin);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        otpTextView = findViewById(R.id.otp_view);
        userName = sp.getString(USER_NAME, "");
        mpin = sp.getString(USER_MPIN, "");

        Toolbar toolbar = findViewById(R.id.mpin_toolbar);
        toolbar.setTitle("Enter MPIN");
        setSupportActionBar(toolbar);
       // toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.ic_back));

        token = getIntent().getStringExtra("token");
        resend = findViewById(R.id.login_resend);


        generate = findViewById(R.id.mpin_generate);
        verify = findViewById(R.id.mpin_verify);
        mpinLayout = findViewById(R.id.mpin_layout);
//        mpinET = findViewById(R.id.mpin);

       //  mpinET = new EditText[]{editTextOne, editTextTwo, editTextThree, editTextFour, editTextFive, editTextSix};

//        mpinET = findViewById(R.id.mpin);


        otpTextView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {
                // fired when user types something in the Otpbox
            }
            @Override
            public void onOTPComplete(String otp) {
                // fired when user has entered the OTP fully.
//                Toast.makeText(AuthMPINActivity.this, "The OTP is " + otp,  Toast.LENGTH_SHORT).show();
            }
        });


        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 mPinValue = otpTextView.getOTP().toString();
                if (mPinValue.length() == 0) {
                    Toast.makeText(AuthMPINActivity.this, "Enter MPIN", Toast.LENGTH_SHORT).show();
                    mpinLayout.setError("Enter MPIN");
                } else {
                    setFBAnalytics("VERIFY_MPIN", userName);
                    loginWithMpin(userName, mPinValue);
                }
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFBAnalytics("REGENERATE_MPIN", userName);
                generateMpin(userName);
            }
        });


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(AuthMPINActivity.this, LoginActivity.class);
                startActivity(in);
                finish();
            }
        });

        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    generateMpin(userName);
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(AuthMPINActivity.this, "OOPS!! , Something Went Wrong. Please try again", Toast.LENGTH_LONG).show();
                }

                setFBAnalytics("GENERATE_MPIN", userName);
//                generateMpin(userName);
            }
        });
    }

    private void generateMpin(String un) {
        dialog = new ProgressDialog(AuthMPINActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/user_mpin/generate_mpin";
        JSONObject obj = new JSONObject();

        try {
            obj.put("token", token);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");
                                if (status == 1) {
                                    timer();
                                    generate.setVisibility(View.GONE);
                                    Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                                }
                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e(TAG, "onError: ANError " + anError.getErrorBody());
                            try {
                                JSONObject errorObject = new JSONObject(anError.getErrorBody());
                                String message = errorObject.getString("message");
                                Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void loginWithMpin(String uN, String pin) {

        dialog = new ProgressDialog(AuthMPINActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/user_mpin/login";
        JSONObject obj = new JSONObject();

        try {
            obj.put("token", token);
            obj.put("m_pin", pin);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");

                                if (status == 1) {
                                    //load main activity
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(USER_MPIN, pin);
                                    editor.apply();

                                    Intent i = new Intent(AuthMPINActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            try {
                                JSONObject errorObject = new JSONObject(anError.getErrorBody());
                                int status = errorObject.getInt("status");
                                if (status == 0) {
                                    String message = "MPIN has been changed.\nEnter the correct one or regenerate.";
                                    Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                                } else if (status == -1) {
                                    String message = "MPIN has been expired.\nRegenrated the MPIN.";
                                    Toast.makeText(AuthMPINActivity.this, message, Toast.LENGTH_LONG).show();
                                }

                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(USER_MPIN, "");
                                editor.apply();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void timer() {
        if (firstResend) {
            firstResend = false;
            resend.setVisibility(View.VISIBLE);
        }

        resend.setEnabled(false);
        resend.setTextColor(getResources().getColor(R.color.black));
        resend.setAlpha(0.5f);
        counter = 0;

        new CountDownTimer(allowed * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int time = allowed - counter;
                resend.setText("Resend OTP after " + time + " seconds");
                counter++;
            }

            @Override
            public void onFinish() {
                resend.setEnabled(true);
                resend.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                resend.setAlpha(1.0f);
                resend.setText("Resend OTP");
            }
        }.start();
    }


    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();


            showExitDialog();



    }

    private void showExitDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(AuthMPINActivity.this);
        builder1.setMessage("Do you want to exit from this app");
        builder1.setTitle("Exit");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        SdkConstants.LogOut = "0"; //For logout from SDK end
                        SdkConstants.BlueToothPairFlag = "0";

                        dialog.cancel();
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}