package com.isuisudmt.prepaid;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.aeps.ReportActivity;
import com.isuisudmt.bbps.SavePDF.PermissionsActivity;
import com.isuisudmt.bbps.SavePDF.PermissionsChecker;
import com.isuisudmt.bbps.SavePDF.PreviewPDFActivity;
import com.isuisudmt.bbps.utils.FileUtils;
import com.isuisudmt.bbps.utils.Operator_model;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.dmtModule.bluetooth.BluetoothPrinter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.isuisudmt.bbps.SavePDF.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.isuisudmt.bbps.SavePDF.PermissionsChecker.REQUIRED_PERMISSION;

public class ActivityReceiptPrepaid extends AppCompatActivity {
    String Response = "";
    String msg = "", txnRefId = "", refId = "", customerName = "", billNumber = "", billPeriod = "", billDate = "", dueDate = "", baseBillAmount = "", agntRefId = "", appRefNumber = "", code = "", amount = "";

    ImageView iv_status_icon;
    TextView tv_status_msg, tv_status_desc, tv_trans_id, tv_agnt_ref_id, tv_ref_id, tv_customer_name, tv_bill_amount, tv_bill_period, tv_bill_date, tv_due_date, tv_base_bill_amount, tv_app_ref_no;
    LinearLayout ll_main_bbps_receipt, ll_trans_id, ll_agnt_ref_id, ll_ref_id, ll_customer_name, ll_bill_number, ll_bill_period, ll_bill_date, ll_due_date, ll_base_bill_amount, ll_app_ref_no;
    Button btnOK, btnSavePDF, btnPrint;

    BluetoothAdapter B;
    PermissionsChecker checker;
    Context mContext;
    private static final int REQUEST_CA_PERMISSIONS = 931;
    private static final int REQUEST_WRITE_PERMISSION = 1001;
    private static final String TAG = ReportActivity.class.getSimpleName ();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_bbps2);

        init();

        Intent intent = getIntent();
        Response = intent.getStringExtra("Response");

        Operator_model operator_model = new Operator_model();

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityReceiptPrepaid.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SdkConstants.BRAND_NAME.trim().length() != 0) {
                    BluetoothDevice bluetoothDevice = SdkConstants.bluetoothDevice;
                    if (bluetoothDevice != null) {

                        if (!B.isEnabled()) {
                           /* Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(turnOn, 0);*/
                            finish();
                            Toast.makeText(getApplicationContext(), "Your Bluetooth is OFF .", Toast.LENGTH_LONG).show();
                        } else {
//                            Toast.makeText(getApplicationContext(), "Bluetooth is already on", Toast.LENGTH_LONG).show();
                            callBluetoothFunction(txnRefId, refId, customerName, billNumber, billPeriod, billDate, dueDate, baseBillAmount, agntRefId, appRefNumber, bluetoothDevice);
                        }
                    } else {
                        finish();
                        Toast.makeText(ActivityReceiptPrepaid.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showBrandSetAlert();
                }
            }
        });

        btnSavePDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                }

                if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                    PermissionsActivity.startActivityForResult(ActivityReceiptPrepaid.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                } else {
                    Date date = new Date();
                    long timeMilli = date.getTime();
                    System.out.println("Time in milliseconds using Date class: " + String.valueOf(timeMilli));
                    createPdf(FileUtils.getAppPath(mContext) + String.valueOf(timeMilli) + "Order_Receipt.pdf");
                }

            }
        });

        //Demo Failure
        //Response = "{\"commonClassObject\":{\"refId\":\"81C002A27077Y4A6EYBBFAYC58106EA869E\",\"agentRefid\":\"ZK9ZVNYRM6Z4W9XQC\",\"msg\":\"Unable to get bill details from biller\",\"code\":\"INRX001\"},\"statusCode\":\"-1\",\"statusDescription\":\"Unable to get bill details from biller\"}";

        //Demo Success
       /* Response = "{\n" +
                "    \"STATUSCODE\": \"0\",\n" +
                "    \"SERCODE\": \"Bill payment success.\"\n" +
                "}";*/


     /*   {
            "STATUSCODE": "2",
                "STATUSMSG": "Duplicate Transaction, Try after 60 minutes",
                "REFNO": "778594900613537792",
                "TRNID": 0,
                "TRNSTATUS": 3,
                "TRNSTATUSDESC": "Duplicate Transaction, Try after 60 minutes",
                "OPRID": "",
                "SERCODE": "RJ",
                "AMOUNT": "10"
        }*/



        try {
            JSONObject obj = new JSONObject(Response);
            String statusCode = obj.getString("status");

            if (statusCode.equals("0")) {
              //  ll_main_bbps_receipt.setBackgroundColor(getResources().getColor(R.color.receipt_success));
              //  iv_status_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_exclamation));

                //Display the receipt
                msg = obj.optString("TRNSTATUSDESC");
                txnRefId = obj.optString("TRNID");
                refId = obj.optString("REFNO");
                code =  obj.optString("SERCODE");
                baseBillAmount = obj.optString("AMOUNT");
                Log.d(TAG,"obj"+obj);

                tv_trans_id.setText("TxnId : " +txnRefId);
                tv_ref_id.setText("RefId : " +refId);
                tv_bill_amount.setText("Rs : " +baseBillAmount);


               /* if (!msg.equals("")) {
                    tv_status_msg.setText("Processing...");
                }*/

                if(code.equalsIgnoreCase("AR")){
                    tv_customer_name.setText("AIRTEL");
                   // checkPaySuccessResponse(customerName, ll_customer_name, tv_customer_name);
                }
                else if(code.equalsIgnoreCase("BS")) {
                    tv_customer_name.setText("BSNL");
                    //checkPaySuccessResponse(customerName, ll_customer_name, tv_customer_name);
                }
                else if(code.equalsIgnoreCase("ID")) {
                    tv_customer_name.setText("IDEA");
                    //checkPaySuccessResponse(customerName, ll_customer_name, tv_customer_name);
                }
                else if(code.equalsIgnoreCase("VF")) {
                    tv_customer_name.setText("VODAFONE");
                   // checkPaySuccessResponse(customerName, ll_customer_name, tv_customer_name);
                }
                else if(code.equalsIgnoreCase("RJ")) {
                    tv_customer_name.setText("RELIANCE JIO");
                   // checkPaySuccessResponse(customerName, ll_customer_name, tv_customer_name);
                }
                else {
                    Toast.makeText(ActivityReceiptPrepaid.this, "no code found", Toast.LENGTH_LONG).show();
                }

               /* customerName = obj.optString("CustomerName");
                billNumber = obj.optString("BillNumber");
                billPeriod = obj.optString("BillPeriod");
                billDate = obj.optString("BillDate");
                dueDate = obj.optString("DueDate");
                baseBillAmount = obj.optString("baseBillAmount");
                agntRefId = obj.optString("agentRefid");
                appRefNumber = obj.optString("AppRefNumber");

                String code = obj.optString("code");
                String customerMobile = obj.optString("customerMobile");
                String dateAndTime = obj.optString("dateAndTime");
                String BASE_BILL_AMOUNT = obj.optString("BASE_BILL_AMOUNT");
                String totalAmount = obj.optString("totalAmount");
                String ccf = obj.optString("ccf");*/

                SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
              //  Date newBillDate = spf.parse(billDate);
             //   Date newDueDate = spf.parse(dueDate);

                spf = new SimpleDateFormat("dd-MM-yyyy");
              //  billDate = spf.format(newBillDate);
             //   dueDate = spf.format(newDueDate);


              /*  if (!statusDescription.equals(""))
                    tv_status_desc.setText(statusDescription);
                else
                    tv_status_desc.setText("Your transaction has been processed successfully.");*/


               /* checkPaySuccessResponse(txnRefId, ll_trans_id, tv_trans_id);
                checkPaySuccessResponse(agntRefId, ll_agnt_ref_id, tv_agnt_ref_id);
                checkPaySuccessResponse(refId, ll_ref_id, tv_ref_id);
                //checkPaySuccessResponse(customerName, ll_customer_name, tv_customer_name);
                checkPaySuccessResponse(billNumber, ll_bill_number, tv_bill_number);
                checkPaySuccessResponse(billPeriod, ll_bill_period, tv_bill_period);
                checkPaySuccessResponse(baseBillAmount, ll_base_bill_amount, tv_base_bill_amount);
                checkPaySuccessResponse(appRefNumber, ll_app_ref_no, tv_app_ref_no);
                checkPaySuccessResponse(billDate, ll_bill_date, tv_bill_date);
                checkPaySuccessResponse(dueDate, ll_due_date, tv_due_date);*/


            } else if (statusCode.equals("-1")) {
                ll_main_bbps_receipt.setBackgroundColor(getResources().getColor(R.color.receipt_failure));
                //iv_status_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_uncheck));


                //Display the receipt
                String msg = obj.optString("TRNSTATUSDESC");
                String txnRefId = obj.optString("TRNID");
                String refId = obj.optString("REFNO");

               /* String customerName = obj.optString("CustomerName");
                String billNumber = obj.optString("BillNumber");
                String billPeriod = obj.optString("BillPeriod");
                String billDate = obj.optString("BillDate");
                String dueDate = obj.optString("DueDate");
                String baseBillAmount = obj.optString("baseBillAmount");
                String agntRefId = obj.optString("agentRefid");
                String appRefNumber = obj.optString("AppRefNumber");

                String customerMobile = obj.optString("customerMobile");
                String dateAndTime = obj.optString("dateAndTime");
                String BASE_BILL_AMOUNT = obj.optString("BASE_BILL_AMOUNT");
                String totalAmount = obj.optString("totalAmount");
                String ccf = obj.optString("ccf");*/


             /*   if (!msg.equals("")) {
                    tv_status_msg.setText("Failed");
                }

                tv_status_desc.setText(statusDescription);*/


                /*checkPaySuccessResponse(refId, ll_ref_id, tv_ref_id);
                checkPaySuccessResponse(agntRefId, ll_agnt_ref_id, tv_agnt_ref_id);

                checkPaySuccessResponse(txnRefId, ll_trans_id, tv_trans_id);
                checkPaySuccessResponse(customerName, ll_customer_name, tv_customer_name);
                checkPaySuccessResponse(billNumber, ll_bill_number, tv_bill_number);
                checkPaySuccessResponse(billPeriod, ll_bill_period, tv_bill_period);
                checkPaySuccessResponse(baseBillAmount, ll_base_bill_amount, tv_base_bill_amount);
                checkPaySuccessResponse(appRefNumber, ll_app_ref_no, tv_app_ref_no);*/


                SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
                Date newBillDate = spf.parse(billDate);
                Date newDueDate = spf.parse(dueDate);

                spf = new SimpleDateFormat("dd-MM-yyyy");
                billDate = spf.format(newBillDate);
                dueDate = spf.format(newDueDate);


              //  checkPaySuccessResponse(billDate, ll_bill_date, tv_bill_date);
               // checkPaySuccessResponse(dueDate, ll_due_date, tv_due_date);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void setOperator(String code, String operator) {

        Operator_model operator_model = new Operator_model();
        operator_model.setCode(code);
        operator_model.setName(operator);

    }

    private void init() {

        B = BluetoothAdapter.getDefaultAdapter();
        //Runtime permission request required if Android permission >= Marshmallow
        checker = new PermissionsChecker(this);
        mContext = getApplicationContext();

        //iv_status_icon = findViewById(R.id.iv_status_icon);
        btnOK = findViewById(R.id.closeBtn);
        btnSavePDF = findViewById(R.id.btnSavePdf);
        btnPrint = findViewById(R.id.btnPrint);

      /*  ll_main_bbps_receipt = findViewById(R.id.ll_main_bbps_receipt);
        ll_trans_id = findViewById(R.id.ll_trans_id);
        ll_ref_id = findViewById(R.id.ll_ref_id);
        ll_customer_name = findViewById(R.id.ll_customer_name);
        ll_bill_number = findViewById(R.id.ll_bill_number);*/
      /*  ll_bill_period = findViewById(R.id.ll_bill_period);
        ll_bill_date = findViewById(R.id.ll_bill_date);
        ll_due_date = findViewById(R.id.ll_due_date);
        ll_base_bill_amount = findViewById(R.id.ll_base_bill_amount);
        ll_agnt_ref_id = findViewById(R.id.ll_agnt_ref_id);
        ll_app_ref_no = findViewById(R.id.ll_app_ref_no);*/


       // tv_status_msg = findViewById(R.id.tv_status_msg);
        tv_status_desc = findViewById(R.id.tv_status_desc);
        tv_trans_id = findViewById(R.id.tv_trans_id);
        tv_ref_id = findViewById(R.id.tv_ref_id);
        tv_customer_name = findViewById(R.id.tv_operator);
        tv_bill_amount = findViewById(R.id.tv_bill_amount);
        tv_bill_period = findViewById(R.id.tv_bill_period);
        tv_bill_date = findViewById(R.id.tv_bill_date);
      //  tv_due_date = findViewById(R.id.tv_due_date);
        tv_base_bill_amount = findViewById(R.id.tv_base_bill_amount);
        tv_agnt_ref_id = findViewById(R.id.tv_agnt_ref_id);
       // tv_app_ref_no = findViewById(R.id.tv_app_ref_no);

    }

 /*   private void checkPaySuccessResponse(String response, LinearLayout linearLayout, TextView textView) {
        if (response.equals(""))
            linearLayout.setVisibility(View.GONE);
        else {
            linearLayout.setVisibility(View.VISIBLE);
            textView.setText(response);
        }

    }*/

    private void callBluetoothFunction(final String txnId, final String refId, final String customerName, final String billNumber, final String billPeriod, final String billDate, final String dueDate, final String baseBillAmount, final String agntRefId, final String appRefNumber, BluetoothDevice bluetoothDevice) {

        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {
            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(SdkConstants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.printText("-----Transaction Report-----");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(tv_status_msg.getText().toString());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                if (!txnId.equals("")) {
                    mPrinter.printText("TXNId: " + txnId);
                    mPrinter.addNewLine();
                }
                if (!refId.equals("")) {
                    mPrinter.printText("Ref ID: " + refId);
                    mPrinter.addNewLine();
                }
                if (!customerName.equals("")) {
                    mPrinter.printText("Customer Name: " + customerName);
                    mPrinter.addNewLine();
                }
                if (!billNumber.equals("")) {
                    mPrinter.printText("Bill Number: " + billNumber);
                    mPrinter.addNewLine();
                }
                if (!billPeriod.equals("")) {
                    mPrinter.printText("Bill Period: " + billPeriod);
                    mPrinter.addNewLine();
                }
                if (!billDate.equals("")) {
                    mPrinter.printText("Bill Date: " + billDate);
                    mPrinter.addNewLine();
                }
                if (!dueDate.equals("")) {
                    mPrinter.printText("Due Date: " + dueDate);
                    mPrinter.addNewLine();
                }
                if (!baseBillAmount.equals("")) {
                    mPrinter.printText("Bill Amount: " + baseBillAmount);
                    mPrinter.addNewLine();
                }
                if (!agntRefId.equals("")) {
                    mPrinter.printText("Agent Ref Id: " + agntRefId);
                    mPrinter.addNewLine();
                }
                if (!appRefNumber.equals("")) {
                    mPrinter.printText("App Ref Number: " + appRefNumber);
                    mPrinter.addNewLine();
                }
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(SdkConstants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");
//                finish();
                Toast.makeText(ActivityReceiptPrepaid.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showBrandSetAlert() {
        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(ActivityReceiptPrepaid.this);
            builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
            builder1.setTitle("Warning!!!");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "GOT IT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } catch (Exception e) {
        }
    }

    public void createPdf(String dest) {
        if (new File(dest).exists()) {
            new File(dest).delete();
        }
        try {
            /**
             * Creating Document
             */
            Document document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(dest));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("");
            document.addCreator("");
            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));
            BaseFont bf = BaseFont.createFont(
                    BaseFont.TIMES_ROMAN,
                    BaseFont.CP1252,
                    BaseFont.EMBEDDED);
            Font font = new Font(bf, 30);
            Font font2 = new Font(bf, 26);
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell(new Paragraph("Transaction Receipt", font));
            cell.setColspan(2); // colspan
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            if (!txnRefId.equals("")) {
                table.addCell(new Paragraph("Transaction ID", font)); // how to change cell to have different font and bold and background color
                table.addCell(new Paragraph(txnRefId, font2)); // how to change cell to have different font and bold and background color
            }
            if (!refId.equals("")) {
                table.addCell(new Paragraph("Ref ID", font));
                table.addCell(new Paragraph(refId, font2));
            }
            if (!customerName.equals("")) {
                table.addCell(new Paragraph("Customer Name", font));
                table.addCell(new Paragraph(customerName, font2));
            }
            if (!billNumber.equals("")) {
                table.addCell(new Paragraph("Bill Number", font));
                table.addCell(new Paragraph(billNumber, font2));
            }
            if (!billPeriod.equals("")) {
                table.addCell(new Paragraph("Bill Period", font));
                table.addCell(new Paragraph(billPeriod, font2));
            }
            if (!billDate.equals("")) {
                table.addCell(new Paragraph("Bill Date", font));
                table.addCell(new Paragraph(billDate, font2));
            }
            if (!dueDate.equals("")) {
                table.addCell(new Paragraph("Due Date", font));
                table.addCell(new Paragraph(dueDate, font2));
            }
            if (!baseBillAmount.equals("")) {
                table.addCell(new Paragraph("Bill Amount", font));
                table.addCell(new Paragraph(baseBillAmount, font2));
            }
            if (!agntRefId.equals("")) {
                table.addCell(new Paragraph("Agnt Ref Id", font));
                table.addCell(new Paragraph(agntRefId, font2));
            }
            if (!appRefNumber.equals("")) {
                table.addCell(new Paragraph("App Ref Number", font));
                table.addCell(new Paragraph(appRefNumber, font2));
            }
            document.add(table);


            // Title Order Details...
            // Adding Title....
            Font mOrderDetailsTitleFont;
            if (tv_status_msg.getText().toString().equalsIgnoreCase("FAILED")) {
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.RED);
            } else {
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.GREEN);
            }
            Chunk mOrderDetailsTitleChunk = new Chunk(tv_status_msg.getText().toString(), mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);
            document.close();
            Toast.makeText(mContext, "PDF saved in the internal storage", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ActivityReceiptPrepaid.this, PreviewPDFActivity.class);
            intent.putExtra("filePath", dest);
            startActivity(intent);
//            showPdf(dest);
//            FileUtils.openFile(mContext, new File(dest));
        } catch (IOException | DocumentException ie) {
            Log.e("createPdf: Error ", "" + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == PermissionsActivity.PERMISSIONS_GRANTED) {
            Toast.makeText(mContext, "Permission Granted to Save", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Permission not granted, Try again!", Toast.LENGTH_SHORT).show();
        }
    }
}
