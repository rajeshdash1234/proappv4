package com.isuisudmt;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.transition.TransitionManager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.QRWeb.QRWebActivity;
import com.isuisudmt.aeps.ReportActivity;
import com.isuisudmt.bbps.ActivityBBPS;
import com.isuisudmt.bbps.ActivityaElectricityBill;
import com.isuisudmt.bbps.BBPSDashboardModel;
import com.isuisudmt.bbps.BBPSmvp.BbpsDashboardContract;
import com.isuisudmt.bbps.BBPSmvp.BbpsDashboardPresenter;
import com.isuisudmt.bbps.BbpsDashboardAdapter;
import com.isuisudmt.bbps.utils.Const;
import com.isuisudmt.bluetooth.SharePreferenceClass;
import com.isuisudmt.cms.CMSActivity;
import com.isuisudmt.configuration.OnboardValidate;
import com.isuisudmt.fragment.AepsbFragment;
import com.isuisudmt.fragment.DmtbFragment;
import com.isuisudmt.fragment.MatmbFragment;
import com.isuisudmt.insurance.Multiple_InsuranceActivity;
import com.isuisudmt.login.LoginActivity;
import com.isuisudmt.password.ChangePasswordActivity;
import com.isuisudmt.prepaid.ActivitybMobilePrepaid;
import com.isuisudmt.report.activity.ActivityReportDashboardNew;
import com.isuisudmt.settings.SettingsNewActivity;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.notification.service.SubscribeGlobal;
import com.neonankiti.android.support.design.widget.FlexibleBottomNavigationView;
import com.salesforce.android.chat.core.ChatConfiguration;
import com.salesforce.android.chat.core.model.ChatEntity;
import com.salesforce.android.chat.core.model.ChatEntityField;
import com.salesforce.android.chat.core.model.ChatUserData;
import com.salesforce.android.chat.ui.ChatUI;
import com.salesforce.android.chat.ui.ChatUIClient;
import com.salesforce.android.chat.ui.ChatUIConfiguration;
import com.salesforce.android.service.common.utilities.control.Async;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Unbinder;

import static com.isuisudmt.BuildConfig.GET_USER_ROLE;
import static com.isuisudmt.bbps.utils.Const.BROADBAND_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.CABLE_TV;
import static com.isuisudmt.bbps.utils.Const.DTH;
import static com.isuisudmt.bbps.utils.Const.EDUCATION_FEES;
import static com.isuisudmt.bbps.utils.Const.ELECTRICITY;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS1;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS2;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_AEPS3;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_BBPS;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_CASHOUT;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_CASHOUT_WALLET1;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_CASHOUT_WALLET2;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_DMT;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_INSURANCE;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_MATM;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_MATM1;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_MATM2;
import static com.isuisudmt.bbps.utils.Const.ERROR_MESSAGE_DISABLED_RECHARGE;
import static com.isuisudmt.bbps.utils.Const.FASTAG;
import static com.isuisudmt.bbps.utils.Const.GAS;
import static com.isuisudmt.bbps.utils.Const.HEALTH_INSURANCE;
import static com.isuisudmt.bbps.utils.Const.HOSPITAL;
import static com.isuisudmt.bbps.utils.Const.HOUSING_SOCIETY;
import static com.isuisudmt.bbps.utils.Const.ID_AEPS1;
import static com.isuisudmt.bbps.utils.Const.ID_AEPS2;
import static com.isuisudmt.bbps.utils.Const.ID_AEPS3;
import static com.isuisudmt.bbps.utils.Const.ID_BBPS;
import static com.isuisudmt.bbps.utils.Const.ID_CASHOUT_WALLET1;
import static com.isuisudmt.bbps.utils.Const.ID_CASHOUT_WALLET2;
import static com.isuisudmt.bbps.utils.Const.ID_DMT;
import static com.isuisudmt.bbps.utils.Const.ID_INSURANCE;
import static com.isuisudmt.bbps.utils.Const.ID_MATM1;
import static com.isuisudmt.bbps.utils.Const.ID_MATM2;
import static com.isuisudmt.bbps.utils.Const.ID_RECHARGE;
import static com.isuisudmt.bbps.utils.Const.INSURANCE;
import static com.isuisudmt.bbps.utils.Const.LANDLINE_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.LIFE_INSURANCE;
import static com.isuisudmt.bbps.utils.Const.LOAN_REPAYMENT;
import static com.isuisudmt.bbps.utils.Const.LPG_GAS;
import static com.isuisudmt.bbps.utils.Const.MOBILE_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.MUNCIPAL_TAX;
import static com.isuisudmt.bbps.utils.Const.MUNICIPAL_SERVICES;
import static com.isuisudmt.bbps.utils.Const.SUBSCRIPTION;
import static com.isuisudmt.bbps.utils.Const.WATER;
import static com.isuisudmt.bbps.utils.Const.isAEPS1enabled;
import static com.isuisudmt.bbps.utils.Const.isAEPS2enabled;
import static com.isuisudmt.bbps.utils.Const.isAEPS3enabled;
import static com.isuisudmt.bbps.utils.Const.isBBPSenabled;
import static com.isuisudmt.bbps.utils.Const.isCashoutWallet1enabled;
import static com.isuisudmt.bbps.utils.Const.isCashoutWallet2enabled;
import static com.isuisudmt.bbps.utils.Const.isDMTenabled;
import static com.isuisudmt.bbps.utils.Const.isInsuranceEnabled;
import static com.isuisudmt.bbps.utils.Const.isMATM1enabled;
import static com.isuisudmt.bbps.utils.Const.isMATM2enabled;
import static com.isuisudmt.bbps.utils.Const.isRechargeEnabled;
import static com.isuisudmt.utils.Constants.BBPS_PREF;
import static com.isuisudmt.utils.Constants.SF_AGENT_ID;
import static com.isuisudmt.utils.Constants.SF_KEYWORD;
import static com.isuisudmt.utils.Constants.SF_LAT_LONG;
import static com.isuisudmt.utils.Constants.SF_MOBILE_NUMBER;
import static com.isuisudmt.utils.Constants.SF_PINCODE;
import static com.isuisudmt.utils.Constants.SF_TERMINAL_ID;


public class MainActivity extends AppCompatActivity implements HomeContract.View, BbpsDashboardContract.View, NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener, FlexibleBottomNavigationView.OnNavigationItemSelectedListener {

    LinearLayout tabLinear1, tabLinear2, tabLinear3, tabLinear4, tabLinear5, tabLinear6, tabLinear7, tabLinear8, tabLinear9, tabLinear10, tabLinear11, tabLinear12;
    ImageView tab_icon1, tab_icon2, tab_icon3, tab_icon4, tab_icon5, tab_icon6, tab_icon7, tab_icon8, tab_icon9, tab_icon10, tab_icon11, tab_icon12;
    LinearLayout bbps_Modules, recharge;
    LinearLayout bbpsLinear1;
    ImageView bbps_icon1;
    ImageView iv_nav_theme_dropdown;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    private Menu menu;
    private CollapsingToolbarLayout toolbar_layout;
    private TextView name, last_seen;
    TextView nav_user_name, das_user_balance,
            das_user_balance2, tv_showqr, tv_cashout, tv_settings, tv_help,
               tv_language, tv_privacy, tv_change_password, tv_logout, tv_nav_add_money1, tv_nav_add_money2;
    ImageView menu_nav, con_notification;

    String alertText = "Thank you for showing interest in our product, Our team will get back to you soon!";
    String home = "HOME";

    RelativeLayout rl_nav_aeps, rl_nav_matm, rl_nav_cashout, rl_nav_wallet,
            rl_nav_commission, rl_nav_Theme, rl_nav_add_money, rl_nav_view_more;

    LinearLayout ll_nav_home, ll_nav_dmt, ll_nav_recharge, ll_nav_bbps, ll_nav_upi, ll_nav_insurance, ll_nav_Settlement, collpase_toolbar, bottomSheet;
    ImageView iv_nav_AEPS_dropdown, iv_nav_mATM_dropdown, iv_nav_cash_out_dropdown, iv_nav_wallet_dropdown, iv_nab_commission_dropdown, iv_nav_view_more;
    ImageView iv_nav_aeps_dropdown, iv_nav_matm_dropdown, iv_nav_cashout_dropdown, iv_nav_commision_dropdown, iv_nav_add_money_dropdown;

    SessionManager session;
    HomePresenter homePresenter;

    String _token = "", _admin = "";

    DrawerLayout drawer;
    NavigationView navigationView;

    String deviceSerialNumber = "0";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";


    BottomNavigationView navigation;
    TextView reload, reload2;
    ProgressBar progressBar, progressBar2;
    String _userName = "", image_url = "", _adminName = "";
    Button addMoney;

    Handler timer_handler;
    ProgressDialog pd;
    boolean session_logout = false;
    public String balance1, balance2;
    boolean bbpsclick = true;

    Boolean isExpandedAEPS = false;
    Boolean isExpandedMATM = false;
    Boolean isExpandedCASHOUT = false;
    Boolean isExpandedWALLET = false;
    Boolean isExpandedCOMMISION = false;
    Boolean isExpandedTheme = false;
    Boolean isExpandedaddMoney = false;

    Boolean isViewMore = false;
    String _is_upi_settle1, _userRequest;

    TextView tv_nav_aeps1, tv_nav_aeps2, tv_nav_aeps3, tv_nav_matm1, tv_nav_matm2,
            tv_nav_cashout1, tv_nav_cashout2, tv_nav_wallet1, tv_nav_wallet2,
            tv_nav_commision1, tv_nav_commision2, tv_viewmore_viewless,tv_upi_settlement;

    SharePreferenceClass sharePreferenceClass;
    TextView tv_nav_ThemeDefault, tv_nav_ThemeRed, tv_nav_ThemeYellow, tv_nav_ThemeGreen;
    RelativeLayout rl_theme_default, rl_theme_yellow, rl_theme_green, rl_theme_red, rl_PermanentGeraniumLake;
    String userName = "", tokenStr = "", _email = "", _firstName = "", _lastName = "";
    SharedPreferences sp, spBbps;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    private static final String TAG = ReportActivity.class.getSimpleName();
    NestedScrollView nestedscroll;
    int scrollRange = -1;
    private String fullName;
    LinearLayout ll_parent;
    FrameLayout fragment_layout;

    //MVP
    private BbpsDashboardPresenter mActionsListener;
    ProgressDialog dialog;


    public MainActivity() {
    }

    private FirebaseAnalytics firebaseAnalytics;
    private FragmentManager fragmentManager;
    private boolean doubleBackToExitPressedOnce;
    private Unbinder unbinder;
    private FragmentTransaction transaction;


    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.onActivityCreateSetTheme(this);
        new CustomThemes(this);
        //setTheme(R.style.MediumSlateBlue);
        setContentView(R.layout.activity_main);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);


        settings = getSharedPreferences("YOUR_PREF_NAME", 0);
        editor = settings.edit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initView();
        subscribeGlobal();
        subscribeGlobal2();

        loadEvent();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_share) {
            try {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                setFBAnalytics("MAIN_QR_CLICK", userName);
                Intent __intent = new Intent(MainActivity.this, QRWebActivity.class);
                startActivity(__intent);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (id == R.id.wallet) {

            setFBAnalytics("MAIN_WALLET_CLICK", userName);
//                Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
            OnboardValidate.showAlert(MainActivity.this, alertText);
            return true;
        } else if (id == R.id.add_money) {
            try {
                setFBAnalytics("MAIN_ADD_MONEY_CLICK", userName);
//                Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
                //  OnboardValidate.showAlert(MainActivity.this, alertText);
                Intent intent = new Intent(MainActivity.this, AddMoneyActivity.class);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        } else if (id == R.id.menu_help) {
            try {
                setFBAnalytics("MAIN_ADD_MONEY_CLICK", userName);

                subscribeGlobalSF();

//                Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
                //  OnboardValidate.showAlert(MainActivity.this, alertText);
                ChatUserData fullNameData = new ChatUserData(
                        "FullName", fullName, true);
                ChatUserData firstNameData = new ChatUserData(
                        "FirstName", _firstName, true);
                ChatUserData lastNameData = new ChatUserData(
                        "LastName", _lastName, true);
                ChatUserData emailData = new ChatUserData(
                        "Email", _email, true);
                ChatUserData phoneumberData = new ChatUserData(
                        "Phone", Constants.USER_MOBILE_NO, true);
                ChatUserData usernameData = new ChatUserData(
                        "UserName", userName, true);
                ChatUserData adminName = new ChatUserData(
                        "AdminName",_adminName,true);
                Log.d(TAG, "onClick: Fst Name & Last Name " + firstNameData + " -- " + lastNameData);
                ChatEntity caseEntity = new ChatEntity.Builder()
                        .linkToTranscriptField("Transcript_Custom__c")
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Email__c", emailData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Name", fullNameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("First_Name__c", firstNameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Last_Name__c", lastNameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Phone__c", phoneumberData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Username__c", usernameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Admin_Name__c", adminName))
                        .build("Transcript_Custom__c");
                final ChatConfiguration.Builder chatConfigurationBuilder = new ChatConfiguration.Builder("00D5g000000KgP5", "5735g000000sbqt", "5725g000000saPq", "d.la2-c1-ukb.salesforceliveagent.com");
                chatConfigurationBuilder.visitorName(fullName);
                chatConfigurationBuilder
                        .chatUserData(emailData, fullNameData, firstNameData, lastNameData, phoneumberData, usernameData,adminName)
                        .chatEntities(caseEntity);
                ChatConfiguration chatConfiguration = chatConfigurationBuilder.build();
                ChatUI.configure(ChatUIConfiguration.create(chatConfiguration))
                        .createClient(getApplicationContext())
                        .onResult(new Async.ResultHandler<ChatUIClient>() {
                            @Override
                            public void handleResult(Async<?> async, @NonNull ChatUIClient chatUIClient) {
                                chatUIClient.startChatSession(MainActivity.this);
                            }
                        });


            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    @SuppressLint({"WrongConstant", "SetTextI18n", "ResourceAsColor", "CommitPrefEdits"})
    private void loadEvent() {

        addMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
                try {
                    setFBAnalytics("MAIN_ADD_MONEY", userName);
//                OnboardValidate.showAlert(MainActivity.this, alertText);

                    Intent intent = new Intent(MainActivity.this, AddMoneyActivity.class);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                //  Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();

            }
        });

        bbps_icon1 = findViewById(R.id.bbps_icon1);

        BBPSDashboardModel[] myListData = new BBPSDashboardModel[]{
                new BBPSDashboardModel("Mobile Postpaid", R.drawable.ic__01_bell, new Intent(MainActivity.this, ActivityBBPS.class), "", MOBILE_POSTPAID),
                new BBPSDashboardModel("Electricity", R.drawable.ic_energy_2_, new Intent(MainActivity.this, ActivityaElectricityBill.class), "", ELECTRICITY),
                new BBPSDashboardModel("DTH", R.drawable.ic__19_volume_up, new Intent(MainActivity.this, ActivityBBPS.class), "", DTH),
                new BBPSDashboardModel("Broadband", R.drawable.ic__20_wifi, new Intent(MainActivity.this, ActivityBBPS.class), "", BROADBAND_POSTPAID),
                new BBPSDashboardModel("Water", R.drawable.ic__13_faucet, new Intent(MainActivity.this, ActivityBBPS.class), "", WATER),
                new BBPSDashboardModel("LandLine", R.drawable.ic__03_telephone, new Intent(MainActivity.this, ActivityBBPS.class), "", LANDLINE_POSTPAID),
                new BBPSDashboardModel("Gas", R.drawable.ic_black_friday_2_, new Intent(MainActivity.this, ActivityBBPS.class), "", GAS),
                new BBPSDashboardModel("LPG Gas", R.drawable.ic_black_friday_2__1, new Intent(MainActivity.this, ActivityBBPS.class), "", LPG_GAS),
                new BBPSDashboardModel("Education Fees", R.drawable.ic__18_book, new Intent(MainActivity.this, ActivityBBPS.class), "", EDUCATION_FEES),
                new BBPSDashboardModel("Cable TV", R.drawable.ic__19_volume_up_1, new Intent(MainActivity.this, ActivityBBPS.class), "", CABLE_TV),
                new BBPSDashboardModel("FasTAG", R.drawable.ic_group_147, new Intent(MainActivity.this, ActivityBBPS.class), "", FASTAG),
                new BBPSDashboardModel("Life Insurance", R.drawable.ic__15_umbrella, new Intent(MainActivity.this, ActivityBBPS.class), "", LIFE_INSURANCE),
                new BBPSDashboardModel("Loan Repayment", R.drawable.ic_energy_2__1, new Intent(MainActivity.this, ActivityBBPS.class), "", LOAN_REPAYMENT),
                new BBPSDashboardModel("Health Insurance", R.drawable.ic_shield_2_, new Intent(MainActivity.this, ActivityBBPS.class), "", HEALTH_INSURANCE),

                new BBPSDashboardModel("Insurance", R.drawable.ic_insurance_bbps, new Intent(MainActivity.this, ActivityBBPS.class), "", INSURANCE),
                new BBPSDashboardModel("Muncipal Tax", R.drawable.ic_municiapal_tax, new Intent(MainActivity.this, ActivityBBPS.class), "", MUNCIPAL_TAX),
                new BBPSDashboardModel("Hospital", R.drawable.ic_hospital, new Intent(MainActivity.this, ActivityBBPS.class), "", HOSPITAL),
                new BBPSDashboardModel("Municipal Services", R.drawable.ic_municiapal_service, new Intent(MainActivity.this, ActivityBBPS.class), "", MUNICIPAL_SERVICES),
                new BBPSDashboardModel("Housing Society", R.drawable.ic_housing, new Intent(MainActivity.this, ActivityBBPS.class), "", HOUSING_SOCIETY),
                new BBPSDashboardModel("Subscription", R.drawable.ic_subscription, new Intent(MainActivity.this, ActivityBBPS.class), "", SUBSCRIPTION),

        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.bbpsList);
        BbpsDashboardAdapter adapter = new BbpsDashboardAdapter(myListData, MainActivity.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(adapter);

        BBPSDashboardModel[] myListRecharge = new BBPSDashboardModel[]{
                new BBPSDashboardModel("Mobile Prepaid", R.drawable.ic__01_bell, new Intent(MainActivity.this, ActivitybMobilePrepaid.class), "MOBILE_PREPAID", ""),
                new BBPSDashboardModel("Mobile Postpaid", R.drawable.ic_phone_call, new Intent(MainActivity.this, ActivityBBPS.class), "", MOBILE_POSTPAID),
        };

        RecyclerView recyclerViewRecharge = (RecyclerView) findViewById(R.id.rechargeList);
        BbpsDashboardAdapter adapterRecharge = new BbpsDashboardAdapter(myListRecharge, MainActivity.this);
        recyclerViewRecharge.setHasFixedSize(true);
        recyclerViewRecharge.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewRecharge.setAdapter(adapterRecharge);


        tabLinear1.setOnClickListener(v -> {
           /* Intent intent = new Intent(MainActivity.this, RechargeActivity.class);
            startActivity(intent);*/
            bbps_Modules.setVisibility(View.GONE);
            if (bbpsclick) {
                bbpsclick = false;
                // bbpsModules.setVisibility(View.GONE);
                recharge.setVisibility(View.VISIBLE);
                tab_icon1.setImageAlpha(255);
                tab_icon2.setImageAlpha(50);
                tab_icon3.setImageAlpha(50);
                tab_icon4.setImageAlpha(50);
                tab_icon5.setImageAlpha(50);
                tab_icon6.setImageAlpha(50);
                tab_icon7.setImageAlpha(50);
                tab_icon8.setImageAlpha(50);
                tab_icon9.setImageAlpha(50);
                tab_icon10.setImageAlpha(50);
                tab_icon11.setImageAlpha(50);
                tab_icon12.setImageAlpha(50);
            } else {
                bbpsclick = true;
                recharge.setVisibility(View.GONE);
                //   bbpsModules.setVisibility(View.VISIBLE);
                tab_icon1.setImageAlpha(255);
                tab_icon2.setImageAlpha(255);
                tab_icon3.setImageAlpha(255);
                tab_icon4.setImageAlpha(255);
                tab_icon5.setImageAlpha(255);
                tab_icon6.setImageAlpha(255);
                tab_icon7.setImageAlpha(255);
                tab_icon8.setImageAlpha(255);
                tab_icon9.setImageAlpha(255);
                tab_icon10.setImageAlpha(255);
                tab_icon11.setImageAlpha(255);
                tab_icon12.setImageAlpha(255);
            }

        });
        tabLinear2.setOnClickListener(v -> {
            if (isBBPSenabled == true) {
                recharge.setVisibility(View.GONE);
                if (bbpsclick) {
                    bbpsclick = false;
                    bbps_Modules.setVisibility(View.VISIBLE);
                    // recharge.setVisibility(View.GONE);
                    tab_icon1.setImageAlpha(50);
                    tab_icon2.setImageAlpha(255);
                    tab_icon3.setImageAlpha(50);
                    tab_icon4.setImageAlpha(50);
                    tab_icon5.setImageAlpha(50);
                    tab_icon6.setImageAlpha(50);
                    tab_icon7.setImageAlpha(50);
                    tab_icon8.setImageAlpha(50);
                    tab_icon9.setImageAlpha(50);
                    tab_icon10.setImageAlpha(50);
                    tab_icon11.setImageAlpha(50);
                    tab_icon12.setImageAlpha(50);
                } else {
                    bbpsclick = true;
                    bbps_Modules.setVisibility(View.GONE);
                    //  recharge.setVisibility(View.VISIBLE);
                    tab_icon1.setImageAlpha(255);
                    tab_icon2.setImageAlpha(255);
                    tab_icon3.setImageAlpha(255);
                    tab_icon4.setImageAlpha(255);
                    tab_icon5.setImageAlpha(255);
                    tab_icon6.setImageAlpha(255);
                    tab_icon7.setImageAlpha(255);
                    tab_icon8.setImageAlpha(255);
                    tab_icon9.setImageAlpha(255);
                    tab_icon10.setImageAlpha(255);
                    tab_icon11.setImageAlpha(255);
                    tab_icon12.setImageAlpha(255);
                }
            } else
                Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_BBPS, Toast.LENGTH_SHORT).show();
        });
        tabLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertCommmingSoon(MainActivity.this, alertText, "MAIN_TAB_TRAIN_TICKET");
            }
        });
        tabLinear4.setOnClickListener(new View.OnClickListener() { //tab_insurance
            @Override
            public void onClick(View v) {
                if (isInsuranceEnabled) {
                    setFBAnalytics("MAIN_TAB_INSURANCE", userName);
                    Intent intent = new Intent(MainActivity.this, Multiple_InsuranceActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_INSURANCE, Toast.LENGTH_SHORT).show();
                }
            }
        });
        tabLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertCommmingSoon(MainActivity.this, alertText, "MAIN_TAB_FLIGHT_TICKET");
            }
        });
        tabLinear6.setOnClickListener(new View.OnClickListener() { //tab_water
            @Override
            public void onClick(View v) {
                if (isBBPSenabled == true) {
                    setFBAnalytics("MAIN_TAB_WATER", userName);
                    isApiCalledInSession(new Intent(MainActivity.this, ActivityBBPS.class), WATER);
                } else
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_BBPS, Toast.LENGTH_SHORT).show();
            }
        });
        tabLinear7.setOnClickListener(new View.OnClickListener() { //tab_pan
            @Override
            public void onClick(View v) {
                showAlertCommmingSoon(MainActivity.this, alertText, "MAIN_TAB_PAN");
            }
        });
        tabLinear8.setOnClickListener(new View.OnClickListener() { // tab_fastag
            @Override
            public void onClick(View v) {
                if (isBBPSenabled == true) {
                    setFBAnalytics("MAIN_TAB_FASTAG", userName);
                    isApiCalledInSession(new Intent(MainActivity.this, ActivityBBPS.class), FASTAG);
                } else
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_BBPS, Toast.LENGTH_SHORT).show();
            }
        });
        tabLinear9.setOnClickListener(new View.OnClickListener() { //tab_special
            @Override
            public void onClick(View v) {
                showAlertCommmingSoon(MainActivity.this, alertText, "MAIN_TAB_SPECIAL");
            }
        });
        tabLinear11.setOnClickListener(new View.OnClickListener() { //tab_invest
            @Override
            public void onClick(View v) {
                setFBAnalytics("MAIN_TAB_INVEST", userName);
                Intent intent = new Intent(MainActivity.this, CMSActivity.class);
                startActivity(intent);
            }
        });
        tabLinear12.setOnClickListener(new View.OnClickListener() { //tab_cms
            @Override
            public void onClick(View v) {
                setFBAnalytics("MAIN_TAB_CMS", userName);
//                Toast.makeText(MainActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();
                OnboardValidate.showAlert(MainActivity.this, alertText);
            }
        });

        tabLinear10.setOnClickListener(new View.OnClickListener() { //tab_repayment
            @Override
            public void onClick(View v) {
                showAlertCommmingSoon(MainActivity.this, alertText, "MAIN_TAB_REPAYMENT");

            }
        });


        @SuppressLint("CutPasteId") final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        CollapsingToolbarLayout toolbar_layout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolbar_layout.setTitle(" ");
        LinearLayout layout = (LinearLayout) findViewById(R.id.bottomSheet);
        menu_nav = (ImageView) findViewById(R.id.menu_nav);
        name = (TextView) findViewById(R.id.name);
        last_seen = (TextView) findViewById(R.id.last_seen);


        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.appbarLayout);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;

            @SuppressLint("SetTextI18n")
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (home.equalsIgnoreCase("HOME")) {
                    if (scrollRange + verticalOffset == 0) {
                        isShow = true;
                        showOption(R.id.add_money);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        //  layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.border_white_bg));
                    } else if (isShow) {
                        isShow = false;
                        hideOption(R.id.add_money);
                        name.setText("");
                        last_seen.setText("");
                        //layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.corner_radius));
                    }
                } else if (home.equalsIgnoreCase("AEPS")) {
                    if (scrollRange + verticalOffset == 0) {
                        isShow = true;
                        showOption(R.id.add_money);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        //  layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.border_white_bg));
                    } else if (isShow) {
                        isShow = true;
                        showOption(R.id.add_money);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        //layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.corner_radius));
                    }
                } else if (home.equalsIgnoreCase("MATM")) {
                    if (scrollRange + verticalOffset == 0) {
                        isShow = true;
                        showOption(R.id.add_money);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        //  layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.border_white_bg));
                    } else if (isShow) {
                        isShow = true;
                        showOption(R.id.add_money);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        //layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.corner_radius));
                    }
                } else if (home.equalsIgnoreCase("DMT")) {
                    if (scrollRange + verticalOffset == 0) {
                        isShow = true;
                        showOption(R.id.add_money);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        //  layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.border_white_bg));
                    } else if (isShow) {
                        isShow = true;
                        showOption(R.id.add_money);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        //layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.corner_radius));
                    }
                }
            }
        });

        menu_nav.setOnClickListener(v -> drawer.openDrawer(Gravity.START));

        rl_nav_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(ll_parent);

                if (isViewMore == false) {
                    isViewMore = true;
                    tv_viewmore_viewless.setText("View Less");
                    iv_nav_view_more.setImageResource(R.drawable.ic_group_296__1_);
                    ll_nav_recharge.setVisibility(View.VISIBLE);
                    ll_nav_bbps.setVisibility(View.VISIBLE);
                    ll_nav_upi.setVisibility(View.VISIBLE);
                    rl_nav_commission.setVisibility(View.VISIBLE);
                    rl_nav_cashout.setVisibility(View.VISIBLE);
                    ll_nav_insurance.setVisibility(View.VISIBLE);
                    rl_nav_add_money.setVisibility(View.VISIBLE);

                    resetNavigationDropdownIcons();
                } else {
                    isViewMore = false;
                    tv_viewmore_viewless.setText("View More");
                    iv_nav_view_more.setImageResource(R.drawable.ic_group_296);
                    ll_nav_recharge.setVisibility(View.GONE);
                    ll_nav_bbps.setVisibility(View.GONE);
                    ll_nav_upi.setVisibility(View.GONE);
                    rl_nav_commission.setVisibility(View.GONE);
                    rl_nav_cashout.setVisibility(View.GONE);
                    ll_nav_insurance.setVisibility(View.GONE);
                    rl_nav_add_money.setVisibility(View.GONE);
                    resetNavigationDropdownIcons();
                }
            }
        });


        ll_nav_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                drawer.closeDrawers();
            }
        });

        rl_nav_aeps.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Setting up Report Visibility
                if (isAEPS1enabled == false && isAEPS2enabled == false && isAEPS3enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_AEPS, Toast.LENGTH_SHORT).show();
                } else {
                    TransitionManager.beginDelayedTransition(ll_parent);

                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    if (!isExpandedAEPS) {
                        isExpandedAEPS = true;
                        isExpandedMATM = false;
                        isExpandedCASHOUT = false;
                        isExpandedWALLET = false;
                        isExpandedCOMMISION = false;
                        isExpandedTheme = false;

                        iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

                        tv_nav_aeps1.setVisibility(View.VISIBLE);
                        tv_nav_aeps2.setVisibility(View.VISIBLE);
                        tv_nav_aeps3.setVisibility(View.VISIBLE);

                        rl_theme_default.setVisibility(View.GONE);
                        rl_theme_yellow.setVisibility(View.GONE);
                        rl_theme_green.setVisibility(View.GONE);
                        rl_theme_red.setVisibility(View.GONE);
                        rl_PermanentGeraniumLake.setVisibility(View.GONE);

                        tv_nav_matm1.setVisibility(View.GONE);
                        tv_nav_matm2.setVisibility(View.GONE);
                        tv_nav_cashout1.setVisibility(View.GONE);
                        tv_nav_cashout2.setVisibility(View.GONE);
                        tv_nav_wallet1.setVisibility(View.GONE);
                        tv_nav_wallet2.setVisibility(View.GONE);
                        tv_nav_commision1.setVisibility(View.GONE);
                        tv_nav_commision2.setVisibility(View.GONE);
                        tv_nav_add_money1.setVisibility(View.GONE);
                        tv_nav_add_money2.setVisibility(View.GONE);


                    } else {
                        resetNavigationDropdownIcons();
                    }
                }

            }
        });

        tv_nav_aeps1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAEPS1enabled == false) {
                    //tv_nav_aeps1.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_AEPS1, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "aeps1";

                    setFBAnalytics("MAIN_NAV_AEPS1_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                }


            }

        });
        tv_nav_aeps2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAEPS2enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_AEPS2, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "aeps2";

                    setFBAnalytics("MAIN_NAV_AEPS2_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);

                    drawer.closeDrawers();
                }
            }
        });
        tv_nav_aeps3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAEPS3enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_AEPS3, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "aeps3";

                    setFBAnalytics("MAIN_NAV_AEPS3_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);

                    drawer.closeDrawers();
                }
            }
        });
        rl_nav_matm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isMATM1enabled == false && isMATM2enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_MATM, Toast.LENGTH_SHORT).show();
                } else {

                    TransitionManager.beginDelayedTransition(ll_parent);

                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    if (!isExpandedMATM) {
                        isExpandedMATM = true;
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        tv_nav_matm1.setVisibility(View.VISIBLE);
                        tv_nav_matm2.setVisibility(View.VISIBLE);

                        isExpandedAEPS = false;
                        isExpandedMATM = true;
                        isExpandedCASHOUT = false;
                        isExpandedWALLET = false;
                        isExpandedCOMMISION = false;
                        isExpandedTheme = false;

                        iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

                        rl_theme_default.setVisibility(View.GONE);
                        rl_theme_yellow.setVisibility(View.GONE);
                        rl_theme_green.setVisibility(View.GONE);
                        rl_theme_red.setVisibility(View.GONE);
                        rl_PermanentGeraniumLake.setVisibility(View.GONE);

                        tv_nav_aeps1.setVisibility(View.GONE);
                        tv_nav_aeps2.setVisibility(View.GONE);
                        tv_nav_aeps3.setVisibility(View.GONE);
                        tv_nav_matm1.setVisibility(View.VISIBLE);
                        tv_nav_matm2.setVisibility(View.VISIBLE);
                        tv_nav_cashout1.setVisibility(View.GONE);
                        tv_nav_cashout2.setVisibility(View.GONE);
                        tv_nav_wallet1.setVisibility(View.GONE);
                        tv_nav_wallet2.setVisibility(View.GONE);
                        tv_nav_commision1.setVisibility(View.GONE);
                        tv_nav_commision2.setVisibility(View.GONE);
                        tv_nav_add_money1.setVisibility(View.GONE);
                        tv_nav_add_money2.setVisibility(View.GONE);


                    } else {
                        resetNavigationDropdownIcons();
                    }
                }

            }
        });
        tv_nav_matm1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isMATM1enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_MATM1, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "matm1";

                    setFBAnalytics("MAIN_NAV_MATM1_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);

                    drawer.closeDrawers();
                }
            }
        });
        tv_nav_matm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMATM2enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_MATM2, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    Constants.REPORT_MODULE = "matm2";

                    setFBAnalytics("MAIN_NAV_MATM2_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);

                    drawer.closeDrawers();
                }


            }
        });
        ll_nav_dmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDMTenabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_DMT, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    Constants.REPORT_MODULE = "dmt";

                    setFBAnalytics("MAIN_NAV_DMT_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);

                    drawer.closeDrawers();
                }
            }
        });
        rl_nav_cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCashoutWallet1enabled == false && isCashoutWallet2enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_CASHOUT, Toast.LENGTH_SHORT).show();
                } else {
                    TransitionManager.beginDelayedTransition(ll_parent);

                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    if (!isExpandedCASHOUT) {
                        isExpandedAEPS = false;
                        isExpandedMATM = false;
                        isExpandedCASHOUT = true;
                        isExpandedWALLET = false;
                        isExpandedCOMMISION = false;
                        isExpandedTheme = false;

                        iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_up);
                        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);
                        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

                        rl_theme_default.setVisibility(View.GONE);
                        rl_theme_yellow.setVisibility(View.GONE);
                        rl_theme_green.setVisibility(View.GONE);
                        rl_theme_red.setVisibility(View.GONE);
                        rl_PermanentGeraniumLake.setVisibility(View.GONE);

                        rl_PermanentGeraniumLake.setVisibility(View.GONE);


                        tv_nav_aeps1.setVisibility(View.GONE);
                        tv_nav_aeps2.setVisibility(View.GONE);
                        tv_nav_aeps3.setVisibility(View.GONE);
                        tv_nav_matm1.setVisibility(View.GONE);
                        tv_nav_matm2.setVisibility(View.GONE);
                        tv_nav_cashout1.setVisibility(View.VISIBLE);
                        tv_nav_cashout2.setVisibility(View.VISIBLE);
                        tv_nav_wallet1.setVisibility(View.GONE);
                        tv_nav_wallet2.setVisibility(View.GONE);
                        tv_nav_commision1.setVisibility(View.GONE);
                        tv_nav_commision2.setVisibility(View.GONE);

                    } else {
                        resetNavigationDropdownIcons();
                    }
                }
            }
        });
        tv_nav_cashout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCashoutWallet1enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    Constants.REPORT_MODULE = "cashout1";
                    setFBAnalytics("MAIN_NAV_CASHOUT1_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_CASHOUT_WALLET1, Toast.LENGTH_SHORT).show();
                }
            }

        });
        tv_nav_cashout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCashoutWallet2enabled) {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    Constants.REPORT_MODULE = "cashout2";
                    setFBAnalytics("MAIN_NAV_CASHOUT2_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_CASHOUT_WALLET2, Toast.LENGTH_SHORT).show();
                }
            }
        });

        ll_nav_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isRechargeEnabled) {
                    Constants.REPORT_MODULE = "recharge";
                    setFBAnalytics("MAIN_NAV_RECHARGE_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    intent.putExtra("report_type", "recharge");
                    startActivity(intent);
                    drawer.closeDrawers();
                } else {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_RECHARGE, Toast.LENGTH_SHORT).show();
                }
            }

        });
        ll_nav_bbps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isBBPSenabled) {
                    Constants.REPORT_MODULE = "bbps";
                    setFBAnalytics("MAIN_NAV_BBPS_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_BBPS, Toast.LENGTH_SHORT).show();
                }

                drawer.closeDrawers();

            }

        });
        ll_nav_upi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "upi";

                setFBAnalytics("MAIN_NAV_UPI_CLICK", userName);
                Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();
            }
        });
        rl_nav_commission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(ll_parent);

                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isExpandedCOMMISION == false) {
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = false;
                    isExpandedCOMMISION = true;
                    isExpandedaddMoney = false;
                    isExpandedTheme = false;

                    iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_up);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_down);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.GONE);
                    tv_nav_wallet2.setVisibility(View.GONE);
                    tv_nav_commision1.setVisibility(View.VISIBLE);
                    tv_nav_commision2.setVisibility(View.VISIBLE);
                    tv_nav_add_money1.setVisibility(View.GONE);
                    tv_nav_add_money2.setVisibility(View.GONE);

                } else {
                    resetNavigationDropdownIcons();
                }
            }
        });
        tv_nav_commision1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "commision1";

                setFBAnalytics("MAIN_NAV_COMMISSION1_CLICK", userName);
                Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                startActivity(intent);
                drawer.closeDrawers();
            }
        });
        tv_nav_commision2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "commision2";

                setFBAnalytics("MAIN_NAV_COMMISSION2_CLICK", userName);
                Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();
            }
        });
        rl_nav_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(ll_parent);

                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isExpandedWALLET == false) {
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = true;
                    isExpandedCOMMISION = false;
                    isExpandedaddMoney = false;
                    isExpandedTheme = false;
                    iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_up);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_down);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.VISIBLE);
                    tv_nav_wallet2.setVisibility(View.VISIBLE);
                    tv_nav_commision1.setVisibility(View.GONE);
                    tv_nav_commision2.setVisibility(View.GONE);
                    tv_nav_add_money1.setVisibility(View.GONE);
                    tv_nav_add_money2.setVisibility(View.GONE);


                } else {
                    resetNavigationDropdownIcons();
                }

            }
        });
        tv_nav_wallet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Constants.REPORT_MODULE = "wallet1";

                setFBAnalytics("MAIN_NAV_WALLET1_CLICK", userName);

                Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });
        tv_nav_wallet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "wallet2";

                setFBAnalytics("MAIN_NAV_WALLET2_CLICK", userName);

                Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });
        ll_nav_insurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isInsuranceEnabled) {
                    Constants.REPORT_MODULE = "insurance";
                    setFBAnalytics("MAIN_NAV_INSURANCE_CLICK", userName);
                    Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_INSURANCE, Toast.LENGTH_SHORT).show();
                }
                drawer.closeDrawers();

            }
        });
        rl_nav_add_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TransitionManager.beginDelayedTransition(ll_parent);

                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (isExpandedaddMoney == false) {
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = false;
                    isExpandedCOMMISION = false;
                    isExpandedaddMoney = true;

                    iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_up);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.GONE);
                    tv_nav_wallet2.setVisibility(View.GONE);
                    tv_nav_commision1.setVisibility(View.GONE);
                    tv_nav_commision2.setVisibility(View.GONE);
                    tv_nav_add_money1.setVisibility(View.VISIBLE);
                    tv_nav_add_money2.setVisibility(View.VISIBLE);

                } else {
                    resetNavigationDropdownIcons();
                }

            }
        });
        tv_nav_add_money1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "addMoney1";

                Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });
        tv_nav_add_money2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                Constants.REPORT_MODULE = "addMoney2";

                Intent intent = new Intent(MainActivity.this, ActivityReportDashboardNew.class);
                startActivity(intent);

                drawer.closeDrawers();

            }
        });


        /*@Author - RashmiRanjan
         * */
        rl_nav_Theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(ll_parent);

                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                if (!isExpandedTheme) {
                    isExpandedTheme = true;
                    isExpandedAEPS = false;
                    isExpandedMATM = false;
                    isExpandedCASHOUT = false;
                    isExpandedWALLET = false;
                    isExpandedCOMMISION = false;

                    iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_up);
                    iv_nav_AEPS_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_mATM_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_cash_out_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
                    iv_nab_commission_dropdown.setImageResource(R.drawable.ic_arrow_down);

                    rl_theme_default.setVisibility(View.VISIBLE);
                    rl_theme_yellow.setVisibility(View.VISIBLE);
                    rl_theme_green.setVisibility(View.VISIBLE);
                    rl_theme_red.setVisibility(View.VISIBLE);
                    rl_PermanentGeraniumLake.setVisibility(View.VISIBLE);

                    tv_nav_aeps1.setVisibility(View.GONE);
                    tv_nav_aeps2.setVisibility(View.GONE);
                    tv_nav_aeps3.setVisibility(View.GONE);
                    tv_nav_matm1.setVisibility(View.GONE);
                    tv_nav_matm2.setVisibility(View.GONE);
                    tv_nav_cashout1.setVisibility(View.GONE);
                    tv_nav_cashout2.setVisibility(View.GONE);
                    tv_nav_wallet1.setVisibility(View.GONE);
                    tv_nav_wallet2.setVisibility(View.GONE);
                    tv_nav_commision1.setVisibility(View.GONE);
                    tv_nav_commision2.setVisibility(View.GONE);

                } else {
                    resetNavigationDropdownIcons();
                }
            }
        });

        rl_theme_default.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(MainActivity.this, Util.THEME_DEFAULT);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                settings.edit();
                SharedPreferences.Editor editor;
                editor = settings.edit();
                /**Saving the theme color name into the sharedPreference**/
                editor.putInt("SNOW_DENSITY", Util.THEME_DEFAULT);
                editor.putString("Theme", "THEME_DEFAULT");
                editor.apply();
                Util.putTheme(MainActivity.this, "THEME_DEFAULT");
                drawer.closeDrawers();
            }
        });
        rl_PermanentGeraniumLake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(MainActivity.this, Util.PermanentGeraniumLake);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.PermanentGeraniumLake);
                editor.putString("Theme", "PermanentGeraniumLake");
                editor.putInt("SNOW_DENSITY", Util.VioletBlue);
                editor.putString("Theme", "VioletBlue");
                editor.apply();
                Util.putTheme(MainActivity.this, "VioletBlue");
                drawer.closeDrawers();
            }

        });//rl_PermanentGeraniumLake
        rl_PermanentGeraniumLake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(MainActivity.this, Util.PermanentGeraniumLake);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.PermanentGeraniumLake);
                editor.putString("Theme", "PermanentGeraniumLake");
                editor.apply();
                Util.putTheme(MainActivity.this, "PermanentGeraniumLake");
                drawer.closeDrawers();
            }

        });
        rl_theme_default.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(MainActivity.this, Util.THEME_DEFAULT);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                settings.edit();
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.THEME_DEFAULT);
                editor.putString("Theme", "THEME_DEFAULT");
                editor.apply();
                Util.putTheme(MainActivity.this, "PermanentGeraniumLake");
                drawer.closeDrawers();
            }

        });
        rl_theme_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(MainActivity.this, Util.Bluetiful);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.Bluetiful);
                editor.putString("Theme", "Bluetiful");
                editor.apply();
                Util.putTheme(MainActivity.this, "Bluetiful");
                drawer.closeDrawers();
            }

        });
        rl_theme_yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(MainActivity.this, Util.MediumSlateBlue);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.MediumSlateBlue);
                editor.putString("Theme", "MediumSlateBlue");
                editor.apply();
                Util.putTheme(MainActivity.this, "MediumSlateBlue");
                drawer.closeDrawers();
            }
        });
        rl_theme_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Util.changeToTheme(MainActivity.this, Util.VioletBlue);
                settings = getSharedPreferences("YOUR_PREF_NAME", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor = settings.edit();
                editor.putInt("SNOW_DENSITY", Util.VioletBlue);
                editor.putString("Theme", "VioletBlue");
                editor.apply();
                Util.putTheme(MainActivity.this, "VioletBlue");
                drawer.closeDrawers();
            }

        });


        tv_cashout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCashoutWallet1enabled == false && isCashoutWallet2enabled == false) {
                    Toast.makeText(MainActivity.this, ERROR_MESSAGE_DISABLED_CASHOUT, Toast.LENGTH_SHORT).show();
                } else {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    setFBAnalytics("MAIN_TV_CASHOUT_CLICK", userName);
                    Intent __intent = new Intent(MainActivity.this, SelectCashoutWalletOptionActivity.class);
                    startActivity(__intent);

                }
            }
        });
        tv_upi_settlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                FetchSettlement();

                //drawer.closeDrawers();
            }
        });
        tv_showqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                setFBAnalytics("MAIN_TV_QR_CLICK", userName);
                Intent __intent = new Intent(MainActivity.this, QRWebActivity.class);
                startActivity(__intent);
            }
        });

        tv_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                setFBAnalytics("MAIN_TV_SETTINGS_CLICK", userName);
               /* startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                finish();*/
                Intent settingsIntent = new Intent(MainActivity.this, SettingsNewActivity.class);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(settingsIntent);
                overridePendingTransition(0, 0);

                drawer.closeDrawers();

            }
        });

        tv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }

                subscribeGlobalSF();

                ChatUserData fullNameData = new ChatUserData(
                        "FullName", fullName, true);
                ChatUserData firstNameData = new ChatUserData(
                        "FirstName", _firstName, true);
                ChatUserData lastNameData = new ChatUserData(
                        "LastName", _lastName, true);
                ChatUserData emailData = new ChatUserData(
                        "Email", _email, true);
                ChatUserData phoneumberData = new ChatUserData(
                        "Phone", Constants.USER_MOBILE_NO, true);
                ChatUserData usernameData = new ChatUserData(
                        "UserName", userName, true);
                ChatUserData adminName = new ChatUserData(
                        "AdminName",_adminName,true);
                Log.d(TAG, "onClick: Fst Name & Last Name " + firstNameData + " -- " + lastNameData);
                ChatEntity caseEntity = new ChatEntity.Builder()
                        .linkToTranscriptField("Transcript_Custom__c")
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Email__c", emailData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Name", fullNameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("First_Name__c", firstNameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Last_Name__c", lastNameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Phone__c", phoneumberData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Username__c", usernameData))
                        .addChatEntityField(
                                new ChatEntityField.Builder()
                                        .doFind(true)
                                        .isExactMatch(true)
                                        .doCreate(true)
                                        .build("Admin_Name__c", adminName))
                        .build("Transcript_Custom__c");
                final ChatConfiguration.Builder chatConfigurationBuilder = new ChatConfiguration.Builder("00D5g000000KgP5", "5735g000000sbqt", "5725g000000saPq", "d.la2-c1-ukb.salesforceliveagent.com");
                chatConfigurationBuilder.visitorName(fullName);
                chatConfigurationBuilder
                        .chatUserData(emailData, fullNameData, firstNameData, lastNameData, phoneumberData, usernameData,adminName)
                        .chatEntities(caseEntity);
                ChatConfiguration chatConfiguration = chatConfigurationBuilder.build();
                ChatUI.configure(ChatUIConfiguration.create(chatConfiguration))
                        .createClient(getApplicationContext())
                        .onResult(new Async.ResultHandler<ChatUIClient>() {
                            @Override
                            public void handleResult(Async<?> async, @NonNull ChatUIClient chatUIClient) {
                                chatUIClient.startChatSession(MainActivity.this);
                            }
                        });
                drawer.closeDrawers();
            }
        });

        tv_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("MAIN_TV_CHANGE_PASSWORD_CLICK", userName);
                Intent pwdIntent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                startActivity(pwdIntent);
            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogoutAlert();
            }
        });



      /*  con_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
            }
        });*/


        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reload.setVisibility(View.GONE);
//                progressBar.setVisibility(View.VISIBLE);
//                progressBar.setProgressTintList(ColorStateList.valueOf(Color.WHITE));
                setFBAnalytics("MAIN_RELOAD_CLICK", userName);
                homePresenter.getHomeDashboardResponse("https://itpl.iserveu.tech");
            }
        });
        reload2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reload2.setVisibility(View.GONE);
                setFBAnalytics("MAIN_RELOAD2_CLICK", userName);
//                progressBar2.setVisibility(View.VISIBLE);
//                progressBar2.setProgressTintList(ColorStateList.valueOf(Color.WHITE));
                // getWallet2Balance();
                homePresenter.getHomeDashboardResponse("https://itpl.iserveu.tech");

            }
        });



    }




    public void subscribeGlobal2() {
        String specialChar = "!@#$%^&*";

        if (SdkConstants.USER_NAME.matches(specialChar)) {
            SdkConstants.DEVICE_TOPIC = "";
        } else {
            SdkConstants.DEVICE_TOPIC = "AEPS3_NOTIFICATION_" + Constants.USER_NAME;
        }

        SdkConstants.COMPLETE_REGISTRATION = "registrationComplete";
        SdkConstants.PUSH_NOTIFICATION = "pushNotification";

        Log.d("TAG", "subscribeGlobal: " + SdkConstants.DEVICE_TOPIC);
        SubscribeGlobal global = new SubscribeGlobal(this);
        global.subscribe();
        global.registerBroadcast();
    }

    public void subscribeGlobal() {
        String specialChar = "!@#$%^&*";

        if (SdkConstants.USER_NAME.matches(specialChar)) {
            SdkConstants.DEVICE_TOPIC = "";
        } else {
            SdkConstants.DEVICE_TOPIC = "AEPS_" + Constants.USER_NAME;
        }
        //topicname : "AEPS_"+recMessage.user_name+"_STAGING"
        SdkConstants.COMPLETE_REGISTRATION = "registrationComplete";
        SdkConstants.PUSH_NOTIFICATION = "pushNotification";

        Log.d("TAG", "subscribeGlobal: " + SdkConstants.DEVICE_TOPIC);
        SubscribeGlobal global = new SubscribeGlobal(this);
        global.subscribe();
        global.registerBroadcast();
    }

    public void subscribeGlobalSF() {
        String specialChar = "!@#$%^&*";
        if (SdkConstants.USER_NAME.matches(specialChar)) {
            SdkConstants.DEVICE_TOPIC = "";
        } else {
            SdkConstants.DEVICE_TOPIC = "SALESFORCE_" + Constants.USER_NAME;
        }
        SdkConstants.COMPLETE_REGISTRATION = "registrationComplete";
        SdkConstants.PUSH_NOTIFICATION = "pushNotification";
        Log.d("TAG", "subscribeGlobal: " + SdkConstants.DEVICE_TOPIC);
        SubscribeGlobal global = new SubscribeGlobal(this);
        global.subscribe();
        global.registerBroadcast();
    }

    private void hideOption(int id) {
        MenuItem item = menu.findItem(R.id.add_money);
        item.setVisible(false);
    }

    private void showOption(int id) {
        try {
            MenuItem item = menu.findItem(R.id.add_money);
            item.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void FetchSettlement() {

        if (pd != null) {
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();
        }
        String Fetch_url = "https://settlementapi.iserveu.website/get_user_settlement_type";

        AndroidNetworking.get(Fetch_url)
                .addHeaders("Authorization", _token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject jsonObject;
                        String resp = response.toString();
                        Log.d(TAG, "onResponse: " + resp);
                        if (response != null) {
                            try {
                                jsonObject = new JSONObject(String.valueOf(response));
                                JSONArray jsonArray = jsonObject.getJSONArray("userData");
                                JSONObject settle1 = (JSONObject) jsonArray.get(0);
                                _is_upi_settle1 = settle1.optString("is_upi_settle");
                                if (!_is_upi_settle1.equals("")) {
                                    pd.cancel();
                                    showAlertDialogSettleUPIdialog();
                                } else {
                                    pd.cancel();
                                    Toast.makeText(MainActivity.this, "Something Went Wrong Please Try again latter", Toast.LENGTH_LONG).show();
                                }

                                Log.d(TAG, "onResponse: " + _is_upi_settle1.toString());
                            } catch (JSONException e) {
                                pd.cancel();
                                e.printStackTrace();
                            }
                        } else {
                            pd.cancel();
                            Toast.makeText(MainActivity.this, "Something Went Wrong Please Try again latter", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        pd.cancel();

                        try {
                            JSONObject jsonObject1 = new JSONObject(anError.getErrorBody().toString());
                            String status = jsonObject1.getString("status");
                            if (status.equals("0")) {
                                String message_ = jsonObject1.getString("message");
                                ShowAlertDialog(message_);
                            } else {
                                ShowAlertDialog("Something went wrong. Please try again...");
                            }
                            pd.cancel();
                        } catch (Exception e) {
                            ShowAlertDialog("Something went wrong. Please try again...");
                            e.printStackTrace();
                        }

                    }
                });

    }


    @SuppressLint("NonConstantResourceId")
    private void showAlertDialogSettleUPIdialog() {
        final Dialog dialog = new Dialog(this);
        Window window = dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        RadioGroup rg_my_qrCode = dialog.findViewById(R.id.rg_choose_settlement);
        RadioButton rb_hourly_settlement = dialog.findViewById(R.id.rb_hr_settlement);
        RadioButton rb_daily_settlement = dialog.findViewById(R.id.rb_dly_settlement);
        Button btn_save_close = dialog.findViewById(R.id.btn_save_close);
        Button btn_close = dialog.findViewById(R.id.btn_cancle);

        if (_is_upi_settle1.equals("1")) {
            rg_my_qrCode.check(R.id.rb_dly_settlement);
            _userRequest = "1";
        } else if (_is_upi_settle1.equals("0")) {
            rg_my_qrCode.check(R.id.rb_hr_settlement);
            _userRequest = "0";
        } else {
            rb_daily_settlement.setChecked(false);
            rb_hourly_settlement.setChecked(false);
        }

        rg_my_qrCode.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rb_hr_settlement:
                    _userRequest = "0";
                    break;
                case R.id.rb_dly_settlement:
                    _userRequest = "1";
                    break;
            }
        });
        btn_save_close.setOnClickListener(v -> {
            changeSettlementAPI();
            dialog.dismiss();
        });
        btn_close.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    private void changeSettlementAPI() {
        if (pd != null) {
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userRequest", _userRequest);

            String url = "https://settlementapi.iserveu.website/change_user_settlement_type";
            //String url = "https://settlement-api-vn3k2k7q7q-uc.a.run.app/change_user_settlement_type";

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", _token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONObject jsonObject;
                            if (response != null) {
                                try {
                                    jsonObject = new JSONObject(String.valueOf(response));
                                    String msg = jsonObject.optString("message");
                                    if (!msg.equals("")) {
                                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
                                        pd.cancel();
                                    } else {
                                        Toast.makeText(MainActivity.this, "something went Wrong Please try again Latter  !!!", Toast.LENGTH_LONG).show();
                                        pd.cancel();
                                    }
                                } catch (JSONException e) {
                                    pd.cancel();
                                    Toast.makeText(MainActivity.this, "something went Wrong Please try again Latter !!!", Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            } else {
                                pd.cancel();
                                Toast.makeText(MainActivity.this, "something went Wrong Please try again Latter !!!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            pd.cancel();


                            try {
                                JSONObject jsonObject1 = new JSONObject(anError.getErrorBody().toString());
                                String status = jsonObject1.getString("status");
                                if (status.equals("0")) {
                                    String message_ = jsonObject1.getString("message");
                                    ShowAlertDialog(message_);
                                } else {
                                    ShowAlertDialog("Something went wrong. Please try again...");
                                }
                            } catch (Exception e) {
                                ShowAlertDialog("Something went wrong. Please try again...");
                                e.printStackTrace();
                            }
                        }
                    });

        } catch (JSONException e) {
            pd.cancel();
            e.printStackTrace();
        }
    }

    void ShowAlertDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage(msg);
        builder1.setTitle("Alert !!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog3, int id) {
                        dialog3.cancel();
                    }
                });
        builder1.show();
    }

    private void resetNavigationDropdownIcons() {
        isExpandedAEPS = false;
        isExpandedMATM = false;
        isExpandedCASHOUT = false;
        isExpandedWALLET = false;
        isExpandedCOMMISION = false;
        isExpandedaddMoney = false;
        isExpandedTheme = false;


        iv_nav_aeps_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_matm_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_cashout_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_wallet_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_commision_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_add_money_dropdown.setImageResource(R.drawable.ic_arrow_down);
        iv_nav_theme_dropdown.setImageResource(R.drawable.ic_arrow_down);

        tv_nav_aeps1.setVisibility(View.GONE);
        tv_nav_aeps2.setVisibility(View.GONE);
        tv_nav_aeps3.setVisibility(View.GONE);
        tv_nav_matm1.setVisibility(View.GONE);
        tv_nav_matm2.setVisibility(View.GONE);
        tv_nav_cashout1.setVisibility(View.GONE);
        tv_nav_cashout2.setVisibility(View.GONE);
        tv_nav_wallet1.setVisibility(View.GONE);
        tv_nav_wallet2.setVisibility(View.GONE);
        tv_nav_commision1.setVisibility(View.GONE);
        tv_nav_commision2.setVisibility(View.GONE);

       /* tv_nav_ThemeYellow.setVisibility(View.GONE);
        tv_nav_ThemeGreen.setVisibility(View.GONE);
        tv_nav_ThemeDefault.setVisibility(View.GONE);
      //  iv_theme_icon.setVisibility(View.GONE);
        tv_nav_ThemeRed.setVisibility(View.GONE);*/
        rl_theme_default.setVisibility(View.GONE);
        rl_theme_yellow.setVisibility(View.GONE);
        rl_theme_green.setVisibility(View.GONE);
        rl_theme_red.setVisibility(View.GONE);
        rl_PermanentGeraniumLake.setVisibility(View.GONE);
        tv_nav_add_money1.setVisibility(View.GONE);
        tv_nav_add_money2.setVisibility(View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        //startTimer();

        homePresenter.getHomeDashboardResponse("https://itpl.iserveu.tech");
    }

    private void initView() {
        pd = new ProgressDialog(MainActivity.this);
        sharePreferenceClass = new SharePreferenceClass(MainActivity.this);

        session = new SessionManager(getApplicationContext());
        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        spBbps = getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE);
        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);
        _userName = user.get(SessionManager.userName);
        tokenStr = user.get(SessionManager.KEY_TOKEN);
        userName = Constants.USER_NAME;

        homePresenter = new HomePresenter(this);

        //MVP
        mActionsListener = new BbpsDashboardPresenter(MainActivity.this);

        navigation = findViewById(R.id.navigation);
        reload = findViewById(R.id.reload);
        reload2 = findViewById(R.id.reload2);
        progressBar = findViewById(R.id.progressBar);
        progressBar2 = findViewById(R.id.progressBar2);
        con_notification = findViewById(R.id.con_notification);
        navigation.setOnNavigationItemSelectedListener(this);


        menu_nav = findViewById(R.id.menu_nav);
        drawer = findViewById(R.id.drawer_layout);
        bbps_Modules = findViewById(R.id.bbpsModules);
        recharge = findViewById(R.id.recharge);
        tab_icon12 = findViewById(R.id.tab_icon12);
        tab_icon11 = findViewById(R.id.tab_icon11);
        tab_icon10 = findViewById(R.id.tab_icon10);
        tab_icon9 = findViewById(R.id.tab_icon9);
        tab_icon8 = findViewById(R.id.tab_icon8);
        tab_icon7 = findViewById(R.id.tab_icon7);
        tab_icon6 = findViewById(R.id.tab_icon6);
        tab_icon5 = findViewById(R.id.tab_icon5);
        tab_icon4 = findViewById(R.id.tab_icon4);
        tab_icon3 = findViewById(R.id.tab_icon3);
        tab_icon2 = findViewById(R.id.tab_icon2);
        tab_icon1 = findViewById(R.id.tab_icon1);
        tabLinear12 = findViewById(R.id.tabLinear12);
        tabLinear11 = findViewById(R.id.tabLinear11);
        tabLinear10 = findViewById(R.id.tabLinear10);
        tabLinear9 = findViewById(R.id.tabLinear9);
        tabLinear8 = findViewById(R.id.tabLinear8);
        tabLinear7 = findViewById(R.id.tabLinear7);
        tabLinear6 = findViewById(R.id.tabLinear6);
        tabLinear5 = findViewById(R.id.tabLinear5);
        tabLinear4 = findViewById(R.id.tabLinear4);
        tabLinear3 = findViewById(R.id.tabLinear3);
        tabLinear2 = findViewById(R.id.tabLinear2);
        tabLinear1 = findViewById(R.id.tabLinear1);
        bbps_Modules.setVisibility(View.GONE);
        recharge.setVisibility(View.GONE);
        bbpsLinear1 = findViewById(R.id.bbpsLinear1);
        addMoney = findViewById(R.id.addMoney);
        fragment_layout = findViewById(R.id.fragment_layout);
        collpase_toolbar = findViewById(R.id.collpase_toolbar);

        das_user_balance = findViewById(R.id.das_user_balance);

        das_user_balance2 = findViewById(R.id.das_user_balance2);
        // das_user_name = findViewById(R.id.das_user_name);
        das_user_balance = findViewById(R.id.balance1);
        // das_user_name2 = findViewById(R.id.das_user_name2);
        das_user_balance2 = findViewById(R.id.balance2);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerview = navigationView.getHeaderView(0);

        ll_parent = headerview.findViewById(R.id.header_parent);

        nav_user_name = headerview.findViewById(R.id.nav_user_name);
        ll_nav_home = headerview.findViewById(R.id.ll_nav_home);
        ll_nav_dmt = headerview.findViewById(R.id.ll_nav_dmt);
        ll_nav_recharge = headerview.findViewById(R.id.ll_nav_recharge);
        ll_nav_bbps = headerview.findViewById(R.id.ll_nav_bbps);
        ll_nav_upi = headerview.findViewById(R.id.ll_nav_upi);
        ll_nav_insurance = headerview.findViewById(R.id.ll_nav_insurance);
        rl_nav_aeps = headerview.findViewById(R.id.rl_nav_aeps);
        rl_nav_matm = headerview.findViewById(R.id.rl_nav_matm);
        rl_nav_cashout = headerview.findViewById(R.id.rl_nav_cashout);
        rl_nav_wallet = headerview.findViewById(R.id.rl_nav_wallet);
        rl_nav_commission = headerview.findViewById(R.id.rl_nav_commission);
        rl_nav_Theme = headerview.findViewById(R.id.rl_nav_theme);
        iv_nav_AEPS_dropdown = headerview.findViewById(R.id.iv_nav_aeps_dropdown);
        iv_nav_mATM_dropdown = headerview.findViewById(R.id.iv_nav_matm_dropdown);
        iv_nav_cash_out_dropdown = headerview.findViewById(R.id.iv_nav_cashout_dropdown);
        rl_nav_add_money = headerview.findViewById(R.id.rl_nav_add_money);
        rl_nav_view_more = headerview.findViewById(R.id.rl_nav_view_more);
        iv_nav_aeps_dropdown = headerview.findViewById(R.id.iv_nav_aeps_dropdown);
        iv_nav_matm_dropdown = headerview.findViewById(R.id.iv_nav_matm_dropdown);
        iv_nav_cashout_dropdown = headerview.findViewById(R.id.iv_nav_cashout_dropdown);
        iv_nav_wallet_dropdown = headerview.findViewById(R.id.iv_nav_wallet_dropdown);
        iv_nav_commision_dropdown = headerview.findViewById(R.id.iv_nav_commision_dropdown);
        iv_nav_add_money_dropdown = headerview.findViewById(R.id.iv_nav_add_money_dropdown);

        iv_nab_commission_dropdown = headerview.findViewById(R.id.iv_nav_commision_dropdown);
        iv_nav_view_more = headerview.findViewById(R.id.iv_nav_view_more);
        iv_nav_theme_dropdown = headerview.findViewById(R.id.iv_nav_theme_dropdown);
        tv_nav_aeps1 = headerview.findViewById(R.id.tv_nav_aeps1);
        tv_nav_aeps2 = headerview.findViewById(R.id.tv_nav_aeps2);
        tv_nav_aeps3 = headerview.findViewById(R.id.tv_nav_aeps3);

        tv_nav_matm1 = headerview.findViewById(R.id.tv_nav_matm1);
        tv_nav_matm2 = headerview.findViewById(R.id.tv_nav_matm2);
        tv_nav_cashout1 = headerview.findViewById(R.id.tv_nav_cashout1);
        tv_nav_cashout2 = headerview.findViewById(R.id.tv_nav_cashout2);
        tv_nav_wallet1 = headerview.findViewById(R.id.tv_nav_wallet1);
        tv_nav_wallet2 = headerview.findViewById(R.id.tv_nav_wallet2);
        tv_nav_commision1 = headerview.findViewById(R.id.tv_nav_commision1);
        tv_nav_commision2 = headerview.findViewById(R.id.tv_nav_commision2);
        tv_nav_ThemeDefault = headerview.findViewById(R.id.tv_nav_ThemeDefault);
        tv_nav_ThemeGreen = headerview.findViewById(R.id.tv_nav_ThemeGreen);
        tv_nav_ThemeYellow = headerview.findViewById(R.id.tv_nav_ThemeYellow);
        tv_nav_ThemeRed = headerview.findViewById(R.id.tv_nav_ThemeRed);
        tv_nav_add_money1 = headerview.findViewById(R.id.tv_nav_add_money1);
        tv_nav_add_money2 = headerview.findViewById(R.id.tv_nav_add_money2);

        tv_viewmore_viewless = headerview.findViewById(R.id.tv_viewmore_viewless);
        tv_cashout = headerview.findViewById(R.id.tv_cashout);
        tv_upi_settlement = headerview.findViewById(R.id.tv_upi_settlement);
        tv_showqr = headerview.findViewById(R.id.tv_showqr);
        tv_settings = headerview.findViewById(R.id.tv_settings);
        tv_help = headerview.findViewById(R.id.tv_help);
        tv_language = headerview.findViewById(R.id.tv_language);
        tv_privacy = headerview.findViewById(R.id.tv_privacy);
        tv_logout = headerview.findViewById(R.id.tv_logout);
        tv_change_password = headerview.findViewById(R.id.tv_change_password);
        rl_theme_default = headerview.findViewById(R.id.rl_theme_default);
        rl_theme_yellow = headerview.findViewById(R.id.rl_theme_yellow);
        rl_theme_green = headerview.findViewById(R.id.rl_theme_green);
        rl_theme_red = headerview.findViewById(R.id.rl_theme_red);
        rl_PermanentGeraniumLake = headerview.findViewById(R.id.rl_PermanentGeraniumLake);
    }


    private void loadDashboardFeature(String base64) {
        loadUserDashboard(base64);
    }

    private void loadUserDashboard(String base64) {
        try {

            if (pd != null) {
                pd = new ProgressDialog(MainActivity.this);
                pd.setMessage("Loading...");
                pd.setCancelable(false);
                pd.show();
            }

            StringRequest request = new StringRequest(Request.Method.GET, base64, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (!response.equals(null)) {

                        if (pd != null && pd.isShowing()) {
                            pd.dismiss();
                            pd.cancel();
                        }

                        reload.setVisibility(View.VISIBLE);

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            JSONObject _userInfo = jsonObject.optJSONObject("userInfo");
                            assert _userInfo != null;
                            _userName = _userInfo.optString("userName");
                            String _userType = _userInfo.optString("userType");
                            String _userBalance = _userInfo.optString("userBalance");
                            _adminName = _userInfo.optString("adminName");
                            String _mposNumber = _userInfo.optString("mposNumber");
                            String _promotionalMessage = _userInfo.optString("promotionalMessage");

                            String _userBrand = _userInfo.optString("userBrand");
                            String _userProfile = String.valueOf(_userInfo.getJSONObject("userProfile"));
                            String _userFeature = String.valueOf(_userInfo.getJSONArray("userFeature"));

                            //Storing User name in session
                            Constants.USER_NAME = _userName;
                            userName = _userName;

                            if (!_userType.equalsIgnoreCase("ROLE_RETAILER")) {
                                showExitUIDialog();
                            }

                            if (_userInfo.has("userBrand")) {

                                if (!_userBrand.equals("")) {
                                    try {
                                        JSONObject jsonObject1 = new JSONObject(_userInfo.getString("userBrand"));
                                        String brand = jsonObject1.getString("brand");
                                        // com.isuisudmt.utils.Constants.BRAND_NAME = brand;
                                        Constants.BRAND_NAME = brand;
                                        SdkConstants.BRAND_NAME = brand;
                                    } catch (Exception e) {

                                    }

                                } else {
                                    //  com.isuisudmt.utils.Constants.BRAND_NAME = "";
                                    Constants.BRAND_NAME = "";
                                    SdkConstants.BRAND_NAME = "";
                                }


                            } else {
                                //com.isuisudmt.utils.Constants.BRAND_NAME = "";
                                Constants.BRAND_NAME = "";
                                SdkConstants.BRAND_NAME = "";
                            }

                            JSONObject obj = new JSONObject(_userProfile);
                            String fname = obj.getString("firstName");
                            String lname = obj.getString("lastName");
                            String mobileNo = obj.getString("mobileNumber");
                            String shopName = obj.getString("shopName");
                            String emailIds = obj.getString("email");
                            Constants.SHOP_NAME = shopName;
                            Constants.USER_MOBILE_NO = mobileNo;

                            String profileName = fname + " " + lname;
                            fullName = fname + " " + lname;
                            _email = emailIds;
                            _firstName = fname;
                            _lastName = lname;

                            session.createUserSession(_userName, _userType, _userBalance, _adminName, _mposNumber, _userName, profileName, mobileNo);

                            //  das_user_name.setText(profileName);
                            das_user_balance.setText(_userBalance);
                            balance1 = _userBalance;
                            Constants.FULL_NAME = profileName;
                            nav_user_name.setText(profileName);

                            ArrayList<String> feature_array = new ArrayList<>();
                            JSONArray jsonArray = _userInfo.getJSONArray("userFeature");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                feature_array.add(jsonObject1.getString("id"));

                                //Checking for BBPS active status
                                if (jsonObject1.getString("id").equals(ID_BBPS)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        //rlMainViewPager.setVisibility(View.VISIBLE);
                                        isBBPSenabled = true;
                                    } else {
                                       // rlMainViewPager.setVisibility(View.GONE);
                                        isBBPSenabled = false;
                                    }
                                }

                                //Checking for ADM_AEPS  active status
                                if (jsonObject1.getString("id").equals(ID_AEPS1)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isAEPS1enabled = true;
                                    } else {
                                        isAEPS1enabled = false;
                                    }
                                }

                                //Checking for AEPS2  active status
                                if (jsonObject1.getString("id").equals(ID_AEPS2)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isAEPS2enabled = true;
                                    } else {
                                        isAEPS2enabled = false;
                                    }
                                }

                                //Checking for AEPS2  active status
                                if (jsonObject1.getString("id").equals(ID_AEPS3)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isAEPS3enabled = true;
                                    } else {
                                        isAEPS3enabled = false;
                                    }
                                }

                                //Checking for MATM_ADM active status
                                if (jsonObject1.getString("id").equals(ID_MATM1)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isMATM1enabled = true;
                                    } else {
                                        isMATM1enabled = false;
                                    }
                                }
                                //Checking for MATM2  active status
                                if (jsonObject1.getString("id").equals(ID_MATM2)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isMATM2enabled = true;
                                    } else {
                                        isMATM2enabled = false;
                                    }
                                }

                                //Checking for DMT active status
                                if (jsonObject1.getString("id").equals(ID_DMT)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isDMTenabled = true;
                                    } else {
                                        isDMTenabled = false;
                                    }
                                }

                                //Checking for CashoutWallet1 active status
                                if (jsonObject1.getString("id").equals(ID_CASHOUT_WALLET1)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isCashoutWallet1enabled = true;
                                    } else {
                                        isCashoutWallet1enabled = false;
                                    }
                                }

                                //Checking for CashoutWallet2 active status
                                if (jsonObject1.getString("id").equals(ID_CASHOUT_WALLET2)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isCashoutWallet2enabled = true;
                                    } else {
                                        isCashoutWallet2enabled = false;
                                    }
                                }

                                //Checking for Insurance active status
                                if (jsonObject1.getString("id").equals(ID_INSURANCE)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isInsuranceEnabled = true;
                                    } else {
                                        isInsuranceEnabled = false;
                                    }
                                }

                                //Recharge
                                if (jsonObject1.getString("id").equals(ID_RECHARGE)) {
                                    if (jsonObject1.getBoolean("active") == true) {
                                        isRechargeEnabled = true;
                                    } else {
                                        isRechargeEnabled = false;
                                    }
                                }


                            }
                            Constants.user_feature_array = feature_array;


                       /* if(_userName!=null) {
                            checkSessionExistance(_userName,true);
                        }*/
                            loadProfileImage();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            reload.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (pd != null && pd.isShowing()) {
                            pd.dismiss();
                            pd.cancel();
                        }
                        reload.setVisibility(View.VISIBLE);
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
//                    progressBar.setVisibility(View.GONE);
                        reload.setVisibility(View.VISIBLE);
                        if (!isFinishing() && !isDestroyed()) {
                            if (pd != null && pd.isShowing()) {
                                pd.dismiss();
                                pd.cancel();
                            }
                            sessionAlert(MainActivity.this);
                        }
                    } catch (Exception e) {

                    }

                }
            }) {
                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization", _token);
                    return params;
                }

                //Pass Your Parameters here
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse
                                                                        response) {
                    int statusCode = response.statusCode;
                    return super.parseNetworkResponse(response);
                }
            };

            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            queue.add(request);

        } catch (Exception e) {

        }

    }





    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.navigation_home:

                fragment_layout.setVisibility(View.GONE);
                collpase_toolbar.setVisibility(View.VISIBLE);
                home = "HOME";
                name.setText("");
                last_seen.setText("");
                scrollRange = -1;
                break;
            case R.id.navigation_aeps:
                try {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    //If 1 option is active than user will be able to go to Aeps Fragment.
                    if (isAEPS1enabled == true || isAEPS2enabled == true || isAEPS3enabled == true) {
                        home = "AEPS";

                        //  toolbar_layout.setVisibility(View.GONE);
                        fragmentManager = getSupportFragmentManager();
                        Fragment fragment = AepsbFragment.getInstance();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fragment_layout, fragment);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        showOption(R.id.add_money);
                        fragment_layout.setVisibility(View.VISIBLE);
                        collpase_toolbar.setVisibility(View.GONE);
                        scrollRange = 0;
                        transaction.commit();

                    } else {
                        Toast.makeText(this, ERROR_MESSAGE_DISABLED_AEPS, Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.navigation_matm:

                try {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }
                    if (isMATM1enabled == true || isMATM2enabled == true) {
                        home = "MATM";
                        fragmentManager = getSupportFragmentManager();
                        Fragment fragment = MatmbFragment.getInstance();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fragment_layout, fragment);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        showOption(R.id.add_money);
                        transaction.commit();
                        fragment_layout.setVisibility(View.VISIBLE);
                        collpase_toolbar.setVisibility(View.GONE);
                        scrollRange = 0;

                    } else {
                        Toast.makeText(this, ERROR_MESSAGE_DISABLED_MATM, Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.navigation_dmt:

                try {
                    if (timer_handler != null) {
                        timer_handler.removeCallbacksAndMessages(null);
                    }

                    if (isDMTenabled == true) {
                        home = "DMT";
                        fragmentManager = getSupportFragmentManager();
                        Fragment fragment = DmtbFragment.getInstance();
                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.fragment_layout, fragment);
                        transaction.commit();
                        fragment_layout.setVisibility(View.VISIBLE);
                        name.setText("W1 \u20B9 " + balance1);
                        last_seen.setText("W2 \u20B9 " + balance2);
                        showOption(R.id.add_money);
                        scrollRange = 0;

                    } else {
                        Toast.makeText(this, ERROR_MESSAGE_DISABLED_DMT, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.setting:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
               /* startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                finish();*/
                Intent settingsIntent = new Intent(MainActivity.this, SettingsNewActivity.class);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(settingsIntent);
                overridePendingTransition(0, 0);

                break;
            /*case R.id.report:
                if(timer_handler!=null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                Intent _intent = new Intent(MainActivity.this, ReportDashboardActivity.class);
                startActivity(_intent);
                break;*/

            //For Report
            /*case R.id.aeps:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;
            case R.id.matm:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;
            case R.id.dmt:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;
            case R.id.bbps:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;*/
            case R.id.mobile_prepaid:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                // startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;
            /*case R.id.item_cashout:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;*/
            case R.id.wallet:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                //  startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;
            case R.id.insurance:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
                //  startActivity(new Intent(MainActivity.this, ReportDashboardActivity.class));
                break;


            case R.id.cashout:
                if (timer_handler != null) {
                    timer_handler.removeCallbacksAndMessages(null);
                }
//                Intent __intent = new Intent(MainActivity.this, SelectCashoutOptionActivity.class);
                Intent __intent = new Intent(MainActivity.this, SelectCashoutWalletOptionActivity.class);
                startActivity(__intent);
                break;
           /* case R.id.report_matm:
                Intent report_matm = new Intent(MainActivity.this, MicroAtmReportActivity.class);
                startActivity(report_matm);
                break;
            case R.id.report_dmt:
//                Intent report_dmt = new Intent(MainActivity.this, FundTransferReport.class);
//                startActivity(report_dmt);
                break;*/


        }

        drawer.closeDrawers();

        return true;
    }

    private void hideOptionBottomNavigation(int id, boolean visibility) {
        Menu nav_Menu = navigation.getMenu();
        //nav_Menu.findItem(id).setVisible(visibility);
        nav_Menu.findItem(id).setEnabled(visibility);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        // return true;
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        hideOption(R.id.add_money);
        //hideOptionDMT();

        return true;
    }

    @Override
    public void fetchedHomeDashboardResponse(boolean status, String response) {
        loadDashboardFeature(response);
        getWallet2Balance();
    }


    private void loadProfileImage() {

        checkUserRole(_userName);
        //startTimer();
    }

    public void checkUserRole(final String admin_name) {
        try {

            AndroidNetworking.get(GET_USER_ROLE)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                getAdminDetails(encodedUrl, admin_name);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAdminDetails(final String encoded_url, final String admin_name) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", admin_name);

            AndroidNetworking.post(encoded_url)
                    .addHeaders("Authorization", _token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //  {"masterDistributor":"gsgswipew"}
                                JSONObject obj = new JSONObject(response.toString());

                                String status_master_distrubuter_value = obj.getString("masterDistributor");

                                if (status_master_distrubuter_value.trim().length() != 0) {
                                    image_url = "https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/MASTERDISTRIBUTOR_PROFILE%2F" + status_master_distrubuter_value + "%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";

                                } else {
                                    image_url = "https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/ADMIN_PROFILE%2F" + _adminName + "%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorBody();

                            image_url = "https://firebasestorage.googleapis.com/v0/b/iserveu_storage/o/ADMIN_PROFILE%2F" + _adminName + "%2FprofileImg.png?alt=media&token=2bc9b5da-1985-4152-8cc3-45ebc1b72ab6";


                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sessionAlert(Context context) {
        try {

            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            String message = "Session Expired, Please login again.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                        /*if(_userName!=null && session_logout==false) {
                            //checkSessionExistance(_userName,false);
                        }*/
                            Intent i = new Intent(MainActivity.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
//                    .show();
        } catch (Exception e) {

        }
    }

    public void showLogoutAlert() {
        Constants.selected_fingerPrint = null;
        Constants.selected_btdevice = null;
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            String message = "Do you want to logout from app";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                        /*if(_userName!=null && session_logout==false) {
                            //checkSessionExistance(_userName,false);
                        }*/
                            setFBAnalytics("MAIN_LOGOUT_YES", userName);
                            Const.viewRequiredInfoResponse = "";
                            Const.isUpdatedResponse = "";
                            Const.usernameForBBPS = "";
                            Const.Wallet2Amount = 0.0;

                            SdkConstants.LogOut = "0"; //For logout from SDK end

                            SdkConstants.BlueToothPairFlag = "0";
                            Constants.bluetoothDevice = null;
                            SdkConstants.bluetoothDevice = null;
                            Constants.selected_fingerPrint = null;
                            Constants.selected_btdevice = null;
                            sharePreferenceClass.clearData();


                            Intent i = new Intent(MainActivity.this, LoginActivity.class);
                            // set the new task and clear flags
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            setFBAnalytics("MAIN_LOGOUT_NO", userName);
                            dialog.dismiss();

                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (home.equalsIgnoreCase("HOME")) {
            showExitDialog();
            name.setText("");
            last_seen.setText("");
            hideOption(R.id.add_money);
            navigation.setSelectedItemId(R.id.navigation_home);
        } else if (home.equalsIgnoreCase("AEPS")) {
            fragment_layout.setVisibility(View.GONE);
            home = "HOME";
            navigation.setSelectedItemId(R.id.navigation_home);
        } else if (home.equalsIgnoreCase("MATM")) {
            fragment_layout.setVisibility(View.GONE);
            home = "HOME";
            navigation.setSelectedItemId(R.id.navigation_home);
        } else if (home.equalsIgnoreCase("DMT")) {
            fragment_layout.setVisibility(View.GONE);
            home = "HOME";
            navigation.setSelectedItemId(R.id.navigation_home);
        } else {
            showExitDialog();
        }


    }

    private void showExitDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage("Do you want to exit from this app");
        builder1.setTitle("Exit");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        SdkConstants.LogOut = "0"; //For logout from SDK end
                        SdkConstants.BlueToothPairFlag = "0";

                        dialog.cancel();
                        finish();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public void getWallet2Balance() {
        try {
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v72")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                getWallet2BalanceEncripted(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getWallet2BalanceEncripted(String encodedUrl) {
        // showLoader();
        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        // hideLoader();
                        reload2.setVisibility(View.VISIBLE);
                        progressBar2.setVisibility(View.GONE);
                        String wallet2_balance = response;
                        Double wallet2_bal = Double.valueOf(wallet2_balance);
                        das_user_balance2.setText("₹ " + wallet2_balance);
                        das_user_balance2.setText(wallet2_balance);
                        balance2 = String.valueOf(wallet2_bal);
                        Const.Wallet2Amount = wallet2_bal;

                        //getWallet1Balance();
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        // hideLoader();
                    }
                });
    }


    private void isApiCalledInSession(Intent intent, String CategoryID) {
        if (Const.isUpdatedResponse.equals("")) {
            mActionsListener.apiIsUpdated(intent, CategoryID, tokenStr);
        } else {
            //Perform here
            try {
                JSONObject objIsUpdated = new JSONObject(Const.isUpdatedResponse);
                int status = objIsUpdated.getInt("status");
                String statusDescription = objIsUpdated.getString("statusDescription");

                if (status == 0) {
                    if (Const.viewRequiredInfoResponse.equals(""))
                        mActionsListener.apiViewRequiredInfo(intent, userName, CategoryID, tokenStr);
                    else
                        intentToActivity(Const.viewRequiredInfoResponse, intent, CategoryID);

                } else if (status == 1) {
                    String userNameData = objIsUpdated.getString("userNameData");

                    if (!userNameData.equals("")) {
                        Const.usernameForBBPS = userNameData;

                        if (Const.viewRequiredInfoResponse.equals(""))
                            mActionsListener.apiViewRequiredInfo(intent, userNameData, CategoryID, tokenStr);
                        else
                            intentToActivity(Const.viewRequiredInfoResponse, intent, CategoryID);

                    } else {
                        Toast.makeText(MainActivity.this, "" + statusDescription, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "" + statusDescription, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    private void intentToActivity(String response, Intent intent, String CategotyID) {
        try {
            JSONObject objViewRqrdDetails = new JSONObject(response);
            String _status = objViewRqrdDetails.getString("status");
            String pincode = objViewRqrdDetails.getString("pincode");
            String mobilenumber = objViewRqrdDetails.getString("mobilenumber");
            String latlong = objViewRqrdDetails.getString("latlong");
            String terminalid = objViewRqrdDetails.getString("terminalid");
            String agentid = objViewRqrdDetails.getString("agentid");
            String city = objViewRqrdDetails.getString("city");
            String state = objViewRqrdDetails.getString("state");
            String keyword = objViewRqrdDetails.getString("keyword");
            String accountcode = objViewRqrdDetails.getString("accountcode");

            SharedPreferences.Editor editor = spBbps.edit();
            editor.putString(SF_PINCODE, pincode);
            editor.putString(SF_MOBILE_NUMBER, mobilenumber);
            editor.putString(SF_LAT_LONG, latlong);
            editor.putString(SF_TERMINAL_ID, terminalid);
            editor.putString(SF_AGENT_ID, agentid);
            editor.putString(SF_KEYWORD, keyword);
            editor.apply();

            //Intent
            intent.putExtra("CategoryID", CategotyID);
            startActivity(intent);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showExitUIDialog() {
        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
            builder1.setMessage("You are not authorised User to access this application");
            builder1.setTitle("Alert");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SdkConstants.LogOut = "0"; //For logout from SDK end
                            SdkConstants.BlueToothPairFlag = "0";
                            dialog.cancel();
                            finish();
                        }
                    });
            builder1.setNegativeButton(
                    "No",
                    (dialog, id) -> dialog.cancel());
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } catch (Exception ignored) {
        }
    }

    public void showAlertCommmingSoon(Context context, String statusDesc, String PropertyKey) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert!")
                    .setMessage(statusDesc)
                    .setPositiveButton(context.getResources().getString(R.string.ok), (dialog, which) -> {
                        // continue with delete
                        setFBAnalytics(PropertyKey, userName);
                        dialog.dismiss();
                        //finish();
                    })
                    .setNegativeButton(context.getResources().getString(R.string.cancel), (dialog, which) -> {
                        dialog.dismiss();
                        //finish();
                    });


//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception ignored) {

        }
    }


    public void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }

    @Override
    public void onReadyIsUpdated(JSONObject response, Intent intent, String CatID) {
        try {
            Const.isUpdatedResponse = response.toString();
            int status = response.getInt("status");
            String statusDescription = response.getString("statusDescription");

            if (status == 0) {
                Const.usernameForBBPS = "";
                mActionsListener.apiViewRequiredInfo(intent, userName, CatID, tokenStr);

            } else if (status == 1) {
                String userNameData = response.getString("userNameData");
                if (!userNameData.equals("")) {
                    Const.usernameForBBPS = userNameData;
                    mActionsListener.apiViewRequiredInfo(intent, userNameData, CatID, tokenStr);
                } else {
                    Toast.makeText(MainActivity.this, "" + statusDescription, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MainActivity.this, "" + statusDescription, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onErrorIsUpdated(String ErrorMsg) {
        try {
            JSONObject errorObject = new JSONObject(ErrorMsg);
            String statusDescription = errorObject.optString("statusDescription");
            String statusDesc = errorObject.optString("statusDesc");
            if (statusDescription.equals(""))
                Toast.makeText(MainActivity.this, statusDesc, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(MainActivity.this, statusDescription, Toast.LENGTH_LONG).show();
        } catch (Exception e) {

        }

    }

    @Override
    public void onReadyViewRequiredInfo(JSONObject response, Intent intent, String CatID) {
        Const.viewRequiredInfoResponse = response.toString();
        intentToActivity(response.toString(), intent, CatID);
    }

    @Override
    public void onErrorViewRequiredInfo(String ErrorMsg) {
        try {
            JSONObject errorObject = new JSONObject(ErrorMsg);
            String statusDescription = errorObject.optString("statusDescription");
            String statusDesc = errorObject.optString("statusDesc");
            if (statusDescription.equals(""))
                Toast.makeText(MainActivity.this, statusDesc, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(MainActivity.this, statusDescription, Toast.LENGTH_LONG).show();

        } catch (Exception e) {

        }
    }

    @Override
    public void hideLoader() {
        dialog.cancel();
    }

    @Override
    public void showLoader() {
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
    }
}
