package com.isuisudmt;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.isuisudmt.cahoutwallet.WalletCashoutActivity;
import com.isuisudmt.cashoutbank.WalletToBankActivity;

import java.util.HashMap;


public class SelectCashoutOptionFragment extends Fragment {

    RelativeLayout bank_trans_lay,wallet_trans_lay;
    ImageView back;
    Handler timer_handler;
    String _userName;
    Toolbar toolbar;
    boolean session_logout = false;
    SessionManager session;
    View view;

    public static Fragment getInstance() {
        return new SelectCashoutOptionFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        new CustomThemes(getActivity());
        View rootview =  inflater.inflate(R.layout.activity_select_cashout_option, container, false);



        session = new SessionManager(getActivity());

        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);
        bank_trans_lay = rootview.findViewById(R.id.bank_trans_lay);
        wallet_trans_lay = rootview.findViewById(R.id.wallet_trans_lay);
        view = rootview.findViewById(R.id.view);
       /* toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        if(Constants.user_feature_array!=null) {
            if (Constants.user_feature_array.contains("50")) {
                bank_trans_lay.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
            }
        }
        bank_trans_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), WalletToBankActivity.class));
            }
        });
        wallet_trans_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), WalletCashoutActivity.class));
            }
        });
        return rootview;
    }



    
}