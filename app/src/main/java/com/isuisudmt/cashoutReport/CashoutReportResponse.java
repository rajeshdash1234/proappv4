package com.isuisudmt.cashoutReport;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CashoutReportResponse {

    @SerializedName("BQReport")
    private ArrayList<CashoutReportModel> mATMTransactionReport;

    public ArrayList<CashoutReportModel> getmATMTransactionReport() {
        return mATMTransactionReport;
    }

    public void setmATMTransactionReport(ArrayList<CashoutReportModel> mATMTransactionReport) {
        this.mATMTransactionReport = mATMTransactionReport;
    }

    public CashoutReportResponse() {
    }

}
