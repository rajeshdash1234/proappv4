package com.isuisudmt.cashoutReport;

import com.isuisudmt.matm.RefreshModel;

import java.util.ArrayList;

public class CashoutReportContract {

    public interface View {


        void reportsReady(ArrayList<CashoutReportModel> reportModelArrayList, String totalAmount);

        void refreshDone(RefreshModel refreshModel);

        void showReports();

        void showLoader();

        void hideLoader();

        void emptyDates();

        void checkAmount(String status);

        void checkTransactionMode(String status);

        void checkTransactionType(String status);

        void checkClientId(String status);

        void emptyRefreshData(String status);


    }

    /**
     * UserActionsListener interface checks the load of Reports
     */
    interface UserActionsListener {
        void loadReports(String fromDate, String toDate, String token, String transactionType, String userName);

        //void loadReports(String fromDate, String toDate, String token, String transactionType,String userName);
        void refreshReports(String token, String amount, String transactionType, String transactionMode, String clientUniqueId);
    }

}
