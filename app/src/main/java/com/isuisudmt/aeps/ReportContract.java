package com.isuisudmt.aeps;

import java.util.ArrayList;


public class ReportContract {

    public interface View {

        void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount);
        void AEPS2reportsReady(ArrayList<AEPS2ReportModel> reportModelArrayList, String totalAmount);
        void showReports();
        void showLoader();
        void hideLoader();
        void emptyDates();


    }

    interface UserActionsListener {
        void loadReports(String fromDate, String toDate, String token, String providerType);
    }


}

