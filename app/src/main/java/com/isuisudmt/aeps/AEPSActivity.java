package com.isuisudmt.aeps;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.Constants;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.bluetooth.SharePreferenceClass;
import com.isuisudmt.fingerprintmodel.MorphoPidData;
import com.isuisudmt.fingerprintmodel.PidData;
import com.isuisudmt.utility.ConnectionDetector;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;
import com.matm.matmsdk.aepsmodule.BioAuthActivity;
import com.matm.matmsdk.aepsmodule.CoreAEPSHomeActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.simpleframework.xml.Serializer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.isuisudmt.BuildConfig.LOGIN_DETAILS_URL;
import static com.isuisudmt.Constants.bluetoothConnector;

public class AEPSActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    String strTransType = "Cash Withdrawal";
    String strAepsType = "";
    private ProgressDialog dialog;
    RadioGroup rg_trans_type;
    RadioButton rb_cw, rb_be, rb_mini, rb_aadhaarPay;
    Handler timer_handler;
    FrameLayout matm_cashwithdraw, matm_reqbalance;
    TextInputEditText amnt_txt;
    Button submit;
    TextView enter_text;
    AlertDialog deleteDialog;
    SessionManager sessionManager;
    String _token = "";
    String _admin = "";
    String _userName;
    boolean session_logout = false;
    boolean aeps_transaction_intent = false;
    ImageView two_fact_fingerprint;
    // HorizontalProgressView two_fact_depositBar;
    Button two_fact_submitButton;
    ProgressDialog progressDialog;
    SharePreferenceClass sharePreferenceClass;
    private Boolean location_flag = false;
    String deviceSerialNumber = "0";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";
    UsbManager musbManager;
    private UsbDevice usbDevice;
    boolean usbconnted = false;
    private ArrayList<String> positions;
    String balanceaadharNo = "";
    private Serializer serializer = null;
    private PidData pidData = null;
    private MorphoPidData morphoPidData;
    public static String mantra_result = "";
    AlertDialog deleteDialogg;
    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    String postalCode = "751017";
    private static final int REQUEST_CA_PERMISSIONS = 931;
    int pincode = 751017;
    String manufactureFlag = "";
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_aeps);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        dialog = new ProgressDialog(AEPSActivity.this);
        positions = new ArrayList<>();
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Aadhaar eBanking");
        setSupportActionBar(toolbar);
        setUpGClient();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sessionManager = new SessionManager(AEPSActivity.this);

        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);


        HashMap<String, String> user = sessionManager.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user_ = sessionManager.getUserSession();
        _userName = user_.get(sessionManager.userName);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sharePreferenceClass = new SharePreferenceClass(AEPSActivity.this);

        submit = findViewById(R.id.submit);
        matm_cashwithdraw = findViewById(R.id.matm_cashwithdraw);
        matm_reqbalance = findViewById(R.id.matm_reqbalance);
        amnt_txt = findViewById(R.id.price);
        rb_cw = findViewById(R.id.rb_cw);
        rb_be = findViewById(R.id.rb_be);
        rb_mini = findViewById(R.id.rb_mini);
        rb_aadhaarPay = findViewById(R.id.rb_aadhaarPay);
        rg_trans_type = findViewById(R.id.rg_trans_type);
        enter_text = findViewById(R.id.enter_text);
       /* musbManager = (UsbManager) getSystemService( Context.USB_SERVICE);
        updateDeviceList ();*/

        rg_trans_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_cw:
                        strTransType = "Cash Withdrawal";
                        amnt_txt.setText("");
                        amnt_txt.setEnabled(true);
                        amnt_txt.setVisibility(View.VISIBLE);
                        enter_text.setVisibility(View.VISIBLE);
                        amnt_txt.setInputType(InputType.TYPE_CLASS_NUMBER);

                        break;
                    case R.id.rb_be:
                        strTransType = "Balance Enquiry";
                        amnt_txt.setText("");
                        amnt_txt.setVisibility(View.GONE);
                        enter_text.setVisibility(View.GONE);
                        amnt_txt.setClickable(false);
                        amnt_txt.setEnabled(false);
                        break;
                    case R.id.rb_mini:
                        strTransType = "Mini Statement";
                        amnt_txt.setText("");
                        amnt_txt.setVisibility(View.GONE);
                        enter_text.setVisibility(View.GONE);
                        amnt_txt.setClickable(false);
                        amnt_txt.setEnabled(false);
                        break;
                    case R.id.rb_aadhaarPay:
                        strTransType = "AadhaarPay";
                        amnt_txt.setText("");
                        amnt_txt.setEnabled(true);
                        amnt_txt.setVisibility(View.VISIBLE);
                        enter_text.setVisibility(View.VISIBLE);
                        amnt_txt.setInputType(InputType.TYPE_CLASS_NUMBER);


                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                ConnectionDetector cd = new ConnectionDetector(
                        AEPSActivity.this);
                if (cd.isConnectingToInternet()) {
                    getMyLocation();
                    if (rb_cw.isChecked() || rb_aadhaarPay.isChecked()) {
                        String amountString = Objects.requireNonNull(amnt_txt.getText().toString().trim());
                        if (amountString.equalsIgnoreCase("")) {
//                        if (amountString.length() == 0) {
                            amnt_txt.setError("Please enter minimum Rs 1 ");
                            //Toast.makeText(AEPSActivity.this, "Please enter minimum Rs 1 ", Toast.LENGTH_SHORT).show();
                        } else if (Double.valueOf(amountString) < 1) {
                            amnt_txt.setError("Please enter minimum Rs 1 ");
                            // Toast.makeText(AEPSActivity.this, "Please enter minimum Rs 1 ", Toast.LENGTH_SHORT).show();
                        } else if (amountString.startsWith("0")) {
                            amnt_txt.setError("Please don't enter zero at begining !");
                        } else {
                            setFBAnalytics("AEPS_CW_SUBMIT", _userName);

                            Intent intent = new Intent(AEPSActivity.this, AepsOptionActivity.class);
                            intent.putExtra("AMOUNT", amnt_txt.getText().toString().trim());
                            intent.putExtra("AEPSTRANS_TYPE", strTransType);
                            startActivity(intent);

//                            showChooseWalletOption();
                        }
                    } else if (rb_aadhaarPay.isChecked()) {
                        String amountString = Objects.requireNonNull(amnt_txt.getText().toString().trim());
                        if (amountString.equalsIgnoreCase("")) {
                            amnt_txt.setError("Please enter minimum Rs 100 ");
                        } else if (Double.valueOf(amountString) < 100) {
                            amnt_txt.setError("Please enter minimum Rs 100 ");
                        } else if (amountString.startsWith("0")) {
                            amnt_txt.setError("Please don't enter zero at begining !");
                        } else {
                            Intent intent = new Intent(AEPSActivity.this, AepsOptionActivity.class);
                            intent.putExtra("AMOUNT", amnt_txt.getText().toString().trim());
                            intent.putExtra("AEPSTRANS_TYPE", strTransType);
                            startActivity(intent);
                        }

                    } else {
                        setFBAnalytics("AEPS_BE_SUBMIT", _userName);

                        Intent intent = new Intent(AEPSActivity.this, AepsOptionActivity.class);
                        intent.putExtra("AMOUNT", amnt_txt.getText().toString().trim());
                        intent.putExtra("AEPSTRANS_TYPE", strTransType);
                        startActivity(intent);

//                        showChooseWalletOption();
                    }
                } else {
                    Toast.makeText(AEPSActivity.this, "No internet connection", Toast.LENGTH_LONG).show();

                }
            }
        });

    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(amnt_txt.getWindowToken(), 0);
    }
    /*public void showChooseWalletOption(){
        try{
            LayoutInflater factory = LayoutInflater.from(AEPSActivity.this);
            final View deleteDialogView = factory.inflate(R.layout.dialog_aeps_info, null);
            deleteDialog = new AlertDialog.Builder(this,android.R.style.Theme_Black_NoTitleBar_Fullscreen).create();
            deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            deleteDialog.setView(deleteDialogView);
            deleteDialog.setCancelable(false);
            RelativeLayout item1 = deleteDialogView.findViewById(R.id.item1);
            RelativeLayout item2 = deleteDialogView.findViewById(R.id.item2);
            TextView service_txt_1 = deleteDialogView.findViewById(R.id.service_txt_1);
            TextView service_txt_2 = deleteDialogView.findViewById(R.id.service_txt_2);

            ImageView back_image = deleteDialogView.findViewById(R.id.close_btn);

            if(Constants.user_feature_array!=null) {
                if (!Constants.user_feature_array.contains("27")) {
                    item1.setBackgroundResource(R.color.service_unavailable_col);
                    service_txt_1.setVisibility(View.VISIBLE);
                    item1.setEnabled(false);
                }

                if(!Constants.user_feature_array.contains("47")){
                    item2.setBackgroundResource(R.color.service_unavailable_col);
                    service_txt_2.setVisibility(View.VISIBLE);
                    item2.setEnabled(false);
                }
            }

            item1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    item1.setBackground(getResources().getDrawable(R.drawable.ripple_effect));

//                SdkConstants.transactionAmount = ""+Long.parseLong(amnt_txt.getText().toString().trim());

                    if(biometricDeviceConnect()){
                        refreshToken("AEPS1");
                    }else{
                        Toast.makeText(AEPSActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            item2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    item2.setBackground(getResources().getDrawable(R.drawable.ripple_effect));



                    if(biometricDeviceConnect()){
                        refreshToken("AEPS2");
                    }else{
                        Toast.makeText(AEPSActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            deleteDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                        try {
                            if(!isFinishing() && !isDestroyed()){
                                if(deleteDialog != null && deleteDialog.isShowing()){
                                    deleteDialog.dismiss();
                                }else {
                                    finish();
                                }
                            } }catch (Exception e){

                        }
                    return false;
                }
            });
            back_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });
            deleteDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/


    public void showChooseWalletOption() {
        try {
            LayoutInflater factory = LayoutInflater.from(AEPSActivity.this);
            final View deleteDialogView = factory.inflate(R.layout.dialog_aeps_radio, null);
            deleteDialog = new AlertDialog.Builder(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen).create();
            deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            deleteDialog.setView(deleteDialogView);
            deleteDialog.setCancelable(false);

            RadioGroup rg_aeps_type = deleteDialogView.findViewById(R.id.rg_aeps_type);
            RadioButton rb_aeps1 = deleteDialogView.findViewById(R.id.rb_aeps1);
            RadioButton rb_aeps2 = deleteDialogView.findViewById(R.id.rb_aeps2);
            Button submitAeps = deleteDialogView.findViewById(R.id.submitAEPS);
            ImageView back_image = deleteDialogView.findViewById(R.id.close_btn);
            submitAeps.setEnabled(true);

            if (Constants.user_feature_array != null) {
                if (!Constants.user_feature_array.contains("27")) {
                    rb_aeps1.setBackgroundResource(R.color.service_unavailable_col);
                    rb_aeps1.setEnabled(false);
                }

                if (!Constants.user_feature_array.contains("47")) {
                    rb_aeps2.setBackgroundResource(R.color.service_unavailable_col);
                    rb_aeps2.setEnabled(false);
                }
            }

            rg_aeps_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.rb_aeps1:
                            strAepsType = "AEPS1";
                            submitAeps.setEnabled(true);

                            break;
                        case R.id.rb_aeps2:
                            strAepsType = "AEPS2";
                            submitAeps.setEnabled(true);
                            break;

                    }
                }
            });


            submitAeps.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitAeps.setEnabled(false);
                    if (rb_aeps1.isChecked()) {


                        if (bluetoothConnector) {
                            showLoader();
                            refreshToken(strAepsType);
                        } else {
                            if (biometricDeviceConnect()) {
                                showLoader();
                                refreshToken(strAepsType);
                            } else {
                                Toast.makeText(AEPSActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();

                            }


                        }


                    } else if (rb_aeps2.isChecked()) {
                        if (bluetoothConnector) {
                            showLoader();
                            refreshToken(strAepsType);
                        } else {
                            if (biometricDeviceConnect()) {
                                showLoader();
                                refreshToken(strAepsType);
                            } else {
                                Toast.makeText(AEPSActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();

                            }


                        }
                    }
                }
            });

            back_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();

                }
            });

            deleteDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                        try {
                            if (!isFinishing() && !isDestroyed()) {
                                if (deleteDialog != null && deleteDialog.isShowing()) {
                                    deleteDialog.dismiss();
                                } else {
                                    finish();
                                }
                            }
                        } catch (Exception e) {

                        }
                    return false;
                }
            });

            deleteDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * AEPS2 Location Auth
     */
    public void showPinCodeDialog() {

        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.dialog_otp_info, null);
        deleteDialogg = new AlertDialog.Builder(this).create();
        deleteDialogg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        deleteDialogg.setView(deleteDialogView);
        deleteDialogg.setCancelable(false);
        final EditText otp_edt = deleteDialogView.findViewById(R.id.otp_edt);

        deleteDialogView.findViewById(R.id.process_profile_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic

                if (otp_edt.getText().toString().length() != 0 && !otp_edt.getText().toString().contains(".")) {
                    //verifyOTPMobile(mobileno,otp_edt.getText().toString());
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("pin", Integer.valueOf(otp_edt.getText().toString()));
                        getAddressFromPin(jsonObject, _token);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(AEPSActivity.this, "Please insert area pin or check pin", Toast.LENGTH_SHORT).show();
                }

            }
        });

        deleteDialogView.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialogg.dismiss();
            }
        });
        deleteDialogg.show();

    }

    private void checkAddressStatus(String _token) {
        // showLoader();
        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v81")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            checkAddressStatus1(encodedUrl, _token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AEPSActivity.this, "Service is unavailable for this user, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void checkAddressStatus1(String url, String _token) {
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (obj.has("response")) {
                                JSONObject res_obj = obj.getJSONObject("response");
                                String pin = res_obj.getString("pincode");
                                String state = res_obj.getString("state");
                                String city = res_obj.getString("city");
                                boolean bio_auth_status = res_obj.getBoolean("bioauth");
                                //if(pin.equalsIgnoreCase("null") || pin.length()==0 || pin == null || state.length() ==0 || city.length()==0) {
                                if (!bio_auth_status) {
                                    JSONObject jsonObject = new JSONObject();

                                    if (postalCode.length() != 0) {
                                        pincode = Integer.valueOf(postalCode);
                                    }
                                    jsonObject.put("pin", pincode);
                                    getAddressFromPin(jsonObject, _token);
                                    // showFingurePrintDialog();
                                    //showPinCodeDialog();
                                } else {
                                    hideLoader();
//                                    sendAEPS2Intent(false,_token);
                                }
                            } else {

                                JSONObject jsonObject = new JSONObject();
                                if (postalCode.length() != 0) {
                                    pincode = Integer.valueOf(postalCode);
                                }
                                jsonObject.put("pin", pincode);
                                getAddressFromPin(jsonObject, _token);
                                //showPinCodeDialog();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                            Toast.makeText(AEPSActivity.this, "Something went wrong, please contact our help desk for details.", Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AEPSActivity.this, "Service is unavailable for this user, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getAddressFromPin(JSONObject obj, String _token) {
        // showLoader();
        AndroidNetworking.post("https://us-central1-iserveustaging.cloudfunctions.net/pincodeFetch/api/v1/getCitystateAndroid")
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            JSONObject data_obj = obj.getJSONObject("data");
                            String status = data_obj.getString("status");
                            if (status.equalsIgnoreCase("success")) {
                                JSONObject data_pin = data_obj.getJSONObject("data");
                                String pincode = data_pin.getString("pincode");
                                String state = data_pin.getString("state");
                                String city = data_pin.getString("city");
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("state", state);
                                jsonObject.put("pincode", pincode);
                                jsonObject.put("city", city);
                                String lat = "0.0";
                                String lng = "0.0";

                                if (mylocation != null) {
                                    lat = String.valueOf(mylocation.getLatitude());
                                    lng = String.valueOf(mylocation.getLongitude());
                                }
                                jsonObject.put("latLong", lat + "," + lng);
                                setAddress(jsonObject, _token);
                            } else {
                                hideLoader();
                                Toast.makeText(AEPSActivity.this, "Invalid area pin, please try after sometimes", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AEPSActivity.this, "Invalid area pin, please try again.", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void setAddress(JSONObject objj, String _token) {

        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v82")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            setAddress1(objj, encodedUrl, _token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }


    private void setAddress1(JSONObject obj, String url, String _token) {

        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());

                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (status.equalsIgnoreCase("0")) {
                                // deleteDialogg.dismiss();
                                // showFingurePrintDialog();
                                sendAEPS2Intent(true, _token);
                            } else {
                                Toast.makeText(AEPSActivity.this, statusDesc, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }


    public void sendAEPS2Intent(boolean bioauth, String _token) {

        try {


            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _admin;
            SdkConstants.applicationUserName = _userName;
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
            SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";
            SdkConstants.bioauth = bioauth;
            aeps_transaction_intent = true;
            if (rb_cw.isChecked()) {
                SdkConstants.transactionType = SdkConstants.cashWithdrawal;
                SdkConstants.transactionAmount = "" + Long.parseLong(amnt_txt.getText().toString().trim());
                if (!bioauth) {
                    Intent intent = new Intent(AEPSActivity.this, BioAuthActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                } else {
                    hideLoader();
                    Intent intent = new Intent(AEPSActivity.this, AEPS2HomeActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                }


            }

            if (rb_be.isChecked()) {
                SdkConstants.transactionType = SdkConstants.balanceEnquiry;
                SdkConstants.transactionAmount = "1";
                if (!bioauth) {
                    Intent intent = new Intent(AEPSActivity.this, BioAuthActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                } else {
                    hideLoader();
                    Intent intent = new Intent(AEPSActivity.this, AEPS2HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                }

            }
            if (rb_mini.isChecked()) {
                SdkConstants.transactionType = SdkConstants.ministatement;
                SdkConstants.transactionAmount = "1";
                if (!bioauth) {
                    Intent intent = new Intent(AEPSActivity.this, BioAuthActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                } else {
                    hideLoader();
                    Intent intent = new Intent(AEPSActivity.this, AEPS2HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                }


            }

        } catch (Exception e) {

        }

    }


   /* @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        if(_userName!=null) {
            aeps_transaction_intent = false;
            if(session_logout==false) {
                checkSessionExistance(_userName, true);
            }
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(!aeps_transaction_intent) {
            if (timer_handler != null) {
                timer_handler.removeCallbacksAndMessages(null);
            }
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false && aeps_transaction_intent ==false) {
            checkSessionExistance(_userName,false);
        }
        if(aeps_transaction_intent){
            startTimer();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!aeps_transaction_intent) {
            if (timer_handler != null) {
                timer_handler.removeCallbacksAndMessages(null);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!aeps_transaction_intent) {
            if (timer_handler != null) {
                timer_handler.removeCallbacksAndMessages(null);
            }
        }
        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
    }

    public void startTimer(){
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(AEPSActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(AEPSActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(AEPSActivity.this, user_name, status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(AEPSActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/


    public void showLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
                dialog = new ProgressDialog(AEPSActivity.this);
                dialog.setMessage("please wait..");
                dialog.setCancelable(false);
                dialog.show();
            }
        } catch (Exception e) {

        }
    }

    public void hideLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {

        }
    }


    /*  *//*
     *  url for the sync of the data for the
     *//*

    private String getAuthURL(String UID) {
        String url = "http://developer.uidai.gov.in/auth/";
        url += "public/" + UID.charAt(0) + "/" + UID.charAt(1) + "/";
        url += "MG41KIrkk5moCkcO8w-2fc01-P7I5S-6X2-X7luVcDgZyOa2LXs3ELI"; //ASA
        return url;
    }*/

 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                       hideLoader();
                    }
                }
                break;
            case 2:
                hideLoader();
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );
                            mantra_result = result;
                            if (result != null) {
                                pidData = serializer.read ( PidData.class, result );

                                if(Float.parseFloat ( pidData._Resp.qScore ) <=60){


                                  *//*  if(two_fact_depositBar!=null) {
     *//**//*    two_fact_depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_depositBar.setProgressTextMoved(true);
                                        two_fact_depositBar.setEndColor(getResources().getColor(R.color.red));
                                        two_fact_depositBar.setStartColor(getResources().getColor(R.color.red));
                                   *//**//* }*//*


                                }else if(Float.parseFloat ( pidData._Resp.qScore ) >=60 && Float.parseFloat ( pidData._Resp.qScore ) <=70){

                                    *//*if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_depositBar.setProgressTextMoved(true);
                                        two_fact_depositBar.setEndColor(getResources().getColor(R.color.yellow));
                                        two_fact_depositBar.setStartColor(getResources().getColor(R.color.yellow));
                                        two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                    }*//*


                                }else{

                                   *//* if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_depositBar.setProgressTextMoved(true);
                                        two_fact_depositBar.setEndColor(getResources().getColor(R.color.green));
                                        two_fact_depositBar.setStartColor(getResources().getColor(R.color.green));
                                     *//*   two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                   // }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

            case 3:
                if (resultCode == RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "DEVICE_INFO" );
                            String rdService = data.getStringExtra ( "RD_SERVICE_INFO" );
                            String display = "";
                            if (rdService != null) {
                                display = "RD Service Info :\n" + rdService + "\n\n";
                            }
                            if (result != null) {
                            }
                        }
                    } catch (Exception e) {
                        hideLoader();
                    }

                }
                break;
            case 4:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra ( "PID_DATA" );


                            if (result != null) {
                                morphoPidData = serializer.read ( MorphoPidData.class, result );

                                pidData = serializer.read ( PidData.class, result );

                                if(Float.parseFloat ( pidData._Resp.qScore ) <=60){
                                     *//*if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_depositBar.setProgressTextMoved(true);
                                        two_fact_depositBar.setEndColor(getResources().getColor(R.color.red));
                                        two_fact_depositBar.setStartColor(getResources().getColor(R.color.red));
                                    }*//*
                                }else if(Float.parseFloat ( pidData._Resp.qScore ) >=60 && Float.parseFloat ( pidData._Resp.qScore ) <=70){

                                    *//*if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_depositBar.setProgressTextMoved(true);
                                        two_fact_depositBar.setEndColor(getResources().getColor(R.color.yellow));
                                        two_fact_depositBar.setStartColor(getResources().getColor(R.color.yellow));
                                   *//*     two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                  //  }


                                }else{

                                   *//* if(two_fact_depositBar!=null) {
                                        two_fact_depositBar.setProgress(Float.parseFloat(pidData._Resp.qScore));
                                        two_fact_depositBar.setProgressTextMoved(true);
                                        two_fact_depositBar.setEndColor(getResources().getColor(R.color.green));
                                        two_fact_depositBar.setStartColor(getResources().getColor(R.color.green));
                                    *//*    two_fact_submitButton.setBackgroundResource(R.drawable.rect_bg);
                                        two_fact_submitButton.setEnabled(true);
                                        two_fact_submitButton.setTextColor(getResources().getColor(R.color.white));
                                   // }

                                }

                            }else{
                                }
                            if (morphoPidData._Resp.errCode.equalsIgnoreCase("720")){
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "0" )){
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "700" )){
                            }else if(morphoPidData._Resp.errCode.equalsIgnoreCase ( "730" )) {
                            }else{
                            }
                        }
                    } catch (Exception e) {

                    }

                }
                break;
        }
    }
*/

    /**
     * Location check
     */

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkPermission();
            }
        } catch (Exception e) {

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermission() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(AEPSActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                // ActivityCompat.requestPermissions(getActivity(), permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CA_PERMISSIONS);
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CA_PERMISSIONS);

            } else {
                getMyLocation();
                //retriveAUTH();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        int permissionLocation = ContextCompat.checkSelfPermission(AEPSActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        } else {
            Toast.makeText(this, "Please accept the location permission", Toast.LENGTH_SHORT).show();

            if (!isFinishing()) {
                finish();
            }
        }
    }


    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();


    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;

        if (mylocation != null) {
            // updateLocationInterface.onLocationUpdate(location);
            location_flag = true;

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            Log.e("latitude", "latitude--" + latitude);
            try {
                Log.e("latitude", "inside latitude--" + latitude);
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses != null && addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    if (postalCode == null) {
                        postalCode = "751017";
                    }
                    String knownName = addresses.get(0).getFeatureName();

//                    updateLocationInterface.onAddressUpdate(address);

                    //locationTxt.setText(address + " " + city + " " + country);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(AEPSActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                            .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(AEPSActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(AEPSActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }


    public void getPin() {
        if (mylocation != null) {
            // updateLocationInterface.onLocationUpdate(location);
            location_flag = true;

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            double latitude = mylocation.getLatitude();
            double longitude = mylocation.getLongitude();
            Log.e("latitude", "latitude--" + latitude);
            try {
                Log.e("latitude", "inside latitude--" + latitude);
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses != null && addresses.size() > 0) {
                    postalCode = addresses.get(0).getPostalCode();
                    if (postalCode == null) {
                        postalCode = "751017";
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public void goTo_AePSHomeActivity(String _token) {
        hideLoader();
        if (rb_cw.isChecked()) {

            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

           /* if (!isFinishing() && !isDestroyed()){

                if(deleteDialog!=null) {
                    deleteDialog.dismiss();
                }
            }*/
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = "" + Long.parseLong(amnt_txt.getText().toString());
            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _admin;
                       /* SdkConstants.paramA = "";
                        SdkConstants.paramB = "";
                        SdkConstants.paramC = "";//loan_id.getText().toString().trim();
                        SdkConstants.loginID = "";
                        SdkConstants.encryptedData = "";*/
            aeps_transaction_intent = true;
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
            SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";

            Intent intent = new Intent(AEPSActivity.this, CoreAEPSHomeActivity.class);
            startActivityForResult(intent, SdkConstants.REQUEST_CODE);
            // }

        }

        if (rb_be.isChecked()) {
//            deleteDialog.dismiss();
            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

           /* if (!isFinishing() && !isDestroyed()){

                if(deleteDialog!=null) {
                    deleteDialog.dismiss();
                }
            }*/
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
            SdkConstants.transactionAmount = "1";

            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _admin;

                   /* SdkConstants.paramA = "annapurna";
                    SdkConstants.paramB = "BLS1";
                    SdkConstants.paramC = "loanID";//loan_id.getText().toString().trim();
                    SdkConstants.loginID = "aepsTestR";
                    SdkConstants.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqno3cxOh6wUl5r4YDSECZYVg%3D%3D";
*/
            aeps_transaction_intent = true;
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
            SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";

            Intent intent = new Intent(AEPSActivity.this, CoreAEPSHomeActivity.class);
            startActivityForResult(intent, SdkConstants.REQUEST_CODE);

        }

        if (rb_mini.isChecked()) {
            Toast.makeText(AEPSActivity.this, "Service not available for aeps1, please choose aeps2 for mini statement", Toast.LENGTH_LONG).show();
        }


    }


    private void GenerateFreshnessFactor(String tokenStr) {
        try {
            AndroidNetworking.get(LOGIN_DETAILS_URL)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                EncriptedFreshnessFactor(tokenStr, encodedUrl);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void EncriptedFreshnessFactor(String tokenStr, String encodedUrl) {
        try {
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject respObj = new JSONObject(response.toString());
                                String respCode = respObj.getString("status");
                                if (respCode.equalsIgnoreCase("0")) {

                                    String nextFreshnessFactor = respObj.getString("nextFreshnessFactor");
                                    SdkConstants.FRESHNESS_FACTOR = nextFreshnessFactor;

                                    goTo_AePSHomeActivity(_token);

                                } else {
                                    showAlertMessage("Freshness factor  mismatch. Please login again.");
                                    // Toast.makeText(AEPSActivity.this,"Error , Please login again.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                showAlertMessage("Freshness factor  mismatch. Please login again.");

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("ERROR: " + anError.getErrorDetail());
                            showAlertMessage("Freshness factor  mismatch. Please login again.");

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //For refresh token

    private void refreshToken(String type) {
        //   showLoader();
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v87")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            //hideLoader();
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            refreshTokenEncodedURL(_token, encodedUrl, type);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            // hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AEPSActivity.this, "Please try again..check issue regarding token ", Toast.LENGTH_LONG).show();
                    }
                });
    }


    private void refreshTokenEncodedURL(String tokenStr, String encodedUrl, String type) {
        try {
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject respObj = new JSONObject(response.toString());
                                _token = respObj.getString("token");
                                _admin = respObj.getString("adminName");

                                HashMap<String, String> _user = sessionManager.getUserDetails();
                                _token = _user.get(SessionManager.KEY_TOKEN);
                                _admin = _user.get(SessionManager.KEY_ADMIN);

                                if (!_token.equalsIgnoreCase("")) {

                                    sessionManager.createLoginSession(_token, _admin);

                                    if (type.equalsIgnoreCase("AEPS1")) {


                                        SdkConstants.FRESHNESS_FACTOR = "";
                                        if (SdkConstants.FRESHNESS_FACTOR.equalsIgnoreCase("")) {
                                            GenerateFreshnessFactor(_token);
                                        } else {

                                            goTo_AePSHomeActivity(_token);
                                        }

                                    } else {
                                        //For AEPS2
                                        sendAEPS2Intent(true, _token); //new changes done
                                    }
                                } else {
                                    Toast.makeText(AEPSActivity.this, "Error in response of token !", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("ERROR: " + anError.getErrorDetail());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {

        }
    }

    private Boolean biometricDeviceConnect() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae();
            return false;
        } else {
            for (UsbDevice device : connectedDevices.values()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private void deviceConnectMessgae() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(AEPSActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(AEPSActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // finish();
                    }
                })
                .show();
    }

    public void showAlertMessage(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(AEPSActivity.this);
            builder.setTitle("");
            builder.setMessage(msg);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }

}
