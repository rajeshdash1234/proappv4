package com.isuisudmt.aeps;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.isuisudmt.Constants;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.bluetooth.SharePreferenceClass;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.AEPS2HomeActivity;
import com.matm.matmsdk.aepsmodule.BioAuthActivity;
import com.matm.matmsdk.aepsmodule.CoreAEPSHomeActivity;
import com.matm.matmsdk.aepsmodule.aeps3.AEPS3sdkActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.isuisudmt.BuildConfig.LOGIN_DETAILS_URL;
import static com.isuisudmt.Constants.bluetoothConnector;
import static com.isuisudmt.bbps.utils.Const.isAEPS1enabled;
import static com.isuisudmt.bbps.utils.Const.isAEPS2enabled;
import static com.isuisudmt.bbps.utils.Const.isAEPS3enabled;

public class AepsOptionActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    boolean bio_auth_status = false;
    Double latitude, longitude;
    private Location mylocation;
    String _token = "";
    SessionManager sessionManager;
    String _admin = "";
    String _userName;
    private Boolean location_flag = false;
    SharePreferenceClass sharePreferenceClass;
    UsbManager musbManager;
    private ProgressDialog dialog;
    String manufactureFlag = "";
    boolean aeps_transaction_intent = false;
    private UsbDevice usbDevice;
    String  strAepsType = "AEPS2";
    String strTransType = "";
    String transactionAmount = "";
    RadioGroup rg_aeps_type;
    RadioButton rb_aeps1, rb_aeps2, rb_aeps3;
    Button submitAeps;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    String postalCode = "751017";
    private static final int REQUEST_CA_PERMISSIONS = 931;
    private static final int REQUEST_WRITE_PERMISSION = 1001;
    int pincode = 751017;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);

        setContentView(R.layout.activity_aeps_option);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Aadhaar eBanking");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sessionManager = new SessionManager(AepsOptionActivity.this);
        setUpGClient();
        musbManager = (UsbManager) getSystemService(Context.USB_SERVICE);


        HashMap<String, String> user = sessionManager.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user_ = sessionManager.getUserSession();
        _userName = user_.get(sessionManager.userName);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sharePreferenceClass = new SharePreferenceClass(AepsOptionActivity.this);

        transactionAmount = getIntent().getStringExtra("AMOUNT");
        strTransType = getIntent().getStringExtra("AEPSTRANS_TYPE");

        rg_aeps_type = findViewById(R.id.rg_aeps_type);
        rb_aeps1 = findViewById(R.id.rb_aeps1);
        rb_aeps2 = findViewById(R.id.rb_aeps2);
        rb_aeps3 = findViewById(R.id.rb_aeps3);

        submitAeps = findViewById(R.id.submitAEPS);
        submitAeps.setEnabled(true);

        if (Constants.user_feature_array != null) {
            if (isAEPS1enabled == false) {
                rb_aeps1.setBackgroundResource(R.color.service_unavailable_col);
                rb_aeps1.setEnabled(false);
            }

            if (isAEPS2enabled == false) {
                rb_aeps2.setBackgroundResource(R.color.service_unavailable_col);
                rb_aeps2.setChecked(false);
                rb_aeps2.setEnabled(false);
            }

            if (isAEPS3enabled == false) {
                rb_aeps3.setBackgroundResource(R.color.service_unavailable_col);
                rb_aeps3.setEnabled(false);
            }
        }

        rg_aeps_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_aeps1:
                        strAepsType = "AEPS1";
                        submitAeps.setEnabled(true);
                        break;
                    case R.id.rb_aeps2:
                        strAepsType = "AEPS2";
                        submitAeps.setEnabled(true);
                        break;
                    case R.id.rb_aeps3:
                        strAepsType = "AEPS3";
                        submitAeps.setEnabled(true);
                        break;

                }
            }
        });


        submitAeps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                submitAeps.setEnabled(false);
                if (strAepsType.equals("AEPS1")) {

                    if (bluetoothConnector) {
                        showLoader();
                        refreshToken(strAepsType);
                    } else {
                        if (biometricDeviceConnect()) {
                            showLoader();
                            refreshToken(strAepsType);
                        } else {
                            Toast.makeText(AepsOptionActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
                        }
                    }

                } else if (strAepsType.equals("AEPS2")) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                    } else {

                        if (bluetoothConnector) {
                            showLoader();
                            refreshToken(strAepsType);
                        } else {
                            if (biometricDeviceConnect()) {
                                showLoader();
                                refreshToken(strAepsType);
                            } else {
                                Toast.makeText(AepsOptionActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } else if (strAepsType.equals("AEPS3")) {
                    if (bluetoothConnector) {
                        showLoader();
                        callAEPS3sdk();
                    } else {
                        if (biometricDeviceConnect()) {
                            showLoader();
                            callAEPS3sdk();
                        } else {
                            Toast.makeText(AepsOptionActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

   /* private void callAEPS3sdk() {
        if (!isFinishing() && !isDestroyed()) {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = "" + Long.parseLong(transactionAmount);
        } else if (strTransType.equalsIgnoreCase("Balance Enquiry")) {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
        } else if (strTransType.equalsIgnoreCase("Mini Statement")) {
            SdkConstants.transactionType = SdkConstants.ministatement;
        } *//*else if (strTransType.equalsIgnoreCase("AadhaarPay")) {
            SdkConstants.transactionType = SdkConstants.adhaarPay;
            SdkConstants.transactionAmount = "" + Long.parseLong(transactionAmount);
        }*//*
        SdkConstants.paramA = "annapurna";
        SdkConstants.paramB = "BLS1";
        SdkConstants.paramC = "loanID";
        SdkConstants.applicationType = "CORE";
        SdkConstants.tokenFromCoreApp = _token;
        SdkConstants.loginID = Constants.USER_NAME;


        SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
        SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";
        hideLoader();
        Intent intent = new Intent(AepsOptionActivity.this, AEPS3HomeActivity.class);
        startActivity(intent);
    }*/

    //For refresh token

    private void refreshToken(String type) {
        //   showLoader();
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v87")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            //hideLoader();
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            refreshTokenEncodedURL(_token, encodedUrl, type);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            // hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AepsOptionActivity.this, "Please try again..check issue regarding token ", Toast.LENGTH_LONG).show();
                    }
                });
    }


    private void refreshTokenEncodedURL(String tokenStr, String encodedUrl, String type) {
        try {
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject respObj = new JSONObject(response.toString());
                                _token = respObj.getString("token");
                                _admin = respObj.getString("adminName");

                                HashMap<String, String> _user = sessionManager.getUserDetails();
                                _token = _user.get(SessionManager.KEY_TOKEN);
                                _admin = _user.get(SessionManager.KEY_ADMIN);

                                if (!_token.equalsIgnoreCase("")) {

                                    sessionManager.createLoginSession(_token, _admin);

                                    if (type.equalsIgnoreCase("AEPS1")) {

                                        hideLoader();
                                        SdkConstants.FRESHNESS_FACTOR = "";
                                        if (SdkConstants.FRESHNESS_FACTOR.equalsIgnoreCase("")) {
                                            GenerateFreshnessFactor(_token);
                                        } else {

                                            goTo_AePSHomeActivity(_token);
                                        }

                                    } else {
                                        //For AEPS2
//                                        getPin();
                                        hideLoader();
                                        checkAddressStatus(_token);

                                    }
                                } else {
                                    Toast.makeText(AepsOptionActivity.this, "Error in response of token !", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("ERROR: " + anError.getErrorDetail());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Boolean biometricDeviceConnect() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        if (connectedDevices.isEmpty()) {
            submitAeps.setEnabled(true);
            deviceConnectMessgae();
            return false;
        } else {
            for (UsbDevice device : connectedDevices.values()) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        usbDevice = device;
                        manufactureFlag = usbDevice.getManufacturerName();
                        return true;
                    }

                }
            }
        }
        return false;
    }

    private void deviceConnectMessgae() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(AepsOptionActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(AepsOptionActivity.this);
        }
        builder.setCancelable(false);
        builder.setTitle("No device found")
                .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        // finish();
                    }
                })
                .show();
    }

    public void showAlertMessage(String msg) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(AepsOptionActivity.this);
            builder.setTitle("");
            builder.setMessage(msg);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendAEPS2Intent(boolean bioauth, String _token) {
        try {
            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _admin;
            SdkConstants.applicationUserName = _userName;
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
            SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";
            SdkConstants.bioauth = bioauth;
            SdkConstants.CustomTheme = Util.getTheme(this);
            aeps_transaction_intent = true;
            if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
                SdkConstants.transactionType = SdkConstants.cashWithdrawal;
                SdkConstants.transactionAmount = "" + Long.parseLong(transactionAmount);
                if (!bioauth) {
                    Intent intent = new Intent(AepsOptionActivity.this, BioAuthActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                } else {
                    hideLoader();
                    Intent intent = new Intent(AepsOptionActivity.this, AEPS2HomeActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                }
            }

            if (strTransType.equalsIgnoreCase("Balance Enquiry")) {
                SdkConstants.transactionType = SdkConstants.balanceEnquiry;
                SdkConstants.transactionAmount = "1";
                if (!bioauth) {
                    Intent intent = new Intent(AepsOptionActivity.this, BioAuthActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                } else {
                    hideLoader();
                    Intent intent = new Intent(AepsOptionActivity.this, AEPS2HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                }

            }
            if (strTransType.equalsIgnoreCase("Mini Statement")) {
                SdkConstants.transactionType = SdkConstants.ministatement;
                SdkConstants.transactionAmount = "1";
                if (!bioauth) {
                    Intent intent = new Intent(AepsOptionActivity.this, BioAuthActivity.class);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                } else {
                    hideLoader();
                    Intent intent = new Intent(AepsOptionActivity.this, AEPS2HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivityForResult(intent, SdkConstants.REQUEST_CODE);
                }


            }

            if (strTransType.equalsIgnoreCase("AadhaarPay")) {
                Toast.makeText(AepsOptionActivity.this, "Service not available for aeps2, please choose aeps3 for AadhaarPay", Toast.LENGTH_LONG).show();
            }


        } catch (Exception e) {

        }

    }


    public void goTo_AePSHomeActivity(String _token) {
        hideLoader();
        if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {

            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

           /* if (!isFinishing() && !isDestroyed()){

                if(deleteDialog!=null) {
                    deleteDialog.dismiss();
                }
            }*/
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = "" + Long.parseLong(transactionAmount);
            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _admin;

                       /* SdkConstants.paramA = "";
                        SdkConstants.paramB = "";
                        SdkConstants.paramC = "";//loan_id.getText().toString().trim();
                        SdkConstants.loginID = "";
                        SdkConstants.encryptedData = "";*/
            aeps_transaction_intent = true;
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
            SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";
            SdkConstants.CustomTheme = Util.getTheme(this);

            Intent intent = new Intent(AepsOptionActivity.this, CoreAEPSHomeActivity.class);
            startActivityForResult(intent, SdkConstants.REQUEST_CODE);
            // }

        }

        if (strTransType.equalsIgnoreCase("Balance Enquiry")) {
//            deleteDialog.dismiss();
            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

           /* if (!isFinishing() && !isDestroyed()){

                if(deleteDialog!=null) {
                    deleteDialog.dismiss();
                }
            }*/
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
            SdkConstants.transactionAmount = "1";

            SdkConstants.applicationType = "CORE";
            SdkConstants.tokenFromCoreApp = _token;
            SdkConstants.userNameFromCoreApp = _admin;

                   /* SdkConstants.paramA = "annapurna";
                    SdkConstants.paramB = "BLS1";
                    SdkConstants.paramC = "loanID";//loan_id.getText().toString().trim();
                    SdkConstants.loginID = "aepsTestR";
                    SdkConstants.encryptedData = "TYqmJRyB%2B4Mb39MQf%2BPqVhCbMYnW%2Bk4%2BiCnH24lkKjr4El8%2BnjuFhZw5lT26iLqno3cxOh6wUl5r4YDSECZYVg%3D%3D";
*/
            aeps_transaction_intent = true;
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
            SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";
            SdkConstants.CustomTheme = Util.getTheme(this);

            Intent intent = new Intent(AepsOptionActivity.this, CoreAEPSHomeActivity.class);
            startActivityForResult(intent, SdkConstants.REQUEST_CODE);

        }

        if (strTransType.equalsIgnoreCase("Mini Statement")) {
            Toast.makeText(AepsOptionActivity.this, "Service not available for aeps1, please choose aeps2 for mini statement", Toast.LENGTH_LONG).show();
        }
        if (strTransType.equalsIgnoreCase("AadhaarPay")) {
            Toast.makeText(AepsOptionActivity.this, "Service not available for aeps1, please choose aeps3 for AadhaarPay", Toast.LENGTH_LONG).show();
        }


    }


    private void GenerateFreshnessFactor(String tokenStr) {
        try {
            AndroidNetworking.get(LOGIN_DETAILS_URL)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                EncriptedFreshnessFactor(tokenStr, encodedUrl);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void EncriptedFreshnessFactor(String tokenStr, String encodedUrl) {
        try {
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject respObj = new JSONObject(response.toString());
                                String respCode = respObj.getString("status");
                                if (respCode.equalsIgnoreCase("0")) {

                                    String nextFreshnessFactor = respObj.getString("nextFreshnessFactor");
                                    SdkConstants.FRESHNESS_FACTOR = nextFreshnessFactor;

                                    goTo_AePSHomeActivity(_token);

                                } else {
                                    showAlertMessage("Freshness factor  mismatch. Please login again.");
                                    // Toast.makeText(AepsOptionActivity.this,"Error , Please login again.",Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                showAlertMessage("Freshness factor  mismatch. Please login again.");

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("ERROR: " + anError.getErrorDetail());
                            showAlertMessage("Freshness factor  mismatch. Please login again.");

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
                dialog = new ProgressDialog(AepsOptionActivity.this);
                dialog.setMessage("please wait..");
                dialog.setCancelable(false);
                dialog.show();
            }
        } catch (Exception e) {

        }
    }

    public void hideLoader() {
        try {
            if (!isFinishing() && !isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {

        }
    }

    private void checkAddressStatus(String _token) {
        showLoader();
        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v81")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            checkAddressStatus1(encodedUrl, _token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AepsOptionActivity.this, "Service is unavailable for this user, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void checkAddressStatus1(String url, String _token) {
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (obj.has("response")) {
                                JSONObject res_obj = obj.getJSONObject("response");
                                String pin = res_obj.getString("pincode");
                                String state = res_obj.getString("state");
                                String city = res_obj.getString("city");
                                bio_auth_status = res_obj.getBoolean("bioauth");
                                //if(pin.equalsIgnoreCase("null") || pin.length()==0 || pin == null || state.length() ==0 || city.length()==0) {
                                if (!bio_auth_status) {
                                    JSONObject jsonObject = new JSONObject();

                                    if (postalCode.length() != 0) {
                                        pincode = Integer.valueOf(postalCode);
                                    }
                                    jsonObject.put("pin", pincode);
                                    getAddressFromPin(jsonObject, _token);
                                    // showFingurePrintDialog();
                                    //showPinCodeDialog();
                                } else {
                                    hideLoader();
                                    sendAEPS2Intent(bio_auth_status, _token);
                                }
                            } else {

                                JSONObject jsonObject = new JSONObject();
                                if (postalCode.length() != 0) {
                                    pincode = Integer.valueOf(postalCode);
                                }
                                jsonObject.put("pin", pincode);
                                getAddressFromPin(jsonObject, _token);
                                //showPinCodeDialog();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                            Toast.makeText(AepsOptionActivity.this, "Something went wrong, please contact our help desk for details.", Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AepsOptionActivity.this, "Service is unavailable for this user, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getAddressFromPin(JSONObject obj, String _token) {
        // showLoader();
        AndroidNetworking.post("https://us-central1-iserveustaging.cloudfunctions.net/pincodeFetch/api/v1/getCitystateAndroid")
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            JSONObject data_obj = obj.getJSONObject("data");
                            String status = data_obj.getString("status");
                            if (status.equalsIgnoreCase("success")) {
                                JSONObject data_pin = data_obj.getJSONObject("data");
                                String pincode = data_pin.getString("pincode");
                                String state = data_pin.getString("state");
                                String city = data_pin.getString("city");
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("state", state);
                                jsonObject.put("pincode", pincode);
                                jsonObject.put("city", city);
                                String lat = "0.0";
                                String lng = "0.0";

                                if (mylocation != null) {
                                    lat = String.valueOf(mylocation.getLatitude());
                                    lng = String.valueOf(mylocation.getLongitude());
                                }
                                jsonObject.put("latLong", latitude + "," + longitude);
                                setAddress(jsonObject, _token);
                            } else {
                                hideLoader();
                                Toast.makeText(AepsOptionActivity.this, "Invalid area pin, please try after sometimes", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(AepsOptionActivity.this, "Invalid area pin, please try again.", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void setAddress(JSONObject objj, String _token) {

        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v82")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            setAddress1(objj, encodedUrl, _token);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }


    private void setAddress1(JSONObject obj, String url, String _token) {

        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());

                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (status.equalsIgnoreCase("0")) {
                                // deleteDialogg.dismiss();
                                // showFingurePrintDialog();
                                hideLoader();
                                sendAEPS2Intent(bio_auth_status, _token);
                            } else {
                                Toast.makeText(AepsOptionActivity.this, statusDesc, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkPermission();
            }
        } catch (Exception e) {

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void checkPermission() {
        if (Build.VERSION.SDK_INT > 15) {
            final String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

            final List<String> permissionsToRequest = new ArrayList<>();
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(AepsOptionActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    permissionsToRequest.add(permission);
                }
            }
            if (!permissionsToRequest.isEmpty()) {
                // ActivityCompat.requestPermissions(getActivity(), permissionsToRequest.toArray(new String[permissionsToRequest.size()]), REQUEST_CA_PERMISSIONS);
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CA_PERMISSIONS);

            } else {
                getMyLocation();
                //retriveAUTH();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        int permissionLocation = ContextCompat.checkSelfPermission(AepsOptionActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        } else {
            Toast.makeText(this, "Please accept the location permission", Toast.LENGTH_SHORT).show();

            if (!isFinishing()) {
                finish();
            }
        }

        if (grantResults.length > 0 && permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // check whether storage permission granted or not.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do what you want;
                if (bluetoothConnector) {
                    showLoader();
                    refreshToken(strAepsType);
                } else {
                    if (biometricDeviceConnect()) {
                        showLoader();
                        refreshToken(strAepsType);
                    } else {
                        Toast.makeText(AepsOptionActivity.this, "Connect your device.", Toast.LENGTH_SHORT).show();

                    }


                }
            }
        }
    }


    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();


    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (location_flag == false) {
            if (mylocation != null) {
                // updateLocationInterface.onLocationUpdate(location);
                location_flag = true;

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                Log.e("latitude", "latitude--" + latitude);
                try {
                    Log.e("latitude", "inside latitude--" + latitude);
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);

                    if (addresses != null && addresses.size() > 0) {
                        String address = addresses.get(0).getAddressLine(0);
                        String city = addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName();
                        postalCode = addresses.get(0).getPostalCode();
                        if (postalCode == null) {
                            postalCode = "751017";
                        }

                        String knownName = addresses.get(0).getFeatureName();

//                    updateLocationInterface.onAddressUpdate(address);

                        //locationTxt.setText(address + " " + city + " " + country);
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }

    }

    @Override
    public void onConnected(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(AepsOptionActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                            .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(AepsOptionActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(AepsOptionActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }


    public void getPin() {
        if (mylocation != null) {
            // updateLocationInterface.onLocationUpdate(location);
            location_flag = true;

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            double latitude = mylocation.getLatitude();
            double longitude = mylocation.getLongitude();
            Log.e("latitude", "latitude--" + latitude);
            try {
                Log.e("latitude", "inside latitude--" + latitude);
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses != null && addresses.size() > 0) {
                    postalCode = addresses.get(0).getPostalCode();
                    if (postalCode == null) {
                        postalCode = "751017";
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    private void callAEPS3sdk() {
        if (!isFinishing() && !isDestroyed()) {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        if (strTransType.equalsIgnoreCase("Cash Withdrawal")) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal;
            SdkConstants.transactionAmount = "" + Long.parseLong(transactionAmount);
        } else if (strTransType.equalsIgnoreCase("Balance Enquiry")) {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry;
        } else if (strTransType.equalsIgnoreCase("Mini Statement")) {
            SdkConstants.transactionType = SdkConstants.ministatement;
        } else if (strTransType.equalsIgnoreCase("AadhaarPay")) {
            SdkConstants.transactionType = SdkConstants.adhaarPay;
            SdkConstants.transactionAmount = "" + Long.parseLong(transactionAmount);
        }
        SdkConstants.paramA = "annapurna";
        SdkConstants.paramB = "BLS1";
        SdkConstants.paramC = "loanID";
        SdkConstants.applicationType = "CORE";
        SdkConstants.tokenFromCoreApp = _token;
        SdkConstants.loginID = Constants.USER_NAME;
        SdkConstants.MANUFACTURE_FLAG = manufactureFlag;
        SdkConstants.DRIVER_ACTIVITY = "com.isuisudmt.DriverActivity";
        hideLoader();

        Intent intent = new Intent(AepsOptionActivity.this, AEPS3sdkActivity.class);
        startActivity(intent);
    }

}