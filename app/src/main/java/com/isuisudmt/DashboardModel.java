package com.isuisudmt;

public class DashboardModel {
    private String userName,userType,userBalance,adminName,mposNumber,userBrand,userProfile,promotionalMessage,userFeature;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getMposNumber() {
        return mposNumber;
    }

    public void setMposNumber(String mposNumber) {
        this.mposNumber = mposNumber;
    }

    public String getUserBrand() {
        return userBrand;
    }

    public void setUserBrand(String userBrand) {
        this.userBrand = userBrand;
    }

    public String getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }

    public String getPromotionalMessage() {
        return promotionalMessage;
    }

    public void setPromotionalMessage(String promotionalMessage) {
        this.promotionalMessage = promotionalMessage;
    }

    public String getUserFeature() {
        return userFeature;
    }

    public void setUserFeature(String userFeature) {
        this.userFeature = userFeature;
    }
}
