package com.isuisudmt.special;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface SpecialApi {
    @POST()
    Call<SpecialResponse> getSpecial(@Header("Authorization") String token, @Body SpecialRequest specialRequest, @Url String url);
}
