package com.isuisudmt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.matm.matmsdk.aepsmodule.utils.Verhoff;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    //Theme_color_codes
    private static int sTheme;
    public final static int THEME_DEFAULT = 0;
    public final static int THEME_DARK = 1;
    public final static int THEME_YELLOW = 2;
    public final static int THEME_BLUE = 3;
    public final static int THEME_RED = 4;
    public final static int THEME_CUSTOM = 5;
    public final static int THEME_GREEN = 6;
    public final static int THEME_BROWN = 7;
   // public final static int THEME_TEAL = 8;
    public final static int MediumSlateBlue = 8;
    public final static int Bluetiful = 9;
    public final static int VioletBlue = 10;
    public final static int PermanentGeraniumLake = 11;
    public static int color = 0xff3b5998;

    public static void changeToTheme(Activity activity, int theme)
    {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }
    /** Set the theme of the activity, according to the configuration. */
    public static void onActivityCreateSetTheme(Activity activity)
    {
        switch (sTheme)
        {
            default:
            case THEME_DEFAULT:
                activity.setTheme(R.style.AppTheme);
                break;
            case THEME_DARK:
                activity.setTheme(R.style.DarkTheme);
                break;
            case THEME_YELLOW:
                activity.setTheme(R.style.YellowTheme);
                break;
            case THEME_BLUE:
                activity.setTheme(R.style.BlueTheme);
                break;
            case THEME_RED:
                activity.setTheme(R.style.RedTheme);
                break;
            case THEME_CUSTOM:
                //activity.setTheme(theme);
                Toast.makeText(activity,"Under development",Toast.LENGTH_LONG).show();
                break;
            case THEME_GREEN:
                activity.setTheme(R.style.GreenTheme);
                break;
            case THEME_BROWN:
                activity.setTheme(R.style.BrownTheme);
                break;
            case MediumSlateBlue:
                activity.setTheme(R.style.MediumSlateBlue);
                break;
            case Bluetiful:
                activity.setTheme(R.style.Bluetiful);
                break;
            case VioletBlue:
                activity.setTheme(R.style.VioletBlue);
                break;
            case PermanentGeraniumLake:
                activity.setTheme(R.style.PermanentGeraniumlake);
                break;
        }
    }


    public static void putTheme(Context ctx, String colorTheme) {
        SharedPreferences settings = ctx.getSharedPreferences("PREFS_NAME",0);
        SharedPreferences.Editor ed = settings.edit();
        ed.clear();
        ed.putString("Theme",colorTheme);
        ed.commit();
    }

    public static String getTheme(Context context){
        SharedPreferences settings= context.getSharedPreferences("PREFS_NAME",0);
        String myRegion = settings.getString("Theme","DEFAULT");
        String gRegion=myRegion;
        return gRegion;
    }




    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static String getCurrentDate() {
        Date todayDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-d");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(todayDate);
        String todayString = formatter.format(calendar.getTime());
        return todayString;
    }

    public static String compareDates(String fromdate, String toDate) {
        String status = "0";
        Date fromDat = convertStrToDate(fromdate);

        Date toDat = convertStrToDate(toDate);

       /* if (fromDat.after() || toDat.after( Util.getDateFromTime ())){
            msg = "From or To Date can not be  greater than Today";

        }else*/
        if (toDat.after(fromDat)) {
            Log.v("subhalaxmi", "Date1 is after Date2");
            status = "1";

        } else if (toDat.before(fromDat)) {
            Log.v("subhalaxmi", "Date1 is before Date2");
            status = "2";
        } else if (toDat.equals(fromDat)) {
            status = "3";

        }
        return status;
    }

    /**
     * Get a diff between two dates
     *
     * @param oldDate the old date
     * @param newDate the new date
     * @return the diff value, in the days
     */
    @SuppressLint("NewApi")
    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getNextDate(String toDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, +1);

        String nextDate = dateFormat.format(calendar.getTime());
        return nextDate;
    }

    public static Date convertStrToDate(String date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
        Date d = null;
        try {
            d = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;


    }

    public static String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("iinlist.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getDateFromTime(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(time);

    }

    public static String getDateFromTime(String time) {
        if(!time.equalsIgnoreCase("null")){
            long timeT  = Long.parseLong(time);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            return formatter.format(timeT);

        }
        else
            return "N/A";
    }

    public static String getDateTime(String time) {
        if(!time.equalsIgnoreCase("null")){
            long timeT  = Long.parseLong(time);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy  hh:mm a");
            return formatter.format(timeT);

        }
        else
            return "N/A";

    }
 /*   public static boolean validateAadharNumber(String aadharNumber){
        Pattern aadharPattern = Pattern.compile("\\d{12}");
        boolean isValidAadhar = aadharPattern.matcher(aadharNumber).matches();
        if(isValidAadhar){
            isValidAadhar = Verhoff.validateVerhoeff(aadharNumber);
        }
        return isValidAadhar;
    }

    public static boolean validateAadharVID(String aadharNumber){
        Pattern aadharPattern = Pattern.compile("\\d{16}");
        boolean isValidAadhar = aadharPattern.matcher(aadharNumber).matches();
        if(isValidAadhar){
            isValidAadhar = Verhoff.validateVerhoeff(aadharNumber);
        }
        return isValidAadhar;
    }*/

    public static boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
//            if(phone.length() < 6 || phone.length() > 13) {
            if (phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public static void showAlert(Context context, String title, String message) {
        try {

            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder.setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();

                        }
                    })
                    .show();
        }catch (Exception e){

        }

    }

    public static class PullTasksThread extends Thread {
        public void run() {
        }
    }


    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%])(?=\\S+$).{8,20}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
    public static String amountFormat(String amnt){
        String amount ="";
        Double antDob = Double.parseDouble(amnt);
        DecimalFormat df2 = new DecimalFormat( "###,###,###.##" );
        System.out.println(df2.format(antDob));
        amount = String.valueOf(df2.format(antDob));
        System.out.println("amount...."+amount);

        return amount;
    }


    public static boolean validateAadharNumber(String aadharNumber){
        Pattern aadharPattern = Pattern.compile("\\d{12}");
        boolean isValidAadhar = aadharPattern.matcher(aadharNumber).matches();
        if(isValidAadhar){
            isValidAadhar = Verhoff.validateVerhoeff(aadharNumber);
        }
        return isValidAadhar;
    }

    public static boolean validateAadharVID(String aadharNumber){
        Pattern aadharPattern = Pattern.compile("\\d{16}");
        boolean isValidAadhar = aadharPattern.matcher(aadharNumber).matches();
        if(isValidAadhar){
            isValidAadhar = Verhoff.validateVerhoeff(aadharNumber);
        }
        return isValidAadhar;
    }


    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String encodeImage(Bitmap bm)
    {
        if(bm.getWidth()<300|| bm.getHeight()<300){
            bm = getResizedBitmap(bm,150,150);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG,100,baos);

            byte[] b = baos.toByteArray();
            String encImage = Base64.encodeToString(b, Base64.DEFAULT);

            return encImage;
        }
        else{
            bm = getResizedBitmap(bm,300,300);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG,70,baos);

            byte[] b = baos.toByteArray();
            String encImage = Base64.encodeToString(b, Base64.DEFAULT);

            return encImage;
        }

    }


    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
    public static String getCreatedDateFromTime(String time) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(time);

    }

    public static void showAlertFinish(Activity context, String title, String message) {
        try {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder.setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            context.finish();
                        }
                    })
                    .show();
        }catch (Exception e){
        }
    }
}
