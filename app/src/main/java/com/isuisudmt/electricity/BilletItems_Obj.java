package com.isuisudmt.electricity;

public class BilletItems_Obj {

    String amountExactness;
    String blrCatId;
    String blrCatName;
    String blrName;
    String params;
    String blrValue;
    String adhocPayment;

    public String getAmountExactness() {
        return amountExactness;
    }

    public void setAmountExactness(String amountExactness) {
        this.amountExactness = amountExactness;
    }

    public String getBlrCatId() {
        return blrCatId;
    }

    public void setBlrCatId(String blrCatId) {
        this.blrCatId = blrCatId;
    }

    public String getBlrCatName() {
        return blrCatName;
    }

    public void setBlrCatName(String blrCatName) {
        this.blrCatName = blrCatName;
    }

    public String getBlrName() {
        return blrName;
    }

    public void setBlrName(String blrName) {
        this.blrName = blrName;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getBlrValue() {
        return blrValue;
    }

    public void setBlrValue(String blrValue) {
        this.blrValue = blrValue;
    }

    public String getAdhocPayment() {
        return adhocPayment;
    }

    public void setAdhocPayment(String adhocPayment) {
        this.adhocPayment = adhocPayment;
    }
}
