package com.isuisudmt.electricity;

public class Biller_Items_Model {
    public BilletItems_Obj billetItems_obj;
    public String billerId;
    public String billerNm;

    public BilletItems_Obj getBilletItems_obj() {
        return billetItems_obj;
    }

    public void setBilletItems_obj(BilletItems_Obj billetItems_obj) {
        this.billetItems_obj = billetItems_obj;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBillerNm() {
        return billerNm;
    }

    public void setBillerNm(String billerNm) {
        this.billerNm = billerNm;
    }
}
