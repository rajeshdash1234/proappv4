package com.isuisudmt.electricity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.R;

import java.util.ArrayList;

public class Electricity_bill_Activity extends AppCompatActivity {

    LinearLayout state_LL,electric_board_LL,consumer_id_LL,customer_mobile_LL,button_LL;
    AppCompatSpinner state_spinner,electricity_spinner;
    EditText consumer_id_et,customer_mobile_et;
    ArrayList<Electricity_State_Model> state_models_arr;
    ArrayList<BillerModel> biller_arr;
    ArrayList<Biller_Items_Model> biller_item_arr;
    ArrayAdapter state_arr_adapter;
    String state_ID="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.electricity_bill_layout);

        initValue();


        state_arr_adapter= new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_dropdown_item,state_models_arr);
        state_spinner.setAdapter(state_arr_adapter);

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (state_models_arr.get(position).state_id.equalsIgnoreCase("IND")){

                    electric_board_LL.setVisibility(View.GONE);
                }else {
                    state_ID=state_models_arr.get(position).state_id;
//                    Toast.makeText(Electricity_bill_Activity.this,state_ID
//                            ,Toast.LENGTH_LONG).show();

                    electric_board_LL.setVisibility(View.VISIBLE);
                    checkElectricity_state(state_ID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });







    }

    public void initValue(){

        Toolbar toolbar =  findViewById(R.id.toolbar);
        toolbar.setTitle("Electricity Bill");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        state_LL=findViewById(R.id.state_LL);
        electric_board_LL=findViewById(R.id.electric_board_LL);
        consumer_id_LL=findViewById(R.id.consumer_id_LL);
        customer_mobile_LL=findViewById(R.id.customer_mobile_LL);
        button_LL=findViewById(R.id.button_LL);

        state_spinner=findViewById(R.id.state_spinner);
        electricity_spinner=findViewById(R.id.elec_board_spinner);

        consumer_id_et=findViewById(R.id.consumer_id_et);
        customer_mobile_et=findViewById(R.id.customer_mobile_et);

        state_models_arr=new ArrayList<>();
        biller_arr=new ArrayList<>();
        biller_item_arr=new ArrayList<>();

        setState("IND","Please select state");
        setState("IND-PUD","Puducherry");
        setState("IND-BIH","Bihar");
        setState("IND-WBL","West Bengal");
        setState("IND-DAD","Daman and Diu");
        setState("IND-PUN","Punjab");
        setState("IND-ODI","Odisha");
        setState("IND-UTT","Uttarakhand");
        setState("IND-UTP","Uttar Pradesh");
        setState("IND-ASM","Assam");
        setState("IND-GUJ","Gujarat");
        setState("IND-GOA","Goa");
        setState("IND-ANP","Andhra Pradesh");
        setState("IND-CHH","Chhattisgarh");
        setState("IND-JHA","Jharkhand");
        setState("IND-DNH","Dadra and Nagar Haveli");
        setState("IND-MIZ","Mizoram");
        setState("IND-TND","Tamil Nadu");
        setState("IND-DEL","Delhi");
        setState("IND-TRI","Tripura");
        setState("IND-MAH","Maharashtra");
        setState("IND-NAG","Nagaland");
        setState("IND-SIK","Sikkim");
        setState("IND-HIP","Himachal Pradesh");
        setState("IND-MEG","Meghalaya");
        setState("IND-RAJ","Rajasthan");
        setState("IND-CHA","Chandigarh");
        setState("IND-KAR","Karnataka");
        setState("IND-MAP","Madhya Pradesh");
        setState("IND-KER","Kerala");
        setState("IND-HAR","Haryana");


    }
    public void setState(String state_id,String state_nm){

        Electricity_State_Model electricity_state_model=new Electricity_State_Model();
        electricity_state_model.setState_id(state_id);
        electricity_state_model.setState_nm(state_nm);

        state_models_arr.add(electricity_state_model);

    }

    private void checkElectricity_state(String state_id){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("Electricity_Collection").document(state_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("billerList")) {
                                String value = ""+document.getData().values();
//                                biller_item_arr.clear();
//                                biller_item_arr.add(map);
//                                for (int i=0;i<biller_item_arr.size() ;i++){
//                                    for (int j=0;j<biller_item_arr.get(i).length();j++){
//
                                        Toast.makeText(Electricity_bill_Activity.this, value, Toast.LENGTH_SHORT).show();
//                                    }
//
//                                }

//                                if(value.equalsIgnoreCase("false")){

                                    Toast.makeText(Electricity_bill_Activity.this, "Working ! ", Toast.LENGTH_SHORT).show();

//                                }else{
//
//                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {
//                        updateSession(user_name);
                    }
                } else {

                }
            }
        });
    }

}
