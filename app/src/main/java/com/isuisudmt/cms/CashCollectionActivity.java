package com.isuisudmt.cms;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class CashCollectionActivity extends AppCompatActivity {


    SessionManager session;
    String borrowerName,borrowerMobileNumber;
    String webUrl="";
    String transactionId="";
    String pinCode="";
    String token="";
    String id="",name="",ttime="",email="",mobile="",username="",shop_name="",date="",address="";
    private WebView webView;
    Activity activity;
    ProgressBar viewProgress;
    Boolean pinFlag = false;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_cash_collection);

        initView();
        callRetailerWallet(viewProgress);

        // DoCheckPin();
    }
    public void initView(){

        pinCode = getIntent().getStringExtra("pinData");
        webView = findViewById(R.id.webview);
        viewProgress = findViewById(R.id.viewProgress);
        webView.getSettings().setJavaScriptEnabled(true);
        session = new SessionManager(CashCollectionActivity.this);
        HashMap<String, String> user = session.getUserDetails();
        HashMap<String, String> userDetails = session.getUserSession();

          borrowerName = userDetails.get(SessionManager.profileName);
          borrowerMobileNumber = userDetails.get(SessionManager.mobileNo);
          token = user.get(SessionManager.KEY_TOKEN);
          activity = this;

        dialog = new Dialog(CashCollectionActivity.this);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                   onBackPressed();
                }
                return true;
            }
        });


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.cashCollection));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

       /* dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface interface, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    this.finish();
                }
                return true;
            }
        });*/







    }
/*
    public void DoCheckPin(){
        // custom dialog

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pincode_dialogrecharge);
        pinFlag = false;

        final ProgressBar progressV = dialog.findViewById(R.id.progressV);


        final EditText pincodeTxt = dialog.findViewById(R.id.pincodeTxt);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        
       
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // callShowCMS(dialog,progressV);

                if(pinFlag){
                    callRetailerWallet(dialog,progressV);  
                }else{
                    Toast.makeText(CashCollectionActivity.this, "Pincode is not valid.", Toast.LENGTH_SHORT).show();
                }

               
            }
        });

        



        pincodeTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==6){
                    pinCode = pincodeTxt.getText().toString();
                    callPinValidation(progressV,pinCode);
                    progressV.setVisibility(View.VISIBLE);
                }else if(s.length()>6){
                    Toast.makeText(CashCollectionActivity.this, "Enter your Pincode.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        dialog.show();

    }
*/

    private void callRetailerWallet(final ProgressBar progressV) {
        progressV.setVisibility(View.VISIBLE);
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v54")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            System.out.println(">>>>-----"+encodedUrl);
                            callEncriptedRetaillerWallet(encodedUrl,progressV);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        progressV.setVisibility(View.GONE);
                    }
                });

    }

    private void callEncriptedRetaillerWallet( String encodedUrl, final ProgressBar progressV) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("borrowerName", borrowerName);
            obj.put("borrowerMobileNumber", borrowerMobileNumber);
            obj.put("operation", "simpl_cash");

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("authorization",token)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //progressV.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("status");
                                if (statusCode.equalsIgnoreCase("0")) {
                                    String statusDescription = obj.getString("statusDesc");
                                    transactionId = obj.getString("transactionId");


                                   // callShowCMS(dialog,progressV);
                                    callUserDetail(progressV);



                                } else {
                                    Toast.makeText(CashCollectionActivity.this, "Failure", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(CashCollectionActivity.this, anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                            progressV.setVisibility(View.GONE);
                        }
                    });
        }catch (JSONException e){
            e.printStackTrace();
        }


    }

    private void callUserDetail( final ProgressBar progressV) {
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v56")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            System.out.println(">>>>-----"+encodedUrl);
                           // callEncriptedCSM(dialog,encodedUrl,progressV);
                            callEncriptedUserDetail(encodedUrl,progressV);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        progressV.setVisibility(View.GONE);
                    }
                });

    }

    private void callEncriptedUserDetail(final String encodedUrl, final ProgressBar progressV) {
        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                .addHeaders("authorization",token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //progressV.setVisibility(View.GONE);
                            JSONObject obj = new JSONObject(response.toString());
                            String statusCode = obj.getString("statuscode");
                            if(statusCode.equalsIgnoreCase("0")){
                                //String statusDescription = obj.getString("statusDesc");
                               // Toast.makeText(CashCollectionActivity.this,statusDescription,Toast.LENGTH_SHORT).show();
                                 id  = obj.getString("id");
                                String firstName = obj.getString("firstName");
                                String lastName = obj.getString("lastName");
                                name = firstName+" "+lastName;
                                email = obj.getString("email");
                                mobile = obj.getString("mobileNumber");
                                username = obj.getString("username");
                                shop_name = obj.getString("shopName");
                                ttime = obj.getString("date");
                                address = obj.getString("address");


                                callShowCMS(progressV);





                            }else{
                                Toast.makeText(CashCollectionActivity.this,"Failure",Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(CashCollectionActivity.this,anError.getErrorDetail(),Toast.LENGTH_SHORT).show();
                        progressV.setVisibility(View.GONE);
                    }
                });


    }

    private void callShowCMS(final ProgressBar progressV) {


            AndroidNetworking.get("https://apps.iserveu.tech/generate/v53")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                System.out.println(">>>>-----"+encodedUrl);
                                callEncriptedCSM(encodedUrl,progressV);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            progressV.setVisibility(View.GONE);
                        }
                    });


    }

    private void callEncriptedCSM(String encodedUrl, final ProgressBar progressV) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("retailer_id", id);
            obj.put("owner_name", name);
            obj.put("pincode",pinCode);
            obj.put("mobile_no", mobile);
            obj.put("current_url", "");
            obj.put("shopLat", "");
            obj.put("shopLong", "");
            obj.put("shopName", shop_name);
            obj.put("addressLine1", address);
            obj.put("addressLine2", "");
            obj.put("shopLandmark", "");
            obj.put("transaction_id", transactionId);
            obj.put("date_time",ttime);
            obj.put("isu_code",token);

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressV.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("status");
                                if (statusCode.equalsIgnoreCase("0")) {
                                    String statusDescription = obj.getString("statusDesc");
                                    JSONObject respObj = obj.getJSONObject("response");
                                    webUrl = respObj.getString("longurl");
                                    dialog.dismiss();

                                    CallWebLink(webUrl);


                                } else {
                                    Toast.makeText(CashCollectionActivity.this, "Failure", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(CashCollectionActivity.this, anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                            progressV.setVisibility(View.GONE);
                        }
                    });
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    private void CallWebLink(String webUrl) {
        Util.hideKeyboard(CashCollectionActivity.this,webView);
        viewProgress.setVisibility(View.VISIBLE);

        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                //Page load finished
                super.onPageFinished(view, url);
                viewProgress.setVisibility(View.GONE);
            }
        });
        webView .loadUrl(webUrl);

    }


}
