package com.isuisudmt;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.bluetooth.SharePreferenceClass;
import com.isuisudmt.settings.BluetoothConnectorActivity;
import com.isuisudmt.settings.SettingsNewActivity;
import com.isuisudmt.utility.Session;
import com.matm.matmsdk.MPOS.BluetoothServiceActivity;
import com.matm.matmsdk.Utils.EnvData;
import com.matm.matmsdk.Utils.SdkConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.UUID;

import fr.ganfra.materialspinner.MaterialSpinner;


public class SettingsFragment extends Fragment {

    private MaterialSpinner deviceSpinner, printDeviceSpinner;
    private static final String[] ITEMS = {"Mantra", "Morpho", "Startek", "Precision", "Evolute","Bluprint"};
    private static final String[] BT_ITEMS = {"Bluprint"};
    Session session;

    UsbManager musbManager;
    private UsbDevice usbDevice;
    //  LoadingView loadingView;
    boolean usbconnted = false;
    String deviceSerialNumber = "0";
    String morphodeviceid = "SAGEM SA";
    String mantradeviceid = "MANTRA";
    String morphoe2device = "Morpho";
    String precisiondeviceid = "Mvsilicon";
    String fmDeviceId = "Startek Eng-Inc.";
    String fmDeviceId2 = "Startek Eng-Inc.\u0000";
    String fmDeviceId3 = "Startek Eng. Inc.";
    String fmDeviceId4 = "Startek";

    SessionManager sessionManager;
    TextView print_device_name, device_name;
    boolean first_run = true;
    SharePreferenceClass sharePreferenceClass;
    private static final UUID MY_UUID = UUID.fromString("0000110E-0000-1000-8000-00805F9B34FB");
    /**
     * The BluetoothAdapter is the gateway to all bluetooth functions
     **/
    protected BluetoothAdapter bluetoothAdapter = null;

    /**
     * We will write our message to the socket
     **/
    protected BluetoothSocket socket = null;

    /**
     * The Bluetooth is an external device, which will receive our message
     **/
    BluetoothDevice blueToothDevice = null;
    Button matm1_pair_btn, matm2_pair_btn;
    String _token, _admin;
    Handler timer_handler;
    String _userName;
    boolean session_logout = false;

    public static Fragment getInstance() {
        return new SettingsFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        new CustomThemes(getActivity());
        View rootview =  inflater.inflate(R.layout.fragment_settings, container, false);




       /* Toolbar toolbar = rootview.findViewById(R.id.toolbar);

        toolbar = rootview.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });*/
        session = new Session(getActivity());

        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> user = sessionManager.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user_ = sessionManager.getUserSession();
        _userName = user_.get(sessionManager.userName);

        sharePreferenceClass = new SharePreferenceClass(getActivity());

        deviceSpinner = rootview.findViewById(R.id.deviceSpinner);
        printDeviceSpinner = rootview.findViewById(R.id.printDeviceSpinner);
        device_name = rootview.findViewById(R.id.device_name);
        print_device_name = rootview.findViewById(R.id.print_device_name);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deviceSpinner.setAdapter(adapter);

        ArrayAdapter<String> bt_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, BT_ITEMS);
        bt_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        printDeviceSpinner.setAdapter(bt_adapter);

        getUserId(_token, "https://mobile.9fin.co.in/user/user_details");


        if (Constants.selected_fingerPrint != null && Constants.from_BT == true) {
            device_name.setVisibility(View.VISIBLE);
            device_name.setText("Connected Device : " + Constants.selected_fingerPrint.getName());

        } else {
            device_name.setVisibility(View.INVISIBLE);
        }

        if (Constants.selected_btdevice != null && Constants.from_PT == true) {
            print_device_name.setVisibility(View.VISIBLE);
            print_device_name.setText("Connected Device : " + Constants.selected_btdevice.getName());

        } else {
            print_device_name.setVisibility(View.INVISIBLE);
        }


        deviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {


                    Constants.from_BT = false;
                    Constants.from_RD = true;
                    Constants.from_PT = false;
                    showLoader();
                    musbManager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
                    updateDeviceList();
                }

                if (position == 1) {
                    Constants.from_BT = false;
                    Constants.from_RD = true;
                    Constants.from_PT = false;
                    showLoader();
                    musbManager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
                    updateDeviceList();
                }
                if (position == 2) {
                    Constants.from_BT = false;
                    Constants.from_RD = true;
                    Constants.from_PT = false;
                    showLoader();
                    musbManager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
                    updateDeviceList();
                }
                if (position == 3) {
                    Constants.from_BT = false;
                    Constants.from_RD = true;
                    Constants.from_PT = false;
                    showLoader();
                    musbManager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
                    updateDeviceList();
                }

                if (position == 4) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getActivity(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                            Constants.from_BT = true;
                            Constants.from_RD = false;
                            Constants.from_PT = false;
                            Intent in = new Intent(getActivity(), BluetoothConnectorActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        } else {
                            Constants.from_BT = true;
                            Constants.from_RD = false;
                            Constants.from_PT = false;
                            Intent in = new Intent(getActivity(), BluetoothConnectorActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        }
                    } else {

                    }
                }
                if (position == 5) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getActivity(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                            Constants.from_BT = true;
                            Constants.from_RD = false;
                            Constants.from_PT = false;
                            Intent in = new Intent(getActivity(), BluetoothConnectorActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        } else {
                            Constants.from_BT = true;
                            Constants.from_RD = false;
                            Constants.from_PT = false;
                            Intent in = new Intent(getActivity(), BluetoothConnectorActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        }
                    } else {

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        printDeviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getActivity(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                            Constants.from_BT = false;
                            Constants.from_RD = false;
                            Constants.from_PT = true;
                            Intent in = new Intent(getActivity(), BluetoothConnectorActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        } else {
                            Constants.from_BT = false;
                            Constants.from_RD = false;
                            Constants.from_PT = true;
                            Intent in = new Intent(getActivity(), BluetoothConnectorActivity.class);
                            startActivity(in);
                            getActivity().finish();
                        }
                    } else {

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getActivity(), "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });

        matm1_pair_btn = rootview.findViewById(R.id.matm1_pair_btn);

        matm1_pair_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean installed = appInstalledOrNot("com.matm.matmservice");
                if (installed) {
                    Intent intent = new Intent(Intent.ACTION_DATE_CHANGED);
                    PackageManager manager = getActivity().getPackageManager();
                    intent = manager.getLaunchIntentForPackage("com.matm.matmservice");
                    intent.putExtra("RequestData", "");
                    intent.putExtra("HeaderData", "");
                    intent.putExtra("ReturnTime", 5);
                    intent.putExtra("IS_PAIR_DEVICE", true);
                    intent.putExtra("Flag", "bluetooth");
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    startActivityForResult(intent, 3);
                    System.out.println("App already installed om your phone");
                } else {
                    showAlert(getActivity());
                    System.out.println("App is not installed on your phone");
                }
            }
        });
        matm2_pair_btn = rootview.findViewById(R.id.matm2_pair_btn);
        matm2_pair_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                        Toast.makeText(getActivity(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                    } else {
                        SdkConstants.applicationType = "CORE";
                        Intent intent = new Intent(getActivity(), BluetoothServiceActivity.class);
                        intent.putExtra("userName", EnvData.UserName);
                        intent.putExtra("user_id", EnvData.user_id);
                        intent.putExtra("user_token", _token);
                        startActivity(intent);
                    }
                } else {
                    SdkConstants.applicationType = "CORE";
                    Intent intent = new Intent(getActivity(), BluetoothServiceActivity.class);
                    intent.putExtra("userName", EnvData.UserName);
                    intent.putExtra("user_id", EnvData.user_id);
                    intent.putExtra("user_token", _token);
                    startActivity(intent);
                }
            }
        });

        return rootview;
    }


    public void showAlert(Context context) {
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(context);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Please download the MATM service app from the playstore.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        redirectToPlayStore();
                    }
                })
                .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void redirectToPlayStore() {
        Uri uri = Uri.parse("market://details?id=com.matm.matmservice");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.matm.matmservice")));
        }
    }

   /* @Override
    protected void onResume() {
        super.onResume();

        if (from_RD) {

            if ( Constants.selected_fingerPrint != null) {
                device_name.setVisibility(View.VISIBLE);
                device_name.setText("Connected Device : " + Constants.selected_fingerPrint.getName());

            } else {
                device_name.setVisibility(View.INVISIBLE);
            }
        }

    }
*/

    public void showLoader() {
        /*if (loadingView ==null){
            loadingView = showProgress(this);
        }
        loadingView.show();*/
    }

    public void hideLoader() {
       /* if (loadingView!=null){
            loadingView.hide();
        }*/
    }


    /*
   app installation check
   */
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }



    protected void simpleComm(Integer port) {
        // byte [] inputBytes = null;

        // The documents tell us to cancel the discovery process.
        bluetoothAdapter.cancelDiscovery();

        Log.d(this.toString(), "Port = " + port);
        try {
            // This is a hack to access "createRfcommSocket which is does not
            // have public access in the current api.
            // Note: BlueToothDevice.createRfcommSocketToServiceRecord (UUID
            // uuid) does not work in this type of application. .
            Method m = blueToothDevice.getClass().getMethod(
                    "createRfcommSocket", new Class[]{int.class});
            socket = (BluetoothSocket) m.invoke(blueToothDevice, port);

            // debug check to ensure socket was set.
            assert (socket != null) : "Socket is Null";

            // attempt to connect to device
            socket.connect();
            try {
                Log.d(this.toString(),
                        "************ CONNECTION SUCCEES! *************");

                // Grab the outputStream. This stream will send bytes to the
                // external/second device. i.e it will sent it out.
                // Note: this is a Java.io.OutputStream which is used in several
                // types of Java programs such as file io, so you may be
                // familiar with it.
                OutputStream outputStream = socket.getOutputStream();

                // Create the String to send to the second device.
                // Most devices require a '\r' or '\n' or both at the end of the
                // string.
                // @todo set your message
                String message = "---";

                // Convert the message to bytes and blast it through the
                // bluetooth
                // to the second device. You may want to use:
                // public byte[] getBytes (Charset charset) for proper String to
                // byte conversion.
                outputStream.write(message.getBytes());

            } finally {
                // close the socket and we are done.
                socket.close();
            }
            // IOExcecption is thrown if connect fails.
        } catch (IOException ex) {
            Log.e(this.toString(), "IOException " + ex.getMessage());
            // NoSuchMethodException IllegalAccessException
            // InvocationTargetException
            // are reflection exceptions.
        } catch (NoSuchMethodException ex) {
            Log.e(this.toString(), "NoSuchMethodException " + ex.getMessage());
        } catch (IllegalAccessException ex) {
            Log.e(this.toString(), "IllegalAccessException " + ex.getMessage());
        } catch (InvocationTargetException ex) {
            Log.e(this.toString(),
                    "InvocationTargetException " + ex.getMessage());
        }
    }

    public void getUserId(String token, String urlString) {
        AndroidNetworking.get(urlString)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String id = obj.getString("id");
                            String user_name = obj.getString("username");
                            EnvData.user_id = id;
                            EnvData.UserName = user_name;
                            System.out.println(obj);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("Error  " + anError.getErrorDetail());
                    }
                });
    }


    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    /*
     *
     * usbmanger is checking the connection
     *
     * wether a usb device is connnected to the device or not
     */
    private void updateDeviceList() {
        HashMap<String, UsbDevice> connectedDevices = musbManager.getDeviceList();
        usbDevice = null;
        hideLoader();
        if (connectedDevices.isEmpty()) {
            usbconnted = false;
            // Toast.makeText(DashboardActivity.this, "No Devices Currently Connected" + usbconnted, Toast.LENGTH_LONG).show();
            deviceConnectMessgae();
        } else {
            for (UsbDevice device : connectedDevices.values()) {
                usbconnted = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.getManufacturerName() != null) {
                        if (device.getManufacturerName().equalsIgnoreCase(mantradeviceid) || device.getManufacturerName().equalsIgnoreCase(morphodeviceid)
                                || device.getManufacturerName().trim().equalsIgnoreCase(morphoe2device) || device.getManufacturerName().trim().equalsIgnoreCase(precisiondeviceid)
                                || device.getManufacturerName().trim().equalsIgnoreCase(fmDeviceId) || device.getManufacturerName().trim().equalsIgnoreCase(fmDeviceId2)
                                || device.getManufacturerName().trim().equalsIgnoreCase(fmDeviceId3) || device.getManufacturerName().trim().equalsIgnoreCase(fmDeviceId4)) {
                            usbDevice = device;
                            deviceSerialNumber = usbDevice.getManufacturerName();
                            session.setUsbDevice(usbDevice.getManufacturerName());
                            sharePreferenceClass.setUsbDevice(usbDevice.getManufacturerName());
                            sharePreferenceClass.setUsbDeviceSerial(deviceSerialNumber);
                        }

                        sharePreferenceClass.setConnectedRD_Device(device.getManufacturerName());

                    }
                }
            }
            devicecheck();
        }
    }

    private void updateDeviceInfo() {
        String rd_device_name = sharePreferenceClass.getConnectedRD_Device().trim();
        if (rd_device_name != null && rd_device_name.length() != 0) {
            device_name.setVisibility(View.VISIBLE);
            if (rd_device_name.equalsIgnoreCase(mantradeviceid)) {
                device_name.setText("Connected Device : " + mantradeviceid);
            }
            if (rd_device_name.equalsIgnoreCase(morphodeviceid)) {
                device_name.setText("Connected Device : " + morphodeviceid);
            }
            if (rd_device_name.equalsIgnoreCase(morphoe2device)) {
                device_name.setText("Connected Device : " + morphodeviceid);
            }

        } else {
            device_name.setVisibility(View.GONE);

        }

    }


    private void deviceConnectMessgae() {
        hideLoader();
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.device_connect))
                .setMessage(getResources().getString(R.string.decive_please_connect))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .show();
    }

    private void rdserviceMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_install))
                .setMessage(getResources().getString(R.string.mantra_rd_service))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void mantraMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(R.string.mantra_client_management_install))
                .setMessage(getResources().getString(R.string.mantra))
                .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.mantra.clientmanagement"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void morphoMessage() {
        try {

            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(getActivity());
            }
            builder.setCancelable(false);
            builder.setTitle(getResources().getString(R.string.morpho))
                    .setMessage(getResources().getString(R.string.install_morpho_message))
                    .setPositiveButton(getResources().getString(R.string.ok_error), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            /*
                             * play store intent
                             */
                            final String appPackageName = "com.scl.rdservice"; // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    })
                    .show();

        } catch (Exception e) {

        }
    }

    private void startekMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setCancelable(false);
        builder.setTitle(getResources().getString(isumatm.androidsdk.equitas.R.string.Fm220U_install))
                .setMessage(getResources().getString(isumatm.androidsdk.equitas.R.string.fm220U_service))
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.acpl.registersdk"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    private void precisionMessage() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setCancelable(false);
        builder.setTitle("Precision RD Service")
                .setMessage("Unable to find Precision Service app. Please install it from playstore.")
                .setPositiveButton(getResources().getString(isumatm.androidsdk.equitas.R.string.ok_error), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final String appPackageName = "com.precision.pb510.rdservice"; // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void devicecheck() {
        if (usbDevice == null) {
            deviceConnectMessgae();
        } else {
            if (deviceSpinner.getSelectedItemPosition() == 0) {
                if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(mantradeviceid)) {
                    installcheck();
                } else {
                    deviceConnectMessgae();
                }
            }
            else if (deviceSpinner.getSelectedItemPosition() == 1) {
                if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphodeviceid) || usbDevice.getManufacturerName().trim().equalsIgnoreCase(morphoe2device)) {
                    morphoinstallcheck();
                } else {
                    deviceConnectMessgae();
                }
            } else if (deviceSpinner.getSelectedItemPosition() == 2) {
                if (usbDevice.getManufacturerName().trim().contains(fmDeviceId4) || usbDevice.getManufacturerName().trim().equalsIgnoreCase(fmDeviceId2) ||
                        usbDevice.getManufacturerName().trim().equalsIgnoreCase(fmDeviceId) || usbDevice.getManufacturerName().trim().equalsIgnoreCase(fmDeviceId3)) {

                    startekinstallcheck();
                } else {
                    deviceConnectMessgae();
                }


            } else if (deviceSpinner.getSelectedItemPosition() == 3) {
                if (usbDevice.getManufacturerName().trim().equalsIgnoreCase(precisiondeviceid)) {
                    precisioninstallcheck();
                } else {
                    deviceConnectMessgae();
                }
            }
        }
    }

    private void installcheck() {
        boolean isAppInstalled = appInstalledOrNot("com.mantra.clientmanagement");
        boolean serviceAppInstalled = appInstalledOrNot("com.mantra.rdservice");
        if (isAppInstalled) {
// This intent will help you to launch if the package is already installed
            if (serviceAppInstalled) {
                Intent intent = new Intent("in.gov.uidai.rdservice.fp.INFO");
                intent.setPackage("com.mantra.rdservice");
                startActivityForResult(intent, 1);
            } else {
                rdserviceMessage();
            }
        } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
            mantraMessage();
        }
    }

    private void morphoinstallcheck() {
        try {

            boolean isAppInstalled = appInstalledOrNot("com.scl.rdservice");
            if (isAppInstalled) {
//This intent will help you to launch if the package is already installed
                Intent intent1 = new Intent();
                intent1.setAction("in.gov.uidai.rdservice.fp.INFO");
                intent1.setPackage("com.scl.rdservice");
//            intent1.addFlags ( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivityForResult(intent1, 2);
            } else {
// Do whatever we want to do if application not installed
// For example, Redirect to play store
                morphoMessage();
            }

        } catch (Exception e) {

        }
    }

    private void startekinstallcheck() {
        boolean isAppInstalled = appInstalledOrNot("com.acpl.registersdk");
        if (isAppInstalled) {
            Intent intent1 = new Intent();
            intent1.setAction("in.gov.uidai.rdservice.fp.INFO");
            intent1.setPackage("com.acpl.registersdk");
            startActivityForResult(intent1, 1);
        } else {
            startekMessage();
        }
    }

    private void precisioninstallcheck() {
        boolean isAppInstalled = appInstalledOrNot("com.precision.pb510.rdservice");
        if (isAppInstalled) {
            Intent intent1 = new Intent();
            intent1.setAction("in.gov.uidai.rdservice.fp.INFO");
            intent1.setPackage("com.precision.pb510.rdservice");
            startActivityForResult(intent1, 1);
        } else {
            precisionMessage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == getActivity().RESULT_OK) {
                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                hideLoader();
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText(getActivity(), "Your device is ready for use", Toast.LENGTH_SHORT).show();
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(), SettingsNewActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);

                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "result"+""+result, Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                        /*if (loadingView != null) {
                            loadingView.hide();
                        }*/
                    }
                }
                break;
            case 2:
                if (resultCode == getActivity().RESULT_OK) {

                    try {
                        if (data != null) {
                            String result = data.getStringExtra("DEVICE_INFO");
                            String rdService = data.getStringExtra("RD_SERVICE_INFO");
                            String display = "";
                            if (rdService != null) {
                                hideLoader();
                                display = "RD Service Info :\n" + rdService + "\n\n";
                                Toast.makeText(getActivity(), "Your device is ready for use", Toast.LENGTH_SHORT).show();
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(), SettingsNewActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                startActivity(intent);

                            }
                            if (result != null) {
//                                Toast.makeText ( DashboardActivity.this, "Decive info check", Toast.LENGTH_SHORT ).show ();
                            }
                        }
                    } catch (Exception e) {
                       /* if (loadingView != null) {
                            loadingView.hide();
                        }*/
                    }
                }
                break;
        }
    }
    
}