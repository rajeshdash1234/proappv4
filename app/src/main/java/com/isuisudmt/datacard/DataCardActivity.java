package com.isuisudmt.datacard;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.isuisudmt.CommonUtil;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.dmt.MyErrorMessage;
import com.isuisudmt.recharge.ReacargeStatusActivity;
import com.isuisudmt.utils.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import static com.isuisudmt.BuildConfig.GET_AEPS_RECHARGE_URL;


public class DataCardActivity extends AppCompatActivity {

    //SearchableSpinner datacard_select_operator;
    //String operatorString;

    String[] operator = {"VODAFONE DATACARD","Airtel DATACARD","TATA PHOTON PLUS DATACARD","RELIANCE DATACARD","RELIANCE BROADBAND DATACARD",
            "MTS DATACARD","BSNL DATACARD","MTNL MUMBAI DATACARD","IDEA DATACARD","TataIndicom DATACARD","TATADocomo DATACARD","AIRCEL DATACARD"};

   // Button datacard_button;

    TextInputEditText _mobile, _operator, _circel, _amount;
    Button submit;

    private String mobilenoString, amountString, operatorString, rechargetypeString, stdCodeString, pincodeString, accountNoString, latitudeString, circleCodeString, longitudeString;

    TextInputLayout layout_mobile, layout_operator, Layout_circel, layout_amount;

    private APIService apiService;
    SessionManager session;

    String _token = "", _admin = "", userName = "";

    ProgressDialog pd;
    private FirebaseAnalytics firebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_card);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.datacard));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        _mobile = findViewById(R.id.mobile);
        _circel = findViewById(R.id.circel);
        _amount = findViewById(R.id.amount);
        _operator = findViewById(R.id.operator);
        submit = findViewById(R.id.submit);

        Layout_circel = findViewById(R.id.Layout_circel);
        layout_mobile = findViewById(R.id.layout_mobile);
        layout_operator = findViewById(R.id.layout_operator);
        layout_amount = findViewById(R.id.layout_amount);

        Layout_circel.setVisibility(View.GONE);
        rechargetypeString = "Rr";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";


        session = new SessionManager(DataCardActivity.this);

        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);
        userName = user.get(SessionManager.userName);
        _mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 10) {
                    CommonUtil.hideKeyboard(getApplicationContext(), _mobile);
                    mobilenoString = _mobile.getText().toString();
                } else {
                    mobilenoString = null;
                }
            }
        });

        _amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                try {
                    if (editable.length() == 0) {
                        amountString = null;
                    } else {
                        amountString = _amount.getText().toString();
                        if ((operatorString.equals("AIRTEL") || operatorString.equals("VODAFONE") || operatorString.equals("IDEA")) && (amountString.equals("10") || amountString.equals("20") || amountString.equals("30") || amountString.equals("50"))) {
                            submit.setEnabled(false);
                            submit.setBackgroundColor(DataCardActivity.this.getResources().getColor(R.color.colorGrey));
                        } else {
                            submit.setEnabled(true);
                            submit.setBackground(DataCardActivity.this.getResources().getDrawable(R.drawable.bottomnavigation_bg));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_mobile.getText().toString().isEmpty()) {
                    layout_mobile.setError("Enter 10 Digit MobileNo ");
                } else if (_operator.getText().toString().isEmpty()) {
                    layout_operator.setError("Select Operator");
                } else if (_amount.getText().toString().isEmpty()) {
                    layout_amount.setError("Enter amount");
                } else if ((operatorString.equals("AIRTEL") || operatorString.equals("VODAFONE") || operatorString.equals("IDEA")) && (amountString.equals("10") || amountString.equals("20") || amountString.equals("30") || amountString.equals("50"))) {
                    ErrorDialog("This denomination is blocked by operator. Kindly try with a valid amount.");
                } else {
                    layout_mobile.setError(null);
                    layout_operator.setError(null);
                    layout_amount.setError(null);

                    setFBAnalytics("DATA_CARD_SUBMIT", userName);

                    rechargeDatacard();
                }
            }
        });



        _operator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setFBAnalytics("DATA_CARD_OPERATOR", userName);
                View _view = getLayoutInflater().inflate(R.layout.custom_postpaid_operator, null);
                BottomSheetDialog dialog = new BottomSheetDialog(DataCardActivity.this);
                dialog.setContentView(_view);

                ListView listView = _view.findViewById(R.id.listing);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(DataCardActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, operator);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String itemValue = (String) listView.getItemAtPosition(position);
                        operatorString = itemValue;
                        _operator.setText(itemValue);
                        Layout_circel.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

    }




    private void rechargeDatacard() {
        showLoader();
        final DataCardRequest dataCardRequest = new DataCardRequest(stdCodeString,pincodeString,amountString,mobilenoString,accountNoString,latitudeString,circleCodeString,operatorString,rechargetypeString,longitudeString);

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            //performEncodedRecharge(dataCardRequest,encodedUrl);
                            proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
    public void proceedRecharge(String url){

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode",stdCodeString);
            obj.put("amount",amountString);
            obj.put("mobileNumber",mobilenoString);
            obj.put("accountNo",accountNoString);
            obj.put("circleCode",circleCodeString);
            obj.put("operatorCode",operatorString);
            obj.put("rechargeType",rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization",_token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if(obj.getString("status").equalsIgnoreCase("0")){
                                    SuccessDialog(obj.getString("statusDesc"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message=gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            //transaction failed
                            ErrorDialog("RECHARGE FAILED! "+message.getMessage());
                            //  anError.getResponse();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ErrorDialog(String msg) {

        clear();

        Intent intent = new Intent(DataCardActivity.this, ReacargeStatusActivity.class);
        intent.putExtra("title", "Data card");
        intent.putExtra("status", "false");
        intent.putExtra("message", msg);
        startActivity(intent);
        /*AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        clear();
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    private void SuccessDialog(String msg) {

        clear();

        Intent intent = new Intent(DataCardActivity.this, ReacargeStatusActivity.class);
        intent.putExtra("title", "Data card");
        intent.putExtra("status", "true");
        intent.putExtra("message", msg);
        startActivity(intent);

     /*   AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                clear();
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    public void showLoader() {
        pd = new ProgressDialog(DataCardActivity.this);
        pd.setMessage("loading");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void hideLoader() {
        pd.hide();
    }

    private void clear() {
        rechargetypeString = "Rr";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";

        _mobile.setText("");
        _operator.setText("");
        _circel.setText("");
        _amount.setText("");

        layout_mobile.setError(null);
        layout_operator.setError(null);
        layout_amount.setError(null);
    }
    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }
}
