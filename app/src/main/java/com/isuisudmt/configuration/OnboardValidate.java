package com.isuisudmt.configuration;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;

import com.isuisudmt.R;
import com.isuisudmt.login.LoginActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OnboardValidate {
    public static boolean isValidPhone(String ph) {
        boolean check = false;
        if (ph.contains("+")) {
            ph = ph.replace("+", "");
        }
        if (ph.contains(" ")) {
            ph = ph.replace(" ", "");
        }
        if (ph.length() >= 10 && ph.length() <= 12) {
            check = android.util.Patterns.PHONE.matcher(ph).matches();
        }
        return check;

    }

    public static boolean isPassword(String password){
        Matcher matcher = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%]).{4,20})").matcher(password);
        return matcher.matches();
    }

    public static String getValidPhone(String phone) {
        String number = phone;
        if (number.contains("+")) {
            number = number.replace("+", "");
        }
        if (number.contains(" ")) {
            number = number.replace(" ", "");
        }
        if (phone.length() > 10) {
            int len = number.length();
            number = number.substring(len - 10);
        }
        return number;
    }

    //6370991241

    public static boolean isAddress(String address) {
        String regex = "^[/:#.0-9a-zA-Z\\s,-]+$";
//        String regex = "\\d+\\s+([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)";
        return address.matches(regex);
    }

    public static boolean isPhone(String p) {
        String regex = "^[6-9][0-9]{9}$";
        return p.matches(regex);
    }

    public static boolean isValidName(String name) {
        String regx = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);
        return matcher.find();
    }

    public static boolean isValidMail(String email) {
        String regex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        return email.matches(regex);
    }

    public static void showAlert(Context context, String statusDesc) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert!")
                    .setMessage(statusDesc)
                    .setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            //finish();
                        }
                    })
                    .setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            //finish();
                        }
                    });



//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }

    public static void showExitAlert(Context context) {
        String msg = "Do you want to exit the process?\nYou can resume your process later.";
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(msg)
                    .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(intent);
                            dialog.dismiss();
                            //finish();
                        }
                    });
            alertbuilderupdate.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }


}
