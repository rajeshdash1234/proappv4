package com.isuisudmt.recharge;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.load.Key;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.common.net.HttpHeaders;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.isuisudmt.BuildConfig;
import com.isuisudmt.CommonUtil;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.dmt.MyErrorMessage;
import com.isuisudmt.utils.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class PrepaidActivity extends AppCompatActivity {
    TextInputLayout Layout_circel;
    String _admin="",_token="";
    TextInputEditText _amount;
    TextInputEditText _circel;
    TextInputEditText _mobile;
    TextInputEditText _operator;
    private String accountNoString;
    ArrayAdapter<String> adapter;
    /* access modifiers changed from: private */
    public String amountString;
    Boolean amount_flag = Boolean.valueOf(false);
    private APIService apiService;
    private String circleCodeString;
    private String latitudeString;
    TextInputLayout layout_amount;
    TextInputLayout layout_mobile;
    TextInputLayout layout_operator;
    ListView listView;
    private String longitudeString;
    /* access modifiers changed from: private */
    public String mobilenoString;
    String[] operator = {"AIRCEL", "AIRTEL", "BSNL", "IDEA", "JIO", "LOOP", "MTNL DELHI", "MTNL MUMBAI", "MTS", "RELIANCE CDMA", "RELIANCE GSM", "TATA DOCOMO", "TELENOR", "VIDEOCON", "VIRGIN CDMA", "VIRGIN GSM", "VODAFONE", "TataIndicom", "T24(Flexi)"};
    /* access modifiers changed from: private */
    public String operatorString;

    /* renamed from: pd */
    ProgressDialog pd;
    private String pincodeString;
    private String rechargetypeString;
    SessionManager session;
    private String stdCodeString;
    Button submit;
    private FirebaseAnalytics firebaseAnalytics;


    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_prepaid);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        _mobile =  findViewById(R.id.mobile);
        _circel =  findViewById(R.id.circel);
        _amount =  findViewById(R.id.amount);
        _operator =  findViewById(R.id.operator);
        submit =  findViewById(R.id.submit);
        Layout_circel =  findViewById(R.id.Layout_circel);
        layout_mobile =  findViewById(R.id.layout_mobile);
        layout_operator =  findViewById(R.id.layout_operator);
        layout_amount =  findViewById(R.id.layout_amount);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        toolbar.setTitle("Prepaid");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        Layout_circel.setVisibility(View.GONE);
        rechargetypeString = "Rr";
        String str = "";
        stdCodeString = str;
        pincodeString = str;
        accountNoString = str;
        latitudeString = str;
        circleCodeString = str;
        longitudeString = str;
        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        _token = (String) user.get(SessionManager.KEY_TOKEN);
        _admin = (String) user.get(SessionManager.KEY_ADMIN);
        _mobile.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
                if (editable.length() < 10) {
                    mobilenoString = null;
                    layout_mobile.setError("Enter 10 digit mobile no ");
                    return;
                }
                PrepaidActivity prepaidActivity = PrepaidActivity.this;
                CommonUtil.hideKeyboard(prepaidActivity, prepaidActivity._mobile);
                layout_mobile.setError(null);
                PrepaidActivity prepaidActivity2 = PrepaidActivity.this;
                prepaidActivity2.mobilenoString = prepaidActivity2._mobile.getText().toString().trim();
            }
        });
        _amount.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            public void afterTextChanged(Editable editable) {
                try {
                    if (editable.length() == 0) {
                        amountString = null;
                        return;
                    }
                    amountString = _amount.getText().toString().trim();
                    if (operatorString.equals("AIRTEL") || operatorString.equals("VODAFONE") || operatorString.equals("IDEA")) {
                        if (!amountString.equals("10") && !amountString.equals("20") && !amountString.equals("30")) {
                            if (amountString.equals("50")) {

                                Toast.makeText(PrepaidActivity.this, "Not allowed !", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    else {
                        layout_amount.setError(null);
                        submit.setEnabled(true);
                        submit.setBackground(getResources().getDrawable(R.drawable.bottomnavigation_bg));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        submit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (_mobile.getText().toString().trim().length() != 10) {
                    layout_mobile.setError("Enter 10 digit mobile no ");
                } else if (_operator.getText().toString().isEmpty()) {
                    layout_operator.setError("Select operator");
                } else if (_amount.getText().toString().isEmpty()) {
                    layout_amount.setError("Enter amount");
                } else if (_amount.getText().toString().trim().equalsIgnoreCase("0")) {
                    layout_amount.setError("Enter valid amount");
                }
                else if ((operatorString.equals("AIRTEL") || operatorString.equals("VODAFONE") ||
                        operatorString.equals("IDEA")) && (amountString.equals("10") || amountString.equals("20") ||
                        amountString.equals("30") || amountString.equals("50"))) {
                    ErrorDialog("This denomination is blocked by operator. Kindly try with a valid amount.");
                } else {
                    layout_mobile.setError(null);
                    layout_operator.setError(null);
                    layout_amount.setError(null);
                    rechargePrepaid();
                    setFBAnalytics("RECHARGE_SUBMIT", com.isuisudmt.Constants.USER_NAME);
                }
            }
        });
        _operator.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                setFBAnalytics("RECHARGE_OPERATOR", com.isuisudmt.Constants.USER_NAME);
                View _view = getLayoutInflater().inflate(R.layout.custom_postpaid_operator, null);
                final BottomSheetDialog dialog = new BottomSheetDialog(PrepaidActivity.this);
                dialog.setContentView(_view);
                listView = (ListView) _view.findViewById(R.id.listing);
                adapter = new ArrayAdapter<String>(PrepaidActivity.this, android.R.layout.simple_list_item_1, operator);
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        String itemValue = (String) listView.getItemAtPosition(position);
                        operatorString = itemValue;
                        _operator.setText(itemValue);
                        layout_operator.setError(null);
                        Layout_circel.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void rechargePrepaid() {
        showLoader();
        PrepaidRequest prepaidRequest = new PrepaidRequest(this.stdCodeString, this.pincodeString, this.amountString, this.mobilenoString, this.accountNoString, this.latitudeString, this.circleCodeString, this.operatorString, this.rechargetypeString, this.longitudeString);
        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(BuildConfig.GET_AEPS_RECHARGE_URL).setPriority(Priority.HIGH).build().getAsJSONObject(new JSONObjectRequestListener() {
            public void onResponse(JSONObject response) {
                try {
                    String key = new JSONObject(response.toString()).getString("hello");
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append(">>>>-----");
                    sb.append(key);
                    printStream.println(sb.toString());
                    proceedRecharge(new String(Base64.decode(key, 0), Key.STRING_CHARSET_NAME));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }

            public void onError(ANError anError) {
            }
        });
    }

    public void proceedRecharge(String url) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode", stdCodeString);
            obj.put("amount", amountString);
            obj.put("mobileNumber", mobilenoString);
            obj.put("accountNo", accountNoString);
            obj.put("circleCode", circleCodeString);
            obj.put("operatorCode", operatorString);
            obj.put("rechargeType", rechargetypeString);
            AndroidNetworking.post(url).setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders(HttpHeaders.AUTHORIZATION, _token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject obj = new JSONObject(response.toString());
                        System.out.println(obj.toString());
                        hideLoader();
                        if (obj.getString(NotificationCompat.CATEGORY_STATUS).equalsIgnoreCase("0")) {
                            SuccessDialog(obj.getString("statusDesc"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        hideLoader();
                        ErrorDialog("RECHARGE FAILED! undefined");
                    }
                }

                public void onError(ANError anError) {
                    hideLoader();
                    MyErrorMessage message = (MyErrorMessage) new Gson().fromJson(anError.getErrorBody(), MyErrorMessage.class);
                    StringBuilder sb = new StringBuilder();
                    sb.append("RECHARGE FAILED! ");
                    sb.append(message.getMessage());
                    ErrorDialog(sb.toString());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void ErrorDialog(String msg) {
        clear();
        Intent intent = new Intent(this, ReacargeStatusActivity.class);
        intent.putExtra("title", "Prepaid Mobile");
        intent.putExtra(NotificationCompat.CATEGORY_STATUS, "false");
        intent.putExtra("message", msg);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void SuccessDialog(String msg) {
        clear();
        Intent intent = new Intent(this, ReacargeStatusActivity.class);
        intent.putExtra("title", "Prepaid Mobile");
        intent.putExtra(NotificationCompat.CATEGORY_STATUS, "true");
        intent.putExtra("message", msg);
        startActivity(intent);
    }

    public void showLoader() {
        try {
           pd = new ProgressDialog(PrepaidActivity.this);
           pd.setMessage("Loading...");
           pd.setCanceledOnTouchOutside(false);
           pd.show();
        } catch (Exception e) {
        }
    }

    public void hideLoader() {
        try {
            if (!isFinishing() && !isDestroyed() && pd != null && pd.isShowing()) {
                pd.dismiss();
                pd.cancel();
            }
        } catch (Exception e) {
        }
    }

    private void clear() {
       rechargetypeString = "Rr";
        String str = "";
       stdCodeString = str;
       pincodeString = str;
       accountNoString = str;
       latitudeString = str;
       circleCodeString = str;
       longitudeString = str;
       _mobile.setText(str);
       _operator.setText(str);
       _circel.setText(str);
       _amount.setText(str);
       _mobile.requestFocus();
       layout_mobile.setError(null);
       layout_operator.setError(null);
       layout_amount.setError(null);
    }
    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }
}
