package com.isuisudmt.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.isuisudmt.R;

public class StoreListActivity extends AppCompatActivity {
    Button search;
    TextView textView;
    String appName, appText, mainApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_list);
        textView = findViewById(R.id.search_text);
        search = findViewById(R.id.list_search);

        appName = getIntent().getStringExtra("appName");
        mainApp = getIntent().getStringExtra("mainApp");
        appText = "A new version of " + appName + " has been released with improved speed and user experience please get the updated app from the following link.";
        textView.setText(appText);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+mainApp));
                startActivity(intent);
            }
        });
    }
}
