package com.isuisudmt.matm;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.report.adapter.AdapterReportMATM;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;


public class MicroAtmReportActivity extends AppCompatActivity implements MicroReportContract.View, DatePickerDialog.OnDateSetListener {
    private RecyclerView reportRecyclerView;
    private MicroReportPresenter mActionsListener;
    ArrayList<MicroReportModel> reportResponseArrayList ;
    LinearLayout dateLayout,detailsLayout;

    private AdapterReportMATM adapterReportMATM;
    TextView noData,totalreport,amount;
    TextView chooseDateRange;
    //private static final String TAG = ReportActivity.class.getSimpleName ();
    //LoadingView loadingView;
    //Session session;
    String fromdate = "";
    String todate = "";
    String clientId = "";

    TransactionStatusModel transactionStatusModel;
    SearchView searchView;

    private boolean ascending = true;
    SessionManager session;
    String tokenStr="";
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_micro_atm_report );

        setToolbar ();
        session = new SessionManager(getApplicationContext());

        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);

        //session = new Session(MicroAtmReportActivity.this);
        noData = findViewById ( R.id.noData );
        amount = findViewById ( R.id.amount );
        totalreport = findViewById ( R.id.totalreport );
        chooseDateRange = findViewById ( R.id.chooseDateRange );
        dateLayout = findViewById ( R.id.dateLayout );
        detailsLayout = findViewById ( R.id.detailsLayout );
        reportRecyclerView = findViewById ( R.id.reportRecyclerView );
        pd = new ProgressDialog(MicroAtmReportActivity.this);
        reportRecyclerView.setHasFixedSize ( true );
        reportRecyclerView.setLayoutManager ( new LinearLayoutManager(MicroAtmReportActivity.this, LinearLayoutManager.VERTICAL,false ) );
        mActionsListener = new MicroReportPresenter( this );

        dateLayout.setOnClickListener(new View.OnClickListener () {
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(MicroAtmReportActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Datepickerdialog");
                dpd.setMaxDate ( Calendar.getInstance () );
                dpd.setAutoHighlight ( true );
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            return false;
        }
        return false;
    }

    private void setToolbar() {
        // Inflate the layout for this fragment
        Toolbar mToolbar = findViewById ( R.id.toolbar );
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle (getResources().getString(R.string.matm_report_title) );

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //What to do on back clicked
            }
        });
    }


    @Override
    public void reportsReady(ArrayList<MicroReportModel> reportModelArrayList, String totalAmount) {
        reportResponseArrayList = reportModelArrayList;
        amount.setText(getResources().getString(R.string.toatlamountacitvity)+totalAmount);
    }

    @Override
    public void refreshDone(RefreshModel refreshModel) {
       /* Intent intent = new Intent(getBaseContext(), com.finopaytech.finosdk.activity.MainTransactionActivity.class);
        intent.putExtra("RequestData", refreshModel.getEncData());
        intent.putExtra("HeaderData", refreshModel.getAuthentication());
        intent.putExtra("ReturnTime", 5);// Application return time in second
        startActivityForResult(intent, 1);*/
    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null & resultCode == RESULT_OK && requestCode == 1)
        {
            transactionStatusModel = new TransactionStatusModel();
            String response;
            if (data.hasExtra( "ClientResponse")) {
                hideLoader();
                response = data.getStringExtra("ClientResponse");
                if (!response.equalsIgnoreCase("")) {
                    try{
                        String strDecryptResponse = AES_BC.getInstance().decryptDecode(Utils.replaceNewLine(response), Constants.CLIENT_REQUEST_ENCRYPTION_KEY);
                        JSONObject jsonObject = new JSONObject(strDecryptResponse);
                        transactionStatusModel.setStatus(jsonObject.getString("status"));
                        transactionStatusModel.setAmount(jsonObject.getString("Amount"));
                        transactionStatusModel.setLocaldate(jsonObject.getString("Localdate"));
                        transactionStatusModel.setRRN(jsonObject.getString("RRN"));
                        transactionStatusModel.setTransactionType(jsonObject.getString("TransactionType"));
                        transactionStatusModel.setClientId(clientId);
                        //Utils.showOneBtnDialog(this, getString(com.finopaytech.finosdk.R.string.STR_INFO), strDecryptResponse, false);
                    }
                    catch (Exception e){
                        transactionStatusModel = null;
                        Log.v("test","error : "+e);
                    }
                }else{
                    transactionStatusModel = null;
                }
            } else if (data.hasExtra("ErrorDtls")) {
                hideLoader();
                response = data.getStringExtra("ErrorDtls");
                if (!response.equalsIgnoreCase("")) {
                    try {
                        String[] error_dtls = response.split("\\|");
                        String errorMsg = error_dtls[0];
                        String errorDtlsMsg = error_dtls[1];
                        transactionStatusModel.setErrorMsg(errorMsg);
                        //Utils.showOneBtnDialog(this, getString(com.finopaytech.finosdk.R.string.STR_INFO), "Error Message : " + errorMsg + "\n" + " Error Details : " + errorDtlsMsg, false);
                    }
                    catch (Exception exp)
                    {
                        transactionStatusModel = null;
                        Log.v("test","Error : " + exp.toString());
                    }
                }
            }else{
                transactionStatusModel = null;
            }
            nextPage();

        }else if(requestCode == Constants.RELOADREPORTS){
            mActionsListener.loadReports ( fromdate, Util.getNextDate (todate), session.getUserToken (),"IATM_TRANSACTION");
        }

    }*/

    public void nextPage(){

/*        Intent intentAtm = new Intent(MicroAtmReportActivity.this,TransactionStatusReportActivity.class);
        intentAtm.putExtra(Constants.TRANSACTION_STATUS_KEY_SDK,transactionStatusModel);
        startActivityForResult(intentAtm,Constants.RELOADREPORTS);*/

    }

    @Override
    public void showReports() {

        if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {

            reportRecyclerView.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.VISIBLE);
            noData.setVisibility(View.GONE);
            adapterReportMATM = new AdapterReportMATM(this,reportResponseArrayList,"IATM_SETTLED", new AdapterReportMATM.IMethodCaller() {
                @Override
                public void refreshMethod(MicroReportModel microReportModel) {
                    clientId = String.valueOf(microReportModel.getId());
                    mActionsListener.refreshReports(tokenStr,String.valueOf(microReportModel.getAmountTransacted()),"statusEnquiry","mobile",String.valueOf(microReportModel.getId()));
                }
            }, amount, totalreport);
            reportRecyclerView.getRecycledViewPool().clear();
            adapterReportMATM.notifyDataSetChanged();
            reportRecyclerView.setAdapter(adapterReportMATM);
            totalreport.setText("Found "+reportResponseArrayList.size()+"/"+reportResponseArrayList.size()+" Entries");
        }else{
            reportRecyclerView.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showLoader() {
        try {
            pd.setMessage("Loading....");
            pd.setCancelable(false);
            pd.show();
        }catch (Exception e){

        }

    }

    @Override
    public void hideLoader() {
        try{
            pd.dismiss();
        }catch (Exception e){

        }

    }

  /*  @Override
    public void showLoader() {
        if (loadingView ==null){
            loadingView = showProgress(MicroAtmReportActivity.this);
        }
        loadingView.show();
    }*/

  /*  @Override
    public void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }
    }*/

    @Override
    public void emptyDates() {
        Toast.makeText(MicroAtmReportActivity.this,getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
    }

    @Override
    public void checkAmount(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(MicroAtmReportActivity.this,getResources().getString(R.string.amountrefersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionMode(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(MicroAtmReportActivity.this,getResources().getString(R.string.transaction_mode_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkTransactionType(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(MicroAtmReportActivity.this,getResources().getString(R.string.transaction_type_refersh) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkClientId(String status) {
        if(status != null && status.matches("1")){
            Toast.makeText(MicroAtmReportActivity.this,getResources().getString(R.string.clientUniqueId) , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void emptyRefreshData(String status) {
        Toast.makeText(MicroAtmReportActivity.this, getResources().getString(R.string.severerror), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

        fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + dayOfMonthEnd;
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+dayOfMonthEnd+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        chooseDateRange.setText ( date );

        if(chooseDateRange.getText () !=null && !chooseDateRange.getText ().toString ().matches ( "" ) && !chooseDateRange.getText ().toString ().trim ().matches ( "Choose Date Range for Reports" )) {

            if (!(Util.getDateDiff ( new SimpleDateFormat( "yyyy-MM-dd" ),fromdate, todate ) > 10)) {
                if (Util.compareDates ( fromdate, todate ) == "1") {
                    noData.setVisibility ( View.GONE );
                    reportRecyclerView.setVisibility ( View.VISIBLE );
                    mActionsListener.loadReports ( fromdate, Util.getNextDate (todate),tokenStr,"IATM_TRANSACTION","");
                } else if (Util.compareDates ( fromdate, todate ) == "2") {
                    noData.setVisibility ( View.VISIBLE );
                    reportRecyclerView.setVisibility ( View.GONE );
                    Toast.makeText ( MicroAtmReportActivity.this, "Please select the Appropriate Dates", Toast.LENGTH_SHORT ).show ();
                } else if(Util.compareDates ( fromdate,todate) == "3" ) {
                    noData.setVisibility ( View.GONE );
                    reportRecyclerView.setVisibility ( View.VISIBLE );
                    mActionsListener.loadReports ( fromdate, Util.getNextDate (todate),tokenStr,"IATM_TRANSACTION","");
                }
            } else {
                Toast.makeText ( MicroAtmReportActivity.this, "Please choose the date within 10 days from the current date ", Toast.LENGTH_LONG ).show ();
                noData.setVisibility ( View.VISIBLE );
                reportRecyclerView.setVisibility ( View.GONE );
            }
        }else{
            noData.setVisibility ( View.VISIBLE );
            reportRecyclerView.setVisibility ( View.GONE );
        }
    }
    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_menu, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_calender).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(R.id.search_src_text);

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }

        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // vendorPriceListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
                    // filter recycler view when text is changed
                    adapterReportMATM.getFilter().filter(query);
                }else{
                    Toast.makeText(MicroAtmReportActivity.this,getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }else if(id == R.id.action_sort){
            sortData(ascending);
            ascending = !ascending;
        }*/

        return super.onOptionsItemSelected(item);
    }
    private void sortData(boolean asc)
    {
        //SORT ARRAY ASCENDING AND DESCENDING
        if (asc) {
            Collections.reverse(reportResponseArrayList);
        }
        else {
            Collections.reverse(reportResponseArrayList);
        }
        //ADAPTER
        reportRecyclerView.setAdapter(adapterReportMATM);

    }
}
