package com.isuisudmt.matm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefreshModel {

    @SerializedName("encData")
    @Expose
    private String encData;

    @SerializedName("authentication")
    @Expose
    private String authentication;

    @SerializedName("errorResponse")
    @Expose
    private String errorResponse;

    public RefreshModel(){

    }

    public String getEncData() {
        return encData;
    }

    public void setEncData(String encData) {
        this.encData = encData;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(String errorResponse) {
        this.errorResponse = errorResponse;
    }
}

