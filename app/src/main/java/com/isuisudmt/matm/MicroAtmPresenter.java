package com.isuisudmt.matm;

import android.util.Base64;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.isuisudmt.BuildConfig.GET_LOADED_FEATURE_URL;
import static com.isuisudmt.BuildConfig.GET_TBalanceEnquiryStatus_MICRO_ATM_URL;


public class MicroAtmPresenter implements MicroAtmContract.UserActionsListener {

    private MicroAtmContract.View microAtmContractView;
    private AEPSAPIService aepsapiService;

    public MicroAtmPresenter(MicroAtmContract.View microAtmContractView) {
        this.microAtmContractView = microAtmContractView;
    }

    @Override
    public void performRequestData(final String token, final MicroAtmRequestModel microAtmRequestModel) {

//        if(microAtmRequestModel!=null && microAtmRequestModel.getAmount()!=null && microAtmRequestModel.getTransactionType()!=null
//                && !microAtmRequestModel.getTransactionType().matches("") && microAtmRequestModel.getTransactionMode()!=null && !microAtmRequestModel.getTransactionMode().matches(""))
//        {
            microAtmContractView.showLoader();

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            AndroidNetworking.get(GET_TBalanceEnquiryStatus_MICRO_ATM_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        microAtmContractView.hideLoader();
                        try {

                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            encodedUrl = "https://matm1.iserveu.website/sendEncryptedRequestNFS";

                            encriptPerformRequestData(token,microAtmRequestModel,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

          //  {"BalanceEnquiryStatus":"Balance Enquiry Successful","RRN":"900515029759","CardNumber":"**********956444","AvailableBalance":"29924.68","TransactionDatetime":"2019-01-05 15:41:56","AccountNo":"50100109092763","TerminalID":"IS000008"}


//        } else {
//            microAtmContractView.hideLoader();
//            microAtmContractView.checkEmptyFields();
//        }

    }

    public void encriptPerformRequestData(String token, MicroAtmRequestModel microAtmRequestModel, String encodedUrl){

        MicroAtmAPI microAtmAPI =this.aepsapiService.getClient().create(MicroAtmAPI.class);

        microAtmAPI.checkRequestCode(token,microAtmRequestModel,encodedUrl).enqueue(new Callback<MicroAtmResponse>() {
            @Override
            public void onResponse(Call<MicroAtmResponse> call, Response<MicroAtmResponse> response) {
                if(response.isSuccessful()) {
                    // String message = "";
                    if (response.body().getAuthentication()!=null && !response.body().getAuthentication().matches("")) {
                        //message = "Login Successful";
                        microAtmContractView.hideLoader();
                        microAtmContractView.checkRequestCode(response.body().getAuthentication(), response.body().getEncData(),response.body());
                    }else{
                        microAtmContractView.hideLoader();
                        microAtmContractView.checkRequestCode("", "Encryption Failed",null);
                        Log.v("laxmi","hf"+microAtmContractView);
                    }

                } else {
                    if(response.errorBody() != null) {
                        microAtmContractView.hideLoader();
//                        microAtmContractView.checkRequestCode("","Something went wrong, please try after sometimes",null);

                        //Just open and print the error response coming in json i.e. size=77 text={"encData":null,"authentication":null,"errorResponse":"Service Unavailable..]
                        JsonParser parser = new JsonParser();
                        try {
                            JsonElement mJson = null;
                            mJson = parser.parse(response.errorBody().string());
                           //Added new-----
//                            Gson gson = new GsonBuilder()
//                                    .setLenient()
//                                    .create();
                            Gson gson = new Gson(); //added previously
                            MicroAtmResponse errorResponse = gson.fromJson(mJson, MicroAtmResponse.class);
                            microAtmContractView.hideLoader();
                            microAtmContractView.checkRequestCode("",errorResponse.getErrorResponse(),null);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }else{
                        microAtmContractView.hideLoader();
                        microAtmContractView.checkRequestCode("","Service is currently unavailable",null);
                    }
                }
            }

            @Override
            public void onFailure(Call<MicroAtmResponse> call, Throwable t) {

                microAtmContractView.hideLoader();
                microAtmContractView.checkRequestCode("", "Encryption Failed",null);

            }
        });
    }

    @Override
    public void loadFeature(final String token) {
        if (token!=null) {

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            microAtmContractView.showLoader ();
            // this.aepsapiService = new AEPSAPIService();

            AndroidNetworking.get(GET_LOADED_FEATURE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            enCryptLoadFeature(aepsapiService,token,encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        }
    }

    public void enCryptLoadFeature(AEPSAPIService aepsapiService, String token, String encodedUrl){
        BalanceApi balanceApi = this.aepsapiService.getClient().create(BalanceApi.class);
        Call<BalanceResponse> respuesta = balanceApi.getBalanceDetails(token,encodedUrl);
        respuesta.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                if (response.isSuccessful()){
                    String json = response.body().getUserInfoModel().getUserBrand();

                    try {

                        JSONObject obj = new JSONObject(json);

                        if (obj.has("brand")){
                            response.body().getUserInfoModel().setUserBrand(obj.getString("brand"));
                        }

                        //balanceView.showBalance(response.body().getUserInfoModel().getUserBrand());


                    } catch (Throwable t) {
                    }
                    microAtmContractView.hideLoader ();
                    microAtmContractView.showFeature(response.body().getUserInfoModel().getFeatureIdList());
                }

            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                microAtmContractView.hideLoader ();
            }


        });
    }

    private class GET_LOADED_FEATURE_URL {
    }
}
