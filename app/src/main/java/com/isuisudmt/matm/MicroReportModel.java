package com.isuisudmt.matm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MicroReportModel {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("previousAmount")
    @Expose
    private String previousAmount;
    @SerializedName("amountTransacted")
    @Expose
    private String amountTransacted;

    @SerializedName("amountTransactedValue")
    @Expose
    private Float amountTransactedValue;
    @SerializedName("apiTId")
    @Expose
    private String apiTId;
    @SerializedName("balanceAmount")
    @Expose
    private String balanceAmount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;
    @SerializedName("masterName")
    @Expose
    private String masterName;

    @SerializedName("userTrackId")
    @Expose
    private String userTrackId;

    @SerializedName("cardDetail")
    @Expose
    private String cardDetail;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    @SerializedName("operationPerformed")
    @Expose
    private String operationPerformed;

    @SerializedName("transactionMode")
    @Expose
    private String transactionMode;




    @SerializedName("previousBalance")
    @Expose
    private String previousBalance;
    @SerializedName("currentBalance")
    @Expose
    private Float currentBalance;
    @SerializedName("relationalId")
    @Expose
    private String relationalId;
    @SerializedName("relationalOperation")
    @Expose
    private String relationalOperation;






    public Float getAmountTransactedValue() {
        return amountTransactedValue;
    }

    public void setAmountTransactedValue(Float amountTransactedValue) {
        this.amountTransactedValue = amountTransactedValue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        this.previousAmount = previousAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(String amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public String getApiTId() {
        return apiTId;
    }

    public void setApiTId(String apiTId) {
        this.apiTId = apiTId;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getUserTrackId() {
        return userTrackId;
    }

    public void setUserTrackId(String userTrackId) {
        this.userTrackId = userTrackId;
    }

    public String getCardDetail() {
        return cardDetail;
    }

    public void setCardDetail(String cardDetail) {
        this.cardDetail = cardDetail;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String  createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getOperationPerformed() {
        return operationPerformed;
    }

    public void setOperationPerformed(String operationPerformed) {
        this.operationPerformed = operationPerformed;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(String previousBalance) {
        this.previousBalance = previousBalance;
    }

    public Float getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Float currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getRelationalId() {
        return relationalId;
    }

    public void setRelationalId(String relationalId) {
        this.relationalId = relationalId;
    }

    public String getRelationalOperation() {
        return relationalOperation;
    }

    public void setRelationalOperation(String relationalOperation) {
        this.relationalOperation = relationalOperation;
    }
}
