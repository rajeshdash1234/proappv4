package com.isuisudmt.bbps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.bbps.BBPSmvp.BBPSContract;
import com.isuisudmt.bbps.BBPSmvp.BbpsModelFetchBill;
import com.isuisudmt.bbps.BBPSmvp.BbpsPresenter;
import com.isuisudmt.bbps.utils.BillerListPojo;
import com.isuisudmt.bbps.utils.Const;
import com.isuisudmt.bbps.utils.GetBillerParameters;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityBBPS extends AppCompatActivity implements BBPSContract.View {

    //Part1
    Spinner spinnerOperator;
    EditText etField26, etField1, etField27, etField28, etField29;
    TextView tvField26, tvField1, tvField27, tvField28, tvField29;
    TextView tvHintField26, tvHintField27;
    LinearLayout llMainContentOne, llField26, llField1, llField27, llField28, llField29;

    ImageView ivOperator, ivPaymentType;
    ProgressBar pbOperator, pbPaymentType;

    //Part2
    CardView cvMainContentTwo;
    TextView tvCustomerName, tvBillDate, tvBillAmount, tvBillPeriod, tvBillNumber, tvDueDate, tvFixedCharges, tvAdditionalCharges,
            tvEarlyPaymentFees, tvLatePaymentFees, tvModifySearch;
    Button btnProceedToPay;
    LinearLayout llFetchResponseCustomerName, llFetchResponseBillDate, llFetchResponseBillAmount, llFetchResponseBillPeriod,
            llFetchResponseBillNumber, llFetchResponseDueDate, llFetchResponseFixedCharges, llFetchResponseAdditionalCharges,
            llFetchResponseEarlyPaymentFees, llFetchResponseLatePaymentFees;

    //Part3
    EditText etOptionAmount, etCCF, etTotalAmount, etRemarks;
    Spinner spinnerPaymentMode;
    LinearLayout llMainThree;


    //Buttons
    LinearLayout llBtnFetchPay;
    Button btnPayBill, btnFetchBill;


    String BlrName = "", CatName = "", Bill_Amount = "", Late_Payment_Fees = "", Early_Payment_Fees = "", Reference_id = "", selected_type = "", AdhocPayment = "", selected_payment_type = "", AgentReferenceId = "", tokenStr = "", ccf = "", QuickPay = "", TextField1 = "";
    Boolean IS_BILL_FETCHED = false;
    Double dTotalAmount = 0.00, dBillAmount = 0.00;

    SessionManager session;
    ArrayList<BillerListPojo> biller_item_arr;
    ArrayAdapter adapterOperator;
    String blrID = "";
    Map<String, Object> otherParameters;

    String CategoryID = "0";
    DecimalFormat decim;

    //MVP
    private BbpsPresenter mActionsListener;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_bbps);

        initValue();

        getOperators();

        setupPaymentMode();

        btnFetchBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IS_BILL_FETCHED = true;

                String _field_26 = etField26.getText().toString().trim();
                String _field_1 = etField1.getText().toString().trim();
                String _field27 = "";
                String _field28 = "";
                String _field29 = "";
                String _field2 = "";

                //Different IF condition for different fields
                if (blrID.equals(Const.BLR_ID_LPG_GAS_1) || blrID.equals(Const.BLR_ID_LPG_GAS_2) || blrID.equals(Const.BLR_ID_LPG_GAS_3) || blrID.equals(Const.BLR_ID_BROADBAND_POSTPAID_21) || blrID.equals(Const.BLR_ID_WATER_9) || blrID.equals(Const.BLR_ID_WATER_13) || blrID.equals(Const.BLR_ID_WATER_16) || blrID.equals(Const.BLR_ID_WATER_17) || blrID.equals(Const.BLR_ID_WATER_28) || blrID.equals(Const.BLR_ID_LANDLINE_POSTPAID_5) || blrID.equals(Const.BLR_ID_LANDLINE_POSTPAID_6) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_1) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_2) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_3) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_7) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_8) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_2) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_3) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_10) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_16) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_18) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_19) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_24) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_29) || blrID.equals(Const.BLR_ID_HEALTH_INSURANCE_1) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_36) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_37) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_52) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_56) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_57) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_66) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_69) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_71) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_80) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_82) || blrID.equals(Const.BLR_ID_INSURANCE_1) || blrID.equals(Const.BLR_ID_INSURANCE_2) || blrID.equals(Const.BLR_ID_INSURANCE_4) || blrID.equals(Const.BLR_ID_INSURANCE_5) || blrID.equals(Const.BLR_ID_INSURANCE_6) || blrID.equals(Const.BLR_ID_INSURANCE_7) || blrID.equals(Const.BLR_ID_INSURANCE_8) || blrID.equals(Const.BLR_ID_INSURANCE_9) || blrID.equals(Const.BLR_ID_INSURANCE_10) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_1) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_5) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_6) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_7) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_12))
                    _field27 = etField27.getText().toString().trim();

                if (blrID.equals(Const.BLR_ID_LPG_GAS_3) || blrID.equals(Const.BLR_ID_WATER_13) || blrID.equals(Const.BLR_ID_WATER_16) || blrID.equals(Const.BLR_ID_WATER_17) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_8) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_37) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_52) || blrID.equals(Const.BLR_ID_INSURANCE_5) || blrID.equals(Const.BLR_ID_INSURANCE_6) || blrID.equals(Const.BLR_ID_INSURANCE_7))
                    _field28 = etField28.getText().toString().trim();

                if (blrID.equals(Const.BLR_ID_LPG_GAS_3) || blrID.equals(Const.BLR_ID_WATER_16) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_8) || blrID.equals(Const.BLR_ID_INSURANCE_5))
                    _field29 = etField29.getText().toString().trim();


                if (_field_26.equals(""))
                    Toast.makeText(ActivityBBPS.this, "Please enter " + TextField1, Toast.LENGTH_SHORT).show();
                else if (_field_1.equals(""))
                    Toast.makeText(ActivityBBPS.this, Const.ERROR_MSG_NULL_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else if (_field_1.length() < 6)
                    Toast.makeText(ActivityBBPS.this, Const.ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else {
                    mActionsListener.apiFetchBill(blrID, GetBillerParameters.getFieldValuesJsonCommonFetchBill(ActivityBBPS.this, _field_1, _field_26, _field27, _field28, _field29, _field2), tokenStr);
                }


            }
        });

        btnPayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IS_BILL_FETCHED == true)
                    QuickPay = "0";
                else
                    QuickPay = "1";


                String billAmount = etOptionAmount.getText().toString().trim();
                dBillAmount = Double.parseDouble(billAmount);
                String _postpaid_mobile_no = etField26.getText().toString().trim();
                String _customer_mobile_no = etField1.getText().toString().trim();
                String _field27 = "";
                String _field28 = "";
                String _field29 = "";
                String _field2 = "";

                int checkWalletBalance = Double.compare(Const.Wallet2Amount, dTotalAmount);

                //Different IF condition for different fields
                if (blrID.equals(Const.BLR_ID_LPG_GAS_1) || blrID.equals(Const.BLR_ID_LPG_GAS_2) || blrID.equals(Const.BLR_ID_LPG_GAS_3) || blrID.equals(Const.BLR_ID_BROADBAND_POSTPAID_21) || blrID.equals(Const.BLR_ID_WATER_9) || blrID.equals(Const.BLR_ID_WATER_13) || blrID.equals(Const.BLR_ID_WATER_16) || blrID.equals(Const.BLR_ID_WATER_17) || blrID.equals(Const.BLR_ID_WATER_28) || blrID.equals(Const.BLR_ID_LANDLINE_POSTPAID_5) || blrID.equals(Const.BLR_ID_LANDLINE_POSTPAID_6) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_1) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_2) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_3) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_7) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_8) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_2) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_3) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_10) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_16) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_18) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_19) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_24) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_29) || blrID.equals(Const.BLR_ID_HEALTH_INSURANCE_1) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_36) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_37) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_52) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_56) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_57) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_66) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_69) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_71) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_80) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_82) || blrID.equals(Const.BLR_ID_INSURANCE_1) || blrID.equals(Const.BLR_ID_INSURANCE_2) || blrID.equals(Const.BLR_ID_INSURANCE_4) || blrID.equals(Const.BLR_ID_INSURANCE_5) || blrID.equals(Const.BLR_ID_INSURANCE_6) || blrID.equals(Const.BLR_ID_INSURANCE_7) || blrID.equals(Const.BLR_ID_INSURANCE_8) || blrID.equals(Const.BLR_ID_INSURANCE_9) || blrID.equals(Const.BLR_ID_INSURANCE_10) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_1) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_5) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_6) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_7) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_12))
                    _field27 = etField27.getText().toString().trim();

                if (blrID.equals(Const.BLR_ID_LPG_GAS_3) || blrID.equals(Const.BLR_ID_WATER_13) || blrID.equals(Const.BLR_ID_WATER_16) || blrID.equals(Const.BLR_ID_WATER_17) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_8) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_37) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_52) || blrID.equals(Const.BLR_ID_INSURANCE_5) || blrID.equals(Const.BLR_ID_INSURANCE_6) || blrID.equals(Const.BLR_ID_INSURANCE_7))
                    _field28 = etField28.getText().toString().trim();

                if (blrID.equals(Const.BLR_ID_LPG_GAS_3) || blrID.equals(Const.BLR_ID_WATER_16) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_8) || blrID.equals(Const.BLR_ID_INSURANCE_5))
                    _field29 = etField29.getText().toString().trim();


                if (_postpaid_mobile_no.equals(""))
                    Toast.makeText(ActivityBBPS.this, "Please enter " + TextField1, Toast.LENGTH_SHORT).show();
                else if (_customer_mobile_no.equals(""))
                    Toast.makeText(ActivityBBPS.this, Const.ERROR_MSG_NULL_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else if (billAmount.equals(""))
                    Toast.makeText(ActivityBBPS.this, "Please enter Amount", Toast.LENGTH_SHORT).show();
                else if (_customer_mobile_no.length() < 6)
                    Toast.makeText(ActivityBBPS.this, Const.ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else if (selected_payment_type.equals("Choose Payment Mode") || selected_payment_type.equals(""))
                    Toast.makeText(ActivityBBPS.this, "Please select a payment mode", Toast.LENGTH_SHORT).show();
                else if (checkWalletBalance < 0)
                    Toast.makeText(ActivityBBPS.this, Const.ERROR_MSG_LOW_BALANCE_WALLET_2, Toast.LENGTH_SHORT).show();
                else {
                    String remarks = etRemarks.getText().toString().trim();
                    String bill_amount = decim.format(dBillAmount);
                    String total_amount = decim.format(dTotalAmount);
                    String pin_code = getSharedPreferences(Const.BBPS_PREF, Context.MODE_PRIVATE).getString(Const.SF_PINCODE, "");
                    String pos_mobile_number = getSharedPreferences(Const.BBPS_PREF, Context.MODE_PRIVATE).getString(Const.SF_MOBILE_NUMBER, "");
                    String lat_long = getSharedPreferences(Const.BBPS_PREF, Context.MODE_PRIVATE).getString(Const.SF_LAT_LONG, "");
                    String terminal_id = getSharedPreferences(Const.BBPS_PREF, Context.MODE_PRIVATE).getString(Const.SF_TERMINAL_ID, "");

                    if (blrID.equals("MAHA00000MAH01") || blrID.equals("RELI00000MUM03")) {
                        mActionsListener.apiPayBill(bill_amount, blrID, _postpaid_mobile_no, _customer_mobile_no,
                                _field27, _field28, _field29, _field2, Bill_Amount, Early_Payment_Fees, Late_Payment_Fees,
                                Reference_id, remarks, QuickPay, ccf, total_amount, CatName, BlrName, pin_code, pos_mobile_number, lat_long,
                                terminal_id, tokenStr);
                    } else {
                        mActionsListener.apiPayBill(bill_amount, blrID, _postpaid_mobile_no, _customer_mobile_no,
                                _field27, _field28, _field29, _field2, Bill_Amount, Late_Payment_Fees, Early_Payment_Fees,
                                Reference_id, remarks, QuickPay, ccf, total_amount, CatName, BlrName, pin_code, pos_mobile_number, lat_long,
                                terminal_id, tokenStr);
                    }
                }

            }
        });

        btnProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMainContentTwo.setVisibility(View.GONE);
                llMainThree.setVisibility(View.VISIBLE);
                llBtnFetchPay.setVisibility(View.VISIBLE);
                btnFetchBill.setVisibility(View.GONE);
                btnPayBill.setVisibility(View.VISIBLE);

                etOptionAmount.setText(Bill_Amount);
                setEdittextEditable(etField26, false, R.drawable.disable_edittext);
                setEdittextEditable(etField1, false, R.drawable.disable_edittext);
                setEdittextEditable(etField27, false, R.drawable.disable_edittext);
                setEdittextEditable(etField28, false, R.drawable.disable_edittext);
                setEdittextEditable(etField29, false, R.drawable.disable_edittext);
                setEdittextEditable(etCCF, false, R.drawable.disable_edittext);
                setEdittextEditable(etTotalAmount, false, R.drawable.disable_edittext);
                if (AdhocPayment.equals("0")) {
                    setEdittextEditable(etOptionAmount, false, R.drawable.disable_edittext);
                } else if (AdhocPayment.equals("1")) {
                    setEdittextEditable(etOptionAmount, true, R.drawable.border_white_bg);
                }

                spinnerOperator.setEnabled(false);
                spinnerOperator.setBackgroundResource(R.drawable.disable_edittext);

            }
        });

        tvModifySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMainContentTwo.setVisibility(View.GONE);
                llMainContentOne.setVisibility(View.VISIBLE);
                llBtnFetchPay.setVisibility(View.VISIBLE);
                btnFetchBill.setVisibility(View.VISIBLE);
                btnPayBill.setVisibility(View.GONE);
            }
        });

        etOptionAmount.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    if (selected_payment_type.equals("") || selected_payment_type.equals("Choose Payment Mode")) {

                    } else {
                        //Reset Payment Mode
                        setupPaymentMode();

                        etTotalAmount.setText("");
                        etCCF.setText("");

                        setEdittextEditable(etCCF, true, R.drawable.border_white_bg);
                        setEdittextEditable(etTotalAmount, true, R.drawable.border_white_bg);
                    }

                }
            }
        });

    }

    public void initValue() {

        Intent intent = getIntent();
        CategoryID = intent.getStringExtra("CategoryID");

        SetTitle();

        //MVP
        mActionsListener = new BbpsPresenter(ActivityBBPS.this);


        session = new SessionManager(ActivityBBPS.this);
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);

        biller_item_arr = new ArrayList<>();
        decim = new DecimalFormat("0.00");

        llMainContentOne = (LinearLayout) findViewById(R.id.ll_main_content_one);
        spinnerOperator = (Spinner) findViewById(R.id.operator_spinner);

        TextView tvBoardOrOperator = (TextView) findViewById(R.id.tv_board_operator);
        tvBoardOrOperator.setText("Operator");

        /*Part 1 of layout*/
        tvField26 = (TextView) findViewById(R.id.tv_field26);
        tvField1 = (TextView) findViewById(R.id.tv_field1);
        tvField27 = (TextView) findViewById(R.id.tv_field27);
        tvField28 = (TextView) findViewById(R.id.tv_field28);
        tvField29 = (TextView) findViewById(R.id.tv_field29);
        tvHintField26 = (TextView) findViewById(R.id.tv_field26_hint);
        tvHintField27 = (TextView) findViewById(R.id.tv_field27_hint);

        etField26 = (EditText) findViewById(R.id.et_field26);
        etField1 = (EditText) findViewById(R.id.et_field1);
        etField27 = (EditText) findViewById(R.id.et_field27);
        etField28 = (EditText) findViewById(R.id.et_field28);
        etField29 = (EditText) findViewById(R.id.et_field29);

        llField26 = (LinearLayout) findViewById(R.id.ll_field26);
        llField1 = (LinearLayout) findViewById(R.id.ll_field1);
        llField27 = (LinearLayout) findViewById(R.id.ll_field27);
        llField28 = (LinearLayout) findViewById(R.id.ll_field28);
        llField29 = (LinearLayout) findViewById(R.id.ll_field29);
        ivOperator = findViewById(R.id.iv_arrowdown_select_operator);
        pbOperator = findViewById(R.id.progressbar_operator);
        ivPaymentType = findViewById(R.id.iv_arrowdown_select_payment_mode);
        pbPaymentType = findViewById(R.id.progressbar_payment_mode);

        /*Part 2 of layout*/
        cvMainContentTwo = findViewById(R.id.cv_bill_details);
        llFetchResponseCustomerName = findViewById(R.id.ll_fetch_response_customer_name);
        llFetchResponseBillDate = findViewById(R.id.ll_fetch_response_bill_date);
        llFetchResponseBillAmount = findViewById(R.id.ll_fetch_response_bill_amount);
        llFetchResponseBillPeriod = findViewById(R.id.ll_fetch_response_bill_period);
        llFetchResponseBillNumber = findViewById(R.id.ll_fetch_response_bill_number);
        llFetchResponseDueDate = findViewById(R.id.ll_fetch_response_due_date);
        llFetchResponseFixedCharges = findViewById(R.id.ll_fetch_response_fixed_charges);
        llFetchResponseAdditionalCharges = findViewById(R.id.ll_fetch_response_additional_charges);
        llFetchResponseEarlyPaymentFees = findViewById(R.id.ll_fetch_response_early_payment_fee);
        llFetchResponseLatePaymentFees = findViewById(R.id.ll_fetch_response_late_payment_fee);

        tvCustomerName = findViewById(R.id.tv_bill_details_customer_name);
        tvBillDate = findViewById(R.id.tv_bill_details_bill_date);
        tvBillAmount = findViewById(R.id.tv_bill_details_bill_amount);
        tvBillPeriod = findViewById(R.id.tv_bill_details_bill_period);
        tvBillNumber = findViewById(R.id.tv_bill_details_bill_number);
        tvDueDate = findViewById(R.id.tv_bill_details_due_date);
        tvFixedCharges = findViewById(R.id.tv_bill_details_fixed_charges);
        tvAdditionalCharges = findViewById(R.id.tv_bill_details_additional_charges);
        tvEarlyPaymentFees = findViewById(R.id.tv_bill_details_early_payment_fee);
        tvLatePaymentFees = findViewById(R.id.tv_bill_details_late_payment_fee);

        tvModifySearch = findViewById(R.id.tv_modify_input_data);
        btnProceedToPay = findViewById(R.id.btn_proceed_to_pay);
        /*----------*/

        /*Part 3 of layout*/
        etOptionAmount = findViewById(R.id.et_amount);
        spinnerPaymentMode = findViewById(R.id.spinner_payment_mode);
        etCCF = findViewById(R.id.et_ccf);
        etTotalAmount = findViewById(R.id.et_total_amount);
        etRemarks = findViewById(R.id.et_remarks);
        llMainThree = findViewById(R.id.ll_main_three);
        /*----------*/

        /*Buttons*/
        llBtnFetchPay = findViewById(R.id.ll_button_fetch_pay);
        btnFetchBill = findViewById(R.id.btn_fetch_bill);
        btnPayBill = findViewById(R.id.btn_paybill);
        /*----------*/
    }

    private void SetTitle() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(GetBillerParameters.getBBPSTitle(CategoryID));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    private void getOperators() {
        ivOperator.setVisibility(View.GONE);
        pbOperator.setVisibility(View.VISIBLE);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection(Const.DEFAULT_BILLER_COLLECTION).document(Const.PREFIX_DEFAULT_BILLER_DOCUMENT + CategoryID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                ivOperator.setVisibility(View.VISIBLE);
                pbOperator.setVisibility(View.GONE);

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if (document.contains("billerList")) {
                                //String value = "" + document.getData().values();
                                List<Map<String, Object>> billerList = (List<Map<String, Object>>) document.get("billerList");
                                biller_item_arr.clear();

                                setOperator("NOT_AN_ID", "Select Operator", null);

                                for (int i = 0; i < billerList.size(); i++) {
                                    setOperator("" + billerList.get(i).get("blrId"), "" + billerList.get(i).get("blrName"), (Map<String, Object>) billerList.get(i).get(billerList.get(i).get("blrId")));
                                }

                                adapterOperator = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, biller_item_arr);
                                spinnerOperator.setAdapter(adapterOperator);

                                spinnerOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        resetAllFields();

                                        ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                                        if (biller_item_arr.get(position).blrId.equalsIgnoreCase("NOT_AN_ID")) {
                                            //Doing just nothing
                                        } else {
                                            llField26.setVisibility(View.VISIBLE);
                                            llField1.setVisibility(View.VISIBLE);

                                            try {
                                                blrID = biller_item_arr.get(position).blrId;
                                                BlrName = biller_item_arr.get(position).blrName;
                                                otherParameters = biller_item_arr.get(position).other_parameters;
                                                CatName = otherParameters.get("blrCatName").toString();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            try {
                                                TextField1 = GetBillerParameters.getField26ParamBBPS(blrID);
                                                if (!TextField1.equals("")) {
                                                    tvField26.setText(TextField1 + ": ");
                                                    etField26.setHint("Enter " + TextField1);

                                                    //field26ValidationMaxLength(etField26, CategoryID);
                                                    //field26ValidationInputType(etField26, CategoryID);

                                                    int MAX_LENGTH = GetBillerParameters.field26ValidationMaxLength(etField26, CategoryID, blrID);
                                                    int INPUT_TYPE = GetBillerParameters.field26ValidationInputType(etField26, CategoryID, blrID);

                                                    etField26.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH)});
                                                    etField26.setInputType(INPUT_TYPE);

                                                    //As in Life insurance
                                                    setupHintForField26(tvHintField26, blrID);

                                                    checkField_27_28_29();

                                                    AdhocPayment = otherParameters.get("adhocPayment").toString();
                                                    buttonVisibilityAmountEditableOperation(otherParameters.get("billAcceptanceType").toString(), AdhocPayment);

                                                } else
                                                    resetAllFields();

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
//                        updateSession(user_name);
                    }
                } else {

                }
            }
        });
    }

    private void checkField_27_28_29() {
        if (blrID.equals(Const.BLR_ID_LPG_GAS_1) || blrID.equals(Const.BLR_ID_LPG_GAS_2) || blrID.equals(Const.BLR_ID_BROADBAND_POSTPAID_21) || blrID.equals(Const.BLR_ID_WATER_9) || blrID.equals(Const.BLR_ID_WATER_28) || blrID.equals(Const.BLR_ID_LANDLINE_POSTPAID_7) || blrID.equals(Const.BLR_ID_LANDLINE_POSTPAID_8) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_2) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_3) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_10) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_16) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_18) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_19) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_24) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_27) || blrID.equals(Const.BLR_ID_HEALTH_INSURANCE_1) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_36) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_56) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_57) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_66) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_69) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_71) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_80) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_82) || blrID.equals(Const.BLR_ID_INSURANCE_1) || blrID.equals(Const.BLR_ID_INSURANCE_2) || blrID.equals(Const.BLR_ID_INSURANCE_4) || blrID.equals(Const.BLR_ID_INSURANCE_8) || blrID.equals(Const.BLR_ID_INSURANCE_9) || blrID.equals(Const.BLR_ID_INSURANCE_10) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_1) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_5) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_6) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_7) || blrID.equals(Const.BLR_ID_HOUSING_SOCIETY_12)) {
            llField27.setVisibility(View.VISIBLE);
            tvField27.setText(GetBillerParameters.getParamCommonField27(blrID));
            field27Validation(etField27, blrID);
        }

        if (blrID.equals(Const.BLR_ID_WATER_13) || blrID.equals(Const.BLR_ID_WATER_17) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_37) || blrID.equals(Const.BLR_ID_LOAN_REPAYMENT_52) || blrID.equals(Const.BLR_ID_INSURANCE_6) || blrID.equals(Const.BLR_ID_INSURANCE_7)) {
            llField27.setVisibility(View.VISIBLE);
            llField28.setVisibility(View.VISIBLE);
            tvField27.setText(GetBillerParameters.getParamCommonField27(blrID));
            tvField28.setText(GetBillerParameters.getParamCommonField28(blrID));
            field27Validation(etField27, blrID);
            field28Validation(etField28, blrID);
        }

        if (blrID.equals(Const.BLR_ID_LPG_GAS_3) || blrID.equals(Const.BLR_ID_WATER_16) || blrID.equals(Const.BLR_ID_INSURANCE_5)) {
            llField27.setVisibility(View.VISIBLE);
            llField28.setVisibility(View.VISIBLE);
            llField29.setVisibility(View.VISIBLE);
            tvField27.setText(GetBillerParameters.getParamCommonField27(blrID));
            tvField28.setText(GetBillerParameters.getParamCommonField28(blrID));
            tvField29.setText(GetBillerParameters.getParamCommonField29(blrID));
            field27Validation(etField27, blrID);
            field28Validation(etField28, blrID);
            field29Validation(etField29, blrID);
        }


        if (blrID.equals(Const.BLR_ID_LIFE_INSURANCE_1) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_2) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_3) || blrID.equals(Const.BLR_ID_LIFE_INSURANCE_7)) {
            llField27.setVisibility(View.VISIBLE);
            tvField27.setText(GetBillerParameters.getParamCommonField27(blrID));
            field27Validation(etField27, blrID);
            setupHintForField27(tvHintField27, blrID);
        } else if (blrID.equals(Const.BLR_ID_LIFE_INSURANCE_8)) {
            llField27.setVisibility(View.VISIBLE);
            llField28.setVisibility(View.VISIBLE);
            llField29.setVisibility(View.VISIBLE);

            tvField27.setText(GetBillerParameters.getParamCommonField27(blrID));
            tvField28.setText(GetBillerParameters.getParamCommonField28(blrID));
            tvField29.setText(GetBillerParameters.getParamCommonField29(blrID));

            field27Validation(etField27, blrID);
            field28Validation(etField28, blrID);
            field29Validation(etField29, blrID);

            setupHintForField27(tvHintField27, blrID);

        }


    }

    private void setupPaymentMode() {
        selected_payment_type = "";

        final List<String> PaymentList = new ArrayList<>(Arrays.asList(Const.arrPaymentType));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, PaymentList) {
            @Override
            public boolean isEnabled(int position) {
                if (position != 1) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 1) {
                    // Set the disable item text color
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.GRAY);
                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerPaymentMode.setAdapter(spinnerArrayAdapter);
        spinnerArrayAdapter.notifyDataSetChanged();


        spinnerPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selected_payment_type = PaymentList.get(position);

                if (position == 1) {
                    if (etOptionAmount.getText().toString().trim().equals("") || etOptionAmount.getText().toString().trim().equals("0")
                            || etOptionAmount.getText().toString().trim().equals(null)) {
                        Toast.makeText(ActivityBBPS.this, "Please enter amount.", Toast.LENGTH_SHORT).show();
                    } else {
                        Double amount = Double.parseDouble(etOptionAmount.getText().toString().trim());
                        mActionsListener.apiLoadGetCcf(selected_payment_type, decim.format(amount), blrID, tokenStr);
                        //getCCF(selected_payment_type, Double.parseDouble(etOptionAmount.getText().toString().trim()));
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void buttonVisibilityAmountEditableOperation(String billAcceptanceType, String adhocPayment) {

        if (billAcceptanceType.equals("0")) {
            //Fetch Bill active
            QuickPay = "0";

            llBtnFetchPay.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.VISIBLE);
            btnPayBill.setVisibility(View.GONE);
        } else if (billAcceptanceType.equals("1")) {
            //Pay Bill active
            QuickPay = "1";

            llBtnFetchPay.setVisibility(View.VISIBLE);
            llMainThree.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.GONE);
            btnPayBill.setVisibility(View.VISIBLE);
        } else if (billAcceptanceType.equals("2")) {
            //Both active

            llBtnFetchPay.setVisibility(View.VISIBLE);
            llMainThree.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.VISIBLE);
            btnPayBill.setVisibility(View.VISIBLE);
        }

        if (adhocPayment.equals("0")) {
            setEdittextEditable(etOptionAmount, false, R.drawable.disable_edittext);
        } else if (adhocPayment.equals("1")) {
            setEdittextEditable(etOptionAmount, true, R.drawable.border_white_bg);
        }

    }

    private void resetAllFields() {
        llMainThree.setVisibility(View.GONE);
        llBtnFetchPay.setVisibility(View.GONE);

        llField26.setVisibility(View.GONE);
        llField1.setVisibility(View.GONE);
        llField27.setVisibility(View.GONE);
        llField28.setVisibility(View.GONE);
        llField29.setVisibility(View.GONE);

        etField26.setText("");
        etField1.setText("");
        etField27.setText("");
        etField28.setText("");
        etField29.setText("");

        etOptionAmount.setText("");
        etTotalAmount.setText("");
        etCCF.setText("");
        etRemarks.setText("");

        //reset bill details(Fetch Bill data)
        tvCustomerName.setText("");
        tvBillDate.setText("");
        tvBillAmount.setText("");
        tvBillPeriod.setText("");
        tvBillNumber.setText("");
        tvDueDate.setText("");

        setEdittextEditable(etCCF, true, R.drawable.border_white_bg);
        setEdittextEditable(etTotalAmount, true, R.drawable.border_white_bg);

        setupPaymentMode();

    }

    public void setOperator(String board_id, String board_name, Map<String, Object> other_parameters) {

        BillerListPojo billerListPojo = new BillerListPojo();
        billerListPojo.setBlrId(board_id);
        billerListPojo.setBlrName(board_name);
        billerListPojo.setOther_parameters(other_parameters);

        biller_item_arr.add(billerListPojo);
    }

    public void setEdittextEditable(EditText et, Boolean editable, int drawable) {
        et.setEnabled(editable);
        et.setBackgroundResource(drawable);
        et.setPadding(12, 0, 12, 0);
    }

    private void checkFetchResponse(String response, LinearLayout llFetchResponseCustomerName, TextView tvCustomerName) {
        if (response.equals("") || response.equals("NA"))
            llFetchResponseCustomerName.setVisibility(View.GONE);
        else {
            llFetchResponseCustomerName.setVisibility(View.VISIBLE);
            tvCustomerName.setText(response);
        }

    }

    //Done
    private void field27Validation(EditText etField27, String blrID) {
        switch (blrID) {
            case "HPCL00000NAT01":
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                etField27.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case "BHAR00000NATR4":
            case "ADIT00000NATRA":
            case "FAIR00000NAT6Z":
            case "LAMP00000NAT7E":
            case "LAND00000NATRD":
            case "SHRI00000NAT7D":
            case "TATA00000NATGS":
            case "IIFL00000NAT5F":
            case "IIFL00000NATMF":
            case "MAXV00000NAT33":
            case "MONE00000NAT7P":
            case "VIST00000NAT6D":
            case "AMRU00000PUNIU":
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                etField27.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case "INDI00000NATT5":
            case "RELI00000NATQ9":
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                etField27.setInputType(InputType.TYPE_CLASS_TEXT);
                break;

            case "MAGM00000NAT61":
            case "MAGM00000NATQI":
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                etField27.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;


            default:
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                etField27.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
        }

    }

    //Done
    private void field28Validation(EditText etField28, String blrID) {
        switch (blrID) {
            case "INDI00000NATT5":
            case "AXIS00000NATM1":
                etField28.setInputType(InputType.TYPE_CLASS_NUMBER);
                etField28.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
            case "MAGM00000NAT61":
            case "MAGM00000NATQI":
                etField28.setInputType(InputType.TYPE_CLASS_NUMBER);
                etField28.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                break;
            case "MAGM00000NAT6B":
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
                etField27.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;

            default:
                etField28.setInputType(InputType.TYPE_CLASS_TEXT);
                etField28.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
        }
    }

    //Done
    private void field29Validation(EditText etField29, String blrID) {
        switch (blrID) {
            case "INDI00000NATT5":
                etField29.setInputType(InputType.TYPE_CLASS_TEXT);
                etField29.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
            default:
                etField29.setInputType(InputType.TYPE_CLASS_TEXT);
                etField29.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
        }
    }

    //Done
    private void setupHintForField26(TextView tvHintField26, String blrID) {
        switch (blrID) {
            case "EXID00000NAT25":
                tvHintField26.setVisibility(View.VISIBLE);
                tvHintField26.setText("Please enter valid date (YYYYMMDD)");
                break;

            case "FUTU00000NAT09":
                tvHintField26.setVisibility(View.VISIBLE);
                tvHintField26.setText("Please type in in DD/MM/YYYY format");
                break;
            default:
                tvHintField26.setVisibility(View.GONE);
                break;
        }

    }

    //Done
    private void setupHintForField27(TextView tvHintField27, String blrID) {
        switch (blrID) {
            case "HDFC00000NATV4":
            case "SHRI00000NATRI":
            case "STAR00000NATXZ":
                tvHintField27.setVisibility(View.VISIBLE);
                tvHintField27.setText("Please enter valid date (DDMMYYYY)");
                break;

            default:
                tvHintField27.setVisibility(View.GONE);
                break;
        }

    }


    @Override
    public void BbpsFetchBillReady(BbpsModelFetchBill bbpsModel) {
        //Process the response
        llMainContentOne.setVisibility(View.VISIBLE);
        llBtnFetchPay.setVisibility(View.GONE);
        cvMainContentTwo.setVisibility(View.VISIBLE);

        Bill_Amount = bbpsModel.getBill_Amount();

        checkFetchResponse(bbpsModel.getCustomer_Name(), llFetchResponseCustomerName, tvCustomerName);
        checkFetchResponse(bbpsModel.getBill_Date(), llFetchResponseBillDate, tvBillDate);
        checkFetchResponse(bbpsModel.getBill_Amount(), llFetchResponseBillAmount, tvBillAmount);
        checkFetchResponse(bbpsModel.getBill_Period(), llFetchResponseBillPeriod, tvBillPeriod);
        checkFetchResponse(bbpsModel.getBill_Number(), llFetchResponseBillNumber, tvBillNumber);
        checkFetchResponse(bbpsModel.getDue_Date(), llFetchResponseDueDate, tvDueDate);
        checkFetchResponse(bbpsModel.getFixed_Charges(), llFetchResponseFixedCharges, tvFixedCharges);
        checkFetchResponse(bbpsModel.getAdditional_Charges(), llFetchResponseAdditionalCharges, tvAdditionalCharges);
        checkFetchResponse(bbpsModel.getEarly_Payment_Fee(), llFetchResponseEarlyPaymentFees, tvEarlyPaymentFees);
        checkFetchResponse(bbpsModel.getLate_Payment_Fee(), llFetchResponseLatePaymentFees, tvLatePaymentFees);
    }

    @Override
    public void BbpsGetCcfReady(String ccf, Double dBillAmount) {
        this.dBillAmount = dBillAmount;
        this.ccf = ccf;

        etCCF.setText(ccf);
        dTotalAmount = dBillAmount + Double.parseDouble(ccf);
        etTotalAmount.setText("" + decim.format(dTotalAmount));

        etCCF.setEnabled(false);
        etCCF.setBackgroundResource(R.drawable.disable_edittext);
        etCCF.setPadding(12, 0, 12, 0);
        etTotalAmount.setEnabled(false);
        etTotalAmount.setBackgroundResource(R.drawable.disable_edittext);
        etTotalAmount.setPadding(12, 0, 12, 0);
    }

    @Override
    public void BbpsPayBillReady(String response) {
        Intent intent = new Intent(ActivityBBPS.this, ActivityReceiptBbps.class);
        intent.putExtra("Response", response.toString());
        startActivity(intent);
        finish();

    }

    @Override
    public void OnErrorBbps(String ErrorMsg) {
        Toast.makeText(ActivityBBPS.this, ErrorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void OnErrorBbpsPayBill(String errMSG, String response) {
        if (errMSG.equals("TAKE_TO_RECEIPT")) {
            Intent intent = new Intent(ActivityBBPS.this, ActivityReceiptBbps.class);
            intent.putExtra("Response", response);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(ActivityBBPS.this, errMSG, Toast.LENGTH_LONG).show();
        }
    }

    ProgressDialog dialog;

    @Override
    public void showLoader() {
        dialog = new ProgressDialog(ActivityBBPS.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

    }

    @Override
    public void showLoaderGetCCF() {
        ivPaymentType.setVisibility(View.GONE);
        pbPaymentType.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        dialog.cancel();
    }

    @Override
    public void hideLoaderGetCCF() {
        ivPaymentType.setVisibility(View.VISIBLE);
        pbPaymentType.setVisibility(View.GONE);
    }

}
