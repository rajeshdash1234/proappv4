package com.isuisudmt.bbps;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.isuisudmt.R;
import com.isuisudmt.recharge.RechargeActivity;

import static com.isuisudmt.bbps.utils.Const.BROADBAND_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.CABLE_TV;
import static com.isuisudmt.bbps.utils.Const.DTH;
import static com.isuisudmt.bbps.utils.Const.EDUCATION_FEES;
import static com.isuisudmt.bbps.utils.Const.ELECTRICITY;
import static com.isuisudmt.bbps.utils.Const.FASTAG;
import static com.isuisudmt.bbps.utils.Const.GAS;
import static com.isuisudmt.bbps.utils.Const.HEALTH_INSURANCE;
import static com.isuisudmt.bbps.utils.Const.LANDLINE_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.LIFE_INSURANCE;
import static com.isuisudmt.bbps.utils.Const.LOAN_REPAYMENT;
import static com.isuisudmt.bbps.utils.Const.LPG_GAS;
import static com.isuisudmt.bbps.utils.Const.MOBILE_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.WATER;

public class DashBoardActivity extends AppCompatActivity {

    LinearLayout tabLinear1, tabLinear2, tabLinear3, tabLinear4, tabLinear5, tabLinear6, tabLinear7, tabLinear8, tabLinear9, tabLinear10, tabLinear11, tabLinear12;
    ImageView tab_icon1, tab_icon2, tab_icon3, tab_icon4, tab_icon5, tab_icon6, tab_icon7, tab_icon8, tab_icon9, tab_icon10, tab_icon11, tab_icon12;
    LinearLayout bbpsModules;
    LinearLayout bbpsLinear1;
    ImageView bbps_icon1;

    private Menu menu;
    private CollapsingToolbarLayout toolbar_layout;
    private LinearLayout layout;
    private ImageView menu_nav;
    private TextView name, last_seen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        bbpsModules = findViewById(R.id.bbpsModules);

        tab_icon12 = findViewById(R.id.tab_icon12);
        tab_icon11 = findViewById(R.id.tab_icon11);
        tab_icon10 = findViewById(R.id.tab_icon10);
        tab_icon9 = findViewById(R.id.tab_icon9);
        tab_icon8 = findViewById(R.id.tab_icon8);
        tab_icon7 = findViewById(R.id.tab_icon7);
        tab_icon6 = findViewById(R.id.tab_icon6);
        tab_icon5 = findViewById(R.id.tab_icon5);
        tab_icon4 = findViewById(R.id.tab_icon4);
        tab_icon3 = findViewById(R.id.tab_icon3);
        tab_icon2 = findViewById(R.id.tab_icon2);
        tab_icon1 = findViewById(R.id.tab_icon1);
        tabLinear12 = findViewById(R.id.tabLinear12);
        tabLinear11 = findViewById(R.id.tabLinear11);
        tabLinear10 = findViewById(R.id.tabLinear10);
        tabLinear9 = findViewById(R.id.tabLinear9);
        tabLinear8 = findViewById(R.id.tabLinear8);
        tabLinear7 = findViewById(R.id.tabLinear7);
        tabLinear6 = findViewById(R.id.tabLinear6);
        tabLinear5 = findViewById(R.id.tabLinear5);
        tabLinear4 = findViewById(R.id.tabLinear4);
        tabLinear3 = findViewById(R.id.tabLinear3);
        tabLinear2 = findViewById(R.id.tabLinear2);
        tabLinear1 = findViewById(R.id.tabLinear1);
        bbpsModules.setVisibility(View.GONE);
        bbpsLinear1 = findViewById(R.id.bbpsLinear1);

        bbps_icon1 = findViewById(R.id.bbps_icon1);
        BBPSDashboardModel[] myListData = new BBPSDashboardModel[]{
                new BBPSDashboardModel("Mobile Postpaid", R.drawable.ic__01_bell, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", MOBILE_POSTPAID),
                new BBPSDashboardModel("Electricity", R.drawable.ic_energy_2_, new Intent(DashBoardActivity.this, ActivityaElectricityBill.class), "", ELECTRICITY),
                new BBPSDashboardModel("DTH", R.drawable.ic__19_volume_up, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", DTH),
                new BBPSDashboardModel("Broadband", R.drawable.ic__20_wifi, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", BROADBAND_POSTPAID),
                new BBPSDashboardModel("Water", R.drawable.ic__13_faucet, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", WATER),
                new BBPSDashboardModel("LandLine", R.drawable.ic__03_telephone, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", LANDLINE_POSTPAID),
                new BBPSDashboardModel("Gas", R.drawable.ic_black_friday_2_, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", GAS),
                new BBPSDashboardModel("LPG Gas", R.drawable.ic_black_friday_2__1, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", LPG_GAS),
                new BBPSDashboardModel("Education Fees", R.drawable.ic__18_book, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", EDUCATION_FEES),
                new BBPSDashboardModel("Cable TV", R.drawable.ic__19_volume_up_1, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", CABLE_TV),
                new BBPSDashboardModel("FasTAG", R.drawable.ic_group_147, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", FASTAG),
                new BBPSDashboardModel("Life Insurance", R.drawable.ic__15_umbrella, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", LIFE_INSURANCE),
                new BBPSDashboardModel("Loan Repayment", R.drawable.ic_energy_2__1, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", LOAN_REPAYMENT),
                new BBPSDashboardModel("Health Insurance", R.drawable.ic_shield_2_, new Intent(DashBoardActivity.this, ActivityBBPS.class), "", HEALTH_INSURANCE),

        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.bbpsList);
        BbpsDashboardAdapter adapter = new BbpsDashboardAdapter(myListData, DashBoardActivity.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(adapter);


        tabLinear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, RechargeActivity.class);
                startActivity(intent);
                bbpsModules.setVisibility(View.GONE);
                tab_icon2.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);

            }
        });
        tabLinear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bbpsModules.setVisibility(View.VISIBLE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tabLinear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon12.setImageAlpha(110);
            }
        });
        tabLinear12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon10.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
            }
        });

        tabLinear10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bbpsModules.setVisibility(View.GONE);
                tab_icon1.setImageAlpha(110);
                tab_icon3.setImageAlpha(110);
                tab_icon4.setImageAlpha(110);
                tab_icon5.setImageAlpha(110);
                tab_icon6.setImageAlpha(110);
                tab_icon7.setImageAlpha(110);
                tab_icon8.setImageAlpha(110);
                tab_icon9.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
                tab_icon11.setImageAlpha(110);
                tab_icon2.setImageAlpha(110);
            }
        });



        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        toolbar_layout = (CollapsingToolbarLayout)findViewById(R.id.toolbar_layout);
        toolbar_layout.setTitle(" ");
        layout = (LinearLayout)findViewById(R.id.bottomSheet);
        menu_nav= (ImageView)findViewById(R.id.menu_nav);
        name = (TextView)findViewById(R.id.name);
        last_seen = (TextView)findViewById(R.id.last_seen);

        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.appbarLayout);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    Animation animation = new TranslateAnimation(0, 0,0, 500); //May need to check the direction you want.
                    animation.setDuration(1000);
                    animation.setFillAfter(true);
                    mToolbar.startAnimation(animation);
                    isShow = true;
                    showOption(R.id.add_money);
                    name.setText("w1 \u20B9 12,345");
                    last_seen.setText("w2 \u20B9 12,345");
                  //  layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.border_white_bg));
                } else if (isShow) {
                    isShow = false;
                    hideOption(R.id.add_money);
                    name.setText("");
                    last_seen.setText("");
                    //layout.setBackground(ContextCompat.getDrawable(DashBoardActivity.this,R.drawable.corner_radius));
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        // return true;
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        hideOption(R.id.add_money);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_share) {
            return true;
        } else if (id == R.id.wallet) {
            return true;
        }
        else if (id == R.id.add_money) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hideOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

}