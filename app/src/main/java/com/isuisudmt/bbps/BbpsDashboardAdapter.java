package com.isuisudmt.bbps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.isuisudmt.Constants;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.aeps.ReportActivity;
import com.isuisudmt.bbps.BBPSmvp.BbpsDashboardContract;
import com.isuisudmt.bbps.BBPSmvp.BbpsDashboardPresenter;
import com.isuisudmt.bbps.utils.Const;

import org.json.JSONObject;

import java.util.HashMap;

import static com.isuisudmt.bbps.utils.Const.isBBPSenabled;
import static com.isuisudmt.utils.Constants.BBPS_PREF;
import static com.isuisudmt.utils.Constants.SF_AGENT_ID;
import static com.isuisudmt.utils.Constants.SF_KEYWORD;
import static com.isuisudmt.utils.Constants.SF_LAT_LONG;
import static com.isuisudmt.utils.Constants.SF_MOBILE_NUMBER;
import static com.isuisudmt.utils.Constants.SF_PINCODE;
import static com.isuisudmt.utils.Constants.SF_TERMINAL_ID;

public class BbpsDashboardAdapter extends RecyclerView.Adapter<BbpsDashboardAdapter.ViewHolder> implements BbpsDashboardContract.View {
    private BBPSDashboardModel[] listdata;
    Context context;
    String userName = "", tokenStr = "";
    SessionManager session;
    SharedPreferences sp, spBbps;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    private static final String TAG = ReportActivity.class.getSimpleName();

    //MVP
    private BbpsDashboardPresenter mActionsListener;
    ProgressDialog dialog;


    // RecyclerView recyclerView;
    public BbpsDashboardAdapter(BBPSDashboardModel[] listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);


        session = new SessionManager(context);
        sp = context.getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        spBbps = context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE);
        mActionsListener = new BbpsDashboardPresenter((BbpsDashboardContract.View) context);

        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        userName = Constants.USER_NAME;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final BBPSDashboardModel myListData = listdata[position];
        holder.textView.setText(listdata[position].getDescription());
        Glide.with(context)
                .load(listdata[position].getImgId()) // ima ge url
                .into(holder.imageView);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (myListData.getType().equals("MOBILE_PREPAID")) {
                    goToActivity(myListData.getIntent(), myListData.CategoryID);

                } else {

                    if (myListData.getIntent() != null) {
                        if (isBBPSenabled == true) {
                            isApiCalledInSession(myListData.getIntent(), myListData.CategoryID);
                        } else
                            Toast.makeText(context, Const.ERROR_MESSAGE_DISABLED_BBPS, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Coming Soon..", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public LinearLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.bbps_icon1);
            this.textView = (TextView) itemView.findViewById(R.id.bbps_name1);
            relativeLayout = (LinearLayout) itemView.findViewById(R.id.bbpsLinear1);
        }
    }


    private void isApiCalledInSession(Intent intent, String CategoryID) {
        try {
            if (Const.isUpdatedResponse.equals(""))
                mActionsListener.apiIsUpdated(intent, CategoryID, tokenStr);
            else {
                //Perform here
                try {
                    JSONObject objIsUpdated = new JSONObject(Const.isUpdatedResponse);
                    int status = objIsUpdated.getInt("status");
                    String statusDescription = objIsUpdated.getString("statusDescription");

                    if (status == 0) {
                        if (Const.viewRequiredInfoResponse.equals(""))
                            mActionsListener.apiViewRequiredInfo(intent, userName, CategoryID, tokenStr);
                        else
                            intentToActivity(Const.viewRequiredInfoResponse, intent, CategoryID);

                    } else if (status == 1) {
                        String userNameData = objIsUpdated.getString("userNameData");

                        if (!userNameData.equals("")) {
                            Const.usernameForBBPS = userNameData;

                            if (Const.viewRequiredInfoResponse.equals(""))
                                mActionsListener.apiViewRequiredInfo(intent, userNameData, CategoryID, tokenStr);
                            else
                                intentToActivity(Const.viewRequiredInfoResponse, intent, CategoryID);
                        } else {
                            Toast.makeText(context, "" + statusDescription, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "" + statusDescription, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void intentToActivity(String response, Intent intent, String CategoryID) {
        try {
            JSONObject objViewRqrdDetails = new JSONObject(response);
            String _status = objViewRqrdDetails.getString("status");
            String pincode = objViewRqrdDetails.getString("pincode");
            String mobilenumber = objViewRqrdDetails.getString("mobilenumber");
            String latlong = objViewRqrdDetails.getString("latlong");
            String terminalid = objViewRqrdDetails.getString("terminalid");
            String agentid = objViewRqrdDetails.getString("agentid");
            String city = objViewRqrdDetails.getString("city");
            String state = objViewRqrdDetails.getString("state");
            String keyword = objViewRqrdDetails.getString("keyword");
            String accountcode = objViewRqrdDetails.getString("accountcode");

            SharedPreferences.Editor editor = spBbps.edit();
            editor.putString(SF_PINCODE, pincode);
            editor.putString(SF_MOBILE_NUMBER, mobilenumber);
            editor.putString(SF_LAT_LONG, latlong);
            editor.putString(SF_TERMINAL_ID, terminalid);
            editor.putString(SF_AGENT_ID, agentid);
            editor.putString(SF_KEYWORD, keyword);
            editor.apply();

            //Intent
            goToActivity(intent, CategoryID);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void goToActivity(Intent intent, String CategoryID) {
        intent.putExtra("CategoryID", CategoryID);
        context.startActivity(intent);
    }


    @Override
    public void onReadyIsUpdated(JSONObject response, Intent intent, String CatID) {
        try {
            Const.isUpdatedResponse = response.toString();
            int status = response.getInt("status");
            String statusDescription = response.getString("statusDescription");

            if (status == 0) {
                Const.usernameForBBPS = "";
                mActionsListener.apiViewRequiredInfo(intent, userName, CatID, tokenStr);

            } else if (status == 1) {
                String userNameData = response.getString("userNameData");
                if (!userNameData.equals("")) {
                    Const.usernameForBBPS = userNameData;
                    mActionsListener.apiViewRequiredInfo(intent, userNameData, CatID, tokenStr);
                } else {
                    Toast.makeText(context, "" + statusDescription, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "" + statusDescription, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onErrorIsUpdated(String ErrorMsg) {
        try {
            JSONObject errorObject = new JSONObject(ErrorMsg);
            String statusDescription = errorObject.optString("statusDescription");
            String statusDesc = errorObject.optString("statusDesc");
            if (statusDescription.equals(""))
                Toast.makeText(context, statusDesc, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context, statusDescription, Toast.LENGTH_LONG).show();
        } catch (Exception e) {

        }

    }

    @Override
    public void onReadyViewRequiredInfo(JSONObject response, Intent intent, String CatID) {
        Const.viewRequiredInfoResponse = response.toString();
        intentToActivity(response.toString(), intent, CatID);
    }

    @Override
    public void onErrorViewRequiredInfo(String ErrorMsg) {
        try {
            JSONObject errorObject = new JSONObject(ErrorMsg);
            String statusDescription = errorObject.optString("statusDescription");
            String statusDesc = errorObject.optString("statusDesc");
            if (statusDescription.equals(""))
                Toast.makeText(context, statusDesc, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context, statusDescription, Toast.LENGTH_LONG).show();

        } catch (Exception e) {

        }
    }

    @Override
    public void hideLoader() {
        dialog.cancel();
    }

    @Override
    public void showLoader() {
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
    }

}