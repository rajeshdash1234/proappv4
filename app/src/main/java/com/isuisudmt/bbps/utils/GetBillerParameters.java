package com.isuisudmt.bbps.utils;

import android.content.Context;
import android.text.InputType;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.isuisudmt.bbps.utils.Const.BBPS_PREF;
import static com.isuisudmt.bbps.utils.Const.CABLE_TV;
import static com.isuisudmt.bbps.utils.Const.DTH;
import static com.isuisudmt.bbps.utils.Const.EDUCATION_FEES;
import static com.isuisudmt.bbps.utils.Const.FASTAG;
import static com.isuisudmt.bbps.utils.Const.HEALTH_INSURANCE;
import static com.isuisudmt.bbps.utils.Const.LANDLINE_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.LPG_GAS;
import static com.isuisudmt.bbps.utils.Const.MOBILE_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.SF_LAT_LONG;
import static com.isuisudmt.bbps.utils.Const.SF_MOBILE_NUMBER;
import static com.isuisudmt.bbps.utils.Const.SF_PINCODE;
import static com.isuisudmt.bbps.utils.Const.SF_TERMINAL_ID;

public class GetBillerParameters {

    //PARAMS- ELECTRICITY (Total 23)
    public static String PARAM_BLR_ELECTRICITY_1 = "Consumer Number";
    public static String PARAM_BLR_ELECTRICITY_2 = "Consumer ID";
    public static String PARAM_BLR_ELECTRICITY_3 = "Service Number";
    public static String PARAM_BLR_ELECTRICITY_4 = "CA Number";
    public static String PARAM_BLR_ELECTRICITY_5 = "Account No without(/)";
    public static String PARAM_BLR_ELECTRICITY_6 = "Business Partner Number";
    public static String PARAM_BLR_ELECTRICITY_7 = "Account Number";
    public static String PARAM_BLR_ELECTRICITY_8 = "Contract Account No";
    public static String PARAM_BLR_ELECTRICITY_9 = "Account ID";
    public static String PARAM_BLR_ELECTRICITY_10 = "Customer ID / Account ID";
    public static String PARAM_BLR_ELECTRICITY_11 = "Account ID (RAPDRP) OR Consumer Number / Connection ID (Non-RAPDRP)";
    public static String PARAM_BLR_ELECTRICITY_12 = "Consumer Number/IVRS";
    public static String PARAM_BLR_ELECTRICITY_13 = "IVRS";
    public static String PARAM_BLR_ELECTRICITY_14 = "Customer Number";
    public static String PARAM_BLR_ELECTRICITY_15 = "K Number";
    public static String PARAM_BLR_ELECTRICITY_16 = "Contract Acc Number";
    public static String PARAM_BLR_ELECTRICITY_17 = "ACCOUNTNO";
    public static String PARAM_BLR_ELECTRICITY_18 = "Electricity Account No";
    public static String PARAM_BLR_ELECTRICITY_19 = "Customer ID (Not Consumer No)";
    public static String PARAM_BLR_ELECTRICITY_66 = "RR Number";


    public static String PARAM_BLR_ELECTRICITY_24_FIELD27 = "Mobile Number";
    public static String PARAM_BLR_ELECTRICITY_25_FIELD27 = "Mobile Number";
    public static String PARAM_BLR_ELECTRICITY_27_FIELD27 = "Subdivision Code";
    public static String PARAM_BLR_ELECTRICITY_34_FIELD27 = "Billing Unit(BU)";
    public static String PARAM_BLR_ELECTRICITY_68_FIELD27 = "City";
    public static String PARAM_BLR_ELECTRICITY_71_FIELD27 = "Mobile Number";


    //PARAMS- MOBILE POSTPAID(Total 11)
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_1 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_2 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_3 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_4 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_5 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_6 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_7 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_8 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_9 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_10 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_11 = "Mobile Number";

    //PARAMS- DTH(3)
    public static String PARAM_BLR_ID_DTH_1 = "Registered Mobile Number / Viewing Card Number";
    public static String PARAM_BLR_ID_DTH_2 = "Subscriber Number";
    public static String PARAM_BLR_ID_DTH_3 = "Customer ID / Registered Telephone No";

    //PARAMS- BROADBAND POSTPAID(Total 23)
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_1 = "Account Number/User Name";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_2 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_3 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_4 = "Customer Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_5 = "Directory Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_6 = "User Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_7 = "customerId";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_8 = "Customer Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_9 = "Customer Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_10 = "Customer ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_11 = "UserId/Mobile Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_12 = "Customer Id  /User Name";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_13 = "UserId";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_14 = "Billing Account Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_15 = "Consumer ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_16 = "CAN/Account ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_17 = "Service Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_18 = "CustomerId/RMN";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_19 = "User Name";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_20 = "Account No";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_21 = "Customer ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_22 = "Landline Number";

    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_21_FIELD27 = "Mobile Number";


    //PARAMS- WATER(Total 39)
    //Field 26
    public static String PARAM_BLR_ID_WATER_1 = "Connection ID";
    public static String PARAM_BLR_ID_WATER_2 = "RR Number";
    public static String PARAM_BLR_ID_WATER_3 = "Customer ID";
    public static String PARAM_BLR_ID_WATER_4 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_5 = "K No";
    public static String PARAM_BLR_ID_WATER_6 = "Connection ID";
    public static String PARAM_BLR_ID_WATER_7 = "CONNECTION ID";
    public static String PARAM_BLR_ID_WATER_8 = "CAN Number";
    public static String PARAM_BLR_ID_WATER_9 = "Site Code";
    public static String PARAM_BLR_ID_WATER_10 = "Service Number";
    public static String PARAM_BLR_ID_WATER_11 = "Service Number";
    public static String PARAM_BLR_ID_WATER_12 = "Consumer ID";
    public static String PARAM_BLR_ID_WATER_13 = "Account No";
    public static String PARAM_BLR_ID_WATER_14 = "Customer_ID";
    public static String PARAM_BLR_ID_WATER_15 = "K No";
    public static String PARAM_BLR_ID_WATER_16 = "Account No";
    public static String PARAM_BLR_ID_WATER_17 = "Email Id";
    public static String PARAM_BLR_ID_WATER_18 = "Account No without(/)";
    public static String PARAM_BLR_ID_WATER_19 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_20 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_21 = "Consumer No";
    public static String PARAM_BLR_ID_WATER_22 = "Consumer No";
    public static String PARAM_BLR_ID_WATER_23 = "Form No";
    public static String PARAM_BLR_ID_WATER_24 = "Connection Number";
    public static String PARAM_BLR_ID_WATER_25 = "Customer ID";
    public static String PARAM_BLR_ID_WATER_26 = "Consumer Number (Last 7 Digits)";
    public static String PARAM_BLR_ID_WATER_27 = "Business Partner Number";
    public static String PARAM_BLR_ID_WATER_28 = "Zone ID and Ward ID";
    public static String PARAM_BLR_ID_WATER_29 = "Citizen ID";
    public static String PARAM_BLR_ID_WATER_30 = "Computer Code";

    public static String PARAM_BLR_ID_WATER_9_FIELD27 = "Consumer No";
    public static String PARAM_BLR_ID_WATER_13_FIELD27 = "Consumer Mobile No";
    public static String PARAM_BLR_ID_WATER_16_FIELD27 = "Consumer Mobile No";
    public static String PARAM_BLR_ID_WATER_17_FIELD27 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_28_FIELD27 = "Connection No";

    public static String PARAM_BLR_ID_WATER_13_FIELD28 = "UID";
    public static String PARAM_BLR_ID_WATER_16_FIELD28 = "Consumer Email ID";
    public static String PARAM_BLR_ID_WATER_17_FIELD28 = "Mobile Number";

    public static String PARAM_BLR_ID_WATER_16_FIELD29 = "UID";


    //PARAMS- LANDLINE_POSTPAID(Total 9)
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_1 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_2 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_3 = "Telephone Number";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_4 = "Account Number";

    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_7 = "Account Number";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_8 = "Telephone Number";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_9 = "Landline Number with STD Code (without 0)";


    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_7_FIELD27 = "Telephone Number";//Field27
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_8_FIELD27 = "Account Number";//Field27


    //PARAMS- GAS(22)
    public static String PARAM_BLR_ID_GAS_1 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_2 = "Customer No";
    public static String PARAM_BLR_ID_GAS_3 = "CustomerID";
    public static String PARAM_BLR_ID_GAS_4 = "CRN No";
    public static String PARAM_BLR_ID_GAS_5 = "Customer Number";
    public static String PARAM_BLR_ID_GAS_6 = "Customer Code / CRN Number";
    public static String PARAM_BLR_ID_GAS_7 = "BP NO";
    public static String PARAM_BLR_ID_GAS_8 = "CRN Number";
    public static String PARAM_BLR_ID_GAS_9 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_10 = "CRN Number";
    public static String PARAM_BLR_ID_GAS_11 = "Customer_ID";
    public static String PARAM_BLR_ID_GAS_12 = "BP Number";
    public static String PARAM_BLR_ID_GAS_13 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_14 = "CA Number";
    public static String PARAM_BLR_ID_GAS_15 = "BP No";
    public static String PARAM_BLR_ID_GAS_16 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_17 = "ARN Number";
    public static String PARAM_BLR_ID_GAS_18 = "Consumer Number";
    public static String PARAM_BLR_ID_GAS_19 = "Customer No";
    public static String PARAM_BLR_ID_GAS_20 = "Consumer Number";
    public static String PARAM_BLR_ID_GAS_21 = "BP No";
    public static String PARAM_BLR_ID_GAS_22 = "CRN NO";


    //PARAMS -EDUCATION_FEES(1)
    public static String PARAM_BLR_ID_EDUCATION_FEES_1 = "Student Id";


    //PARAMS -CABLE_TV(1)
    public static String PARAM_BLR_ID_CABLE_TV_1 = "Account No/ VC No/ MAC ID/ VSC No/ RMN";


    //PARAMS -LPG_GAS(8)
    public static String PARAM_BLR_ID_LPG_GAS_1 = "Registered Contact Number";
    public static String PARAM_BLR_ID_LPG_GAS_2 = "Consumer Number";
    public static String PARAM_BLR_ID_LPG_GAS_3 = "Mobile Number";

    public static String PARAM_BLR_ID_LPG_GAS_1_FIELD_27 = "LPG ID";
    public static String PARAM_BLR_ID_LPG_GAS_2_FIELD_27 = "Distributor ID";
    public static String PARAM_BLR_ID_LPG_GAS_3_FIELD_27 = "Consumer Number";
    public static String PARAM_BLR_ID_LPG_GAS_3_FIELD_28 = "Distributor Code";
    public static String PARAM_BLR_ID_LPG_GAS_3_FIELD_29 = "Consumer ID";


    //PARAMS -FASTTAG(7)
    public static String PARAM_BLR_ID_FASTTAG_1 = "Vehicle Registration Number";
    public static String PARAM_BLR_ID_FASTTAG_2 = "Vehicle Number";
    public static String PARAM_BLR_ID_FASTTAG_3 = "Vehicle Registration Number";
    public static String PARAM_BLR_ID_FASTTAG_4 = "Vehicle Registration Number";
    public static String PARAM_BLR_ID_FASTTAG_5 = "Vehicle Registration Number";
    public static String PARAM_BLR_ID_FASTTAG_6 = "Vehicle Registered Number";
    public static String PARAM_BLR_ID_FASTTAG_7 = "Vehicle Registration Number ID";
    public static String PARAM_BLR_ID_FASTTAG_8 = "Vehicle Registration Number";


    public static String PARAM_BLR_ID_FASTTAG_1_FIELD_27 = "Amount";


    //PARAMS -LIFE_INSURANCE(16)
    public static String PARAM_BLR_ID_LIFE_INSURANCE_1 = "DOB(YYYYMMDD)";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_2 = "DOB";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_3 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_4 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_5 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_6 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_7 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_8 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_9 = "Policy Number";

    public static String PARAM_BLR_ID_LIFE_INSURANCE_1_FIELD_27 = "POLICY NO";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_2_FIELD_27 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_3_FIELD_27 = "Date Of Birth (DDMMYYYY)";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_7_FIELD_27 = "Date Of Birth (DDMMYYYY)";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_27 = "Date Of Birth";

    public static String PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_28 = "Mobile Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_29 = "Email Id";


    //PARAMS -LOAN_REPAYMENT(37)
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_1 = "Loan no";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_2 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_3 = "mobile";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_4 = "Customer ID/Loan Account Number/Mobile Number/Provisional RC No/Registration Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_5 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_6 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_7 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_8 = "loanApplicationNumber";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_9 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_10 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_11 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_12 = "Application ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_13 = "Loan ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_14 = "Loan No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_15 = "Loan Agreement No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_16 = "LanNo";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_17 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_18 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_19 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_20 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_21 = "Agreement Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_22 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_23 = "Load ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_24 = "LOAN NUMBER";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_25 = "Registered Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_26 = "Customer ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_27 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_28 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_29 = "Loan Account No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_30 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_31 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_32 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_33 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_34 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_35 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_36 = "Loan Account No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_37 = "Loan Account No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_38 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_39 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_40 = "Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_41 = "ClientID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_42 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_43 = "Date of birth (dd-mm-yyyy)";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_44 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_45 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_46 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_47 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_48 = "Loan ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_49 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_50 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_51 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_52 = "Application Id";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_53 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_54 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_55 = "Application ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_56 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_57 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_58 = "Loan ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_59 = "Application ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_60 = "Loan Account No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_61 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_62 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_63 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_64 = "Vehicle Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_65 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_66 = "Account number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_67 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_68 = "Application Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_69 = "Date of Birth (DD-MM-YYYY)";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_70 = "Reference Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_71 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_72 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_73 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_74 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_75 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_76 = "Loan Number/Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_77 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_78 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_79 = "Loan number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_80 = "Mobile number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_81 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_82 = "Loan Account Number";


    public static String PARAM_BLR_ID_LOAN_REPAYMENT_2_FIELD_27 = "Registered Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_3_FIELD_27 = "DOB";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_10_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_16_FIELD_27 = "DOB (In dd-mm-yyyy)";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_18_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_19_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_24_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_27_FIELD_27 = "Mobile Number";

    public static String PARAM_BLR_ID_LOAN_REPAYMENT_36_FIELD_27 = "Date of Birth (YYYY-MM-DD)";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_37_FIELD_27 = "Date of Birth (YYYY-MM-DD)";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_52_FIELD_27 = "Application Payment Type";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_56_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_57_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_66_FIELD_27 = "Mobile number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_69_FIELD_27 = "Mobile number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_71_FIELD_27 = "Loan Payment Mode";//Doubt.Edittext(For Now) or Spinner
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_80_FIELD_27 = "Date of Birth(YYYY-MM-DD)";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_82_FIELD_27 = "Mobile Number";

    public static String PARAM_BLR_ID_LOAN_REPAYMENT_37_FIELD_28 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_52_FIELD_28 = "Annexure ID";


    //PARAMS -HEALTH_INSURANCE(2)
    public static String PARAM_BLR_ID_HEALTH_INSURANCE_1 = "Policy Number";
    public static String PARAM_BLR_ID_HEALTH_INSURANCE_1_FIELD_27 = "DOB(YYYY-MM-DD)";

    //PARAMS -Insurance(16)
    public static String PARAM_BLR_ID_INSURANCE_1 = "Policy Number";
    public static String PARAM_BLR_ID_INSURANCE_2 = "Policy Number";
    public static String PARAM_BLR_ID_INSURANCE_3 = "Policy No";
    public static String PARAM_BLR_ID_INSURANCE_4 = "Application Number";
    public static String PARAM_BLR_ID_INSURANCE_5 = "Policy Number";
    public static String PARAM_BLR_ID_INSURANCE_6 = "Mobile Number";
    public static String PARAM_BLR_ID_INSURANCE_7 = "Policy Number";
    public static String PARAM_BLR_ID_INSURANCE_8 = "Policy Number";
    public static String PARAM_BLR_ID_INSURANCE_9 = "Policy Number";
    public static String PARAM_BLR_ID_INSURANCE_10 = "PolicyNo";

    public static String PARAM_BLR_ID_INSURANCE_1_FIELD_27 = "DOB (YYYY-MM-DD)";
    public static String PARAM_BLR_ID_INSURANCE_2_FIELD_27 = "Date of Birth(YYYYMMDD)";
    public static String PARAM_BLR_ID_INSURANCE_4_FIELD_27 = "Date of Birth(YYYYMMDD)";
    public static String PARAM_BLR_ID_INSURANCE_5_FIELD_27 = "Email ID";
    public static String PARAM_BLR_ID_INSURANCE_6_FIELD_27 = "Policy Number";
    public static String PARAM_BLR_ID_INSURANCE_7_FIELD_27 = "Email ID";
    public static String PARAM_BLR_ID_INSURANCE_8_FIELD_27 = "Date Of Birth";
    public static String PARAM_BLR_ID_INSURANCE_9_FIELD_27 = "Expiry date (DD/MM/YYYY)";
    public static String PARAM_BLR_ID_INSURANCE_10_FIELD_27 = "DOB(YYYY/MM/DD)";

    public static String PARAM_BLR_ID_INSURANCE_5_FIELD_28 = "Mobile Number";
    public static String PARAM_BLR_ID_INSURANCE_6_FIELD_28 = "Email ID";
    public static String PARAM_BLR_ID_INSURANCE_7_FIELD_28 = "Mobile Number";

    public static String PARAM_BLR_ID_INSURANCE_5_FIELD_29 = "Registration Number";


    //PARAMS -Muncipal Tax(17)
    public static String PARAM_BLR_ID_MUNCIPAL_TAX_1 = "Property ID";
    public static String PARAM_BLR_ID_MUNCIPAL_TAX_2 = "Computer code";


    //PARAMS -Hospital(18)
    public static String PARAM_BLR_ID_HOSPITAL_1 = "Registered Mobile Number";


    //PARAMS -Muncipal Service(19)
    public static String PARAM_BLR_ID_MUNCIPAL_SERVICE_1 = "Shop ID";


    //PARAMS -Housing Society(21)
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_1 = "Flat Number";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_2 = "Unit No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_3 = "Flat No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_4 = "Flat Number";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_5 = "Block No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_6 = "Mobile No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_7 = "Flat No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_8 = "Flat no";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_9 = "Flat no";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_10 = "Flat no";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_11 = "Flat Number";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_12 = "Mobile Number";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_13 = "Flat or Shop No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_14 = "Flat Number";

    public static String PARAM_BLR_ID_HOUSING_SOCIETY_1_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_5_FIELD_27 = "Flat No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_6_FIELD_27 = "Flat No";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_7_FIELD_27 = "Block";
    public static String PARAM_BLR_ID_HOUSING_SOCIETY_12_FIELD_27 = "Flat Number";


    //PARAMS -Subscription(22)
    public static String PARAM_BLR_ID_SUBSCRIPTION_1 = "Mobile Number";
    public static String PARAM_BLR_ID_SUBSCRIPTION_2 = "Email of beneficiary";


    /*************************************************************************************************************************************/
    /*Get Params*/

    /*************************************************************************************************************************************/

    public static String getParamElectricity(String BLR_ID) {

        if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_1) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_3) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_4) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_16) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_20) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_21) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_22) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_23) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_27) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_31) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_33) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_34) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_44) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_54) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_57) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_58) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_63) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_64) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_65) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_69))
            return PARAM_BLR_ELECTRICITY_1;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_2) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_7) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_8) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_26) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_41) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_43) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_55) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_60) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_71))
            return PARAM_BLR_ELECTRICITY_2;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_5) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_6) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_68))
            return PARAM_BLR_ELECTRICITY_3;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_9) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_10) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_14) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_15) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_17))
            return PARAM_BLR_ELECTRICITY_4;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_11))
            return PARAM_BLR_ELECTRICITY_5;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_12) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_62))
            return PARAM_BLR_ELECTRICITY_6;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_13) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_24) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_25) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_45))
            return PARAM_BLR_ELECTRICITY_7;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_19))
            return PARAM_BLR_ELECTRICITY_8;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_28))
            return PARAM_BLR_ELECTRICITY_9;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_29))
            return PARAM_BLR_ELECTRICITY_10;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_30) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_32))
            return PARAM_BLR_ELECTRICITY_11;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_35) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_38) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_40))
            return PARAM_BLR_ELECTRICITY_12;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_36) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_37))
            return PARAM_BLR_ELECTRICITY_13;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_39) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_42))
            return PARAM_BLR_ELECTRICITY_14;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_46) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_47) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_48) ||
                BLR_ID.equals(Const.BLR_ID_ELECTRICITY_49) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_50) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_51) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_67))
            return PARAM_BLR_ELECTRICITY_15;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_52) || BLR_ID.equals(Const.BLR_ID_ELECTRICITY_53))
            return PARAM_BLR_ELECTRICITY_16;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_56))
            return PARAM_BLR_ELECTRICITY_17;

        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_59))
            return PARAM_BLR_ELECTRICITY_18;
        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_61))
            return PARAM_BLR_ELECTRICITY_19;
        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_66))
            return PARAM_BLR_ELECTRICITY_66;

        else
            return "";
    }

    public static String getField26ParamBBPS(String BLR_ID) {
        if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_1))
            return PARAM_BLR_ID_MOBILE_POSTPAID_1;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_2))
            return PARAM_BLR_ID_MOBILE_POSTPAID_2;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_3))
            return PARAM_BLR_ID_MOBILE_POSTPAID_3;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_4))
            return PARAM_BLR_ID_MOBILE_POSTPAID_4;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_5))
            return PARAM_BLR_ID_MOBILE_POSTPAID_5;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_6))
            return PARAM_BLR_ID_MOBILE_POSTPAID_6;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_7))
            return PARAM_BLR_ID_MOBILE_POSTPAID_7;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_8))
            return PARAM_BLR_ID_MOBILE_POSTPAID_8;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_9))
            return PARAM_BLR_ID_MOBILE_POSTPAID_9;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_10))
            return PARAM_BLR_ID_MOBILE_POSTPAID_10;
        else if (BLR_ID.equals(Const.BLR_ID_MOBILE_POSTPAID_11))
            return PARAM_BLR_ID_MOBILE_POSTPAID_11;


        else if (BLR_ID.equals(Const.BLR_ID_DTH_1))
            return PARAM_BLR_ID_DTH_1;
        else if (BLR_ID.equals(Const.BLR_ID_DTH_2))
            return PARAM_BLR_ID_DTH_2;
        else if (BLR_ID.equals(Const.BLR_ID_DTH_3))
            return PARAM_BLR_ID_DTH_3;


        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_1))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_1;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_2))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_2;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_3))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_3;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_4))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_4;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_5))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_5;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_6))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_6;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_7))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_7;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_8))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_8;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_9))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_9;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_10))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_10;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_11))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_11;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_12))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_12;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_13))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_13;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_14))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_14;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_15))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_15;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_16))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_16;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_17))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_17;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_18))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_18;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_19))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_19;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_20))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_20;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_21))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_21;
        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_22))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_22;


        else if (BLR_ID.equals(Const.BLR_ID_WATER_1))
            return PARAM_BLR_ID_WATER_1;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_2))
            return PARAM_BLR_ID_WATER_2;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_3))
            return PARAM_BLR_ID_WATER_3;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_4))
            return PARAM_BLR_ID_WATER_4;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_5))
            return PARAM_BLR_ID_WATER_5;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_6))
            return PARAM_BLR_ID_WATER_6;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_7))
            return PARAM_BLR_ID_WATER_7;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_8))
            return PARAM_BLR_ID_WATER_8;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_9))
            return PARAM_BLR_ID_WATER_9;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_10))
            return PARAM_BLR_ID_WATER_10;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_11))
            return PARAM_BLR_ID_WATER_11;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_12))
            return PARAM_BLR_ID_WATER_12;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_13))
            return PARAM_BLR_ID_WATER_13;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_14))
            return PARAM_BLR_ID_WATER_14;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_15))
            return PARAM_BLR_ID_WATER_15;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_17))
            return PARAM_BLR_ID_WATER_17;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_18))
            return PARAM_BLR_ID_WATER_18;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_19))
            return PARAM_BLR_ID_WATER_19;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_20))
            return PARAM_BLR_ID_WATER_20;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_21))
            return PARAM_BLR_ID_WATER_21;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_22))
            return PARAM_BLR_ID_WATER_22;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_23))
            return PARAM_BLR_ID_WATER_23;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_24))
            return PARAM_BLR_ID_WATER_24;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_25))
            return PARAM_BLR_ID_WATER_25;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_26))
            return PARAM_BLR_ID_WATER_26;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_27))
            return PARAM_BLR_ID_WATER_27;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_28))
            return PARAM_BLR_ID_WATER_28;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_29))
            return PARAM_BLR_ID_WATER_29;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_30))
            return PARAM_BLR_ID_WATER_30;


        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_1))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_1;
        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_2))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_2;
        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_3))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_3;
        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_4))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_4;
        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_7))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_7;
        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_8))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_8;
        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_9))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_9;


        else if (BLR_ID.equals(Const.BLR_ID_GAS_1))
            return PARAM_BLR_ID_GAS_1;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_2))
            return PARAM_BLR_ID_GAS_2;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_3))
            return PARAM_BLR_ID_GAS_3;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_4))
            return PARAM_BLR_ID_GAS_4;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_5))
            return PARAM_BLR_ID_GAS_5;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_6))
            return PARAM_BLR_ID_GAS_6;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_7))
            return PARAM_BLR_ID_GAS_7;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_8))
            return PARAM_BLR_ID_GAS_8;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_9))
            return PARAM_BLR_ID_GAS_9;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_10))
            return PARAM_BLR_ID_GAS_10;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_11))
            return PARAM_BLR_ID_GAS_11;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_12))
            return PARAM_BLR_ID_GAS_12;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_13))
            return PARAM_BLR_ID_GAS_13;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_14))
            return PARAM_BLR_ID_GAS_14;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_15))
            return PARAM_BLR_ID_GAS_15;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_16))
            return PARAM_BLR_ID_GAS_16;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_17))
            return PARAM_BLR_ID_GAS_17;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_18))
            return PARAM_BLR_ID_GAS_18;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_19))
            return PARAM_BLR_ID_GAS_19;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_20))
            return PARAM_BLR_ID_GAS_20;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_21))
            return PARAM_BLR_ID_GAS_21;
        else if (BLR_ID.equals(Const.BLR_ID_GAS_22))
            return PARAM_BLR_ID_GAS_22;


        else if (BLR_ID.equals(Const.BLR_ID_EDUCATION_FEES_1))
            return PARAM_BLR_ID_EDUCATION_FEES_1;


        else if (BLR_ID.equals(Const.BLR_ID_CABLE_TV_1))
            return PARAM_BLR_ID_CABLE_TV_1;


        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_1))
            return PARAM_BLR_ID_LPG_GAS_1;
        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_2))
            return PARAM_BLR_ID_LPG_GAS_2;
        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3;


        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_1))
            return PARAM_BLR_ID_FASTTAG_1;
        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_2))
            return PARAM_BLR_ID_FASTTAG_2;
        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_3))
            return PARAM_BLR_ID_FASTTAG_3;
        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_4))
            return PARAM_BLR_ID_FASTTAG_4;
        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_5))
            return PARAM_BLR_ID_FASTTAG_5;
        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_6))
            return PARAM_BLR_ID_FASTTAG_6;
        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_7))
            return PARAM_BLR_ID_FASTTAG_7;
        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_8))
            return PARAM_BLR_ID_FASTTAG_8;


        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_1))
            return PARAM_BLR_ID_LIFE_INSURANCE_1;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_2))
            return PARAM_BLR_ID_LIFE_INSURANCE_2;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_3))
            return PARAM_BLR_ID_LIFE_INSURANCE_3;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_4))
            return PARAM_BLR_ID_LIFE_INSURANCE_4;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_5))
            return PARAM_BLR_ID_LIFE_INSURANCE_5;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_6))
            return PARAM_BLR_ID_LIFE_INSURANCE_6;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_7))
            return PARAM_BLR_ID_LIFE_INSURANCE_7;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_9))
            return PARAM_BLR_ID_LIFE_INSURANCE_9;


        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_1))
            return PARAM_BLR_ID_LOAN_REPAYMENT_1;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_2))
            return PARAM_BLR_ID_LOAN_REPAYMENT_2;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_3))
            return PARAM_BLR_ID_LOAN_REPAYMENT_3;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_4))
            return PARAM_BLR_ID_LOAN_REPAYMENT_4;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_5))
            return PARAM_BLR_ID_LOAN_REPAYMENT_5;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_6))
            return PARAM_BLR_ID_LOAN_REPAYMENT_6;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_7))
            return PARAM_BLR_ID_LOAN_REPAYMENT_7;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_8))
            return PARAM_BLR_ID_LOAN_REPAYMENT_8;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_9))
            return PARAM_BLR_ID_LOAN_REPAYMENT_9;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_10))
            return PARAM_BLR_ID_LOAN_REPAYMENT_10;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_11))
            return PARAM_BLR_ID_LOAN_REPAYMENT_11;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_12))
            return PARAM_BLR_ID_LOAN_REPAYMENT_12;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_13))
            return PARAM_BLR_ID_LOAN_REPAYMENT_13;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_14))
            return PARAM_BLR_ID_LOAN_REPAYMENT_14;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_15))
            return PARAM_BLR_ID_LOAN_REPAYMENT_15;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_16))
            return PARAM_BLR_ID_LOAN_REPAYMENT_16;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_17))
            return PARAM_BLR_ID_LOAN_REPAYMENT_17;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_18))
            return PARAM_BLR_ID_LOAN_REPAYMENT_18;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_19))
            return PARAM_BLR_ID_LOAN_REPAYMENT_19;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_20))
            return PARAM_BLR_ID_LOAN_REPAYMENT_20;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_21))
            return PARAM_BLR_ID_LOAN_REPAYMENT_21;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_22))
            return PARAM_BLR_ID_LOAN_REPAYMENT_22;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_23))
            return PARAM_BLR_ID_LOAN_REPAYMENT_23;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_24))
            return PARAM_BLR_ID_LOAN_REPAYMENT_24;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_25))
            return PARAM_BLR_ID_LOAN_REPAYMENT_25;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_26))
            return PARAM_BLR_ID_LOAN_REPAYMENT_26;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_27))
            return PARAM_BLR_ID_LOAN_REPAYMENT_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_28))
            return PARAM_BLR_ID_LOAN_REPAYMENT_28;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_29))
            return PARAM_BLR_ID_LOAN_REPAYMENT_29;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_30))
            return PARAM_BLR_ID_LOAN_REPAYMENT_30;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_31))
            return PARAM_BLR_ID_LOAN_REPAYMENT_31;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_32))
            return PARAM_BLR_ID_LOAN_REPAYMENT_32;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_33))
            return PARAM_BLR_ID_LOAN_REPAYMENT_33;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_34))
            return PARAM_BLR_ID_LOAN_REPAYMENT_34;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_35))
            return PARAM_BLR_ID_LOAN_REPAYMENT_35;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_36))
            return PARAM_BLR_ID_LOAN_REPAYMENT_36;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_37))
            return PARAM_BLR_ID_LOAN_REPAYMENT_37;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_38))
            return PARAM_BLR_ID_LOAN_REPAYMENT_38;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_39))
            return PARAM_BLR_ID_LOAN_REPAYMENT_39;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_40))
            return PARAM_BLR_ID_LOAN_REPAYMENT_40;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_41))
            return PARAM_BLR_ID_LOAN_REPAYMENT_41;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_42))
            return PARAM_BLR_ID_LOAN_REPAYMENT_42;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_43))
            return PARAM_BLR_ID_LOAN_REPAYMENT_43;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_44))
            return PARAM_BLR_ID_LOAN_REPAYMENT_44;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_45))
            return PARAM_BLR_ID_LOAN_REPAYMENT_45;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_46))
            return PARAM_BLR_ID_LOAN_REPAYMENT_46;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_47))
            return PARAM_BLR_ID_LOAN_REPAYMENT_47;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_48))
            return PARAM_BLR_ID_LOAN_REPAYMENT_48;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_49))
            return PARAM_BLR_ID_LOAN_REPAYMENT_49;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_50))
            return PARAM_BLR_ID_LOAN_REPAYMENT_50;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_51))
            return PARAM_BLR_ID_LOAN_REPAYMENT_51;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_52))
            return PARAM_BLR_ID_LOAN_REPAYMENT_52;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_53))
            return PARAM_BLR_ID_LOAN_REPAYMENT_53;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_54))
            return PARAM_BLR_ID_LOAN_REPAYMENT_54;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_55))
            return PARAM_BLR_ID_LOAN_REPAYMENT_55;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_56))
            return PARAM_BLR_ID_LOAN_REPAYMENT_56;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_57))
            return PARAM_BLR_ID_LOAN_REPAYMENT_57;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_58))
            return PARAM_BLR_ID_LOAN_REPAYMENT_58;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_59))
            return PARAM_BLR_ID_LOAN_REPAYMENT_59;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_60))
            return PARAM_BLR_ID_LOAN_REPAYMENT_60;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_61))
            return PARAM_BLR_ID_LOAN_REPAYMENT_61;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_62))
            return PARAM_BLR_ID_LOAN_REPAYMENT_62;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_63))
            return PARAM_BLR_ID_LOAN_REPAYMENT_63;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_64))
            return PARAM_BLR_ID_LOAN_REPAYMENT_64;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_65))
            return PARAM_BLR_ID_LOAN_REPAYMENT_65;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_66))
            return PARAM_BLR_ID_LOAN_REPAYMENT_66;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_67))
            return PARAM_BLR_ID_LOAN_REPAYMENT_67;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_68))
            return PARAM_BLR_ID_LOAN_REPAYMENT_68;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_69))
            return PARAM_BLR_ID_LOAN_REPAYMENT_69;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_70))
            return PARAM_BLR_ID_LOAN_REPAYMENT_70;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_71))
            return PARAM_BLR_ID_LOAN_REPAYMENT_71;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_72))
            return PARAM_BLR_ID_LOAN_REPAYMENT_72;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_73))
            return PARAM_BLR_ID_LOAN_REPAYMENT_73;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_74))
            return PARAM_BLR_ID_LOAN_REPAYMENT_74;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_75))
            return PARAM_BLR_ID_LOAN_REPAYMENT_75;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_76))
            return PARAM_BLR_ID_LOAN_REPAYMENT_76;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_77))
            return PARAM_BLR_ID_LOAN_REPAYMENT_77;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_78))
            return PARAM_BLR_ID_LOAN_REPAYMENT_78;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_79))
            return PARAM_BLR_ID_LOAN_REPAYMENT_79;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_80))
            return PARAM_BLR_ID_LOAN_REPAYMENT_80;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_81))
            return PARAM_BLR_ID_LOAN_REPAYMENT_81;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_82))
            return PARAM_BLR_ID_LOAN_REPAYMENT_82;

        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_1))
            return PARAM_BLR_ID_INSURANCE_1;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_2))
            return PARAM_BLR_ID_INSURANCE_2;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_3))
            return PARAM_BLR_ID_INSURANCE_3;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_4))
            return PARAM_BLR_ID_INSURANCE_4;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_5))
            return PARAM_BLR_ID_INSURANCE_5;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_6))
            return PARAM_BLR_ID_INSURANCE_6;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_7))
            return PARAM_BLR_ID_INSURANCE_7;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_8))
            return PARAM_BLR_ID_INSURANCE_8;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_9))
            return PARAM_BLR_ID_INSURANCE_9;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_10))
            return PARAM_BLR_ID_INSURANCE_10;

        else if (BLR_ID.equals(Const.BLR_ID_MUNCIPAL_TAX_1))
            return PARAM_BLR_ID_MUNCIPAL_TAX_1;
        else if (BLR_ID.equals(Const.BLR_ID_MUNCIPAL_TAX_2))
            return PARAM_BLR_ID_MUNCIPAL_TAX_2;

        else if (BLR_ID.equals(Const.BLR_ID_HOSPITAL_1))
            return PARAM_BLR_ID_HOSPITAL_1;

        else if (BLR_ID.equals(Const.BLR_ID_MUNCIPAL_SERVICE_1))
            return PARAM_BLR_ID_MUNCIPAL_SERVICE_1;

        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_1))
            return PARAM_BLR_ID_HOUSING_SOCIETY_1;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_2))
            return PARAM_BLR_ID_HOUSING_SOCIETY_2;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_3))
            return PARAM_BLR_ID_HOUSING_SOCIETY_3;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_4))
            return PARAM_BLR_ID_HOUSING_SOCIETY_4;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_5))
            return PARAM_BLR_ID_HOUSING_SOCIETY_5;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_6))
            return PARAM_BLR_ID_HOUSING_SOCIETY_6;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_7))
            return PARAM_BLR_ID_HOUSING_SOCIETY_7;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_8))
            return PARAM_BLR_ID_HOUSING_SOCIETY_8;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_9))
            return PARAM_BLR_ID_HOUSING_SOCIETY_9;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_10))
            return PARAM_BLR_ID_HOUSING_SOCIETY_10;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_11))
            return PARAM_BLR_ID_HOUSING_SOCIETY_11;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_12))
            return PARAM_BLR_ID_HOUSING_SOCIETY_12;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_13))
            return PARAM_BLR_ID_HOUSING_SOCIETY_13;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_14))
            return PARAM_BLR_ID_HOUSING_SOCIETY_14;

        else if (BLR_ID.equals(Const.BLR_ID_SUBSCRIPTION_1))
            return PARAM_BLR_ID_SUBSCRIPTION_1;
        else if (BLR_ID.equals(Const.BLR_ID_SUBSCRIPTION_2))
            return PARAM_BLR_ID_SUBSCRIPTION_2;


        else if (BLR_ID.equals(Const.BLR_ID_HEALTH_INSURANCE_1))
            return PARAM_BLR_ID_HEALTH_INSURANCE_1;

        else
            return "";
    }

    public static String getParamCommonField27(String BLR_ID) {
        if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_24))
            return PARAM_BLR_ELECTRICITY_24_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_25))
            return PARAM_BLR_ELECTRICITY_25_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_27))
            return PARAM_BLR_ELECTRICITY_27_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_34))
            return PARAM_BLR_ELECTRICITY_34_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_68))
            return PARAM_BLR_ELECTRICITY_68_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_ELECTRICITY_71))
            return PARAM_BLR_ELECTRICITY_71_FIELD27;



        if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_2))
            return PARAM_BLR_ID_LOAN_REPAYMENT_2_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_3))
            return PARAM_BLR_ID_LOAN_REPAYMENT_3_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_10))
            return PARAM_BLR_ID_LOAN_REPAYMENT_10_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_16))
            return PARAM_BLR_ID_LOAN_REPAYMENT_16_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_18))
            return PARAM_BLR_ID_LOAN_REPAYMENT_18_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_19))
            return PARAM_BLR_ID_LOAN_REPAYMENT_19_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_24))
            return PARAM_BLR_ID_LOAN_REPAYMENT_24_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_27))
            return PARAM_BLR_ID_LOAN_REPAYMENT_27_FIELD_27;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_36))
            return PARAM_BLR_ID_LOAN_REPAYMENT_36_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_37))
            return PARAM_BLR_ID_LOAN_REPAYMENT_37_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_52))
            return PARAM_BLR_ID_LOAN_REPAYMENT_52_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_56))
            return PARAM_BLR_ID_LOAN_REPAYMENT_56_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_57))
            return PARAM_BLR_ID_LOAN_REPAYMENT_57_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_66))
            return PARAM_BLR_ID_LOAN_REPAYMENT_66_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_69))
            return PARAM_BLR_ID_LOAN_REPAYMENT_69_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_71))
            return PARAM_BLR_ID_LOAN_REPAYMENT_71_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_80))
            return PARAM_BLR_ID_LOAN_REPAYMENT_80_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_82))
            return PARAM_BLR_ID_LOAN_REPAYMENT_82_FIELD_27;

        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_1))
            return PARAM_BLR_ID_INSURANCE_1_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_2))
            return PARAM_BLR_ID_INSURANCE_2_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_4))
            return PARAM_BLR_ID_INSURANCE_4_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_5))
            return PARAM_BLR_ID_INSURANCE_5_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_6))
            return PARAM_BLR_ID_INSURANCE_6_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_7))
            return PARAM_BLR_ID_INSURANCE_7_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_8))
            return PARAM_BLR_ID_INSURANCE_8_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_9))
            return PARAM_BLR_ID_INSURANCE_9_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_10))
            return PARAM_BLR_ID_INSURANCE_10_FIELD_27;

        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_1))
            return PARAM_BLR_ID_HOUSING_SOCIETY_1_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_5))
            return PARAM_BLR_ID_HOUSING_SOCIETY_5_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_6))
            return PARAM_BLR_ID_HOUSING_SOCIETY_6_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_7))
            return PARAM_BLR_ID_HOUSING_SOCIETY_7_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_HOUSING_SOCIETY_12))
            return PARAM_BLR_ID_HOUSING_SOCIETY_12_FIELD_27;


        else if (BLR_ID.equals(Const.BLR_ID_HEALTH_INSURANCE_1))
            return PARAM_BLR_ID_HEALTH_INSURANCE_1_FIELD_27;

        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_1))
            return PARAM_BLR_ID_LIFE_INSURANCE_1_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_2))
            return PARAM_BLR_ID_LIFE_INSURANCE_2_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_3))
            return PARAM_BLR_ID_LIFE_INSURANCE_3_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_7))
            return PARAM_BLR_ID_LIFE_INSURANCE_7_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_27;

        else if (BLR_ID.equals(Const.BLR_ID_FASTTAG_1))
            return PARAM_BLR_ID_FASTTAG_1_FIELD_27;

        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_1))
            return PARAM_BLR_ID_LPG_GAS_1_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_2))
            return PARAM_BLR_ID_LPG_GAS_2_FIELD_27;
        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3_FIELD_27;

        else if (BLR_ID.equals(Const.BLR_ID_BROADBAND_POSTPAID_21))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_21_FIELD27;

        else if (BLR_ID.equals(Const.BLR_ID_WATER_9))
            return PARAM_BLR_ID_WATER_9_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_13))
            return PARAM_BLR_ID_WATER_13_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_17))
            return PARAM_BLR_ID_WATER_17_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_28))
            return PARAM_BLR_ID_WATER_28_FIELD27;

        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_7))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_7_FIELD27;
        else if (BLR_ID.equals(Const.BLR_ID_LANDLINE_POSTPAID_8))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_8_FIELD27;

        else
            return "";
    }

    public static String getParamCommonField28(String BLR_ID) {
        if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_28;

        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3_FIELD_28;

        else if (BLR_ID.equals(Const.BLR_ID_WATER_13))
            return PARAM_BLR_ID_WATER_13_FIELD28;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16_FIELD28;
        else if (BLR_ID.equals(Const.BLR_ID_WATER_17))
            return PARAM_BLR_ID_WATER_17_FIELD28;

        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_37))
            return PARAM_BLR_ID_LOAN_REPAYMENT_37_FIELD_28;
        else if (BLR_ID.equals(Const.BLR_ID_LOAN_REPAYMENT_52))
            return PARAM_BLR_ID_LOAN_REPAYMENT_52_FIELD_28;

        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_5))
            return PARAM_BLR_ID_INSURANCE_5_FIELD_28;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_6))
            return PARAM_BLR_ID_INSURANCE_6_FIELD_28;
        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_7))
            return PARAM_BLR_ID_INSURANCE_7_FIELD_28;

        else
            return "";
    }

    public static String getParamCommonField29(String BLR_ID) {
        if (BLR_ID.equals(Const.BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_29;

        else if (BLR_ID.equals(Const.BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3_FIELD_29;

        else if (BLR_ID.equals(Const.BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16_FIELD29;

        else if (BLR_ID.equals(Const.BLR_ID_INSURANCE_5))
            return PARAM_BLR_ID_INSURANCE_5_FIELD_29;

        else
            return "";
    }

    /*************************************************************************************************************************************/

    public static String getBillImage(String blrID) {
        switch (blrID) {

            case "WESCO0000ODI01":
                return "https://storage.googleapis.com/biller/OD-wu.jpg";
            case "MESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-mescom1.webp";
            case "KSEBL0000KER01":
                return "https://storage.googleapis.com/biller/KE-kseb.webp";
            case "UPCL00000UTT01":
                return "https://storage.googleapis.com/biller/UT-upcl.webp";
            case "GESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-gescom.jpg";
            case "CESC00000KOL01":
                return "https://storage.googleapis.com/biller/WB-CESC.jpg";
            case "MPDC00000MEG01":
                return "https://storage.googleapis.com/biller/MG-mpdcl.jpg";
            case "SOUTHCO00ODI01":
                return "https://storage.googleapis.com/biller/OD-su.jpg";
            case "HESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-hescom.jpg";
            case "RELI00000MUM03":
                return "https://storage.googleapis.com/biller/MA-ae.jpg";
            case "AVVNL0000RAJ01":
                return "https://storage.googleapis.com/biller/RA-avvnl.jpg";
            case "JDVVNL000RAJ01":
                return "https://storage.googleapis.com/biller/RA-jdvnl.jpg";
            case "UPPCL0000UTP01":
                return "https://storage.googleapis.com/biller/UP-UPPCL.jpg";
            case "SPDCLOB00ANP01":
                return "https://storage.googleapis.com/biller/AP-spdcl.jpg";
            case "WBSEDCL00WBL01":
                return "https://storage.googleapis.com/biller/WB-WBSEDCL.jpg";
            case "PSPCL0000PUN01":
                return "https://storage.googleapis.com/biller/PU-pspcl.jpg";
            case "APDCL0000ASM01":
                return "https://storage.googleapis.com/biller/AS-apdcl.jpg";
            case "APDCL0000ASM02":
                return "https://storage.googleapis.com/biller/AS-apdcl.jpg";
            case "DGVCL0000GUJ01":
                return "https://storage.googleapis.com/biller/GU-dgvcl.jpg";
            case "DHBVN0000HAR01":
                return "https://storage.googleapis.com/biller/HA-dhbvn.jpg";
            case "JVVNL0000RAJ01":
                return "https://storage.googleapis.com/biller/RA-jvvnl.jpg";
            case "PGVCL0000GUJ01":
                return "https://storage.googleapis.com/biller/GU-pgvcl.jpg";
            case "UGVCL0000GUJ01":
                return "https://storage.googleapis.com/biller/GU-ugvcl.jpg";
            case "EPDCLOB00ANP01":
                return "https://storage.googleapis.com/biller/A-apepdcl.jpg";
            case "CSPDCL000CHH01":
                return "https://storage.googleapis.com/biller/C-cspdcl.jpg";
            case "BEST00000MUM01":
                return "https://storage.googleapis.com/biller/MA-BEST.jpg";
            case "MPPO00000MAP0Y":
                return "https://storage.googleapis.com/biller/MP-pkvb.jpg";
            case "MPEZ00000MAP02":
                return "https://storage.googleapis.com/biller/MP-pkvb.jpg";
            case "MPEZ00000MAP01":
                return "https://storage.googleapis.com/biller/MP-pkvb.jpg";
            case "SBPDCL000BHI01":
                return "https://storage.googleapis.com/biller/BI-sbpd.jpg";
            case "TATAPWR00MUM01":
                return "https://storage.googleapis.com/biller/MA-TataPower.jpg";
            case "BESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-bescom.jpg";
            case "JUSC00000JAM01":
                return "https://storage.googleapis.com/biller/JH-jusco.jpg";
            case "NESCO0000ODI01":
                return "https://storage.googleapis.com/biller/OD-nu.jpg";
            case "BESLOB000RAJ02":
                return "https://storage.googleapis.com/biller/RA-besl.jpg";
            case "KEDLOB000RAJ02":
                return "https://storage.googleapis.com/biller/RA-kedl.jpg";
            case "NBPDCL000BHI01":
                return "https://storage.googleapis.com/biller/BI-nbpd.jpg";
            case "MPPK00000MAP01":
                return "https://storage.googleapis.com/biller/MP-pkvvcl.jpg";


            default:
                return "";
        }
    }

    public static int field26ValidationMaxLength(EditText etField26, String CategoryID, String blrID) {
        int MAX_LENGTH;

        switch (blrID) {

            case "CGSM00000GUJ01":
                MAX_LENGTH = 5;
                break;
            case "LOKS00000NATC9":
                MAX_LENGTH = 6;
                break;
            case "LAMP00000NAT7E":
            case "VGL000000GUJ01":
            case "NEXTRA000NAT01":
                MAX_LENGTH = 7;
                break;

            case "EXID00000NAT25":
            case "HDFC00000NATV4":
            case "IPRU00000NAT01":
            case "PRAM00000NATYI":
            case "RELI00000NAT3O":
            case "HPCL00000NAT01":
            case "SVAT00000NATUB":
            case "UCPGPL000MAH01":
            case "BWSSB0000KAR01":
            case "FLAS00000NATVZ":
                MAX_LENGTH = 8;
                break;

            case "MEGH00000NATLV":
            case "MCA000000PUN01":
            case "MCJ000000PUN01":
            case "CHAI00000NATYY":
            case "DIGA00000NAT3C":
                MAX_LENGTH = 9;
                break;


            case "BHAR00000NATR4":
            case "INDI00000NATT5":
            case "AVAI00000NAT7J":
            case "FAIR00000NAT6Z":
            case "I2IF00000NAT6K":
            case "SNAP00000NAT61":
            case "FUTU00000NAT09":
            case "TATA00000NATLP":
            case "ADAN00000NAT01":
            case "GAIL00000IND01":
            case "INDRAPGASDEL02":
            case "IOAG00000DEL01":
            case "MNGL00000MAH02":
            case "GAIL00000NATTZ":
            case "BSNL00000NAT5C":
            case "BSNL00000NATPZ":
            case "MTNL00000DEL01":
            case "MTNL00000MUM01":
            case "DLJB00000DEL01":
            case "NDMC00000DEL01":
            case "PMC000000PUN01":
            case "UNN000000MAP01":
            case "NETP00000PUNS8":
            case "TIKO00000NAT01":
            case "TIMB00000NATRQ":
            case "TTN000000NAT01":
            case "VFIB00000NATJJ":
            case "AVIV00000NAT5I":
            case "CANA00000NATPI":
            case "FURL00000NATF6":
            case "BAID00000NATWG":
            case "CRED00000NAT0U":
            case "THEF00000NATZO":
            case "PAYT00000NATTQ":
                MAX_LENGTH = 10;
                break;

            case "ATLLI0000NAT01":
            case "ATLLI0000NAT02":
            case "ATBROAD00NAT01":
            case "ATBROAD00NAT02":
            case "SWIF00000NATVE":
            case "SBIL00000NATT0":
                MAX_LENGTH = 11;
                break;

            case "IDFC00000NATCK":
            case "GUJGAS000GUJ01":
            case "HCG000000HAR02":
            case "MAHA00000MUM01":
            case "SGL000000GUJ01":
            case "SITI00000UTP03":
            case "IEPL00000GUJ01":
            case "TATADLLI0NAT01":
                MAX_LENGTH = 12;
                break;
            case "ASSA00000ASMA5":
            case "DELH00000DEL6Q":
            case "KERA00000KERMO":
            case "ALTU00000NATG5":
            case "AVAN00000NATHI":
                MAX_LENGTH = 13;
                break;
            case "SHRI00000NATRI":
            case "SMC000000DNH01":
            case "MAXB00000NAT28":
            case "FINC00000NAT3E":
                MAX_LENGTH = 14;
                break;
            case "VAST00000NATLW":
            case "AGL000000MAP01":
            case "BHAG00000NATBJ":
            case "GGL000000UTP01":
            case "DEPA00000MIZ9U":
            case "GMC000000MAP01":
            case "GWMC00000WGL01":
            case "JALK00000UTP0P":
            case "IMC000000MAP01":
            case "JMC000000MAP01":
            case "HATHWAY00NAT01":
            case "EXCE00000NATDP":
            case "COMWBB000NAT01":
            case "CONBB0000PUN01":
            case "DVOIS0000NAT02":
            case "FUSNBB000NAT01":
            case "ANNA00000NATUO":
            case "AXIS00000NATJD":
                MAX_LENGTH = 15;
                break;
            case "JANA00000NATO4":
            case "BHAR00000NAT52":
            case "HERO00000NATI6":
                MAX_LENGTH = 16;
                break;
            case "ESSK00000NATFR":
            case "BFL000000NAT01":
                MAX_LENGTH = 18;
                break;
            case "HERO00000NAT7F":
                MAX_LENGTH = 19;
                break;


            case "MCG000000GUR01":
            case "MUNI00000CHANI":
            case "PUNE00000MAHSE":
            case "SMC000000GUJ01":
            case "PORT00000ANI1K":
            case "UITWOB000BHW02":
            case "TNGCLOB00TRI01":
            case "CUGL00000UTP01":
            case "AAVA00000NATMF":
            case "CAPR00000NATC0":
            case "CAPR00000NATUB":
            case "INDI00000NAT2P":
            case "HUBL00000KAR9T":
            case "AXIS00000NATM1":
            case "BERA00000NATOY":
            case "EDUV00000NATF1":
            case "FULL00000NATD4":
            case "HOME00000NATWT":
                MAX_LENGTH = 20;
                break;
            case "TATA00000NATGS":
                MAX_LENGTH = 21;
                break;
            case "UJS000000UTT01":
            case "DCBB00000NAT2K":
                MAX_LENGTH = 22;
                break;

            case "BAJA00000NAT58":
                MAX_LENGTH = 24;
                break;

            case "SHRI00000NAT7D":
            case "HMWSS0000HYD01":
            case "DENB00000NATIO":
            case "INST00000CHHZV":
            case "PRAY00000ALLXR":
            case "GUFI00000NATV8":
                MAX_LENGTH = 25;
                break;
            case "RMC000000JHA01":
            case "ANNA00000NATMR":
                MAX_LENGTH = 26;
                break;
            case "STAR00000NATXZ":
            case "SPENET000NAT01":
            case "AMRU00000PUNIU":
            case "EBON00000UTP1N":
            case "FAIR00000MAHKJ":
            case "GANE00000PUNJR":
            case "INDR00000SURMW":
            case "JANA00000HYDD6":
            case "PARI00000GUJJL":
            case "PARK00000MAHPU":
            case "PARK00000THACF":
            case "PARK00000THAMY":
            case "SHRE00000MAHS4":
            case "SHRE00000RGM9F":
            case "SOME00000RGM7X":
            case "SUSH00000PUNML":
                MAX_LENGTH = 30;
                break;

            case "INST00000BIHKL":
            case "MNET00000ASM5W":
                MAX_LENGTH = 40;
                break;

/*
            case "FLEX00000NATJL": case "ADIT00000NATRA": case "BAJA00000NATV1": case "CLIX00000NATST": case "INDI00000NATYG": case "LAND00000NATRD": case "MANA00000NATWG": case "MOTI00000NATHD": case "PAIS00000NATCV": case "VART00000NATHC": case "BMC000000MAP01": case "MCL000000PUN01": case "ACT000000NAT01": case "AEGO00000NATRJ": case "MAGM00000NAT61": case "MAGM00000NAT6B": case "MAGM00000NATQI": case "ROYA00000NAT2C": case "BKAR00000NATNS": case "PORT00000ANIWE": case "HTDI00000NATC5": case "ADAN00000NATI9": case "ASCE00000NATGK": case "BAJA00000NATP2": case "FLEX00000NAT3Z": case "FULL00000NAT8X": case "HIRA00000NATSP": case "HOME00000NATVX": case "IIFL00000NAT5F": case "IIFL00000NATMF": case "INCR00000NATJG":
                MAX_LENGTH = 50;
                break;
*/
            case "VASA00000THAE9":
                MAX_LENGTH = 60;
                break;

            default:
                MAX_LENGTH = 50;
                break;
        }

        if (CategoryID.equals(MOBILE_POSTPAID) || CategoryID.equals(HEALTH_INSURANCE) || CategoryID.equals(FASTAG))
            MAX_LENGTH = 10;
        else if (CategoryID.equals(DTH))
            MAX_LENGTH = 11;
        else if (CategoryID.equals(CABLE_TV))
            MAX_LENGTH = 12;
        else if (CategoryID.equals(EDUCATION_FEES))
            MAX_LENGTH = 50;

        return MAX_LENGTH;
    }

    public static int field26ValidationInputType(EditText etField26, String CategoryID, String blrID) {
        int INPUT_TYPE;

        //Input type: Number
        switch (blrID) {
            case "AVAI00000NAT7J":
            case "ESSK00000NATFR":
            case "FAIR00000NAT6Z":
            case "FLEX00000NATJL":
            case "HERO00000NAT7F":
            case "I2IF00000NAT6K":
            case "JANA00000NATO4":
            case "LOKS00000NATC9":
            case "SNAP00000NAT61":
            case "SVAT00000NATUB":
            case "EXID00000NAT25":
            case "HDFC00000NATV4":
            case "IPRU00000NAT01":
            case "PRAM00000NATYI":
            case "RELI00000NAT3O":
            case "ADAN00000NAT01":
            case "CGSM00000GUJ01":
            case "GAIL00000IND01":
            case "GUJGAS000GUJ01":
            case "HCG000000HAR02":
            case "INDRAPGASDEL02":
            case "IOAG00000DEL01":
            case "MAHA00000MUM01":
            case "MNGL00000MAH02":
            case "SGL000000GUJ01":
            case "SITI00000UTP03":
            case "TNGCLOB00TRI01":
            case "VGL000000GUJ01":
            case "GAIL00000NATTZ":
            case "MEGH00000NATLV":
            case "DLJB00000DEL01":
            case "HUDA00000HAR01":
            case "IMC000000MAP01":
            case "JMC000000MAP01":
            case "MCA000000PUN01":
            case "MCC000000KAR01":
            case "MCJ000000PUN01":
            case "NDMC00000DEL01":
            case "PMC000000PUN01":
            case "SMC000000DNH01":
            case "UITWOB000BHW02":
            case "UJS000000UTT01":
            case "UNN000000MAP01":
            case "ATBROAD00NAT01":
            case "ATBROAD00NAT02":
            case "FLAS00000NATVZ":
            case "HATHWAY00NAT01":
            case "NETP00000PUNS8":
            case "NEXTRA000NAT01":
            case "TIKO00000NAT01":
            case "TIMB00000NATRQ":
            case "TTN000000NAT01":
            case "VFIB00000NATJJ":
            case "EXCE00000NATDP":
            case "CANA00000NATPI":
            case "MAXB00000NAT28":
            case "FURL00000NATF6":
            case "ALTU00000NATG5":
            case "ASCE00000NATGK":
            case "AXIS00000NATJD":
            case "CHAI00000NATYY":
            case "FINC00000NAT3E":
            case "FULL00000NAT8X":
            case "FULL00000NATD4":
            case "GUFI00000NATV8":
            case "HERO00000NATI6":
            case "HOME00000NATWT":
                INPUT_TYPE = InputType.TYPE_CLASS_NUMBER;
                break;

                /*case "AAVA00000NATMF": case "ADIT00000NATRA": case "BAJA00000NATV1": case "BFL000000NAT01": case "CAPR00000NATC0": case "CAPR00000NATUB": case "CLIX00000NATST": case "IDFC00000NATCK": case "INDI00000NAT2P": case "INDI00000NATYG": case "LAMP00000NAT7E": case "LAND00000NATRD": case "MANA00000NATWG": case "MOTI00000NATHD": case "PAIS00000NATCV": case "SHRI00000NAT7D": case "TATA00000NATGS": case "VART00000NATHC": case "VAST00000NATLW": case "FUTU00000NAT09": case "SHRI00000NATRI": case "STAR00000NATXZ": case "TATA00000NATLP": case "AGL000000MAP01": case "ASSA00000ASMA5": case "BHAG00000NATBJ": case "CUGL00000UTP01": case "GGL000000UTP01": case "IEPL00000GUJ01": case "UCPGPL000MAH01": case "BMC000000MAP01": case "BWSSB0000KAR01": case "DELH00000DEL6Q": case "DEPA00000MIZ9U": case "GMC000000MAP01": case "GWMC00000WGL01": case "HMWSS0000HYD01": case "KERA00000KERMO": case "MCG000000GUR01": case "MUNI00000CHANI": case "PUNE00000MAHSE": case "RMC000000JHA01": case "SMC000000GUJ01": case "VASA00000THAE9": case "PORT00000ANI1K": case "JALK00000UTP0P": case "ACT000000NAT01": case "COMWBB000NAT01": case "CONBB0000PUN01": case "DENB00000NATIO": case "DVOIS0000NAT02": case "FUSNBB000NAT01": case "INST00000BIHKL": case "INST00000CHHZV": case "MNET00000ASM5W": case "SPENET000NAT01": case "SWIF00000NATVE": case "AEGO00000NATRJ": case "AVIV00000NAT5I": case "BAJA00000NAT58": case "MAGM00000NAT61": case "MAGM00000NAT6B": case "MAGM00000NATQI": case "ROYA00000NAT2C": case "SBIL00000NATT0": case "HUBL00000KAR9T": case "PRAY00000ALLXR": case "BKAR00000NATNS": case "PORT00000ANIWE": case "AMRU00000PUNIU": case "EBON00000UTP1N": case "FAIR00000MAHKJ": case "GANE00000PUNJR": case "INDR00000SURMW": case "JANA00000HYDD6": case "PARI00000GUJJL": case "PARK00000MAHPU": case "PARK00000THACF": case "PARK00000THAMY": case "SHRE00000MAHS4": case "SHRE00000RGM9F": case "SOME00000RGM7X": case "SUSH00000PUNML": case "ADAN00000NATI9": case "ANNA00000NATMR": case "ANNA00000NATUO": case "AVAN00000NATHI": case "AXIS00000NATM1": case "BAID00000NATWG": case "BAJA00000NATP2": case "BERA00000NATOY": case "BHAR00000NAT52": case "CRED00000NAT0U": case "DCBB00000NAT2K": case "DIGA00000NAT3C": case "EDUV00000NATF1": case "FLEX00000NAT3Z": case "HIRA00000NATSP": case "HOME00000NATVX": case "IIFL00000NAT5F": case "IIFL00000NATMF": case "INCR00000NATJG":
                INPUT_TYPE = InputType.TYPE_CLASS_TEXT;
                break;*/

            case "MCL000000PUN01":
            case "HTDI00000NATC5":
                INPUT_TYPE = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
                break;

            default:
                INPUT_TYPE = InputType.TYPE_CLASS_TEXT;
                break;


        }

        if (CategoryID.equals(MOBILE_POSTPAID) ||CategoryID.equals(DTH) || CategoryID.equals(CABLE_TV) || CategoryID.equals(LPG_GAS) || CategoryID.equals(LANDLINE_POSTPAID)) {
            INPUT_TYPE = InputType.TYPE_CLASS_NUMBER;
        } else if (CategoryID.equals(FASTAG) || CategoryID.equals(EDUCATION_FEES)) {
            INPUT_TYPE = InputType.TYPE_CLASS_TEXT;
        }

        return INPUT_TYPE;
    }

    public static String getBBPSTitle(String CategoryID) {
        String Title = "";

        if (CategoryID.equals(Const.MOBILE_POSTPAID))
            Title = "Mobile Postpaid";
        else if (CategoryID.equals(Const.DTH))
            Title = "DTH";
        else if (CategoryID.equals(Const.LPG_GAS))
            Title = "LPG Gas";
        else if (CategoryID.equals(Const.BROADBAND_POSTPAID))
            Title = "Broadband Postpaid";
        else if (CategoryID.equals(Const.GAS))
            Title = "Gas";
        else if (CategoryID.equals(Const.LANDLINE_POSTPAID))
            Title = "Landline Postpaid";
        else if (CategoryID.equals(Const.WATER))
            Title = "Water";
        else if (CategoryID.equals(Const.LOAN_REPAYMENT))
            Title = "Loan Repayment";
        else if (CategoryID.equals(Const.LIFE_INSURANCE))
            Title = "Life Insurance";
        else if (CategoryID.equals(Const.FASTAG))
            Title = "Fasttag";
        else if (CategoryID.equals(Const.CABLE_TV))
            Title = "Cable TV";
        else if (CategoryID.equals(Const.HEALTH_INSURANCE))
            Title = "Health Insurance";
        else if (CategoryID.equals(Const.EDUCATION_FEES))
            Title = "Education Fees";

        else if (CategoryID.equals(Const.INSURANCE))
            Title = "Insurance";
        else if (CategoryID.equals(Const.MUNCIPAL_TAX))
            Title = "Muncipal Tax";
        else if (CategoryID.equals(Const.SUBSCRIPTION))
            Title = "Subscription";
        else if (CategoryID.equals(Const.HOUSING_SOCIETY))
            Title = "Housing Society";
        else if (CategoryID.equals(Const.MUNICIPAL_SERVICES))
            Title = "Municipal Services";
        else if (CategoryID.equals(Const.HOSPITAL))
            Title = "Hospital";

        return Title;
    }


    /*************************************************************************************************************************************/
    /*****Error Validation*****/
    public static JSONObject getFieldValuesJsonCommonPayBill(String Pincode, String PosMobileNumber, String LatLong, String TerminalID, String Field1, String Field26, String Field27, String Field28, String Field29, String Field2, String BillAmount, String latePaymentAmount, String earlyPaymentAmount) {
        JSONObject objFieldValues = new JSONObject();
        try {

            objFieldValues.put("Field16", TerminalID);
            objFieldValues.put("Field17", PosMobileNumber);
            objFieldValues.put("Field18", LatLong);
            objFieldValues.put("Field19", Pincode);
            objFieldValues.put("Field1", Field1); //Payee Mobile Number
            objFieldValues.put("Field26", Field26);
            objFieldValues.put("Field27", Field27);
            objFieldValues.put("Field28", Field28);
            objFieldValues.put("Field29", Field29);
            objFieldValues.put("Field2", Field2);

            objFieldValues.put("Field36", BillAmount);
            objFieldValues.put("Field37", latePaymentAmount);
            objFieldValues.put("Field38", earlyPaymentAmount);

            /*
            objFieldValues.put("BlrId", BLR_ID);
            objFieldValues.put("Keyword", Keyword);
            objFieldValues.put("AgntId", AgentId);
            objFieldValues.put("PayChannel", PAY_CHANNEL);
            objFieldValues.put("AgntRefId", AgentReferenceId);
*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        return objFieldValues;

    }

    public static JSONObject getFieldValuesJsonCommonFetchBill(Context context, String Field1, String Field26, String Field27, String Field28, String Field29, String Field2) {
        JSONObject objFieldValues = new JSONObject();
        try {

            objFieldValues.put("Field19", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_PINCODE, ""));
            objFieldValues.put("Field17", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_MOBILE_NUMBER, ""));
            objFieldValues.put("Field18", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_LAT_LONG, ""));
            objFieldValues.put("Field16", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_TERMINAL_ID, ""));
            objFieldValues.put("Field26", Field26);
            objFieldValues.put("Field1", Field1);
            objFieldValues.put("Field2", Field2);
            objFieldValues.put("Field27", Field27);
            objFieldValues.put("Field28", Field28);
            objFieldValues.put("Field29", Field29);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return objFieldValues;

    }

    public static String popupError(JSONObject obj) {
        String ERROR_MSG = "";
        String commonClassObject = "", statusDescription = "";

        try {

            commonClassObject = obj.optString("commonClassObject");
            statusDescription = obj.optString("statusDescription");


            if (commonClassObject.equals("null") || commonClassObject.equals(null) || commonClassObject.equals("")) {
                ERROR_MSG = statusDescription;
            } else {
                JSONObject jsonObjectCommonClassObject = new JSONObject(commonClassObject);
                String msg = jsonObjectCommonClassObject.optString("msg");
                String errors = jsonObjectCommonClassObject.optString("errors");

                if (errors.equals("")) {
                    ERROR_MSG = msg;
                } else {
                    //Currently showing first error msg, Show all errors
                    JSONArray jsonArray = new JSONArray(errors);
                    String err_msg = jsonArray.getJSONObject(0).optString("err_msg");
                    ERROR_MSG = err_msg;
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            ERROR_MSG = "OOPS!!! Something went wrong. Please try again.";
        }

        return ERROR_MSG;
    }

    public static String getFetchBillError(JSONObject obj) {
        String ERROR_MSG = "";
        String statusCode = "", commonClassObject = "", statusDescription = "";

        try {
            statusCode = obj.optString("statusCode");
            commonClassObject = obj.optString("commonClassObject");
            statusDescription = obj.optString("statusDescription");

            if (statusCode.equals("-1")) {
                if (!commonClassObject.equals("")) {
                    String errors = new JSONObject(commonClassObject).optString("errors");
                    if (!errors.equals("")) {
                        String err_type = new JSONArray(errors).getJSONObject(0).optString("err_type");
                        if (err_type.equals("1")) {
                            ERROR_MSG = new JSONArray(errors).getJSONObject(0).optString("err_msg");
                        } else if (err_type.equals("0")) {
                            ERROR_MSG = "Cannot fetch the bill amount , coz" + statusDescription;
                        } else {
                            ERROR_MSG = statusDescription;
                        }
                    } else {
                        ERROR_MSG = statusDescription;
                    }
                } else {
                    ERROR_MSG = statusDescription;
                }
            } else {
                ERROR_MSG = "Failed! Please Try Again";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ERROR_MSG;

    }

    public static String getPayBillError(JSONObject obj) {
        String ERROR_MSG = "";
        String statusCode = "", commonClassObject = "", statusDescription = "";

        try {
            statusCode = obj.optString("statusCode");
            commonClassObject = obj.optString("commonClassObject");
            statusDescription = obj.optString("statusDescription");

            if (statusCode.equals("-1")) {
                if (commonClassObject.equals("null") || commonClassObject.equals(null) || commonClassObject.equals("")) {
                    ERROR_MSG = statusDescription;
                } else {
                    String errors = new JSONObject(commonClassObject).optString("errors");
                    String msg = new JSONObject(commonClassObject).optString("msg");

                    if (!errors.equals("")) {
                        String err_type = new JSONArray(errors).getJSONObject(0).optString("err_type");
                        if (err_type.equals("1")) {
                            ERROR_MSG = new JSONArray(errors).getJSONObject(0).optString("err_msg");
                        } else if (err_type.equals("0")) {
                            ERROR_MSG = "Cannot pay the bill amount , coz" + statusDescription;
                        }
                    } else {
                        //Take to receipt
                        ERROR_MSG = "TAKE_TO_RECEIPT";
                    }
                }
            } else {
                ERROR_MSG = "Failed! Please Try Again";
            }
        } catch (Exception e) {
            e.printStackTrace();
            ERROR_MSG = "OOPS!!! Something went wrong. Please try again.";
        }

        return ERROR_MSG;
    }

}


