package com.isuisudmt.bbps.utils;

import java.util.Map;

public class BillerListPojo {
    public String blrId;
    public String blrName;
    public Map<String, Object> other_parameters;

    public BillerListPojo(String blrId, String blrName, Map<String, Object> other_parameters) {
        this.blrId = blrId;
        this.blrName = blrName;
        this.other_parameters = other_parameters;
    }

    public BillerListPojo() {
    }

    public String getBlrId() {
        return blrId;
    }

    public void setBlrId(String blrId) {
        this.blrId = blrId;
    }

    public String getBlrName() {
        return blrName;
    }

    public void setBlrName(String blrName) {
        this.blrName = blrName;
    }

    public Map<String, Object> getOther_parameters() {
        return other_parameters;
    }

    public void setOther_parameters(Map<String, Object> other_parameters) {
        this.other_parameters = other_parameters;
    }

    @Override
    public String toString() {
        return  blrName;
    }

}