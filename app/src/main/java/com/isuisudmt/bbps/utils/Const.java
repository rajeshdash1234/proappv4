package com.isuisudmt.bbps.utils;


/**
 * This class represents the constants used in the BBPS.
 *
 * @author Prabhakar Panda
 * @date 08/10/2020.
 */

public class Const {
    /**
     * Base Urls
     */
    public static final String URL_IS_UPDATED = "https://indusindtest.iserveu.online/BBPS/bbps/isUpdated";
    public static final String URL_VIEW_REQUIRED_INFO = "https://indusindtest.iserveu.online/BBPS/bbps/viewRequiredInfo";
    public static final String URL_FETCH_BILL = "https://indusindtest.iserveu.online/BBPS/bbps/fetchBill";
    public static final String URL_PAY_BILL = "https://indusindtest.iserveu.online/BBPS/bbps/payBill";
    public static final String URL_GET_CCF = "https://indusindtest.iserveu.online/BBPS/bbps/getCCf";
    public static final String URL_GET_REPORT_BBPS = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/all_transaction_report";
    public static final String URL_PAY_BILL_PREPAID = "https://wallet-deduct-recharge-vn3k2k7q7q-uc.a.run.app/walletDeductMobileRecharge";
    //public static final String URL_GET_REPORT_PREPAID = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report_staging_temp/all_transaction_report";
    public static final String URL_GET_REPORT_PREPAID = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/all_transaction_report";

    //Dynamic feature check
    public static boolean isBBPSenabled = false;
    public static boolean isRechargeEnabled = false;
    public static boolean isAEPS1enabled = false;
    public static boolean isAEPS2enabled = false;
    public static boolean isAEPS3enabled = false;
    public static boolean isMATM1enabled = false;
    public static boolean isMATM2enabled = false;
    public static boolean isDMTenabled = false;
    public static boolean isCashoutWallet1enabled = false;
    public static boolean isCashoutWallet2enabled = false;
    public static boolean isInsuranceEnabled = false;

    //Feature IDs
    public static String ID_CASHOUT_WALLET1 = "19";
    public static String ID_RECHARGE = "20";
    public static String ID_AEPS1 = "27";
    public static String ID_DMT = "30";
    public static String ID_MATM1 = "33";
    public static String ID_INSURANCE = "34";
    public static String ID_AEPS2 = "47";
    public static String ID_MATM2 = "48";
    public static String ID_CASHOUT_WALLET2 = "50";
    public static String ID_BBPS = "52";
    public static String ID_AEPS3 = "57";

    //Feature Disable Error Message
    public static String ERROR_MESSAGE_DISABLED_BBPS = "Pay Bill feature is not available";
    public static String ERROR_MESSAGE_DISABLED_DMT = "DMT feature not available";
    public static String ERROR_MESSAGE_DISABLED_INSURANCE = "Insurance feature not available";
    public static String ERROR_MESSAGE_DISABLED_RECHARGE = "Recharge feature not available";
    public static String ERROR_MESSAGE_DISABLED_CASHOUT = "Cashout feature not available";
    public static String ERROR_MESSAGE_DISABLED_CASHOUT_WALLET1 = "Cashout wallet 1 feature not available";
    public static String ERROR_MESSAGE_DISABLED_CASHOUT_WALLET2 = "Cashout wallet 2 feature not available";
    public static String ERROR_MESSAGE_DISABLED_MATM = "mATM feature not available";
    public static String ERROR_MESSAGE_DISABLED_MATM1 = "mATM1 feature not available";
    public static String ERROR_MESSAGE_DISABLED_MATM2 = "mATM2 feature not available";
    public static String ERROR_MESSAGE_DISABLED_AEPS = "AePS feature not available";
    public static String ERROR_MESSAGE_DISABLED_AEPS1 = "AePS1 feature not available";
    public static String ERROR_MESSAGE_DISABLED_AEPS2 = "AePS2 feature not available";
    public static String ERROR_MESSAGE_DISABLED_AEPS3 = "AePS3 feature not available";

    //ERROR TOAST MSG
    public static String ERROR_MSG_LOW_BALANCE_WALLET_2 = "Wallet2 doesn't have sufficient balance to make this transaction";
    public static String ERROR_MSG_NULL_CUSTOMER_MOBILE_NO = "Please enter customer mobile number";
    public static String ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO = "Please enter valid phone number";

    //isUpdated Response
    public static String isUpdatedResponse = "";
    public static String viewRequiredInfoResponse = "";
    public static String usernameForBBPS = "";// Use this for indusUserName in ccf, fetch, pay

    public static Double Wallet2Amount = 0.0;

    //Firestore Collection
    public static String DEFAULT_BILLER_COLLECTION = "DefaultBiller_Collection";
    public static String PREFIX_DEFAULT_BILLER_DOCUMENT = "CategoryId_";

    //BBPS: CATEGORY_IDs (Except Electricity)
    public static String BROADBAND_POSTPAID = "1";
    public static String DTH = "2";
    public static String ELECTRICITY = "3";
    public static String GAS = "4";
    public static String LANDLINE_POSTPAID = "5";
    public static String MOBILE_POSTPAID = "6";
    public static String WATER = "7";
    public static String LOAN_REPAYMENT = "8";
    public static String LIFE_INSURANCE = "10";
    public static String FASTAG = "11";
    public static String LPG_GAS = "12";
    public static String CABLE_TV = "13";
    public static String HEALTH_INSURANCE = "14";
    public static String EDUCATION_FEES = "15";
    public static String INSURANCE = "16";
    public static String MUNCIPAL_TAX = "17";
    public static String HOSPITAL = "18";
    public static String MUNICIPAL_SERVICES = "19";
    public static String HOUSING_SOCIETY = "21";
    public static String SUBSCRIPTION = "22";

    //BBPS Required Parameters
    public static String BBPS_PREF = "bbpsPref";
    public static String SF_PINCODE = "pincodeKey";
    public static String SF_MOBILE_NUMBER = "mobileNumberKey";
    public static String SF_LAT_LONG = "latlongKey";
    public static String SF_TERMINAL_ID = "terminalIdKey";
    public static String SF_AGENT_ID = "agentIDKey";
    public static String SF_KEYWORD = "keyboardKey";

    public static String PAY_CHANNEL = "BSC";
    public static String PAY_MODE = "Cash";

    public static String[] arrPaymentType = {"Choose Payment Mode", "Cash", "Credit Card", "Debit Card", "IMPS", "Internet Banking", "NEFT", "Prepaid Card", "UPI", "Wallet"};


    //BILLERID -ELECTRICITY
    public static String BLR_ID_ELECTRICITY_1 = "WESCO0000ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_2 = "CESU00000ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_3 = "NESCO0000ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_4 = "SOUTHCO00ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_5 = "EPDCLOB00ANP01";//ANP
    public static String BLR_ID_ELECTRICITY_6 = "SPDCLOB00ANP01";//ANP
    public static String BLR_ID_ELECTRICITY_7 = "APDCL0000ASM02";//ASM
    public static String BLR_ID_ELECTRICITY_8 = "APDCL0000ASM01";//ASM
    public static String BLR_ID_ELECTRICITY_9 = "NBPDCL000BHI01";//BIH
    public static String BLR_ID_ELECTRICITY_10 = "SBPDCL000BHI01";//BIH
    public static String BLR_ID_ELECTRICITY_11 = "ELEC00000CHA3L";//CHA
    public static String BLR_ID_ELECTRICITY_12 = "CSPDCL000CHH01";//CHH
    public static String BLR_ID_ELECTRICITY_13 = "DDED00000DAD01";//DAD
    public static String BLR_ID_ELECTRICITY_14 = "BSESRAJPLDEL01";//DEL
    public static String BLR_ID_ELECTRICITY_15 = "BSESYAMPLDEL01";//DEL
    public static String BLR_ID_ELECTRICITY_16 = "NDMC00000DEL02";//DEL
    public static String BLR_ID_ELECTRICITY_17 = "TATAPWR00DEL01";//DEL
    public static String BLR_ID_ELECTRICITY_19 = "GED000000GOA01";//GOA
    public static String BLR_ID_ELECTRICITY_20 = "UGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_21 = "DGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_22 = "MGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_23 = "PGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_24 = "UHBVN0000HAR01";//HAR
    public static String BLR_ID_ELECTRICITY_25 = "DHBVN0000HAR01";//HAR
    public static String BLR_ID_ELECTRICITY_26 = "HPSEB0000HIP02";//HIP
    public static String BLR_ID_ELECTRICITY_27 = "JBVNL0000JHA01";//JHA
    public static String BLR_ID_ELECTRICITY_62 = "JUSC00000JAM01";//JHA
    public static String BLR_ID_ELECTRICITY_28 = "MESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_29 = "BESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_30 = "CESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_31 = "GESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_32 = "HESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_33 = "KSEBL0000KER01";//KER
    public static String BLR_ID_ELECTRICITY_34 = "MAHA00000MAH01";//MAH
    public static String BLR_ID_ELECTRICITY_63 = "RELI00000MUM03";//MAH
    public static String BLR_ID_ELECTRICITY_64 = "BEST00000MUM01";//MAH
    public static String BLR_ID_ELECTRICITY_65 = "TATAPWR00MUM01";//MAH
    public static String BLR_ID_ELECTRICITY_35 = "MPEZ00000MAP02";//MAP
    public static String BLR_ID_ELECTRICITY_36 = "MPCZ00000MAP01";//MAP
    public static String BLR_ID_ELECTRICITY_37 = "MPCZ00000MAP02";//MAP
    public static String BLR_ID_ELECTRICITY_38 = "MPEZ00000MAP01";//MAP
    public static String BLR_ID_ELECTRICITY_39 = "MPPK00000MAP01";//MAP
    public static String BLR_ID_ELECTRICITY_40 = "MPPO00000MAP0Y";//MAP
    public static String BLR_ID_ELECTRICITY_41 = "MPDC00000MEG01";//MEG
    public static String BLR_ID_ELECTRICITY_42 = "PEDM00000MIZ01";//MIZ
    public static String BLR_ID_ELECTRICITY_43 = "DOPN00000NAG01";//NAG
    public static String BLR_ID_ELECTRICITY_44 = "GOVE00000PUDN0";//PUD
    public static String BLR_ID_ELECTRICITY_45 = "PSPCL0000PUN01";//PUN
    public static String BLR_ID_ELECTRICITY_46 = "JVVNL0000RAJ01";//RAJ
    public static String BLR_ID_ELECTRICITY_47 = "AVVNL0000RAJ01";//RAJ
    public static String BLR_ID_ELECTRICITY_48 = "BESLOB000RAJ02";//RAJ
    public static String BLR_ID_ELECTRICITY_49 = "BKESL0000RAJ02";//RAJ
    public static String BLR_ID_ELECTRICITY_50 = "JDVVNL000RAJ01";//RAJ
    public static String BLR_ID_ELECTRICITY_51 = "KEDLOB000RAJ02";//RAJ
    public static String BLR_ID_ELECTRICITY_52 = "SKPR00000SIK02";//SIK
    public static String BLR_ID_ELECTRICITY_53 = "SKPR00000SIK01";//SIK
    public static String BLR_ID_ELECTRICITY_54 = "TNEB00000TND01";//TND
    public static String BLR_ID_ELECTRICITY_55 = "TSEC00000TRI01";//TRI
    public static String BLR_ID_ELECTRICITY_56 = "KESCO0000UTP01";//UTP
    public static String BLR_ID_ELECTRICITY_57 = "UPPCL0000UTP01";//UTP
    public static String BLR_ID_ELECTRICITY_58 = "UPPCL0000UTP02";//UTP
    public static String BLR_ID_ELECTRICITY_59 = "UPCL00000UTT01";//UTT
    public static String BLR_ID_ELECTRICITY_60 = "WBSEDCL00WBL01";//WBL
    public static String BLR_ID_ELECTRICITY_61 = "CESC00000KOL01";//WBL

    public static String BLR_ID_ELECTRICITY_66 = "MANG00000KAR75";//KAR     //F26 RR Number
    public static String BLR_ID_ELECTRICITY_67 = "TPADL0000AJM02";//DEL     //K Number
    public static String BLR_ID_ELECTRICITY_68 = "TORR00000NATLX";          //Service Number, 27: City
    public static String BLR_ID_ELECTRICITY_69 = "NPCL00000NOI01";          //Consumer Number
    public static String BLR_ID_ELECTRICITY_70 = "UTTA00000UTT7";
    public static String BLR_ID_ELECTRICITY_71 = "WEST00000WBL75";          //Consumer ID, 27: Mobile Number




    //BILLERID -MOBILE POSTPAID
    public static String BLR_ID_MOBILE_POSTPAID_1 = "ATPOST000NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_2 = "ATPOST000NAT02";
    public static String BLR_ID_MOBILE_POSTPAID_3 = "BSNLMOB00NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_4 = "IDEA00000NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_5 = "JIO000000NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_6 = "JIO000000NAT02";
    public static String BLR_ID_MOBILE_POSTPAID_7 = "TATADCDMANAT01";
    public static String BLR_ID_MOBILE_POSTPAID_8 = "TATADGSM0NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_9 = "VODA00000NAT96";
    public static String BLR_ID_MOBILE_POSTPAID_10 = "VODA00000NATVA";
    public static String BLR_ID_MOBILE_POSTPAID_11 = "MTNL00000MAH8E";


    //BILLERID -DTH
    public static String BLR_ID_DTH_1 = "DISH00000NAT01";
    public static String BLR_ID_DTH_2 = "TATASKY00NAT01";
    public static String BLR_ID_DTH_3 = "VIDEOCON0NAT01";


    //BILLERID -BROADBAND POSTPAID
    public static String BLR_ID_BROADBAND_POSTPAID_1 = "ACT000000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_2 = "ATBROAD00NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_3 = "ATBROAD00NAT02";
    public static String BLR_ID_BROADBAND_POSTPAID_4 = "COMWBB000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_5 = "CONBB0000PUN01";
    public static String BLR_ID_BROADBAND_POSTPAID_6 = "DENB00000NATIO";
    public static String BLR_ID_BROADBAND_POSTPAID_7 = "DVOIS0000NAT02";
    public static String BLR_ID_BROADBAND_POSTPAID_8 = "FLAS00000NATVZ";
    public static String BLR_ID_BROADBAND_POSTPAID_9 = "FUSNBB000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_10 = "HATHWAY00NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_11 = "INST00000BIHKL";
    public static String BLR_ID_BROADBAND_POSTPAID_12 = "INST00000CHHZV";
    public static String BLR_ID_BROADBAND_POSTPAID_13 = "MNET00000ASM5W";
    public static String BLR_ID_BROADBAND_POSTPAID_14 = "NETP00000PUNS8";
    public static String BLR_ID_BROADBAND_POSTPAID_15 = "NEXTRA000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_16 = "SPENET000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_17 = "TIKO00000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_18 = "TIMB00000NATRQ";
    public static String BLR_ID_BROADBAND_POSTPAID_19 = "TTN000000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_20 = "VFIB00000NATJJ";
    public static String BLR_ID_BROADBAND_POSTPAID_21 = "EXCE00000NATDP";
    public static String BLR_ID_BROADBAND_POSTPAID_22 = "SWIF00000NATVE";


    //BILLERID -WATER
    public static String BLR_ID_WATER_1 = "BMC000000MAP01";
    public static String BLR_ID_WATER_2 = "BWSSB0000KAR01";
    public static String BLR_ID_WATER_3 = "DELH00000DEL6Q";
    public static String BLR_ID_WATER_4 = "DEPA00000MIZ9U";
    public static String BLR_ID_WATER_5 = "DLJB00000DEL01";
    public static String BLR_ID_WATER_6 = "GMC000000MAP01";
    public static String BLR_ID_WATER_7 = "GWMC00000WGL01";
    public static String BLR_ID_WATER_8 = "HMWSS0000HYD01";
    public static String BLR_ID_WATER_9 = "HUDA00000HAR01";
    public static String BLR_ID_WATER_10 = "IMC000000MAP01";
    public static String BLR_ID_WATER_11 = "JMC000000MAP01";
    public static String BLR_ID_WATER_12 = "KERA00000KERMO";
    public static String BLR_ID_WATER_13 = "MCA000000PUN01";
    public static String BLR_ID_WATER_14 = "MCC000000KAR01";
    public static String BLR_ID_WATER_15 = "MCG000000GUR01";
    public static String BLR_ID_WATER_16 = "MCJ000000PUN01";
    public static String BLR_ID_WATER_17 = "MCL000000PUN01";
    public static String BLR_ID_WATER_18 = "MUNI00000CHANI";
    public static String BLR_ID_WATER_19 = "NDMC00000DEL01";
    public static String BLR_ID_WATER_20 = "PMC000000PUN01";
    public static String BLR_ID_WATER_21 = "PUNE00000MAHSE";
    public static String BLR_ID_WATER_22 = "RMC000000JHA01";
    public static String BLR_ID_WATER_23 = "SMC000000DNH01";
    public static String BLR_ID_WATER_24 = "SMC000000GUJ01";
    public static String BLR_ID_WATER_25 = "UITWOB000BHW02";
    public static String BLR_ID_WATER_26 = "UJS000000UTT01";
    public static String BLR_ID_WATER_27 = "UNN000000MAP01";
    public static String BLR_ID_WATER_28 = "VASA00000THAE9";
    public static String BLR_ID_WATER_29 = "PORT00000ANI1K";
    public static String BLR_ID_WATER_30 = "JALK00000UTP0P";


    //BILLERID -LANDLINE_POSTPAID
    public static String BLR_ID_LANDLINE_POSTPAID_1 = "ATLLI0000NAT01";
    public static String BLR_ID_LANDLINE_POSTPAID_2 = "ATLLI0000NAT02";
    public static String BLR_ID_LANDLINE_POSTPAID_3 = "BSNL00000NAT5C";
    public static String BLR_ID_LANDLINE_POSTPAID_4 = "BSNL00000NATPZ";
    public static String BLR_ID_LANDLINE_POSTPAID_5 = "BSNLLLCORNAT01";
    public static String BLR_ID_LANDLINE_POSTPAID_6 = "BSNLLLINDNAT01";
    public static String BLR_ID_LANDLINE_POSTPAID_7 = "MTNL00000DEL01";
    public static String BLR_ID_LANDLINE_POSTPAID_8 = "MTNL00000MUM01";
    public static String BLR_ID_LANDLINE_POSTPAID_9 = "TATADLLI0NAT01";


    //BILLERID -GAS
    public static String BLR_ID_GAS_1 = "ADAN00000NAT01";
    public static String BLR_ID_GAS_2 = "AGL000000MAP01";
    public static String BLR_ID_GAS_3 = "ASSA00000ASMA5";
    public static String BLR_ID_GAS_4 = "BHAG00000NATBJ";
    public static String BLR_ID_GAS_5 = "CGSM00000GUJ01";
    public static String BLR_ID_GAS_6 = "CUGL00000UTP01";
    public static String BLR_ID_GAS_7 = "GAIL00000IND01";
    public static String BLR_ID_GAS_8 = "GGL000000UTP01";
    public static String BLR_ID_GAS_9 = "GUJGAS000GUJ01";
    public static String BLR_ID_GAS_10 = "HCG000000HAR02";
    public static String BLR_ID_GAS_11 = "IEPL00000GUJ01";
    public static String BLR_ID_GAS_12 = "INDRAPGASDEL02";
    public static String BLR_ID_GAS_13 = "IOAG00000DEL01";
    public static String BLR_ID_GAS_14 = "MAHA00000MUM01";
    public static String BLR_ID_GAS_15 = "MNGL00000MAH02";
    public static String BLR_ID_GAS_16 = "SGL000000GUJ01";
    public static String BLR_ID_GAS_17 = "SITI00000UTP03";
    public static String BLR_ID_GAS_18 = "TNGCLOB00TRI01";
    public static String BLR_ID_GAS_19 = "UCPGPL000MAH01";
    public static String BLR_ID_GAS_20 = "VGL000000GUJ01";
    public static String BLR_ID_GAS_21 = "GAIL00000NATTZ";
    public static String BLR_ID_GAS_22 = "MEGH00000NATLV";


    //BILLERID -EDUCATION_FEES(15)
    public static String BLR_ID_EDUCATION_FEES_1 = "MOUN00000DEL3N";


    //BILLERID -CABLE_TV(13)
    public static String BLR_ID_CABLE_TV_1 = "HATH00000NATRZ";


    //BILLERID -LPG_GAS(12)
    public static String BLR_ID_LPG_GAS_1 = "BHAR00000NATR4";
    public static String BLR_ID_LPG_GAS_2 = "HPCL00000NAT01";
    public static String BLR_ID_LPG_GAS_3 = "INDI00000NATT5";


    //BILLERID -FASTTAG(11)
    public static String BLR_ID_FASTTAG_1 = "AXIS00000NATSN";
    public static String BLR_ID_FASTTAG_2 = "BANK00000NATDH";
    public static String BLR_ID_FASTTAG_3 = "INDI00000NATTR";
    public static String BLR_ID_FASTTAG_4 = "INDU00000NATR2";
    public static String BLR_ID_FASTTAG_5 = "EQUI00000NATNF";
    public static String BLR_ID_FASTTAG_6 = "KOTA00000NATJZ";
    public static String BLR_ID_FASTTAG_7 = "THEF00000NATZO";
    public static String BLR_ID_FASTTAG_8 = "PAYT00000NATTQ";

    //BILLERID -LIFE_INSURANCE(10)
    public static String BLR_ID_LIFE_INSURANCE_1 = "EXID00000NAT25";
    public static String BLR_ID_LIFE_INSURANCE_2 = "FUTU00000NAT09";
    public static String BLR_ID_LIFE_INSURANCE_3 = "HDFC00000NATV4";
    public static String BLR_ID_LIFE_INSURANCE_4 = "IPRU00000NAT01";
    public static String BLR_ID_LIFE_INSURANCE_5 = "PRAM00000NATYI";
    public static String BLR_ID_LIFE_INSURANCE_6 = "RELI00000NAT3O";
    public static String BLR_ID_LIFE_INSURANCE_7 = "SHRI00000NATRI";
    public static String BLR_ID_LIFE_INSURANCE_8 = "STAR00000NATXZ";
    public static String BLR_ID_LIFE_INSURANCE_9 = "TATA00000NATLP";


    //BILLERID -LOAN_REPAYMENT(8)
    public static String BLR_ID_LOAN_REPAYMENT_1 = "AAVA00000NATMF";
    public static String BLR_ID_LOAN_REPAYMENT_2 = "ADIT00000NATRA";
    public static String BLR_ID_LOAN_REPAYMENT_3 = "AVAI00000NAT7J";
    public static String BLR_ID_LOAN_REPAYMENT_4 = "BAJA00000NATV1";
    public static String BLR_ID_LOAN_REPAYMENT_5 = "BFL000000NAT01";
    public static String BLR_ID_LOAN_REPAYMENT_6 = "CAPR00000NATC0";
    public static String BLR_ID_LOAN_REPAYMENT_7 = "CAPR00000NATUB";
    public static String BLR_ID_LOAN_REPAYMENT_8 = "CLIX00000NATST";
    public static String BLR_ID_LOAN_REPAYMENT_9 = "ESSK00000NATFR";
    public static String BLR_ID_LOAN_REPAYMENT_10 = "FAIR00000NAT6Z";
    public static String BLR_ID_LOAN_REPAYMENT_11 = "FLEX00000NATJL";
    public static String BLR_ID_LOAN_REPAYMENT_12 = "HERO00000NAT7F";
    public static String BLR_ID_LOAN_REPAYMENT_13 = "I2IF00000NAT6K";
    public static String BLR_ID_LOAN_REPAYMENT_14 = "IDFC00000NATCK";
    public static String BLR_ID_LOAN_REPAYMENT_15 = "INDI00000NAT2P";
    public static String BLR_ID_LOAN_REPAYMENT_16 = "INDI00000NATYG";
    public static String BLR_ID_LOAN_REPAYMENT_17 = "JANA00000NATO4";
    public static String BLR_ID_LOAN_REPAYMENT_18 = "LAMP00000NAT7E";
    public static String BLR_ID_LOAN_REPAYMENT_19 = "LAND00000NATRD";
    public static String BLR_ID_LOAN_REPAYMENT_20 = "LOKS00000NATC9";
    public static String BLR_ID_LOAN_REPAYMENT_21 = "MANA00000NATWG";
    public static String BLR_ID_LOAN_REPAYMENT_22 = "MOTI00000NATHD";
    public static String BLR_ID_LOAN_REPAYMENT_23 = "PAIS00000NATCV";
    public static String BLR_ID_LOAN_REPAYMENT_24 = "SHRI00000NAT7D";
    public static String BLR_ID_LOAN_REPAYMENT_25 = "SNAP00000NAT61";
    public static String BLR_ID_LOAN_REPAYMENT_26 = "SVAT00000NATUB";
    public static String BLR_ID_LOAN_REPAYMENT_27 = "TATA00000NATGS";
    public static String BLR_ID_LOAN_REPAYMENT_28 = "VART00000NATHC";
    public static String BLR_ID_LOAN_REPAYMENT_29 = "VAST00000NATLW";

    public static String BLR_ID_LOAN_REPAYMENT_30 = "ADAN00000NATI9";
    public static String BLR_ID_LOAN_REPAYMENT_31 = "ALTU00000NATG5";
    public static String BLR_ID_LOAN_REPAYMENT_32 = "ANNA00000NATMR";
    public static String BLR_ID_LOAN_REPAYMENT_33 = "ANNA00000NATUO";
    public static String BLR_ID_LOAN_REPAYMENT_34 = "ASCE00000NATGK";
    public static String BLR_ID_LOAN_REPAYMENT_35 = "AVAN00000NATHI";
    public static String BLR_ID_LOAN_REPAYMENT_36 = "AXIS00000NATJD";
    public static String BLR_ID_LOAN_REPAYMENT_37 = "AXIS00000NATM1";
    public static String BLR_ID_LOAN_REPAYMENT_38 = "BAID00000NATWG";
    public static String BLR_ID_LOAN_REPAYMENT_39 = "BAJA00000NATP2";

    public static String BLR_ID_LOAN_REPAYMENT_40 = "BERA00000NATOY";
    public static String BLR_ID_LOAN_REPAYMENT_41 = "BHAR00000NAT52";
    public static String BLR_ID_LOAN_REPAYMENT_42 = "CHAI00000NATYY";
    public static String BLR_ID_LOAN_REPAYMENT_43 = "CRED00000NAT0U";
    public static String BLR_ID_LOAN_REPAYMENT_44 = "DCBB00000NAT2K";
    public static String BLR_ID_LOAN_REPAYMENT_45 = "DIGA00000NAT3C";
    public static String BLR_ID_LOAN_REPAYMENT_46 = "EDUV00000NATF1";
    public static String BLR_ID_LOAN_REPAYMENT_47 = "FINC00000NAT3E";
    public static String BLR_ID_LOAN_REPAYMENT_48 = "FLEX00000NAT3Z";
    public static String BLR_ID_LOAN_REPAYMENT_49 = "FULL00000NAT8X";

    public static String BLR_ID_LOAN_REPAYMENT_50 = "FULL00000NATD4";
    public static String BLR_ID_LOAN_REPAYMENT_51 = "GUFI00000NATV8";
    public static String BLR_ID_LOAN_REPAYMENT_52 = "HERO00000NATI6";
    public static String BLR_ID_LOAN_REPAYMENT_53 = "HIRA00000NATSP";
    public static String BLR_ID_LOAN_REPAYMENT_54 = "HOME00000NATVX";
    public static String BLR_ID_LOAN_REPAYMENT_55 = "HOME00000NATWT";
    public static String BLR_ID_LOAN_REPAYMENT_56 = "IIFL00000NAT5F";
    public static String BLR_ID_LOAN_REPAYMENT_57 = "IIFL00000NATMF";
    public static String BLR_ID_LOAN_REPAYMENT_58 = "INCR00000NATJG";
    public static String BLR_ID_LOAN_REPAYMENT_59 = "INDI00000NAT12";

    public static String BLR_ID_LOAN_REPAYMENT_60 = "INDU00000NATQH";
    public static String BLR_ID_LOAN_REPAYMENT_61 = "JANA00000NATV5";
    public static String BLR_ID_LOAN_REPAYMENT_62 = "KANA00000NATQV";
    public static String BLR_ID_LOAN_REPAYMENT_63 = "LIGH00000NATKW";
    public static String BLR_ID_LOAN_REPAYMENT_64 = "MAHA00000NATSI";
    public static String BLR_ID_LOAN_REPAYMENT_65 = "MAHI00000NATIC";
    public static String BLR_ID_LOAN_REPAYMENT_66 = "MAXV00000NAT33";
    public static String BLR_ID_LOAN_REPAYMENT_67 = "MIDL00000NATIP";
    public static String BLR_ID_LOAN_REPAYMENT_68 = "MINT00000NATUB";
    public static String BLR_ID_LOAN_REPAYMENT_69 = "MONE00000NAT7P";

    public static String BLR_ID_LOAN_REPAYMENT_70 = "MUTH00000NATB0";
    public static String BLR_ID_LOAN_REPAYMENT_71 = "MUTH00000NATYF";
    public static String BLR_ID_LOAN_REPAYMENT_72 = "NIDH00000NAT9K";
    public static String BLR_ID_LOAN_REPAYMENT_73 = "NMFI00000NATGX";
    public static String BLR_ID_LOAN_REPAYMENT_74 = "OHMY00000NATFH";
    public static String BLR_ID_LOAN_REPAYMENT_75 = "OMLP00000NAT0A";
    public static String BLR_ID_LOAN_REPAYMENT_76 = "ORAN00000NATJX";
    public static String BLR_ID_LOAN_REPAYMENT_77 = "OROB00000NAT7P";
    public static String BLR_ID_LOAN_REPAYMENT_78 = "POOJ00000NATWO";
    public static String BLR_ID_LOAN_REPAYMENT_79 = "SHRI00000NAT23";

    public static String BLR_ID_LOAN_REPAYMENT_80 = "STAS00000NATX1";
    public static String BLR_ID_LOAN_REPAYMENT_81 = "UJJI00000NATAW";
    public static String BLR_ID_LOAN_REPAYMENT_82 = "VIST00000NAT6D";


    //BILLERID -HEALTH_INSURANCE(14)
    public static String BLR_ID_HEALTH_INSURANCE_1 = "RELI00000NATQ9";

    //Insurance(16)
    public static String BLR_ID_INSURANCE_1 = "AEGO00000NATRJ";
    public static String BLR_ID_INSURANCE_2 = "AVIV00000NAT5I";
    public static String BLR_ID_INSURANCE_3 = "BAJA00000NAT58";
    public static String BLR_ID_INSURANCE_4 = "CANA00000NATPI";
    public static String BLR_ID_INSURANCE_5 = "MAGM00000NAT61";
    public static String BLR_ID_INSURANCE_6 = "MAGM00000NAT6B";
    public static String BLR_ID_INSURANCE_7 = "MAGM00000NATQI";
    public static String BLR_ID_INSURANCE_8 = "MAXB00000NAT28";
    public static String BLR_ID_INSURANCE_9 = "ROYA00000NAT2C";
    public static String BLR_ID_INSURANCE_10 = "SBIL00000NATT0";


    //Municipal Tax(17)
    public static String BLR_ID_MUNCIPAL_TAX_1 = "HUBL00000KAR9T";
    public static String BLR_ID_MUNCIPAL_TAX_2 = "PRAY00000ALLXR";


    //Hospital(18)
    public static String BLR_ID_HOSPITAL_1 = "BKAR00000NATNS";


    //Municipal Service(19)
    public static String BLR_ID_MUNCIPAL_SERVICE_1 = "PORT00000ANIWE";


    //Housing Society(21)
    public static String BLR_ID_HOUSING_SOCIETY_1 = "AMRU00000PUNIU";
    public static String BLR_ID_HOUSING_SOCIETY_2 = "EBON00000UTP1N";
    public static String BLR_ID_HOUSING_SOCIETY_3 = "FAIR00000MAHKJ";
    public static String BLR_ID_HOUSING_SOCIETY_4 = "GANE00000PUNJR";
    public static String BLR_ID_HOUSING_SOCIETY_5 = "INDR00000SURMW";
    public static String BLR_ID_HOUSING_SOCIETY_6 = "JANA00000HYDD6";
    public static String BLR_ID_HOUSING_SOCIETY_7 = "PARI00000GUJJL";
    public static String BLR_ID_HOUSING_SOCIETY_8 = "PARK00000MAHPU";
    public static String BLR_ID_HOUSING_SOCIETY_9 = "PARK00000THACF";
    public static String BLR_ID_HOUSING_SOCIETY_10 = "PARK00000THAMY";
    public static String BLR_ID_HOUSING_SOCIETY_11 = "SHRE00000MAHS4";
    public static String BLR_ID_HOUSING_SOCIETY_12 = "SHRE00000RGM9F";
    public static String BLR_ID_HOUSING_SOCIETY_13 = "SOME00000RGM7X";
    public static String BLR_ID_HOUSING_SOCIETY_14 = "SUSH00000PUNML";

    //Subscription(22)
    public static String BLR_ID_SUBSCRIPTION_1 = "FURL00000NATF6";
    public static String BLR_ID_SUBSCRIPTION_2 = "HTDI00000NATC5";
}
