package com.isuisudmt.bbps.utils;

public class Operator_model {
    public String code;
    public String name;

    public Operator_model(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public Operator_model() {
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return  name;
    }
}
