package com.isuisudmt.bbps.BBPSmvp;

public class BbpsResponseFetchBill {
    BbpsModelFetchBill bbpsModelFetchBill;

    public BbpsModelFetchBill getBbpsModelFetchBill() {
        return bbpsModelFetchBill;
    }

    public void setBbpsModelFetchBill(BbpsModelFetchBill bbpsModelFetchBill) {
        this.bbpsModelFetchBill = bbpsModelFetchBill;
    }
}
