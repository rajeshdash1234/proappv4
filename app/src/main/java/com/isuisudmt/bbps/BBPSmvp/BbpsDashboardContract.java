package com.isuisudmt.bbps.BBPSmvp;

import android.content.Intent;

import org.json.JSONObject;

public class BbpsDashboardContract {

    public interface View {
        void onReadyIsUpdated(JSONObject response, Intent intent, String CatID);

        void onErrorIsUpdated(String ErrorMsg);

        void onReadyViewRequiredInfo(JSONObject response, Intent intent, String CatID);

        void onErrorViewRequiredInfo(String ErrorMsg);

        void hideLoader();

        void showLoader();
    }

    interface UserActionsListener {
        void apiIsUpdated(Intent intent, String CategoryID, String tokenStr);

        void apiViewRequiredInfo(Intent intent, String UserName, String CategotyID, String tokenStr);
    }

}

