package com.isuisudmt.bbps.BBPSmvp;
import org.json.JSONObject;

public class BBPSContract {

    public interface View {
        void BbpsFetchBillReady(BbpsModelFetchBill bbpsModel);
        void BbpsGetCcfReady(String response, Double totalAmount);
        void BbpsPayBillReady(String response);
        void OnErrorBbps(String ErrorMsg);
        void OnErrorBbpsPayBill(String errorMSG, String response);
        void showLoader();
        void showLoaderGetCCF();
        void hideLoader();
        void hideLoaderGetCCF();
    }

    interface UserActionsListener {
        void apiFetchBill(final String blrID, JSONObject fieldValuesJsonCommonFetchBill, String tokenStr);
        void apiLoadGetCcf(final String paymentType, String amount, String blrID, String tokenStr);
        void apiPayBill(String billAmount, String blrID, String field26, String field1, String field27, String field28, String field29, String field2,
                        String BillAmount, String LatePaymentFees, String EarlyPaymentFees, String Reference_id, String remarks, String QuickPay, String ccf,
                        String total_amount, String CatName, String BlrName, String pin_code, String pos_mobile_number, String lat_long, String terminal_id, String tokenStr);
    }

}

