package com.isuisudmt.bbps.BBPSmvp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BbpsModelFetchBill {
    @SerializedName("Customer_Name")
    @Expose
    private String Customer_Name;
    @SerializedName("Bill_Date")
    @Expose
    private String Bill_Date;
    @SerializedName("Bill_Amount")
    @Expose
    private String Bill_Amount;
    @SerializedName("Bill_Period")
    @Expose
    private String Bill_Period;
    @SerializedName("Bill_Number")
    @Expose
    private String Bill_Number;
    @SerializedName("Due_Date")
    @Expose
    private String Due_Date;
    @SerializedName("Fixed_Charges")
    @Expose
    private String Fixed_Charges;
    @SerializedName("Additional_Charges")
    @Expose
    private String Additional_Charges;
    @SerializedName("Early_Payment_Fee")
    @Expose
    private String Early_Payment_Fee;
    @SerializedName("Late_Payment_Fee")
    @Expose
    private String Late_Payment_Fee;
    @SerializedName("Reference_id")
    @Expose
    private String Reference_id;
    @SerializedName("Agnt_Reference_Id")
    @Expose
    private String Agnt_Reference_Id;

    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String customer_Name) {
        Customer_Name = customer_Name;
    }

    public String getBill_Date() {
        return Bill_Date;
    }

    public void setBill_Date(String bill_Date) {
        Bill_Date = bill_Date;
    }

    public String getBill_Amount() {
        return Bill_Amount;
    }

    public void setBill_Amount(String bill_Amount) {
        Bill_Amount = bill_Amount;
    }

    public String getBill_Period() {
        return Bill_Period;
    }

    public void setBill_Period(String bill_Period) {
        Bill_Period = bill_Period;
    }

    public String getBill_Number() {
        return Bill_Number;
    }

    public void setBill_Number(String bill_Number) {
        Bill_Number = bill_Number;
    }

    public String getDue_Date() {
        return Due_Date;
    }

    public void setDue_Date(String due_Date) {
        Due_Date = due_Date;
    }

    public String getFixed_Charges() {
        return Fixed_Charges;
    }

    public void setFixed_Charges(String fixed_Charges) {
        Fixed_Charges = fixed_Charges;
    }

    public String getAdditional_Charges() {
        return Additional_Charges;
    }

    public void setAdditional_Charges(String additional_Charges) {
        Additional_Charges = additional_Charges;
    }

    public String getEarly_Payment_Fee() {
        return Early_Payment_Fee;
    }

    public void setEarly_Payment_Fee(String early_Payment_Fee) {
        Early_Payment_Fee = early_Payment_Fee;
    }

    public String getLate_Payment_Fee() {
        return Late_Payment_Fee;
    }

    public void setLate_Payment_Fee(String late_Payment_Fee) {
        Late_Payment_Fee = late_Payment_Fee;
    }

    public String getReference_id() {
        return Reference_id;
    }

    public void setReference_id(String reference_id) {
        Reference_id = reference_id;
    }

    public String getAgnt_Reference_Id() {
        return Agnt_Reference_Id;
    }

    public void setAgnt_Reference_Id(String agnt_Reference_Id) {
        Agnt_Reference_Id = agnt_Reference_Id;
    }
}
