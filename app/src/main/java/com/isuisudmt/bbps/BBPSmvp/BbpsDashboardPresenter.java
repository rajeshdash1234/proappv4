package com.isuisudmt.bbps.BBPSmvp;

import android.content.Intent;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

import static com.isuisudmt.bbps.utils.Const.URL_IS_UPDATED;
import static com.isuisudmt.bbps.utils.Const.URL_VIEW_REQUIRED_INFO;


public class BbpsDashboardPresenter implements BbpsDashboardContract.UserActionsListener {

    private BbpsDashboardContract.View bbpsView;

    public BbpsDashboardPresenter(BbpsDashboardContract.View reportView) {
        this.bbpsView = reportView;
    }

    @Override
    public void apiIsUpdated(Intent intent, String CategoryID, String tokenStr) {
        bbpsView.showLoader();
        AndroidNetworking.post(URL_IS_UPDATED)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            bbpsView.hideLoader();
                            bbpsView.onReadyIsUpdated(response, intent, CategoryID);
                        } catch (Exception e) {
                            bbpsView.hideLoader();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            bbpsView.hideLoader();
                            bbpsView.onErrorIsUpdated(anError.getErrorBody());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    @Override
    public void apiViewRequiredInfo(Intent intent, String UserName, String CategotyID, String tokenStr) {
        bbpsView.showLoader();

        JSONObject obj = new JSONObject();
        try {
            obj.put("username", UserName);

            AndroidNetworking.post(URL_VIEW_REQUIRED_INFO)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            bbpsView.hideLoader();
                            bbpsView.onReadyViewRequiredInfo(response, intent, CategotyID);
                        }

                        @Override
                        public void onError(ANError anError) {
                            try {
                                bbpsView.hideLoader();
                                bbpsView.onErrorViewRequiredInfo(anError.getErrorBody());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
