package com.isuisudmt.bbps.BBPSmvp;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.bbps.utils.Const;
import com.isuisudmt.bbps.utils.GetBillerParameters;

import org.json.JSONException;
import org.json.JSONObject;


public class BbpsPresenter implements BBPSContract.UserActionsListener {

    private BBPSContract.View bbpsView;

    public BbpsPresenter(BBPSContract.View reportView) {
        this.bbpsView = reportView;
    }

    @Override
    public void apiFetchBill(final String blrID, JSONObject fieldValuesJsonCommonFetchBill, String tokenStr) {

        bbpsView.showLoader();
        JSONObject obj = new JSONObject();
        try {
            obj.put("payChannel", Const.PAY_CHANNEL);
            obj.put("payMode", Const.PAY_MODE);
            obj.put("blrId", blrID);
            obj.put("indusUserName", Const.usernameForBBPS);
            obj.put("fieldValues", fieldValuesJsonCommonFetchBill);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Const.URL_FETCH_BILL)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String statusCode = obj.getString("statusCode");
                            String statusDescription = obj.getString("statusDescription");

                            if (statusCode.equals("0")) {
                                BbpsResponseFetchBill bbpsResponseFetchBill = new BbpsResponseFetchBill();
                                BbpsModelFetchBill bbpsModelFetchBill = new BbpsModelFetchBill();

                                JSONObject objCommonClass = new JSONObject(obj.getString("commonClassObject"));

                                bbpsModelFetchBill.setCustomer_Name(objCommonClass.optString("Customer_Name"));
                                bbpsModelFetchBill.setBill_Date(objCommonClass.optString("Bill_Date"));
                                bbpsModelFetchBill.setBill_Amount(objCommonClass.optString("Bill_Amount"));
                                bbpsModelFetchBill.setBill_Period(objCommonClass.optString("Bill_Period"));
                                bbpsModelFetchBill.setBill_Number(objCommonClass.optString("Bill_Number"));
                                bbpsModelFetchBill.setDue_Date(objCommonClass.optString("Due_Date"));
                                bbpsModelFetchBill.setFixed_Charges(objCommonClass.optString("Fixed_Charges"));
                                bbpsModelFetchBill.setAdditional_Charges(objCommonClass.optString("Additional_Charges"));
                                bbpsModelFetchBill.setEarly_Payment_Fee(objCommonClass.optString("Early_Payment_Fee"));
                                bbpsModelFetchBill.setLate_Payment_Fee(objCommonClass.optString("Late_Payment_Fee"));
                                bbpsModelFetchBill.setReference_id(objCommonClass.optString("Reference_id"));
                                bbpsModelFetchBill.setAgnt_Reference_Id(objCommonClass.optString("Agnt_Reference_Id"));

                                bbpsResponseFetchBill.setBbpsModelFetchBill(bbpsModelFetchBill);

                                bbpsView.BbpsFetchBillReady(bbpsModelFetchBill);
                                bbpsView.hideLoader();

                            } else {
                                bbpsView.OnErrorBbps(GetBillerParameters.popupError(obj));
                                bbpsView.hideLoader();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            bbpsView.hideLoader();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorDetail();
                        bbpsView.hideLoader();
                        try {
                            bbpsView.OnErrorBbps(GetBillerParameters.popupError(new JSONObject(anError.getErrorBody().toString())));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    @Override
    public void apiLoadGetCcf(String paymentType, String amount, String blrID, String tokenStr) {
        bbpsView.showLoaderGetCCF();
        JSONObject obj = new JSONObject();
        try {
            obj.put("billAmt", amount);
            obj.put("blrId", blrID);
            obj.put("indusUserName", Const.usernameForBBPS);
            obj.put("payChannel", Const.PAY_CHANNEL);
            obj.put("payMode", paymentType);

            AndroidNetworking.post(Const.URL_GET_CCF)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            bbpsView.hideLoaderGetCCF();

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("statusCode");
                                String statusDescription = obj.getString("statusDescription");

                                if (statusCode.equals("0")) {
                                    String commonClassObject = obj.getString("commonClassObject");
                                    JSONObject objCommonClass = new JSONObject(commonClassObject);
                                    bbpsView.BbpsGetCcfReady(objCommonClass.getString("ccf"), Double.parseDouble(amount));
                                } else {
                                    bbpsView.OnErrorBbps(GetBillerParameters.popupError(obj));
                                }

                            } catch (JSONException e) {
                                bbpsView.hideLoaderGetCCF();
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            bbpsView.hideLoaderGetCCF();

                            try {
                                bbpsView.OnErrorBbps(GetBillerParameters.popupError(new JSONObject(anError.getErrorBody().toString())));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void apiPayBill(String billAmount, String blrID, String field26, String field1, String field27, String field28, String field29, String field2,
                           String BillAmount, String LatePaymentFees, String EarlyPaymentFees, String Reference_id, String remarks, String QuickPay, String ccf,
                           String total_amount, String CatName, String BlrName, String pin_code, String pos_mobile_number, String lat_long, String terminal_id,
                           String tokenStr) {
        bbpsView.showLoader();

        JSONObject obj = new JSONObject();
        try {
            obj.put("indusUserName", Const.usernameForBBPS);
            obj.put("payChannel", Const.PAY_CHANNEL);
            obj.put("payMode", Const.PAY_MODE);
            obj.put("blrId", blrID);
            obj.put("refId", Reference_id);
            obj.put("debitNar1", remarks);
            obj.put("quickPay", QuickPay);//0:Fetch/1:Pay
            obj.put("ccf", ccf);
            obj.put("billAmt", billAmount);
            obj.put("totalAmt", total_amount);
            obj.put("catName", CatName);
            obj.put("mobileNumber", field1);//Payee Mobile Number
            obj.put("blrName", BlrName);
            obj.put("fieldValues",
                    GetBillerParameters.getFieldValuesJsonCommonPayBill(
                            pin_code, pos_mobile_number, lat_long, terminal_id, field1, field26, field27, field28, field29, field2,
                            BillAmount, LatePaymentFees, EarlyPaymentFees));

        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Const.URL_PAY_BILL)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        bbpsView.hideLoader();

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String statusCode = obj.getString("statusCode");

                            if (statusCode.equals("0")) {
                                bbpsView.BbpsPayBillReady(response.toString());
                            } else {
                                bbpsView.OnErrorBbpsPayBill(GetBillerParameters.getPayBillError(new JSONObject(response.toString())), response.toString());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        bbpsView.hideLoader();

                        try {
                            bbpsView.OnErrorBbpsPayBill(GetBillerParameters.getPayBillError(new JSONObject(anError.getErrorBody().toString())),
                                    anError.getErrorBody().toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });


    }

}
