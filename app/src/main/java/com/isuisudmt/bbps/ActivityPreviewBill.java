package com.isuisudmt.bbps;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.isuisudmt.R;

public class ActivityPreviewBill extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_bill);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Sample Bill");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        imageView = findViewById(R.id.iv_sample_bill);

        String SampleBillURL = getIntent().getStringExtra("SampleBillURL");

        Glide.with(this)
                .load(SampleBillURL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

    }
}