package com.isuisudmt.bbps;

import android.content.Intent;

public class BBPSDashboardModel {
    private String textId;
    private int imgId;
    public Intent intent;
    private String type;
    public String CategoryID;

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }

    public BBPSDashboardModel(String textId, int imgId) {
        this.textId = textId;
        this.imgId = imgId;
    }

    public BBPSDashboardModel(String textId, int imgId, Intent intent, String type, String CategoryID) {
        this.textId = textId;
        this.imgId = imgId;
        this.intent = intent;
        this.type = type;
        this.CategoryID = CategoryID;
    }

    public String getDescription() {
        return textId;
    }
    public void setDescription(String textId) {
        this.textId = textId;
    }
    public int getImgId() {
        return imgId;
    }
    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public String getTextId() {
        return textId;
    }

    public void setTextId(String textId) {
        this.textId = textId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
