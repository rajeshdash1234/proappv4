package com.isuisudmt.bbps;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.isuisudmt.CustomThemes;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.isuisudmt.bbps.SavePDF.PermissionsActivity;
import com.isuisudmt.bbps.SavePDF.PermissionsChecker;
import com.isuisudmt.bbps.SavePDF.PreviewPDFActivity;
import com.isuisudmt.bbps.utils.FileUtils;
import com.isuisudmt.settings.BluetoothConnectorActivity;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.dmtModule.bluetooth.BluetoothPrinter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import static com.isuisudmt.bbps.SavePDF.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.isuisudmt.bbps.SavePDF.PermissionsChecker.REQUIRED_PERMISSION;

public class ActivityReceiptBbps extends AppCompatActivity {

    String Response = "";
    String status = "", msg = "", txnRefId = "", refId = "", customerName = "", billNumber = "", billPeriod = "", billDate = "", dueDate = "", baseBillAmount = "", agntRefId = "", appRefNumber = "";

    ImageView iv_status_icon;
    TextView tv_status_msg, tv_status_desc, tv_trans_id, tv_agnt_ref_id, tv_ref_id, tv_customer_name, tv_bill_number, tv_bill_period, tv_bill_date, tv_due_date, tv_base_bill_amount, tv_app_ref_no, balanceText;
    LinearLayout ll_main_bbps_receipt, ll_trans_id, ll_agnt_ref_id, ll_ref_id, ll_customer_name, ll_bill_number, ll_bill_period, ll_bill_date, ll_due_date, ll_base_bill_amount, ll_app_ref_no;
    Button btnOK, btnSavePDF, btnPrint, btnDetails;

    BluetoothAdapter B;
    PermissionsChecker checker;
    Context mContext;
    private static final int REQUEST_CA_PERMISSIONS = 931;
    private static final int REQUEST_WRITE_PERMISSION = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_receipt_bbps);

        init();

        Intent intent = getIntent();
        Response = intent.getStringExtra("Response");

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityReceiptBbps.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SdkConstants.BRAND_NAME.trim().length() != 0) {
                    BluetoothDevice bluetoothDevice = SdkConstants.bluetoothDevice;
                    if (bluetoothDevice != null) {

                        if (!B.isEnabled()) {
                           /* Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(turnOn, 0);*/
                            finish();
                            Toast.makeText(getApplicationContext(), "Your Bluetooth is OFF .", Toast.LENGTH_LONG).show();
                        } else {
                            if (billDate.equals("") || billDate.equals(null))
                                callBluetoothFunction(txnRefId, refId, customerName, billNumber, billPeriod, billDate, dueDate, baseBillAmount, agntRefId, appRefNumber, bluetoothDevice);
                            else
                                callBluetoothFunction(txnRefId, refId, customerName, billNumber, billPeriod, Util.getDateTime(billDate), dueDate, baseBillAmount, agntRefId, appRefNumber, bluetoothDevice);
                        }
                    } else {
                        Intent in = new Intent(ActivityReceiptBbps.this, BluetoothConnectorActivity.class);
                        startActivity(in);
//                        finish();
//                        Toast.makeText(ActivityReceiptBbps.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showBrandSetAlert();
                }
            }
        });


        btnSavePDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                }

                if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                    PermissionsActivity.startActivityForResult(ActivityReceiptBbps.this, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                } else {
                    Date date = new Date();
                    long timeMilli = date.getTime();
                    System.out.println("Time in milliseconds using Date class: " + String.valueOf(timeMilli));
                    createPdf(FileUtils.getAppPath(mContext) + String.valueOf(timeMilli) + "Order_Receipt.pdf");
                }

            }
        });

        btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTransactionDetails(ActivityReceiptBbps.this);
            }
        });

        //Demo Failure
        //Response = "{\"commonClassObject\":{\"refId\":\"81C002A27077Y4A6EYBBFAYC58106EA869E\",\"agentRefid\":\"ZK9ZVNYRM6Z4W9XQC\",\"msg\":\"Unable to get bill details from biller\",\"code\":\"INRX001\"},\"statusCode\":\"-1\",\"statusDescription\":\"Unable to get bill details from biller\"}";

        //Demo Success

        /*Response = "{\n" +
                "    \"commonClassObject\": {\n" +
                "        \"msg\": \"Successful\",\n" +
                "        \"txnRefId\": \"IN0100605213\",\n" +
                "        \"code\": \"SUCX001\",\n" +
                "        \"refId\": \"2FD72E80BCC1Y4D8AYAD6EY56B3B28C7638\",\n" +
                "        \"agentRefid\": \"783119144386576\",\n" +
                "        \"CustomerName\": \"NA\",\n" +
                "        \"BillNumber\": \"NA\",\n" +
                "        \"BillPeriod\": \"NA\",\n" +
                "        \"BillDate\": \"2020-11-05\",\n" +
                "        \"DueDate\": \"2020-11-05\",\n" +
                "        \"AppRefNumber\": \"AB123456\",\n" +
                "        \"customerMobile\": \"8249636296\",\n" +
                "        \"ccf\": \"0\",\n" +
                "        \"dateAndTime\": 1604562005244,\n" +
                "        \"BASE_BILL_AMOUNT\": \"5000\",\n" +
                "        \"totalAmount\": 50,\n" +
                "        \"baseBillAmount\": \"50\"\n" +
                "    },\n" +
                "    \"statusCode\": \"0\",\n" +
                "    \"statusDescription\": \"Bill payment success.\"\n" +
                "}";
*/
        try {
            JSONObject obj = new JSONObject(Response);
            String statusCode = obj.getString("statusCode");
            String statusDescription = obj.getString("statusDescription");

            if (statusCode.equals("0")) {
                status = "Success";
                iv_status_icon.setImageDrawable(getResources().getDrawable(R.drawable.hero_success));

                JSONObject objCommonClass = new JSONObject(obj.getString("commonClassObject"));
                //Display the receipt
                msg = objCommonClass.optString("msg");

                txnRefId = objCommonClass.optString("txnRefId");
                refId = objCommonClass.optString("refId");
                customerName = objCommonClass.optString("CustomerName");
                billNumber = objCommonClass.optString("BillNumber");
                billPeriod = objCommonClass.optString("BillPeriod");
                billDate = objCommonClass.optString("dateAndTime");
                dueDate = objCommonClass.optString("DueDate");
                baseBillAmount = objCommonClass.optString("baseBillAmount");
                agntRefId = objCommonClass.optString("agntRefId");//This is the transaction ID
                appRefNumber = objCommonClass.optString("AppRefNumber");
                String code = objCommonClass.optString("code");
                String customerMobile = objCommonClass.optString("customerMobile");
                String totalAmount = objCommonClass.optString("totalAmount");
                String ccf = objCommonClass.optString("ccf");

                //billerName (Not used in App)
                //clientUniqueId (Not used in App)
                //code (Not used in App)
                //billDate = objCommonClass.optString("BillDate");(Not there in API)
                //String BASE_BILL_AMOUNT = objCommonClass.optString("BASE_BILL_AMOUNT");(Not there in API)

                checkPaySuccessResponse(baseBillAmount, tv_base_bill_amount, "Rs : ");
                checkPaySuccessResponse(agntRefId, tv_trans_id, "TxnId : ");
                checkPaySuccessResponse(refId, tv_ref_id, "RefId : ");


                if (billDate.equals("") || billDate.equals(null))
                    tv_bill_date.setText("Date: N/A");
                else
                    tv_bill_date.setText("Date: " + Util.getDateTime(billDate));


            } else if (statusCode.equals("-1")) {
                status = "Failed";

                iv_status_icon.setImageDrawable(getResources().getDrawable(R.drawable.hero_failure));
                btnDetails.setVisibility(View.GONE);
                balanceText.setVisibility(View.GONE);
                tv_base_bill_amount.setVisibility(View.GONE);
                tv_bill_date.setVisibility(View.GONE);
                tv_ref_id.setVisibility(View.GONE);
                tv_trans_id.setVisibility(View.GONE);


                JSONObject objCommonClass = new JSONObject(obj.getString("commonClassObject"));
                //Display the receipt
                msg = objCommonClass.optString("msg");
                txnRefId = objCommonClass.optString("txnRefId");
                refId = objCommonClass.optString("refId");
                customerName = objCommonClass.optString("CustomerName");
                billNumber = objCommonClass.optString("BillNumber");
                billPeriod = objCommonClass.optString("BillPeriod");
                dueDate = objCommonClass.optString("DueDate");
                baseBillAmount = objCommonClass.optString("baseBillAmount");
                agntRefId = objCommonClass.optString("agntRefId");
                appRefNumber = objCommonClass.optString("AppRefNumber");
                String customerMobile = objCommonClass.optString("customerMobile");
                billDate = objCommonClass.optString("dateAndTime");
                String totalAmount = objCommonClass.optString("totalAmount");
                String ccf = objCommonClass.optString("ccf");

                //billerName (Not used in App)
                //clientUniqueId (Not used in App)
                //code (Not used in App)
                //billDate = objCommonClass.optString("BillDate");
                //String BASE_BILL_AMOUNT = objCommonClass.optString("BASE_BILL_AMOUNT");

                tv_status_desc.setVisibility(View.VISIBLE);
                tv_status_desc.setText(statusDescription);

                checkPayFailResponse(txnRefId, tv_trans_id, "Tnx ID: ");
                checkPayFailResponse(baseBillAmount, tv_base_bill_amount, "Rs: ");
                checkPayFailResponse(refId, tv_ref_id, "RefId : ");

            }

        } catch (
                Exception e) {
            e.printStackTrace();
        }


    }


    private void init() {

        B = BluetoothAdapter.getDefaultAdapter();
        //Runtime permission request required if Android permission >= Marshmallow
        checker = new PermissionsChecker(this);
        mContext = getApplicationContext();

        iv_status_icon = findViewById(R.id.status_icon);
        btnOK = findViewById(R.id.closeBtn);
        btnSavePDF = findViewById(R.id.btnSavePdf);
        btnPrint = findViewById(R.id.btnPrint);
        btnDetails = findViewById(R.id.txndetailsBtn);
        tv_status_desc = findViewById(R.id.tv_status_desc);
        tv_trans_id = findViewById(R.id.tv_trans_id);
        tv_ref_id = findViewById(R.id.tv_ref_id);
        tv_bill_date = findViewById(R.id.tv_bill_date);
        balanceText = findViewById(R.id.balanceText);
        tv_base_bill_amount = findViewById(R.id.tv_base_bill_amount);

    }

    public void showTransactionDetails(Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.transaction_bbps_details_layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView agent_ref_id = (TextView) dialog.findViewById(R.id.tv_agnt_ref_id);
            TextView tv_customer_name = (TextView) dialog.findViewById(R.id.tv_customer_name);
            TextView tv_bill_number = (TextView) dialog.findViewById(R.id.tv_bill_number);
            TextView tv_bill_period = (TextView) dialog.findViewById(R.id.tv_bill_period);
            TextView tv_bill_date = (TextView) dialog.findViewById(R.id.tv_bill_date);

            if (agntRefId.equals("") || agntRefId.equals(null))
                agent_ref_id.setText("NA");
            else
                agent_ref_id.setText(agntRefId);

            if (customerName.equals("") || customerName.equals(null))
                tv_customer_name.setText("NA");
            else
                tv_customer_name.setText(customerName);

            if (billNumber.equals("") || billNumber.equals(null))
                tv_bill_number.setText("NA");
            else
                tv_bill_number.setText(billNumber);

            if (billPeriod.equals("") || billPeriod.equals(null))
                tv_bill_period.setText("NA");
            else
                tv_bill_period.setText(billPeriod);

            if (dueDate.equals("") || dueDate.equals(null))
                tv_bill_date.setText("NA");
            else
                tv_bill_date.setText(dueDate);

            Button dialogBtn_close = (Button) dialog.findViewById(R.id.close_Btn);
            dialogBtn_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();

                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void checkPaySuccessResponse(String response, TextView textView, String prefix) {

        if (response.equals("") || response.equals(null) || response.equals("null"))
            textView.setText(prefix + " N/A");
        else
            textView.setText(prefix + response);

    }

    private void checkPayFailResponse(String response, TextView textView, String prefix) {
        if (response.equals("") || response.equals(null) || response.equals("null"))
            textView.setVisibility(View.GONE);
        else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(prefix + response);
        }
    }

    private void callBluetoothFunction(final String txnId, final String refId,
                                       final String customerName, final String billNumber, final String billPeriod,
                                       final String billDate, final String dueDate, final String baseBillAmount,
                                       final String agntRefId, final String appRefNumber, BluetoothDevice bluetoothDevice) {

        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {
            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(SdkConstants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.printText("-----Transaction Report-----");
                mPrinter.addNewLine();
                mPrinter.addNewLine();


                if (!status.equals("")) {
                    mPrinter.printText("Status: " + status);
                    mPrinter.addNewLine();
                }
                if (!txnId.equals("")) {
                    mPrinter.printText("TXNId: " + txnId);
                    mPrinter.addNewLine();
                }
                if (!refId.equals("")) {
                    mPrinter.printText("Ref ID: " + refId);
                    mPrinter.addNewLine();
                }
                if (!customerName.equals("")) {
                    mPrinter.printText("Customer Name: " + customerName);
                    mPrinter.addNewLine();
                }
                if (!billNumber.equals("")) {
                    mPrinter.printText("Bill Number: " + billNumber);
                    mPrinter.addNewLine();
                }
                if (!billPeriod.equals("")) {
                    mPrinter.printText("Bill Period: " + billPeriod);
                    mPrinter.addNewLine();
                }
                if (!billDate.equals("")) {
                    mPrinter.printText("Bill Date: " + billDate);
                    mPrinter.addNewLine();
                }
                if (!dueDate.equals("")) {
                    mPrinter.printText("Due Date: " + dueDate);
                    mPrinter.addNewLine();
                }
                if (!baseBillAmount.equals("")) {
                    mPrinter.printText("Bill Amount: " + baseBillAmount);
                    mPrinter.addNewLine();
                }
                if (!agntRefId.equals("")) {
                    mPrinter.printText("Agent Ref Id: " + agntRefId);
                    mPrinter.addNewLine();
                }
                if (!appRefNumber.equals("")) {
                    mPrinter.printText("App Ref Number: " + appRefNumber);
                    mPrinter.addNewLine();
                }
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(SdkConstants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");
//                finish();
                Toast.makeText(ActivityReceiptBbps.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showBrandSetAlert() {
        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(ActivityReceiptBbps.this);
            builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
            builder1.setTitle("Warning!!!");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "GOT IT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } catch (Exception e) {
        }
    }

    public void createPdf(String dest) {
        if (new File(dest).exists()) {
            new File(dest).delete();
        }
        try {
            /**
             * Creating Document
             */
            Document document = new Document();
            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(dest));
            // Open to write
            document.open();
            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("");
            document.addCreator("");
            Rectangle rect = new Rectangle(577, 825, 18, 15);
            rect.enableBorderSide(1);
            rect.enableBorderSide(2);
            rect.enableBorderSide(4);
            rect.enableBorderSide(8);
            rect.setBorder(Rectangle.BOX);
            rect.setBorderWidth(2);
            rect.setBorderColor(BaseColor.BLACK);
            document.add(rect);
            BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
            float mHeadingFontSize = 22.0f;
            float mValueFontSize = 24.0f;
            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));
            BaseFont bf = BaseFont.createFont(
                    BaseFont.TIMES_ROMAN,
                    BaseFont.CP1252,
                    BaseFont.EMBEDDED);
            Font font = new Font(bf, 30);
            Font font2 = new Font(bf, 26);

            Font mOrderDetailsTitleFont = new Font(urName, 36.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderDetailsTitleChunk = new Chunk(SdkConstants.SHOP_NAME, mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);
            document.add(new Paragraph("\n\n"));


            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph p2 = new Paragraph("");
            Image i = Image.getInstance(getFileName(R.drawable.indusind_receipt, 104, 50, "indusind_receipt.png"));
            glue = new Chunk(i,0,0);
            p2.add(new Chunk(glue));
            Image i2 = Image.getInstance(getFileName(R.drawable.bbps_assured, 50, 50, "bbps_assured.png"));
            glue = new Chunk(i2,370,0);
            p2.add(new Chunk(glue));
            document.add(p2);


            Font mOrderShopTitleFont = new Font(urName, 25.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderShopTitleChunk = new Chunk("Receipt", mOrderShopTitleFont);
            Paragraph mOrderShopTitleParagraph = new Paragraph(mOrderShopTitleChunk);
            mOrderShopTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderShopTitleParagraph);
            document.add(new Paragraph("\n\n"));



            Font mOrderDateFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Font mOrderDateValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);

            Paragraph p = new Paragraph();
            p.add(new Chunk("Date/Time : ", mOrderDateFont));
            if (billDate.equals("") || billDate.equals(null) || billDate.equals("null"))
                p.add(new Chunk("N/A", mOrderDateValueFont));
            else
                p.add(new Chunk(Util.getDateTime(billDate), mOrderDateValueFont));

            document.add(p);
            document.add(new Paragraph("\n"));
            Paragraph p1 = new Paragraph();
            p1.add(new Chunk("Operation Performed : ", mOrderDateFont));
            p1.add(new Chunk("BBPS", mOrderDateValueFont));
            document.add(p1);

            document.add(new Paragraph("\n"));


            Font mOrderDetailsFont = new Font(urName, 30.0f, Font.BOLD, mColorAccent);
            Chunk mOrderDetailsChunk = new Chunk("Transaction Details", mOrderDetailsFont);
            Paragraph mOrderDetailsParagraph = new Paragraph(mOrderDetailsChunk);
            mOrderDetailsParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsParagraph);
            document.add(new Paragraph("\n"));
            // document.add(new Chunk(lineSeparator));

            Font mOrderIdFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);

            if (!status.equals("")) {
                Chunk mOrderTxn1Chunk = new Chunk("Status: " + status, mOrderIdFont);
                Paragraph mOrderTxn1Paragraph = new Paragraph(mOrderTxn1Chunk);
                document.add(mOrderTxn1Paragraph);
            } else {

            }
            if (!agntRefId.equals("")) {
                Chunk mOrderTxn1Chunk = new Chunk("Transaction ID: " + agntRefId, mOrderIdFont);
                Paragraph mOrderTxn1Paragraph = new Paragraph(mOrderTxn1Chunk);
                document.add(mOrderTxn1Paragraph);
            } else {
                Chunk mOrderTxn1Chunk = new Chunk("Transaction ID: N/A", mOrderIdFont);
                Paragraph mOrderTxn1Paragraph = new Paragraph(mOrderTxn1Chunk);
                document.add(mOrderTxn1Paragraph);
            }
            if (!refId.equals("")) {
                Chunk mOrderRefChunk = new Chunk("Ref ID: " + refId, mOrderIdFont);
                Paragraph mOrderRefParagraph = new Paragraph(mOrderRefChunk);
                document.add(mOrderRefParagraph);
            } else {
                Chunk mOrderRefChunk = new Chunk("Ref ID: N/A", mOrderIdFont);
                Paragraph mOrderRefParagraph = new Paragraph(mOrderRefChunk);
                document.add(mOrderRefParagraph);
            }
            if (!customerName.equals("")) {
                Chunk mOrderMobChunk = new Chunk("Customer Name: " + customerName, mOrderIdFont);
                Paragraph mOrderMobParagraph = new Paragraph(mOrderMobChunk);
                document.add(mOrderMobParagraph);
            } else {
                Chunk mOrderMobChunk = new Chunk("Customer Name: N/A ", mOrderIdFont);
                Paragraph mOrderMobParagraph = new Paragraph(mOrderMobChunk);
                document.add(mOrderMobParagraph);
            }

            if (!billNumber.equals("")) {
                Chunk mOrderIdChunk = new Chunk("Bill Number: " + billNumber, mOrderIdFont);
                Paragraph mOrderTxnParagraph = new Paragraph(mOrderIdChunk);
                document.add(mOrderTxnParagraph);
            } else {
                Chunk mOrderIdChunk = new Chunk("Bill Number: N/A", mOrderIdFont);
                Paragraph mOrderTxnParagraph = new Paragraph(mOrderIdChunk);
                document.add(mOrderTxnParagraph);
            }
            if (!billPeriod.equals("")) {
                Chunk mOrderIdValueChunk = new Chunk("Bill Period: " + billPeriod, mOrderIdFont);
                Paragraph mOrderaadharParagraph = new Paragraph(mOrderIdValueChunk);
                document.add(mOrderaadharParagraph);
            } else {
                Chunk mOrderIdValueChunk = new Chunk("Bill Period: N/A", mOrderIdFont);
                Paragraph mOrderaadharParagraph = new Paragraph(mOrderIdValueChunk);
                document.add(mOrderaadharParagraph);
            }
            if (!dueDate.equals("")) {
                Chunk mOrderrrnChunk = new Chunk("Due Date: " + dueDate, mOrderIdFont);
                Paragraph mOrderrnParagraph = new Paragraph(mOrderrrnChunk);
                document.add(mOrderrnParagraph);
            } else {
                Chunk mOrderrrnChunk = new Chunk("Due Date: N/A", mOrderIdFont);
                Paragraph mOrderrnParagraph = new Paragraph(mOrderrrnChunk);
                document.add(mOrderrnParagraph);
            }

            if (!baseBillAmount.equals("")) {
                Chunk mOrderbalanceChunk = new Chunk("Bill Amount: " + baseBillAmount, mOrderIdFont);
                Paragraph mOrderbalanceParagraph = new Paragraph(mOrderbalanceChunk);
                document.add(mOrderbalanceParagraph);
            } else {
                Chunk mOrderbalanceChunk = new Chunk("Bill Amount: N/A", mOrderIdFont);
                Paragraph mOrderbalanceParagraph = new Paragraph(mOrderbalanceChunk);
                document.add(mOrderbalanceParagraph);
            }

            if (!appRefNumber.equals("")) {
                Chunk mOrdertxnTypeChunk = new Chunk("App Ref Number: " + appRefNumber, mOrderIdFont);
                Paragraph mOrdertxnTypeParagraph = new Paragraph(mOrdertxnTypeChunk);
                document.add(mOrdertxnTypeParagraph);
            } else {
                Chunk mOrdertxnTypeChunk = new Chunk("App Ref Number: N/A", mOrderIdFont);
                Paragraph mOrdertxnTypeParagraph = new Paragraph(mOrdertxnTypeChunk);
                document.add(mOrdertxnTypeParagraph);
            }
            document.add(new Paragraph("\n"));

            Font mOrderAcNameFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderAcNameChunk = new Chunk("Thank You", mOrderAcNameFont);
            Paragraph mOrderAcNameParagraph = new Paragraph(mOrderAcNameChunk);
            mOrderAcNameParagraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(mOrderAcNameParagraph);
            Font mOrderAcNameValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderAcNameValueChunk = new Chunk(SdkConstants.BRAND_NAME, mOrderAcNameValueFont);
            Paragraph mOrderAcNameValueParagraph = new Paragraph(mOrderAcNameValueChunk);
            mOrderAcNameValueParagraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(mOrderAcNameValueParagraph);

            document.close();
            Toast.makeText(mContext, "PDF saved in the internal storage", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ActivityReceiptBbps.this, PreviewPDFActivity.class);
            intent.putExtra("filePath", dest);
            startActivity(intent);
//            showPdf(dest);
//            FileUtils.openFile(mContext, new File(dest));
        } catch (IOException | DocumentException ie) {
            Log.e("createPdf: Error ", "" + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == PermissionsActivity.PERMISSIONS_GRANTED) {
            Toast.makeText(mContext, "Permission Granted to Save", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Permission not granted, Try again!", Toast.LENGTH_SHORT).show();
        }
    }


    public String getFileName(int image, int dimensionX, int dimentionY, String fileName){
        File mFile1 = null;

        try{
            Bitmap bitMap = BitmapFactory.decodeResource(getResources(), image);
            Bitmap resized = Bitmap.createScaledBitmap(bitMap, dimensionX, dimentionY, true);

            mFile1 = Environment.getExternalStorageDirectory();
            File mFile2 = new File(mFile1, fileName);

            FileOutputStream outStream;
            outStream = new FileOutputStream(mFile2);
            resized.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        return mFile1.getAbsolutePath().toString() + "/" + fileName;
    }
}