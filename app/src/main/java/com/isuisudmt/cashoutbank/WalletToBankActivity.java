package com.isuisudmt.cashoutbank;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WalletToBankActivity extends AppCompatActivity {
    private static final String TAG = WalletToBankActivity.class.getSimpleName();

    Button addAccount;
    SessionManager session;
    String token,admin;
    TextView accNo,amontplacehoder;
    ProgressDialog pd;
    EditText addAmountEdit;
    Button transferAmount;

    String accNoStr="",ifscStr="",bankNameStr="",bankCodeStr="";
    ImageView back;
    TextView wal1_bal_edt;
    ImageView img_prof,imgInfo,accEdit;
    Handler timer_handler;
    String _userName;
    boolean session_logout = false;
    //private RadioGroup settlementType;
    Boolean autoSettled = false;
    //RadioButton manual_sattled,auto_sattled;
    LinearLayout layot_main;
    SwitchCompat swOnOff;

    static String editBank, editAccount, editIFSC, editCode;
    List<String> bankList;
    private FirebaseAnalytics firebaseAnalytics;
    Double balance_wallet = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_wallet_tobank);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        bankList = new ArrayList<>();


        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManager.KEY_TOKEN);
        admin = user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user_ = session.getUserSession();
        _userName = user_.get(session.userName);

        accNo = findViewById(R.id.acc_no);
        addAccount = findViewById(R.id.addAccount);
        addAmountEdit = findViewById(R.id.addAmountEdit);
        transferAmount = findViewById(R.id.transferAmount);
        wal1_bal_edt = findViewById(R.id.wal1_bal_edt);
        img_prof = findViewById(R.id.img_prof);
        imgInfo = findViewById(R.id.imgInfo);
        amontplacehoder = findViewById(R.id.amontplacehoder);
        layot_main = findViewById(R.id.layot_main);
        accEdit = findViewById(R.id.acc_edit);
        getBanks();

        editBank = "";
        editAccount = "";
        editIFSC = "";
        editCode = "";

        swOnOff = findViewById(R.id.swOnOff);
        swOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    updateSettlementStatus(true);
                    autoSettled =true;
                }else{
                    updateSettlementStatus(false);
                    autoSettled =false;
                }
            }
        });
        imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showAlert(WalletToBankActivity.this,"Alert","Sorry, Account No. is not active.");
            }
        });

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        accEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAPI(editBank, editAccount, editIFSC, editCode);
            }
        });


        pd = new ProgressDialog(WalletToBankActivity.this);
        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI("", "", "", "");
            }
        });

        transferAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String addamountStr = addAmountEdit.getText().toString();
                if(addAmountEdit.getText().toString().length()<=0){
                    Toast.makeText(WalletToBankActivity.this,"Please enter amount to transfer.",Toast.LENGTH_SHORT).show();

                }
                else if(balance_wallet<Double.parseDouble(addamountStr)){
                    Toast.makeText(WalletToBankActivity.this,"Insufficient wallet balance.",Toast.LENGTH_SHORT).show();


                }else{
                    callTransferAmount();
                }
            }
        });


        checkBankDetail();

    }

    private void checkSettlementStatus() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_name", _userName);
            AndroidNetworking.post("https://wallet2autocashout-vn3k2k7q7q-uc.a.run.app/fetch_autosettle_status") //fetch_autosettle_status
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                if(obj!=null){
                                    if(status.equalsIgnoreCase("0")) {
                                        Boolean auto_settled_status = obj.getBoolean("auto_settled_status");
                                        autoSettled = auto_settled_status;
                                        if(autoSettled){
                                            swOnOff.setChecked(true);
                                            layot_main.setVisibility(View.GONE);
                                        }else{
                                            swOnOff.setChecked(false);
                                            layot_main.setVisibility(View.VISIBLE);
                                        }
                                    }else{
                                        showAlertOnBack(WalletToBankActivity.this,"Alert","Something went to wrong. Please try again,");
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                showAlertOnBack(WalletToBankActivity.this,"Alert","Something went to wrong. Please try again,");

                                //Toast.makeText(WalletToBankActivity.this,"Exception.. Someting went to wrong.",Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(WalletToBankActivity.this,"Oh.. Something went to wrong.",Toast.LENGTH_SHORT).show();

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    public void callTransferAmount() {
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        pd.show();
        try {
            AndroidNetworking.get("https://apps.iserveu.tech/generate/v79")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                callEncriptedTransferAmount(encodedUrl,pd);

                            } catch (JSONException e) {
//                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
//                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            pd.dismiss();
                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }
    }

    private void updateSettlementStatus(Boolean status) {
        pd.setMessage("Loading......");
        pd.setCancelable(false);
        pd.show();
        JSONObject obj = new JSONObject();
        try {
            obj.put("user_name", _userName);
            obj.put("auto_settled_status",status);

            AndroidNetworking.post("https://wallet2autocashout-vn3k2k7q7q-uc.a.run.app/update_autosettle_status") //update_autosettle_status
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pd.dismiss();
                                JSONObject obj = new JSONObject(response.toString());
                                if(obj!=null){
                                    String status = obj.getString("status");
                                    if(status.equalsIgnoreCase("0")){
                                        if(autoSettled){
                                            layot_main.setVisibility(View.GONE);

                                        }else{
                                            layot_main.setVisibility(View.VISIBLE);
                                        }
                                    }else{
                                        showAlertOnBack(WalletToBankActivity.this,"Alert","Someting went to wrong. Please try again,");

                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                pd.dismiss();
                                showAlertOnBack(WalletToBankActivity.this,"Alert","Someting went to wrong. Please try again,");
                                // Toast.makeText(WalletToBankActivity.this,"Exception.. Someting went to wrong.",Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(WalletToBankActivity.this,"Oh.. Someting went to wrong.",Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    private void callEncriptedTransferAmount(String encodedUrl,ProgressDialog pd) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("amount",addAmountEdit.getText().toString());
            AndroidNetworking.post(encodedUrl)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pd.dismiss();
                                JSONObject obj = new JSONObject(response.toString());
                                String statusDesc = obj.getString("statusDesc");

                                showAlertt(WalletToBankActivity.this,"Alert",statusDesc);
                                addAmountEdit.setText("");
                                Util.hideKeyboard(WalletToBankActivity.this,addAmountEdit);


                                /*{"status":0,"statusDesc":"Transaction Successfull","txnId":109204,"bankRespCode":"00","bankMessage":"Transaction successfull"}*/



                            } catch (JSONException e) {
//                                e.printStackTrace();
                                pd.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            String str = anError.getErrorBody();
                            try {
                                JSONObject errorObj = new JSONObject(str);
                                String statusDesc = errorObj.getString("statusDesc");

                                Util.showAlert(WalletToBankActivity.this,"Alert",statusDesc);
                                pd.dismiss();
                            } catch (JSONException e) {
//                                e.printStackTrace();
                            }
//                            pd.dismiss();


                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }


    }

    private void callAPI(String eName, String eNumber, String eIfsc, String eCode) {
        View _view = getLayoutInflater().inflate(R.layout.add_bankdetail_bottom_sheet, null);
        BottomSheetDialog dialog = new BottomSheetDialog(WalletToBankActivity.this);
        dialog.setContentView(_view);

        Button saveBtn = _view.findViewById(R.id.saveBtn);
        AutoCompleteTextView bankName = _view.findViewById(R.id.bankName);
        EditText accNoEdit = _view.findViewById(R.id.accNo);
        EditText ifscCode = _view.findViewById(R.id.ifscCode);
        EditText bankCode = _view.findViewById(R.id.bankCode);
        ProgressBar progbar = _view.findViewById(R.id.progbar);

        bankName.setDropDownVerticalOffset(0);

        bankName.setText(eName);
        accNoEdit.setText(eNumber);
        ifscCode.setText(eIfsc);
        bankCode.setText(eCode);

        String[] banks = bankList.toArray(new String[0]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,banks);
        bankName.setAdapter(adapter);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String bankNameStr = bankName.getText().toString();
                String accNoStr = accNoEdit.getText().toString();
                String ifscCodeStr = ifscCode.getText().toString();
                String bankCodeStr = bankCode.getText().toString();
                if(bankNameStr.equalsIgnoreCase("")){
                    bankName.setError("Enter bank name");
                }else if(accNoStr.equalsIgnoreCase("")){
                    accNoEdit.setError("Enter Account No");

                }else if(ifscCodeStr.equalsIgnoreCase("")){
                    ifscCode.setError("Enter IFSC code");

                }else if(bankCodeStr.equalsIgnoreCase("")){
                    bankCode.setError("Enter Bank code");

                }else {


                    if (eName.equalsIgnoreCase(bankNameStr) && eNumber.equalsIgnoreCase(accNoStr) && eIfsc.equalsIgnoreCase(ifscCodeStr) && eCode.equalsIgnoreCase(bankCodeStr)) {
                        //Toast.makeText(WalletToBankActivity.this, "Save button not enable", Toast.LENGTH_SHORT).show();
                        Util.showAlert(WalletToBankActivity.this,"Alert","Updated Successfully");
                        dialog.dismiss();
                    } else {
                        SaveBankDetail(dialog,progbar,bankNameStr,accNoStr,ifscCodeStr,bankCodeStr);
                        dialog.dismiss();
                    }
                }




            }
        });




        dialog.show();



    }

    private void SaveBankDetail(BottomSheetDialog dialog,ProgressBar progressBar, String bankNameStr, String accNoStr, String ifscCodeStr,String bankCodeStrr) {
        progressBar.setVisibility(View.VISIBLE);

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v70")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            EncriptedSaveBankDetail(encodedUrl,dialog,progressBar,bankNameStr,accNoStr,ifscCodeStr,bankCodeStrr);

                        } catch (JSONException e) {
//                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
//                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }


    private void EncriptedSaveBankDetail(String encodedUrl,BottomSheetDialog dialog,ProgressBar progressBar, String bankNameStr, String accNoStr, String ifscCodeStr,String bankCodeStrr) {
        //progressBar.setVisibility(View.VISIBLE);
        //String  token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTg4MTQwMDM2MzA2LCJleHAiOjE1ODgxNDE4MzZ9.cpx9v6wrBYokW8LBkO7et4FdrSbYbxLcTN30n59vDsQMVpFj0yixpvVBoBowPq25Q20MMz05s-wKqfUO9jdYiQ";

        editAccount = accNoStr;
        editBank = bankNameStr;
        editIFSC = ifscCodeStr;
        editCode = bankCodeStrr;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accountNumber",accNoStr);
            jsonObject.put("ifsc",ifscCodeStr);
            jsonObject.put("bankName",bankNameStr);
            jsonObject.put("bankCode",bankCodeStrr);

            AndroidNetworking.post(encodedUrl)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String active = obj.getString("active");
                                dialog.dismiss();
                                progressBar.setVisibility(View.GONE);
                                if(active.equalsIgnoreCase("false")){
                                    String message = obj.getString("message");
                                    showAlertOnBack(WalletToBankActivity.this,"Alert",message);
                                    addAmountEdit.setVisibility(View.GONE);
                                    transferAmount.setVisibility(View.GONE);

                                }else{
                                    String accountNo = obj.getString("accountNumber");
                                    accNo.setText("Acc No: "+accountNo);
                                    addAmountEdit.setVisibility(View.VISIBLE);
                                    transferAmount.setVisibility(View.VISIBLE);
                                    addAccount.setVisibility(View.GONE);

                                }

                            } catch (JSONException e) {
//                                e.printStackTrace();
                                progressBar.setVisibility(View.GONE);

                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            String str = anError.getErrorBody();
                            progressBar.setVisibility(View.GONE);

                            try {
                                JSONObject obj = new JSONObject(str);
                                String msg = obj.getString("message");
                                if(!msg.equalsIgnoreCase("")){
                                    showAlertOnBack(WalletToBankActivity.this,"Alert",msg);

                                }else{
                                    showAlertOnBack(WalletToBankActivity.this,"Alert","Error");
                                }


                            } catch (JSONException e) {
                                showAlertOnBack(WalletToBankActivity.this,"Alert","Error");

                            }

                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }


    }


    public void checkBankDetail() {
        try {
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v68")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                ShowBankDetails(encodedUrl);

                            } catch (JSONException e) {
//                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
//                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }
    }
    public void ShowBankDetails(String encodeUrl) {

        //String  token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTg4MTU2MTIzNzMzLCJleHAiOjE1ODgxNTc5MjN9.u2QZ5zMjBPQmQDTwsYAM0KXda8Dtlu_IJIxgtkZqRT8hV4CR43B5T9xJIDZdHzueopegpElzSQwQBlUCYmzKNw";
        //encodeUrl = "https://wallet.iserveu.online/cashout/getbank/cashout.json";


        AndroidNetworking.get(encodeUrl)
                .addHeaders("Authorization",token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            getWallet1Balance(); // calling wallet Balance


                            String active = response.getString("active");
                            /*
                             * If account is active showing bank Info
                             * */
                            if(active.equalsIgnoreCase("true")){
                                String message = response.getString("message");
                                bankNameStr = response.getString("bankName");
                                accNoStr = response.getString("accountNumber");
                                ifscStr = response.getString("ifsc");
                                bankCodeStr = response.getString("bankCode");

                                editAccount = accNoStr;
                                editBank = bankNameStr;
                                editIFSC = ifscStr;
                                editCode = bankCodeStr;

                                accNo.setText("Acc No. :"+accNoStr);
                                addAccount.setVisibility(View.GONE);
                                transferAmount.setVisibility(View.VISIBLE);
                                addAmountEdit.setVisibility(View.VISIBLE);
                                amontplacehoder.setVisibility(View.VISIBLE);
                                imgInfo.setVisibility(View.GONE);
                                transferAmount.setBackground(ContextCompat.getDrawable(WalletToBankActivity.this, R.drawable.enable_button_background));
                                transferAmount.setEnabled(true);
                                addAmountEdit.setEnabled(true);
                                addAmountEdit.setFocusable(true);
                                img_prof.setVisibility(View.VISIBLE);
                                checkSettlementStatus();
                                swOnOff.setEnabled(true);
                                accEdit.setVisibility(View.VISIBLE);

                            }else{
                                accEdit.setVisibility(View.GONE);
                                swOnOff.setEnabled(false);
                                accNoStr = response.getString("accountNumber");

                                if(accNoStr.equalsIgnoreCase("")){
                                    accNo.setText("xxxx xxxx xxxx");
                                    addAccount.setVisibility(View.VISIBLE);
                                    Util.showAlert(WalletToBankActivity.this,"Alert","No Bank account linked with this ID.");
                                    transferAmount.setVisibility(View.GONE);
                                    addAmountEdit.setVisibility(View.GONE);
                                    amontplacehoder.setVisibility(View.GONE);
                                    imgInfo.setVisibility(View.GONE);
                                    img_prof.setVisibility(View.VISIBLE);


                                }
                                else{

                                    //Util.showAlert(WalletToBankActivity.this,"Alert","Sorry, Account No. is not active.");

                                    accNo.setText("Acc no: "+accNoStr);
                                    addAccount.setVisibility(View.GONE);
                                    imgInfo.setVisibility(View.VISIBLE);
                                    img_prof.setVisibility(View.VISIBLE);
                                    transferAmount.setVisibility(View.VISIBLE);
                                    addAmountEdit.setVisibility(View.VISIBLE);
                                    amontplacehoder.setVisibility(View.VISIBLE);
                                    transferAmount.setBackground(ContextCompat.getDrawable(WalletToBankActivity.this, R.drawable.disable_button_background));
                                    transferAmount.setEnabled(false);
                                    addAmountEdit.setEnabled(false);
                                    addAmountEdit.setFocusable(false);
                                }

                            }


                        } catch (JSONException e) {
//                                e.printStackTrace();
                            pd.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        String str = anError.getErrorBody();
                        pd.dismiss();

                    }
                });


    }

    public void getWallet1Balance() {
        try {

            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v72")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                getencriptedWallet1Balance(encodedUrl);

                            } catch (JSONException e) {
//                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
//                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }
    }

    public void getencriptedWallet1Balance(String encodedUrl){
        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                //.addBodyParameter(map)
                .addHeaders("Authorization",token)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        String wallet1_balance = response;
                        if(!wallet1_balance.equalsIgnoreCase("")||wallet1_balance!=null){
                            balance_wallet = Double.parseDouble(wallet1_balance);
                        }

                        wal1_bal_edt.setText("Balance : ₹ "+wallet1_balance);
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        pd.dismiss();
                    }
                });
    }


    public void showAlertt(Context context, String title, String message) {

        try {
            if (!isFinishing() && !isDestroyed()) {

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                dialog.dismiss();
                                getWallet1Balance();
                            }
                        })
                        .show();
            }
        }catch (Exception e){

        }
    }

    public  void showAlertOnBack(Context context, String title, String message) {

        try {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder.setTitle(title)
                    .setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            onBackPressed();

                        }
                    })
                    .show();

        }catch (Exception e){

        }
    }

    private void getBanks() {
        String url = "https://us-central1-iserveustaging.cloudfunctions.net/iin/api/v1/getIIN";
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            JSONArray jsonArray = response.getJSONArray("bankIINs");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject bankObject = jsonArray.getJSONObject(i);
                                String bankname = bankObject.getString("BANKNAME");
                                bankList.add(bankname);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: " + anError.getErrorBody());
                    }
                });
    }
}
