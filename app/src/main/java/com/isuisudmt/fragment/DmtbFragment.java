package com.isuisudmt.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.isuisudmt.CommonUtil;
import com.isuisudmt.Constants;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.bbps.SavePDF.PreviewPDFActivity;
import com.isuisudmt.bluetooth.BluetoothPrinter;
import com.isuisudmt.dmt.AddBeneAPI;
import com.isuisudmt.dmt.AddBeneRequest;
import com.isuisudmt.dmt.AddBeneResponse;
import com.isuisudmt.dmt.AddCustomerAPI;
import com.isuisudmt.dmt.AddCustomerRequest;
import com.isuisudmt.dmt.AddCustomerResponse;
import com.isuisudmt.dmt.AddNPayAPI;
import com.isuisudmt.dmt.AddNPayRequest;
import com.isuisudmt.dmt.AddNPayResponse;
import com.isuisudmt.dmt.BankListModel;
import com.isuisudmt.dmt.BeneListModel;
import com.isuisudmt.dmt.BeneModel;
import com.isuisudmt.dmt.BeneRecycleAdapter;
import com.isuisudmt.dmt.BulkTransactionAPI;
import com.isuisudmt.dmt.BulkTransactionResponse;
import com.isuisudmt.dmt.BulkTransfer_Wallet3_TransactionAPi;
import com.isuisudmt.dmt.Currency;
import com.isuisudmt.dmt.DMTBankNameActivity;
import com.isuisudmt.dmt.FundTransferContract;
import com.isuisudmt.dmt.FundTransferPresenter;
import com.isuisudmt.dmt.MyErrorMessage;
import com.isuisudmt.dmt.ResendOtpAPI;
import com.isuisudmt.dmt.ResendOtpResponse;
import com.isuisudmt.dmt.TransactionAPI;
import com.isuisudmt.dmt.TransactionDetails;
import com.isuisudmt.dmt.TransactionRequest;
import com.isuisudmt.dmt.TransactionResponse;
import com.isuisudmt.dmt.VerifyBeneApi;
import com.isuisudmt.dmt.VerifyBeneRequest;
import com.isuisudmt.dmt.VerifyBeneResponse;
import com.isuisudmt.dmt.VerifyOtpApi;
import com.isuisudmt.dmt.VerifyOtpRequest;
import com.isuisudmt.dmt.VerifyOtpResponse;
import com.isuisudmt.dmt.Wallet2TransactionAPI;
import com.isuisudmt.dmt.Wallet2TransactionRequest;
import com.isuisudmt.dmt.Wallet3TransactionAPI;
import com.isuisudmt.dmt.Wallet3TransactionRequest;
import com.isuisudmt.settings.BluetoothConnectorActivity;
import com.isuisudmt.utils.APIFundTransferService;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.matm.matmsdk.FileUtils;
import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.aepsmodule.utils.Util;
import com.matm.matmsdk.permission.PermissionsActivity;
import com.matm.matmsdk.permission.PermissionsChecker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.isuisudmt.BuildConfig.ADD_AND_PAY;
import static com.isuisudmt.BuildConfig.ADD_BENE_URL;
import static com.isuisudmt.BuildConfig.ADD_CUSTOMER;
import static com.isuisudmt.BuildConfig.BULK_TRANSFER;
import static com.isuisudmt.BuildConfig.DELETE_BENE_DMT;
import static com.isuisudmt.BuildConfig.RESEND_OTP;
import static com.isuisudmt.BuildConfig.TRANSACTION;
import static com.isuisudmt.BuildConfig.VERIFY_BENE_URL;
import static com.isuisudmt.BuildConfig.VERIFY_OTP;
import static com.isuisudmt.BuildConfig.WALLET2_TRANSACTION_API;
import static com.matm.matmsdk.permission.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.matm.matmsdk.permission.PermissionsChecker.REQUIRED_PERMISSION;


public class DmtbFragment extends Fragment implements FundTransferContract.View {

    CoordinatorLayout coordinatorLayout;
    TextInputLayout btn_cross;
    ImageView imageView, addbene, view_bene_list, show_bene_list;
    LinearLayout pay_add, wallet_info, add_verify, single_bene, bank, loadmore, wallet_content_one, wallet_content_two, wallet_content_three, main_container, new_customer, verify_number, add_new_bene, single_bene_bank, bene_ll;
    Button load_more, enroll_number, resend_otp, verify, create_add_bene, verify_bene, addAndPayBtn, submit_pay;
    EditText mobile_number, amount_transfer, ifc_code, customer_name, otp;
    EditText acc_no, ifccode, bene_name;
    RecyclerView benelistData;
    TextView bank_name_autocomplete;
    SessionManager session;
    LinearLayout amountEditText;
    Button wallet1, wallet2, wallet3;
    TextView add_bene, bankname, wallet_balance_one, wallet_balance_two, wallet_balance_three, wallet_one, wallet_two, wallet_three, amount_to_transfer;
    String _token;
    int wallet_selected = 1;
    //    String pipeNo = "3";//Default for wallet 3
    String pipeNo = "1";//Default for wallet 1
    String beneId, customerId, _amount_transfer, uid = "fojoo", transactionMode = "NEFT";
    Boolean splitService1 = true;
    Boolean splitService2 = true;
    Boolean splitService3 = true;
    Boolean showAlert1_Once = true;
    Boolean showAlert2_Once = true;
    Boolean showAlert3_Once = true;
    ProgressBar progressBar, progressBar_main;
    private ArrayList<String> listOfBank = new ArrayList<>();
    final HashMap<String, List<BankListModel>> bankDataMap = new HashMap<>();
    ArrayList<BankListModel> bankDetailList = new ArrayList<>();
    String selectedBank = "", getAccountNo = "";
    Set<String> bank_listSet = new HashSet<>();
    private FundTransferContract.View DMTView;

    BeneRecycleAdapter bebeAdapter;
    ArrayList<BeneModel> beneList = new ArrayList<BeneModel>();
    Button show_recept;
    TextView selectedBeneName, amouttxt, select_bankName, select_ifccode;
    ProgressBar progress_bene;
    RadioGroup rg_add_bene, rg_select_bene;
    RadioButton rb_select_neft, rb_select_imps, rb_add_imps, rb_add_neft;
    String pdfStatus, trackingID;
    Boolean walletInfoStatus = true;

    ArrayList<String> paymentMode = new ArrayList<String>();
    ArrayList<String> paymentMode1 = new ArrayList<String>();
    ArrayList<String> paymentMode2 = new ArrayList<String>();

    private FundTransferPresenter mActionsListener;

    private APIFundTransferService apiFundTransferService;

    AddCustomerRequest addCustomerRequest;

    String amount, transaction_amount_str, bene_name_str, bene_acc_no_str, bank_name_str;
    String currentDateandTime;
    boolean verificationflags;

    BeneModel bModel;
    TransactionRequest task;
    Wallet2TransactionRequest wallet_task;
    Wallet3TransactionRequest wallet3_task;
    ArrayList<TransactionDetails> transactionDetailsList;
    AddNPayRequest addNPayRequest;
    int amountNumber = 0;
    ProgressDialog pd;
    Handler timer_handler;
    BluetoothAdapter b;
    PermissionsChecker checker;
    private Handler handler = new Handler();
    private FirebaseAnalytics firebaseAnalytics;
    private String userName;
    private View DmtView;

    public DmtbFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static Fragment getInstance() {
        return new DmtbFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        new CustomThemes(getActivity());
        DmtView = inflater.inflate(R.layout.fragment_dmtb, container, false);


        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        initView();

        checker = new PermissionsChecker(getActivity());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd : HH.mm.ss");
        currentDateandTime = sdf.format(new Date());


        rg_add_bene.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_add_imps:
                        transactionMode = "IMPS";
                        break;
                    case R.id.rb_add_neft:
                        transactionMode = "NEFT";
                        break;
                }
            }
        });

        rg_select_bene.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_select_neft:
                        transactionMode = "NEFT";
                        break;
                    case R.id.rb_select_imps:
                        transactionMode = "IMPS";
                        break;
                }
            }
        });

        view_bene_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setFBAnalytics("DMT_VIEW_BENE", userName);

                bene_name.setText("");
                acc_no.setText("");
                bank_name_autocomplete.setText("");
                ifccode.setText("");

                create_add_bene.setVisibility(View.VISIBLE);
                verify_bene.setVisibility(View.VISIBLE);

                single_bene.setVisibility(View.GONE);
                bene_ll.setVisibility(View.VISIBLE);
            }
        });

        session = new SessionManager(getActivity());
        HashMap<String, String> user = session.getUserSession();
        userName = user.get(session.userName);
        String _userName = user.get(session.userName);
        String _userType = user.get(session.userType);
        String _userBalance = user.get(session.userBalance);
        String _adminName = user.get(session.adminName);
        String _mposNumber = user.get(session.mposNumber);

        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);

        //For banklist store in session
//        bank_listSet=session.getBankListinSession();
//        if (bank_listSet==null){
//            retriveBankList();
//        }
//        else {
//            retriveBankList();
//        }
        retriveBankList(); //Call bank list from firestore

        bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DMT_BANK", userName);
                loadmore.setVisibility(View.VISIBLE);
                load_more.setVisibility(View.VISIBLE);
            }
        });

        create_add_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String _bank_name = bank_name_autocomplete.getText().toString();
                String _ifccode = ifccode.getText().toString();
                String _acc_no = acc_no.getText().toString().replaceAll("-", "");
                String _bene_name = bene_name.getText().toString();

                if (_bank_name.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (_acc_no.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank account number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (_ifccode.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter IFSC code", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (_bene_name.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter beneficiary name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    setFBAnalytics("DMT_CREATE_BENE", userName);
                    progress_bene.setVisibility(View.VISIBLE);
                    addBene(mobile_number.getText().toString(), mobile_number.getText().toString(), _bank_name, _acc_no, _bene_name, _ifccode);
                }
            }
        });

        verify_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!customerId.equalsIgnoreCase("") || (customerId != null)) {

                    String _bank_name = bank_name_autocomplete.getText().toString();
                    String _ifccode = ifccode.getText().toString();
                    String _acc_no = acc_no.getText().toString().replaceAll("-", "");
                    String _bene_name = bene_name.getText().toString();


                    if (_bank_name.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank name", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (_acc_no.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank account number", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (_ifccode.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter IFSC code", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else if (_bene_name.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter beneficiary name", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    } else {
                        setFBAnalytics("DMT_VERIFY_BENE", userName);
                        progress_bene.setVisibility(View.VISIBLE);
                        verifyBene(customerId, _acc_no, _bank_name, _bene_name, _ifccode);
                    }
                }
            }
        });

        add_bene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DMT_ADD_BENE", userName);
                bene_ll.setVisibility(GONE);
                add_new_bene.setVisibility(View.VISIBLE);
                add_verify.setVisibility(View.VISIBLE);
            }
        });

        addAndPayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (amount_transfer.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter transfer amount", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (bank_name_autocomplete.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (acc_no.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter bank account number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (ifccode.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter IFSC code", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (bene_name.getText().toString().isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter beneficiary name", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    setFBAnalytics("DMT_ADD_AND_PAY", userName);
                    addandpayConformationDialog();
                }
            }
        });

        show_recept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  bene_ll.setVisibility(View.VISIBLE);
                setFBAnalytics("DMT_SHOW_RECEIPT", userName);
                add_new_bene.setVisibility(View.GONE);
                loadmore.setVisibility(View.GONE);
                load_more.setVisibility(View.GONE);
            }
        });

        show_bene_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DMT_SHOW_BENE", userName);

                bank_name_autocomplete.setText("");
                bene_name.setText("");
                acc_no.setText("");
                bank_name_autocomplete.setText("");
                ifccode.setText("");
                create_add_bene.setVisibility(View.VISIBLE);
                verify_bene.setVisibility(View.VISIBLE);

                bene_ll.setVisibility(View.VISIBLE);
                add_new_bene.setVisibility(View.GONE);
            }
        });


        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommonUtil.hideKeyboard(getActivity(), otp);

                String _otp = otp.getText().toString().trim();
                String _mobile_number = mobile_number.getText().toString();

                if (_otp.isEmpty()) {
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Enter valid OTP", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } else if (_mobile_number.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid mobile number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    CommonUtil.hideKeyboard(getActivity(), otp);
                    setFBAnalytics("DMT_VERIFY", userName);
                    verifyOtp(_otp, _mobile_number);
                }
            }
        });

        resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _mobile_number = mobile_number.getText().toString();

                if (_mobile_number.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter valid mobile number", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    setFBAnalytics("DMT_RESEND_OTP", userName);
                    resendOtp(_mobile_number);
                }

            }
        });

        mobile_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (count != 10) {
                        main_container.setVisibility(View.GONE);
                        new_customer.setVisibility(View.GONE);
                        verify_number.setVisibility(View.GONE);
                        add_new_bene.setVisibility(View.GONE);
                        single_bene_bank.setVisibility(View.GONE);
                    }
                } catch (Exception exc) {
                    //exc.printStackTrace();
                    main_container.setVisibility(View.GONE);
                    new_customer.setVisibility(View.GONE);
                    verify_number.setVisibility(View.GONE);
                    add_new_bene.setVisibility(View.GONE);
                    single_bene_bank.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 10) {
                    mobile_number.setCursorVisible(true);
//                    btn_cross.setError(getResources().getString(isumatm.androidsdk.equitas.R.string.mobileerror));
                }
                if (s.length() > 0) {
                    mobile_number.setCursorVisible(true);
//                    btn_cross.setError(null);
                    String x = s.toString();
                    if (x.startsWith("0") || Util.isValidMobile(mobile_number.getText().toString().trim()) == false) {
//                        btn_cross.setError(getResources().getString(isumatm.androidsdk.equitas.R.string.mobilevaliderror));
                    }
                }

                if (s.length() == 10) {

                    CommonUtil.hideKeyboard(getActivity(), mobile_number);
                    progressBar.setVisibility(View.VISIBLE);
                    mobile_number.setCursorVisible(false);
//                        retriveBankList();

                    mActionsListener.loadWallet(_token, mobile_number.getText().toString());


                }
            }
        });

        amount_to_transfer.setText("Transfer from Wallet 1"); //For default
//        amount_to_transfer.setText("Transfer from Wallet 1");

        enroll_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mob = mobile_number.getText().toString();
                String customerName = customer_name.getText().toString();

                if (mob.isEmpty() && customerName.isEmpty()) {
                    //Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "All fields are required", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } else {
                    setFBAnalytics("DMT_ENROLL_NUMBER", userName);
                    addCustomer();
                }
            }
        });

        submit_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                session = new SessionManager(getActivity());

                _amount_transfer = amount_transfer.getText().toString();

                if (_amount_transfer.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, "Enter transfer amount", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    setFBAnalytics("DMT_SUBMIT_PAY", userName);
                    transactionConformationDialog();

                }

            }
        });

        amount_transfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    if (s.toString().isEmpty()) {
                        amouttxt.setText("");
                    } else {
                        String str = Currency.convertToIndianCurrency(s.toString());
                        amouttxt.setText(str);
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                }

            }
        });

        bank_name_autocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DMT_BANK_AUTOCOMPLETE", userName);
                Intent intent = new Intent(getActivity(), DMTBankNameActivity.class);
                intent.putExtra("BANKDATA", listOfBank);
                startActivity(intent);
            }
        });

        acc_no.addTextChangedListener(new TextWatcher() {
            int length_before = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                length_before = charSequence.length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                getAccountNo = acc_no.getText().toString();

                if (charSequence.toString().contains("-")) {
                    getAccountNo = charSequence.toString().replace("-", "");
                    System.out.println(">>---->>>>" + getAccountNo);
                }

                //====================================================================================================================================================================//
                // if(activity.getCurrentFocus()==acc_no) {

                if (charSequence.length() == 0) {
                    ifccode.setText("");
                    bene_name.setText("");
                }
                if (selectedBank.length() != 0) {
                    if (!isValidAccount(getAccountNo)) {
                        acc_no.setError("Invalid AccountNo");
                        ifccode.setText("");
                    } else {
                        ifcsFind();
                    }
                } else {
                    bank_name_autocomplete.setError("Please select bank name");
                }

                // }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                // if(activity.getCurrentFocus()==acc_no) {

                if (editable.toString().length() == 0) {
                    ifccode.setText("");
                    bene_name.setText("");
                }
                // }

                if (length_before < editable.length()) {
                    if (editable.length() == 4 || editable.length() == 9 || editable.length() == 14 || editable.length() == 19)
                        editable.append("-");
                    if (editable.length() > 4) {
                        if (Character.isDigit(editable.charAt(4)))
                            editable.insert(4, "-");
                    }
                    if (editable.length() > 9) {
                        if (Character.isDigit(editable.charAt(9)))
                            editable.insert(9, "-");
                    }
                    if (editable.length() > 14) {
                        if (Character.isDigit(editable.charAt(14)))
                            editable.insert(14, "-");
                    }
                    if (editable.length() > 19) {
                        if (Character.isDigit(editable.charAt(19)))
                            editable.insert(19, "-");
                    }
                }

                if (editable.toString().contains("-")) {
                    getAccountNo = editable.toString().replace("-", "");
                    System.out.println(">>---->>>>" + getAccountNo);
                }
            }

        });

        return DmtView;
    }


    private void addandpayConformationDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.transaction_conformation_dialog);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);

        beneMobileTv.setText(mobile_number.getText().toString());
        transactionAmountTv.setText(amount_transfer.getText().toString());
        transactionTypeTv.setText(transactionMode);

        transaction_amount_str = amount_transfer.getText().toString();

        beneNameTv.setText(bene_name.getText().toString());
        beneAccountTv.setText(acc_no.getText().toString());
        bankNameTv.setText(bank_name_autocomplete.getText().toString());

        bene_name_str = bene_name.getText().toString();
        bene_acc_no_str = acc_no.getText().toString();
        bank_name_str = bank_name_autocomplete.getText().toString();

        Button dialogButtonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);

        dialogButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DMT_DO_ADD_PAY_YES", userName);
                doAddNPay();
                dialog.dismiss();
            }
        });

        Button dialogButtonNo = (Button) dialog.findViewById(R.id.dialog_button_no);

        dialogButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DMT_DO_ADD_PAY_NO", userName);
                dialog.dismiss();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    private void initView() {


        coordinatorLayout = DmtView.findViewById(R.id.coordinatorLayout);
        b = BluetoothAdapter.getDefaultAdapter();
        pd = new ProgressDialog(getActivity());

        single_bene = DmtView.findViewById(R.id.single_bene);
        bankname = DmtView.findViewById(R.id.bankname);
        imageView = DmtView.findViewById(R.id.transfer_nav);
        amountEditText = DmtView.findViewById(R.id.amountEditText);
        addbene = DmtView.findViewById(R.id.addbene);
        bank = DmtView.findViewById(R.id.bank);
        loadmore = DmtView.findViewById(R.id.loadmore);
        load_more = DmtView.findViewById(R.id.submit);
        mobile_number = DmtView.findViewById(R.id.mobile_number);
        amount_transfer = DmtView.findViewById(R.id.amount_transfer);
        ifc_code = DmtView.findViewById(R.id.ifc_code);
        benelistData = DmtView.findViewById(R.id.benelistData);
        view_bene_list = DmtView.findViewById(R.id.view_bene_list);
        show_bene_list = DmtView.findViewById(R.id.show_bene_list);
        pay_add = DmtView.findViewById(R.id.pay_add);
        wallet1 = DmtView.findViewById(R.id.wallet1);
        wallet2 = DmtView.findViewById(R.id.wallet2);
        wallet3 = DmtView.findViewById(R.id.wallet3);
        btn_cross = DmtView.findViewById(R.id.btn_cross);
        benelistData.setLayoutManager(new LinearLayoutManager(getActivity()));

        wallet_info = DmtView.findViewById(R.id.wallet_info);

        wallet_content_one = DmtView.findViewById(R.id.wallet_content_one);
        wallet_content_two = DmtView.findViewById(R.id.wallet_content_two);
        wallet_content_three = DmtView.findViewById(R.id.wallet_content_three);

        wallet_balance_one = DmtView.findViewById(R.id.wallet_balance_one);
        wallet_balance_two = DmtView.findViewById(R.id.wallet_balance_two);
        wallet_balance_three = DmtView.findViewById(R.id.wallet_balance_three);

        add_verify = DmtView.findViewById(R.id.add_verify);
        wallet_one = DmtView.findViewById(R.id.wallet_one);
        wallet_two = DmtView.findViewById(R.id.wallet_two);
        wallet_three = DmtView.findViewById(R.id.wallet_three);
        main_container = DmtView.findViewById(R.id.main_container);
        progressBar = DmtView.findViewById(R.id.progressBar);
        progressBar_main = DmtView.findViewById(R.id.progressBar_main);
        new_customer = DmtView.findViewById(R.id.new_customer);
        customer_name = DmtView.findViewById(R.id.customer_name);
        enroll_number = DmtView.findViewById(R.id.enroll_number);
        otp = DmtView.findViewById(R.id.otp);
        verify_number = DmtView.findViewById(R.id.verify_number);
        resend_otp = DmtView.findViewById(R.id.resend_otp);
        verify = DmtView.findViewById(R.id.verify);
        add_bene = DmtView.findViewById(R.id.add_bene);
        add_new_bene = DmtView.findViewById(R.id.add_new_bene);
        single_bene_bank = DmtView.findViewById(R.id.single_bene_bank);
        create_add_bene = DmtView.findViewById(R.id.create_add_bene);
        bank_name_autocomplete = DmtView.findViewById(R.id.bank_name_autocomplete);
        acc_no = DmtView.findViewById(R.id.acc_no);
        ifccode = DmtView.findViewById(R.id.ifccode);
        bene_name = DmtView.findViewById(R.id.bene_name);
        amount_to_transfer = DmtView.findViewById(R.id.amount_to_transfer);
        bene_ll = DmtView.findViewById(R.id.bene_ll);
        show_recept = DmtView.findViewById(R.id.show_recept);
        selectedBeneName = DmtView.findViewById(R.id.selectedBeneName);
        verify_bene = DmtView.findViewById(R.id.verify_bene);
        progress_bene = DmtView.findViewById(R.id.progress_bene);
        addAndPayBtn = DmtView.findViewById(R.id.addAndPayBtn);
        rg_select_bene = DmtView.findViewById(R.id.rg_select_bene);
        rb_select_neft = DmtView.findViewById(R.id.rb_select_neft);
        rb_select_imps = DmtView.findViewById(R.id.rb_select_imps);
        rg_add_bene = DmtView.findViewById(R.id.rg_add_bene);
        rb_add_imps = DmtView.findViewById(R.id.rb_add_imps);
        rb_add_neft = DmtView.findViewById(R.id.rb_add_neft);
        amouttxt = DmtView.findViewById(R.id.amouttxt);
        submit_pay = DmtView.findViewById(R.id.pay);

        select_bankName = DmtView.findViewById(R.id.select_bankName);
        select_ifccode = DmtView.findViewById(R.id.select_ifccode);
        mActionsListener = new FundTransferPresenter(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        // startTimer();
        selectedBank = Constants.BANK_FLAG;
        System.out.println(">>>>>>>:::" + selectedBank);
        bank_name_autocomplete.setText(selectedBank);
        bank_name_autocomplete.setError(null);
        if (selectedBank.length() != 0) {
            if (getAccountNo.length() != 0) {
                if (!isValidAccount(getAccountNo)) {
                    acc_no.setError("Invalid AccountNo");
                    ifccode.setText("");
                } else {
                    ifcsFind();
                }
            }
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 3) {
            String bankName = data.getDataString();
            System.out.println(">>>>>>>:::" + bankName);
        }
        if (resultCode == PermissionsActivity.PERMISSIONS_GRANTED) {
            Toast.makeText(getActivity(), "Permission Granted to Save", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Permission not granted, Try again!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void showWallet(String status, String _customerId, long id, Boolean flag, Double balance, Boolean imps, Boolean neft, Boolean verificationFlag) {

        customerId = _customerId;
        wallet1.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
        wallet2.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
        wallet3.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
        amount_transfer.getText().clear();
        clearViewList();

        if (mobile_number.getText().toString().length() == 10 && walletInfoStatus) {
            walletInfoStatus = false;
            wallet_info.setVisibility(View.GONE);
        } else {
            walletInfoStatus = true;
            wallet_info.setVisibility(View.VISIBLE);
            mobile_number.setCursorVisible(false);
        }


        if (status.equals("0") && id == 1) {


            if (imps) {
                paymentMode.add("IMPS");
            }
            if (neft) {
                paymentMode.add("NEFT");
            }

            if (flag == false && showAlert1_Once == true) {
                showAlert("Wallet 1  feature is not available for now");
                wallet1.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                wallet1.setEnabled(false);
                splitService1 = false;
            }


            wallet_content_one.setVisibility(View.VISIBLE);
            wallet_balance_one.setText("₹ " + balance.toString());

            amount_to_transfer.setVisibility(View.GONE);
            amountEditText.setVisibility(View.GONE);


            wallet1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bene_ll.setVisibility(View.VISIBLE);
                    setFBAnalytics("DMT_WALLET_CONTENT_ONE", userName);
                    pipeNo = String.valueOf(id);
                    wallet_selected = 1;
                    verificationflags = verificationFlag;
                    amount_to_transfer.setText("Transfer from Wallet 1 ");

                    if (flag == true) {
                        add_new_bene.setVisibility(GONE);
                        add_verify.setVisibility(GONE);
                        bene_ll.setVisibility(View.VISIBLE);
                        single_bene.setVisibility(GONE);
                        amount_to_transfer.setVisibility(View.VISIBLE);
                        amountEditText.setVisibility(View.VISIBLE);
                        wallet1.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet_selected));
                        if (splitService2 == true) {
                            wallet2.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                        } else if (splitService3 == true) {
                            wallet3.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                        }
                    }


//                    pay_add.setVisibility(GONE);
                    pay_add.setVisibility(View.VISIBLE);
                }
            });
        } else if (status.equals("0") && id == 2) {

            if (imps) {
                paymentMode1.add("IMPS");
            }
            if (neft) {
                paymentMode1.add("NEFT");
            }

            if (!imps && !neft) {
                paymentMode1.add("IMPS");
            }

            if (Constants.user_feature_array != null) {
                if (!Constants.user_feature_array.contains("41") && showAlert2_Once == true) {
                    showAlert("Wallet 2 feature is not available for now");
                    wallet2.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                    wallet2.setEnabled(false);
                    splitService2 = false;
                    showAlert2_Once = false;
                }
            }


            wallet_content_two.setVisibility(View.VISIBLE);
            wallet_balance_two.setText("₹ " + balance.toString());

            amount_to_transfer.setVisibility(View.GONE);
            amountEditText.setVisibility(View.GONE);

            wallet2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setFBAnalytics("DMT_WALLET_CONTENT_TWO", userName);
                    bene_ll.setVisibility(View.VISIBLE);
                    pipeNo = String.valueOf(id);
                    verificationflags = verificationFlag;
                    wallet_selected = 2;
                    amount_to_transfer.setText("Transfer from Wallet 2 ");
                    pay_add.setVisibility(GONE);

                    if (flag == true) {
                        add_new_bene.setVisibility(GONE);
                        add_verify.setVisibility(GONE);
                        single_bene.setVisibility(GONE);
                        bene_ll.setVisibility(View.VISIBLE);
                        amount_to_transfer.setVisibility(View.VISIBLE);
                        amountEditText.setVisibility(View.VISIBLE);
                        wallet2.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet_selected));
                        if (splitService1 == true) {
                            wallet1.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                        } else if (splitService3 == true) {
                            wallet3.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                        }
                    }


                }
            });
        } else if (status.equals("0") && id == 3) {

            if (imps) {
                paymentMode2.add("IMPS");
            }

            if (neft) {
                paymentMode2.add("NEFT");
            }

            if (!imps && !neft) {
                paymentMode2.add("IMPS");
                paymentMode2.add("NEFT");
            }

            if (flag == false && showAlert3_Once == true) {
                if (Constants.user_feature_array != null) {
                    if (!Constants.user_feature_array.contains("42")) {
                        showAlert("Wallet 3 feature is not available for now");
                        wallet3.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                        wallet3.setEnabled(false);
                        splitService3 = false;
                        showAlert3_Once = false;
                    }
                }


            }

            wallet_content_three.setVisibility(View.VISIBLE);
            wallet_balance_three.setText("₹ " + balance.toString());

            amount_to_transfer.setVisibility(View.GONE);
            amountEditText.setVisibility(View.GONE);

            wallet3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setFBAnalytics("DMT_WALLET_CONTENT_THREE", userName);
                    bene_ll.setVisibility(View.VISIBLE);

//                    Snackbar snackbar = Snackbar
//                            .make(coordinatorLayout, "Currently service isn't available", Snackbar.LENGTH_LONG);
//                    snackbar.show();
                    pipeNo = String.valueOf(id);
                    verificationflags = verificationFlag;

                    wallet_selected = 3;
                    amount_to_transfer.setText("Transfer from Wallet 3 ");
                    pay_add.setVisibility(GONE);

                    if (flag == true) {
                        add_new_bene.setVisibility(GONE);
                        add_verify.setVisibility(GONE);
                        single_bene.setVisibility(GONE);
                        bene_ll.setVisibility(View.VISIBLE);
                        amount_to_transfer.setVisibility(View.VISIBLE);
                        amountEditText.setVisibility(View.VISIBLE);
                        wallet3.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet_selected));
                        if (splitService1 == true) {
                            wallet1.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                        } else if (splitService2 == true) {
                            wallet2.setBackground(getResources().getDrawable(R.drawable.btn_border_wallet));
                        }
                    }
//                    pay_add.setVisibility(View.VISIBLE);
                }
            });
        } else {
            wallet_info.setVisibility(View.GONE);
        }
    }


    @Override
    public void showMessage(int message) {


        if (message == 0) {
            progressBar.setVisibility(View.GONE);
            main_container.setVisibility(View.VISIBLE);
            benelistData.setVisibility(View.VISIBLE);
            wallet_info.setVisibility(View.VISIBLE);
            bene_ll.setVisibility(View.GONE);
            single_bene.setVisibility(GONE);


        } else if (message == 12) {
            progressBar.setVisibility(View.GONE);
            new_customer.setVisibility(GONE);
            verify_number.setVisibility(View.VISIBLE);
        } else if (message == 11) {
            new_customer.setVisibility(View.VISIBLE);
            verify_number.setVisibility(GONE);

            progressBar.setVisibility(View.GONE);
            main_container.setVisibility(View.GONE);
            benelistData.setVisibility(View.GONE);
            wallet_info.setVisibility(View.GONE);
        } else {
            verifyOtpDialog();
        }
    }


    @Override
    public void showBeniName(BeneListModel beneListModel) {

        if (beneListModel.getBeneName() == null) {
            bebeAdapter = new BeneRecycleAdapter(this, beneList);
            benelistData.setAdapter(bebeAdapter);
        } else {
            BeneModel BM = new BeneModel();
            BM.setBeneId(beneListModel.getId());
            BM.setCustomerNumber(beneListModel.getCustomerNumber());
            BM.setBeneMobileNo(beneListModel.getBeneMobileNo());
            BM.setBeneName(beneListModel.getBeneName());
            BM.setAccountNo(beneListModel.getAccountNo());
            BM.setBankName(beneListModel.getBankName());
            BM.setIfscCode(beneListModel.getIfscCode());
            BM.setBeneStatus(beneListModel.getStatus());
            beneList.add(BM);

            bebeAdapter = new BeneRecycleAdapter(this, beneList);
            benelistData.setAdapter(bebeAdapter);
        }

    }

    @Override
    public void showLoader() {
        try {
            if (pd != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.setMessage("Loading...please wait !");
                        pd.setCancelable(false);
                        pd.show();
                    }
                });
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void hideLoader() {
        try {
            if (!getActivity().isFinishing() && !getActivity().isDestroyed()) {
                if (pd != null && pd.isShowing()) {

                    pd.dismiss();
                    pd.cancel();
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void status() {
        //progressBar.setVisibility(GONE);
        //verify_number.setVisibility(View.VISIBLE);

        try {
            if (beneList.size() > 0) {
                beneList.clear();
                bebeAdapter.notifyDataSetChanged();
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }


    private void verifyOtpDialog() {
        new_customer.setVisibility(GONE);
        verify_number.setVisibility(View.VISIBLE);
    }

    private void retriveBankList() {

        handler.post(new Runnable() {
            @Override
            public void run() {
                showLoader();
            }
        });
//-----For saving data in session
//        if (bank_listSet!=null){
//
//            listOfBank.addAll(bank_listSet);
//            hideLoader();
//        }
//        else {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("BankList")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        try {
                            if (task.isSuccessful()) {

                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    List<BankListModel> downloadInfoList = task.getResult().toObjects(BankListModel.class);
                                    bankDataMap.put(document.getId(), downloadInfoList);
                                    listOfBank.add(document.getId());

//                                    Set<String> bankListSet = new HashSet<>();
//                                    bankListSet.addAll(listOfBank);
//                                    session.saveBankListinSession(bankListSet);

                                }

                                for (Map.Entry<String, List<BankListModel>> entry : bankDataMap.entrySet()) {
                                    bankDetailList = new ArrayList<BankListModel>(entry.getValue());
                                }

                                String _bankDetailList = new Gson().toJson(bankDetailList);
                                String _listOfBank = new Gson().toJson(listOfBank);

//                            Log.i("_bankDetailList :: ", _bankDetailList);
//                            Log.i("_listOfBank :: ", _listOfBank);

                                hideLoader();

//                            progressBar.setVisibility(View.GONE);
//                            main_container.setVisibility(View.VISIBLE);
//                            benelistData.setVisibility(View.VISIBLE);

                            } else {
                                hideLoader();

                                //       Log.w("sdd", "Error getting documents.", task.getException());
                            }
                        } catch (Exception e) {
                            hideLoader();
                            e.printStackTrace();
                        }

                    }
                });

//        }
    }

    private void addCustomer() {

        String mob = mobile_number.getText().toString();
        String customerName = customer_name.getText().toString();

        if (customerName != null && !customerName.isEmpty()) {

            addCustomerRequest = new AddCustomerRequest(customerName, mob);

            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(ADD_CUSTOMER)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }
                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedAccCustomer(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } else {
            //  Log.d("STATUS", "TransactionSuccesscustomername1" + customerName);
            //Toast.makeText(getActivity(), "Please enter customer name", Toast.LENGTH_LONG).show();
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Please enter customer name", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }

    public void encriptedAccCustomer(String encodedUrl) {
        final AddCustomerAPI addCustomerAPI = this.apiFundTransferService.getClient().create(AddCustomerAPI.class);

        addCustomerAPI.getAddCustomer(_token, addCustomerRequest, encodedUrl).enqueue(new Callback<AddCustomerResponse>() {
            @Override
            public void onResponse(Call<AddCustomerResponse> call, Response<AddCustomerResponse> response) {
                if (response.isSuccessful()) {
                    //.d("STATUS", "TransactionSuccessfulAddCustomer" + response.body());
                    verifyOtpDialog();
                    //hideLoader();
                } else {
                    // Log.d("STATUS", "TransactionFailedAddCustomer" + response.body());
                    // hideLoader();
                }
            }

            @Override
            public void onFailure(Call<AddCustomerResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    private void verifyOtp(String otp, final String mob) {
        showLoader();

        final VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest(otp, mob);

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(VERIFY_OTP)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }
                            System.out.println(">>>>-----" + encodedUrl);
                            encriptedVerifyOtp(verifyOtpRequest, encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                    }
                });

    }

    public void encriptedVerifyOtp(VerifyOtpRequest verifyOtpRequest, String encodedUrl) {
        final VerifyOtpApi verifyOtpApi = this.apiFundTransferService.getClient().create(VerifyOtpApi.class);

        verifyOtpApi.getVerifyOtp(_token, verifyOtpRequest, encodedUrl).enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {

                if (response.isSuccessful()) {
                    progressBar.setVisibility(GONE);
                    hideLoader();

                    if (response.body().getStatus().equals("0")) {
                        verify_number.setVisibility(GONE);
                        mActionsListener.loadWallet(_token, mobile_number.getText().toString());
                    } else {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout, "" + response.body().getStatusDesc(), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    // Log.d("STATUS", "TransactionSuccessfulVerifyOtp" + response.body());
                } else {
                    progressBar.setVisibility(GONE);
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    private void resendOtp(String mob) {

        showLoader();

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("number", mob);

            AndroidNetworking.post(RESEND_OTP)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }
                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedResendOtp(encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void encriptedResendOtp(String encodedUrl) {

        final ResendOtpAPI resendOtpAPI = this.apiFundTransferService.getClient().create(ResendOtpAPI.class);

        resendOtpAPI.getResendOtpReport(_token, encodedUrl).enqueue(new Callback<ResendOtpResponse>() {
            @Override
            public void onResponse(Call<ResendOtpResponse> call, Response<ResendOtpResponse> response) {
                if (response.isSuccessful()) {
                    hideLoader();
                    //  Log.d("STATUS", "TransactionSuccessfulResendOtp" + response.body());
                } else {
                    hideLoader();
                    // Log.d("STATUS", "TransactionFailedResendOtp" + response.body());
                }
            }

            @Override
            public void onFailure(Call<ResendOtpResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    public void itemOncliclickView(BeneModel bModel) {
        try {
            this.bModel = bModel;

            bene_ll.setVisibility(View.GONE);
            single_bene.setVisibility(View.VISIBLE);
            select_bankName.setText(bModel.beneName);
            select_ifccode.setText(bModel.ifscCode);

            beneId = bModel.beneId;

        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

    public void itemOncliclickDelete(BeneModel bModel, int position) {

        showLoader();

        try {
            AndroidNetworking.get(DELETE_BENE_DMT)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                System.out.println(">>>>-----" + encodedUrl);
                                enCriptedDeleteBene(mobile_number.getText().toString(), bModel.getBeneId(), encodedUrl, position);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }

    private void enCriptedDeleteBene(String custtomerNo, String beneIdStr, String encodedUrl, int position) {

        JSONObject obj = new JSONObject();

        try {
            obj.put("beneId", beneIdStr);
            obj.put("customerNumber", custtomerNo);

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .setContentType("application/json; charset=utf-8")
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {

                                hideLoader();
                                JSONObject obj = new JSONObject(response.toString());
                                //String key = obj.getString("hello");
                                System.out.println(">>>>-----" + response.toString());

                                beneList.remove(position);
                                bebeAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                        }
                    });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean isValidAccount(String accountPattern) {
        String patternAccountNo = "";
        for (String bankl : listOfBank) {

            if (bankl.equalsIgnoreCase(selectedBank)) {

                int pos = listOfBank.indexOf(selectedBank);

                String strr = bankDetailList.get(pos).getPATTERN();

                if (strr != null) {
                    patternAccountNo = strr;
                }
            }
        }
        Pattern pattern = Pattern.compile(patternAccountNo);
        Matcher matcher = pattern.matcher(accountPattern);
        return matcher.matches();
    }

    private void ifcsFind() {

        setFBAnalytics("DMT_FIND_IFCS", userName);

        for (String bankl : listOfBank) {

            if (bankl.equalsIgnoreCase(selectedBank)) {
                int pos = listOfBank.indexOf(selectedBank);
                String flag = bankDetailList.get(pos).getFLAG();
                String bankCode = bankDetailList.get(pos).getBANKCODE();
                if (flag != null) {
                    if (flag.equalsIgnoreCase("u")) {
                        ifccode.setText(bankCode);
                        ifccode.setEnabled(false);
                    } else if (flag.equalsIgnoreCase("4")) {
                        String first4 = "";
                        try {

                            first4 = getAccountNo.substring(0, 4);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ifccode.setText(bankCode + first4);
                        ifccode.setEnabled(false);
                    } else if (flag.equalsIgnoreCase("3")) {

                        String first3 = "";
                        try {

                            first3 = getAccountNo.substring(0, 3);
                            //first3 = enterAccountNo.getText().toString().substring(0,3);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        ifccode.setText(bankCode + first3);
                        ifccode.setEnabled(false);
                    } else if (flag.equalsIgnoreCase("6")) {
                        String first6 = "";
                        try {
                            first6 = getAccountNo.substring(0, 6);
                            // first6 = enterAccountNo.getText().toString().substring(0,6);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ifccode.setText(bankCode + first6);
                        ifccode.setEnabled(false);
                    }
                } else /*if(bankl.getFLAG().equalsIgnoreCase(null))*/ {
                    ifccode.setText("no IFSC");
                    ifccode.setEnabled(true);
                }
            }
        }
    }

    private void doAddNPay() {
        progress_bene.setVisibility(View.VISIBLE);

        addNPayRequest = new AddNPayRequest(amount_transfer.getText().toString().trim(), mobile_number.getText().toString().trim(), transactionMode, bene_name.getText().toString(), ifccode.getText().toString(), bank_name_autocomplete.getText().toString(), acc_no.getText().toString().replaceAll("-", ""), mobile_number.getText().toString(), pipeNo);

        showLoader();

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(ADD_AND_PAY)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }

                            System.out.println(">>>>-----" + encodedUrl);

                            encriptedAddAndPay(encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                    }
                });

    }

    public void encriptedAddAndPay(String encodedUrl) {
        AddNPayAPI addNPayAPI = this.apiFundTransferService.getClient().create(AddNPayAPI.class);
        addNPayAPI.getAddNPayReport(_token, addNPayRequest, encodedUrl).enqueue(new Callback<AddNPayResponse>() {
            @Override
            public void onResponse(Call<AddNPayResponse> call, Response<AddNPayResponse> response) {
                if (response.isSuccessful()) {

                    hideLoader();

                    progress_bene.setVisibility(GONE);

                    // Log.d("STATUS", "TransactionSuccessfulAddNPAY" + response.body());

                    mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                    transactionDetailsList = new ArrayList<TransactionDetails>();
                    TransactionDetails transactionDetails = new TransactionDetails();
                    transactionDetails.setTransaction_amount(String.valueOf(amount_transfer.getText().toString()));
                    transactionDetails.setTracking_no(response.body().gettxnId());
                    transactionDetails.setTransaction_type(transactionMode);
                    transactionDetailsList.add(transactionDetails);
                    transactionSuccessDialog(response.body().gettxnId(), response.body().getstatusDesc(), transactionDetailsList);

                } else {

                    try {
                        progress_bene.setVisibility(GONE);

                        hideLoader();
                        Gson gson = new Gson();
                        MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                        try {
                            showPaymentFailedDialog(message.getMessage());
                        } catch (Exception exc) {
                            //exc.printStackTrace();
                            showPaymentFailedDialog("Transaction failed");
                        }

//                        Gson gson = new Gson();
//                        MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
//                        showPaymentFailedDialog(message.getMessage());

                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void onFailure(Call<AddNPayResponse> call, Throwable t) {
                hideLoader();
            }
        });
    }

    private void addBene(String _mobile_number, String mobile_number, String _bank_name, String getAccountNo, String _bene_name, String _ifccode) {

        showLoader();

        final AddBeneRequest addBeneRequest = new AddBeneRequest(_mobile_number, mobile_number, _bank_name, getAccountNo.replaceAll("-", ""), _bene_name, _ifccode);

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(ADD_BENE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }
                            System.out.println(">>>>-----" + encodedUrl);
                            encriptAddBene(addBeneRequest, apiFundTransferService, encodedUrl);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        hideLoader();
                    }
                });

    }

    public void encriptAddBene(AddBeneRequest addBeneRequest, APIFundTransferService apiFundTransferService, String encodedUrl) {

        final AddBeneAPI addBeneAPI = this.apiFundTransferService.getClient().create(AddBeneAPI.class);
        addBeneAPI.getAddBene(_token, addBeneRequest, encodedUrl).enqueue(new Callback<AddBeneResponse>() {
            @Override
            public void onResponse(Call<AddBeneResponse> call, Response<AddBeneResponse> response) {
                if (response.isSuccessful()) {
                    // Log.d("STATUS", "TransactionSuccessfulAddBene" + response.body());

                    progress_bene.setVisibility(GONE);

                    bank_name_autocomplete.setText("");
                    bene_name.setText("");
                    acc_no.setText("");
                    ifccode.setText("");

                    create_add_bene.setVisibility(View.VISIBLE);
                    verify_bene.setVisibility(View.VISIBLE);
                    add_new_bene.setVisibility(View.GONE);
                    bene_ll.setVisibility(View.GONE);

                    hideLoader();

                    mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                    //Toast.makeText(getActivity(), response.body().getStatusDesc(), Toast.LENGTH_LONG).show();

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "" + response.body().getStatusDesc(), Snackbar.LENGTH_LONG);
                    snackbar.show();


                } else {
                    progress_bene.setVisibility(GONE);
                    // Log.d("STATUS", "TransactionFailedAddBene" + response.body());

                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<AddBeneResponse> call, Throwable t) {
                hideLoader();
            }
        });

    }

    private void verifyBene(String customerId, String getAccountNo, String bankName, String beneNameEnter, String ifscCode) {

        if (customerId == null || getAccountNo == null || bankName == null || beneNameEnter == null || ifscCode == null) {
            // Toast.makeText(getActivity(), "Beneficiary details field are empty", Toast.LENGTH_LONG).show();

            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, "Beneficiary details field are empty", Snackbar.LENGTH_LONG);
            snackbar.show();

        } else {

            if ((pipeNo.equals("3"))) {
                pipeNo = "3";
            } else if ((pipeNo.equals("1"))) {
                pipeNo = "1";
            } else {
                pipeNo = "2";
            }

            final VerifyBeneRequest verifyBeneRequest = new VerifyBeneRequest(customerId, getAccountNo, bankName, beneNameEnter, ifscCode, "1");


            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(VERIFY_BENE_URL)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }
                                System.out.println(">>>>-----" + encodedUrl);
                                enCriptedVerifyBene(verifyBeneRequest, encodedUrl);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        }
    }

    public void enCriptedVerifyBene(VerifyBeneRequest verifyBeneRequest, String encodedUrl) {

        final VerifyBeneApi verifyBeneApi = this.apiFundTransferService.getClient().create(VerifyBeneApi.class);

        verifyBeneApi.getVerifyBene(_token, verifyBeneRequest, encodedUrl).enqueue(new Callback<VerifyBeneResponse>() {
            @Override
            public void onResponse(Call<VerifyBeneResponse> call, Response<VerifyBeneResponse> response) {

                if (response.isSuccessful()) {
                    //  Log.d("STATUS", "Transaction successful VerifyBene" + response.body());

                    progress_bene.setVisibility(GONE);

                    //add_verify.setVisibility(GONE);

                    bene_name.setText(response.body().getBeneName());

                    //Toast.makeText(getActivity(), response.body().getStatusDesc(), Toast.LENGTH_SHORT).show();

                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "" + response.body().getStatusDesc(), Snackbar.LENGTH_LONG);

                    snackbar.show();


                    //                    bene_name.setText(response.body().getBeneName());
                    //                    bank_name_autocomplete.setText("");
                    //                    bene_name.setText("");
                    //                    acc_no.setText("");
                    //                    bank_name_autocomplete.setText("");
                    //                    ifccode.setText("");
                    //                    create_add_bene.setVisibility(View.VISIBLE);
                    //                    verify_bene.setVisibility(View.VISIBLE);
                    //
                    //                    bene_ll.setVisibility(View.VISIBLE);
                    //                    add_new_bene.setVisibility(View.GONE);
                    //hideLoader();

                } else {
                    //hideLoader();
                    progress_bene.setVisibility(GONE);

                    //  Log.d("STATUS", "Transaction Failed Verify Bene" + response.body());
                    //hideLoader();
                }
            }

            @Override
            public void onFailure(Call<VerifyBeneResponse> call, Throwable t) {
                //hideLoader();
                progress_bene.setVisibility(GONE);
            }
        });

    }

    private void transaction(String pipeNO) { //for wallet 1

        if (pipeNO.equalsIgnoreCase("1")) {

            showLoader();


            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(TRANSACTION)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedTransaction(encodedUrl, pipeNO);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        } else {

//            Toast.makeText(this, "For wallet 3 transaction", Toast.LENGTH_SHORT).show();


            showLoader();


            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get("https://dmt.iserveu.tech/generate/v83")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);
                                encriptedTransaction(encodedUrl, pipeNO);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }


    }

    public void encriptedTransaction(String encodedUrl, String pipeNo) {

        if (pipeNo.equalsIgnoreCase("1")) {


            final TransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(TransactionAPI.class);
            transactionAPI.getTransectionReport(_token, task, encodedUrl).enqueue(new Callback<TransactionResponse>() {
                @Override
                public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

                    if (response.isSuccessful()) {

                        hideLoader();

                        int amountNumber = 0;
                        String amount = amount_transfer.getText().toString();
                        try {
                            amountNumber = Integer.parseInt(amount.trim());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }


                        mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                        transactionDetailsList = new ArrayList<TransactionDetails>();
                        TransactionDetails transactionDetails = new TransactionDetails();
                        transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                        transactionDetails.setTracking_no(response.body().getTxnId());
                        transactionDetails.setTransaction_type(transactionMode);
                        transactionDetailsList.add(transactionDetails);

                        transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);


                    } else {

                        try {
                            hideLoader();

                            Gson gson = new Gson();
                            MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                            //transaction failed

                            try {
                                showPaymentFailedDialog(message.getMessage());
                            } catch (Exception exc) {
                                //exc.printStackTrace();

                            }

                        } catch (Exception e) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<TransactionResponse> call, Throwable t) {
                    hideLoader();
                }
            });


        } else {
            //For wallet3

            final Wallet3TransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(Wallet3TransactionAPI.class);
            transactionAPI.getTransectionReport(_token, wallet3_task, encodedUrl).enqueue(new Callback<TransactionResponse>() {
                @Override
                public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

                    if (response.isSuccessful()) {

                        hideLoader();

                        int amountNumber = 0;
                        String amount = amount_transfer.getText().toString();
                        try {
                            amountNumber = Integer.parseInt(amount.trim());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }


                        mActionsListener.loadWallet(_token, mobile_number.getText().toString());

                        transactionDetailsList = new ArrayList<TransactionDetails>();
                        TransactionDetails transactionDetails = new TransactionDetails();
                        transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                        transactionDetails.setTracking_no(response.body().getTxnId());
                        transactionDetails.setTransaction_type(transactionMode);
                        transactionDetailsList.add(transactionDetails);

                        transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);


                    } else {

                        try {
                            hideLoader();

                            Gson gson = new Gson();
                            MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                            //transaction failed

                            try {
                                showPaymentFailedDialog(message.getMessage());
                            } catch (Exception exc) {
                                //exc.printStackTrace();

                            }

                        } catch (Exception e) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<TransactionResponse> call, Throwable t) {
                    hideLoader();
                }
            });


        }


    }

    private void wallet2transaction() { //for wallet2

        showLoader();

        if (this.apiFundTransferService == null) {
            this.apiFundTransferService = new APIFundTransferService();
        }

        AndroidNetworking.get(WALLET2_TRANSACTION_API)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                encodedUrl = encodedUrl.replace("itpl", "dmt");
                            }
                            System.out.println(">>>>-----" + encodedUrl);
                            wallet2encriptedTransaction(encodedUrl);


                        } catch (JSONException e) {
                            //e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            //e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                    }
                });
    }

    public void wallet2encriptedTransaction(String encodedUrl) {

        final Wallet2TransactionAPI wallet2TransactionAPI = this.apiFundTransferService.getClient()
                .create(Wallet2TransactionAPI.class);
        wallet2TransactionAPI.getTransectionReport(_token, wallet_task, encodedUrl)
                .enqueue(new Callback<TransactionResponse>() {
                    @Override
                    public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                        if (response.isSuccessful()) {

                            hideLoader();

                            transactionDetailsList = new ArrayList<TransactionDetails>();
                            TransactionDetails transactionDetails = new TransactionDetails();
                            transactionDetails.setTransaction_amount(String.valueOf(amountNumber));
                            transactionDetails.setTracking_no(response.body().getTxnId());
                            transactionDetails.setTransaction_type(transactionMode);
                            transactionDetailsList.add(transactionDetails);

                            transactionSuccessDialog(response.body().getTxnId(), response.body().getStatusDesc(), transactionDetailsList);

                        } else {

                            try {

                                hideLoader();
                                Gson gson = new Gson();
                                MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);
                                try {
                                    showPaymentFailedDialog(message.getMessage());
                                } catch (Exception exc) {
                                    //exc.printStackTrace();
                                    showPaymentFailedDialog("Transaction failed");
                                }

                            } catch (Exception e) {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<TransactionResponse> call, Throwable t) {
                        hideLoader();
                    }
                });
    }

    private void transactionConformationDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.transaction_conformation_dialog);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);

        beneMobileTv.setText(mobile_number.getText().toString());
        transactionAmountTv.setText(amount_transfer.getText().toString());
        transactionTypeTv.setText(transactionMode);

        transaction_amount_str = amount_transfer.getText().toString();

        beneNameTv.setText(bModel.getBeneName());
        beneAccountTv.setText(bModel.getAccountNo());
        bankNameTv.setText(bModel.getBankName());
        bene_name_str = bModel.getBeneName();
        bene_acc_no_str = bModel.getAccountNo();
        bank_name_str = bModel.getBankName();

        Button dialogButtonYes = (Button) dialog.findViewById(R.id.dialog_button_yes);

        dialogButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conformTransaction();
                dialog.dismiss();
            }
        });

        Button dialogButtonNo = (Button) dialog.findViewById(R.id.dialog_button_no);

        dialogButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //hideLoader();

    }

    private void conformTransaction() {
        setFBAnalytics("DMT_CONFIRM_TRANSACTION", userName);
//        Log.i("pipeNo ::: ", "" + pipeNo);
//        Log.i("amount_str ::", transaction_amount_str);
//        Log.i("customerId", customerId);
//        Log.i("beneId", beneId);
//        Log.i("transactionMode", transactionMode);


        if (pipeNo.equalsIgnoreCase("2")) { //For wallet 2

            transaction_amount_str = amount_transfer.getText().toString();
            wallet_task = new Wallet2TransactionRequest(transaction_amount_str, customerId, pipeNo, beneId, transactionMode, "");
            wallet2transaction();
        } else if (pipeNo.equalsIgnoreCase("3")) {  //For wallet 3

            transaction_amount_str = amount_transfer.getText().toString();
            wallet3_task = new Wallet3TransactionRequest(amount_transfer.getText().toString(), customerId, pipeNo, beneId, transactionMode);

            try {
                amountNumber = Integer.parseInt(transaction_amount_str.trim());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
            }

            if (amountNumber <= 5000) {
                transaction("3");
            } else {
                bulkTransfer("3");
            }

        } else {   //For pipeNo-1

            transaction_amount_str = amount_transfer.getText().toString();
            task = new TransactionRequest(amount_transfer.getText().toString(), customerId, pipeNo, beneId, transactionMode);

            try {
                amountNumber = Integer.parseInt(transaction_amount_str.trim());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
            }

            if (amountNumber <= 5000) {
                transaction("1");
            } else {
                bulkTransfer("1");
            }

        }

    }

    private void transactionSuccessDialog(final String txnId, String statusDesc, final ArrayList<TransactionDetails> transactionDetailsList) {
        //wallet_info.setVisibility(GONE);

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.transaction_success_dialog);
        dialog.setCancelable(false);

        final TextView tv = dialog.findViewById(R.id.tv);
        final ImageView status_icon = dialog.findViewById(R.id.status_icon);
        final TextView beneNameTv = dialog.findViewById(R.id.bene_name_tv);
        final TextView beneMobileTv = dialog.findViewById(R.id.bene_mobile_tv);
//        final TextView beneAccountTv = dialog.findViewById(R.id.bene_account_tv);
        final TextView bankNameTv = dialog.findViewById(R.id.bank_name_tv);
        final TextView transactionAmountTv = dialog.findViewById(R.id.transaction_amount_tv);
//        final TextView transactionTypeTv = dialog.findViewById(R.id.transaction_type_tv);
//        final TextView trackingId = dialog.findViewById(R.id.tracking_no_tv);
        final Button txndetails = dialog.findViewById(R.id.txndetails);

        if (txnId == null || txnId.trim().length() == 0) {
            pdfStatus = "Failed";
            trackingID = "N/A";
            status_icon.setImageResource(R.drawable.hero_failure);
        } else {
            pdfStatus = "Success";
            trackingID = txnId;
        }


//        trackingId.setText(trackingID);
        beneMobileTv.setText(mobile_number.getText().toString());
        transactionAmountTv.setText(transaction_amount_str);
//        transactionTypeTv.setText(transactionMode);

        beneNameTv.setText(bene_name_str);
//        beneAccountTv.setText(bene_acc_no_str);
        bankNameTv.setText(bank_name_str);

        txndetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("DMT_SHOW_TRAN_DETAILS", userName);
                showTransactionDetails(getActivity());
            }
        });

        Button dialogButtonOK = (Button) dialog.findViewById(R.id.closeBtn);
        Button dialogPrint = (Button) dialog.findViewById(R.id.printBtn);

        dialogButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mActionsListener.loadWallet(_token, mobile_number.getText().toString());
                clearViewList();
            }
        });

        dialog.show();

        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        Button dialogbuttonYes = (Button) dialog.findViewById(R.id.downloadBtn1);
        dialogbuttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.BRAND_NAME.trim().length() != 0) {
                    if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
                        PermissionsActivity.startActivityForResult(getActivity(), PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
                    } else {

                        Date date = new Date();
                        long timeMilli = date.getTime();
                        System.out.println("Time in milliseconds using Date class: " + String.valueOf(timeMilli));
                        createPdf(FileUtils.getAppPath(getActivity()) + String.valueOf(timeMilli) + "Order_Receipt.pdf");
                    }
                } else {
                    showBrandSetAlert();
                }
            }
        });

        dialogPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Constants.BRAND_NAME.trim().length() != 0) {
                    BluetoothDevice bluetoothDevice = Constants.bluetoothDevice;
                    if (bluetoothDevice != null) {
                        if (!b.isEnabled()) {

                           /* Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(turnOn, 0);*/
                            Toast.makeText(getActivity(), "Your Bluetooth is OFF we are turning it ON", Toast.LENGTH_LONG).show();
                        } else {
//                            Toast.makeText(getActivity(), "Bluetooth is already on", Toast.LENGTH_LONG).show();
//                            dialogPrint.setEnabled(false);
//                            dialogPrint.setBackgroundColor(getResources().getColor(R.color.grey));
                            callBluetoothFunction(bene_name_str, mobile_number.getText().toString(), bene_acc_no_str, bank_name_str, transaction_amount_str, txnId, transactionMode, bluetoothDevice);
                        }

                    } else {
//                        dialogPrint.setEnabled(false);
//                        dialogPrint.setBackgroundColor(getResources().getColor(R.color.grey));
                        Intent in = new Intent(getActivity(), BluetoothConnectorActivity.class);
                        startActivity(in);
//                        dialog.dismiss();
//                        Toast.makeText(getActivity(), "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showBrandSetAlert();
                }

            }
        });
    }

    public void createPdf(String dest) {

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            Document document = new Document();

            // Location to save
            PdfWriter.getInstance(document, new FileOutputStream(dest));

            // Open to write
            document.open();

            // Document Settings
            document.setPageSize(PageSize.A4);
            document.addCreationDate();
            document.addAuthor("");
            document.addCreator("");
            Rectangle rect = new Rectangle(577, 825, 18, 15);
            rect.enableBorderSide(1);
            rect.enableBorderSide(2);
            rect.enableBorderSide(4);
            rect.enableBorderSide(8);
            rect.setBorder(Rectangle.BOX);
            rect.setBorderWidth(2);
            rect.setBorderColor(BaseColor.BLACK);
            document.add(rect);
            BaseColor mColorAccent = new BaseColor(0, 153, 204, 255);
            float mHeadingFontSize = 24.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            BaseFont urName = BaseFont.createFont("assets/fonts/brandon_light.otf", "UTF-8", BaseFont.EMBEDDED);

            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator();
            lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));

            BaseFont bf = BaseFont.createFont("assets/fonts/brandon_medium.otf", "UTF-8", BaseFont.EMBEDDED);
            Font font = new Font(bf, 20);
            Font font2 = new Font(bf, 18);

            Font mOrderDetailsTitleFont = new Font(urName, 36.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderDetailsTitleChunk = new Chunk(SdkConstants.SHOP_NAME, mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);
            document.add(new Paragraph("\n\n"));

            Font mOrderShopTitleFont = new Font(urName, 25.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderShopTitleChunk = new Chunk("Receipt", mOrderShopTitleFont);
            Paragraph mOrderShopTitleParagraph = new Paragraph(mOrderShopTitleChunk);
            mOrderShopTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderShopTitleParagraph);
            document.add(new Paragraph("\n\n"));
            Font mOrderDetailsTitleFont1;
            if (pdfStatus.trim().equalsIgnoreCase("Failed")) {
                mOrderDetailsTitleFont1 = new Font(urName, 40.0f, Font.NORMAL, BaseColor.RED);

            } else {
                mOrderDetailsTitleFont1 = new Font(urName, 40.0f, Font.NORMAL, BaseColor.GREEN);
            }

            Chunk mOrderDetailsTitleChunk1 = new Chunk(pdfStatus.trim(), mOrderDetailsTitleFont1);
            Paragraph mOrderDetailsTitleParagraph1 = new Paragraph(mOrderDetailsTitleChunk1);
            mOrderDetailsTitleParagraph1.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph1);

            Font mOrderDateFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Font mOrderDateValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);

            Paragraph p = new Paragraph();
            p.add(new Chunk("Date/Time : ", mOrderDateFont));
            p.add(new Chunk(currentDateandTime, mOrderDateValueFont));
            document.add(p);
            document.add(new Paragraph("\n\n"));
            Paragraph p1 = new Paragraph();
            p1.add(new Chunk("Operation Performed : ", mOrderDateFont));
            p1.add(new Chunk("DMT", mOrderDateValueFont));
            document.add(p1);

            document.add(new Paragraph("\n\n"));

            Font mOrderDetailsFont = new Font(urName, 30.0f, Font.BOLD, mColorAccent);
            Chunk mOrderDetailsChunk = new Chunk("Transaction Details", mOrderDetailsFont);
            Paragraph mOrderDetailsParagraph = new Paragraph(mOrderDetailsChunk);
            mOrderDetailsParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsParagraph);
            document.add(new Paragraph("\n\n"));
            // document.add(new Chunk(lineSeparator));

            // Fields of Order Details...
            // Adding Chunks for Title and value
            Font mOrderIdFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderMobChunk = new Chunk("Contact: " + Constants.USER_MOBILE_NO, mOrderIdFont);
            Paragraph mOrderMobParagraph = new Paragraph(mOrderMobChunk);
            document.add(mOrderMobParagraph);
            Chunk mOrderIdChunk = new Chunk("API TID: " + trackingID, mOrderIdFont);
            Paragraph mOrderTxnParagraph = new Paragraph(mOrderIdChunk);
            document.add(mOrderTxnParagraph);
            Chunk mOrderIdValueChunk = new Chunk("Account No.: " + bene_acc_no_str, mOrderIdFont);
            Paragraph mOrderaadharParagraph = new Paragraph(mOrderIdValueChunk);
            document.add(mOrderaadharParagraph);
            Chunk mBankNameChunk = new Chunk("Bene Name: " + bene_name_str, mOrderIdFont);
            Paragraph mBankNameParagraph = new Paragraph(mBankNameChunk);
            document.add(mBankNameParagraph);
            Chunk mOrderrrnChunk = new Chunk("Bene Mobile " + mobile_number.getText().toString(), mOrderIdFont);
            Paragraph mOrderrnParagraph = new Paragraph(mOrderrrnChunk);
            document.add(mOrderrnParagraph);
            Chunk mOrderbalanceChunk = new Chunk("Bank Name: " + bank_name_str, mOrderIdFont);
            Paragraph mOrderbalanceParagraph = new Paragraph(mOrderbalanceChunk);
            document.add(mOrderbalanceParagraph);
            Chunk mOrdertxnAmtChunk = new Chunk("Transaction Amount: " + transaction_amount_str, mOrderIdFont);
            Paragraph mOrdertxnAmtParagraph = new Paragraph(mOrdertxnAmtChunk);
            document.add(mOrdertxnAmtParagraph);
            Chunk mOrdertxnTypeChunk = new Chunk("Transaction Type: " + transactionMode, mOrderIdFont);
            Paragraph mOrdertxnTypeParagraph = new Paragraph(mOrdertxnTypeChunk);
            document.add(mOrdertxnTypeParagraph);
            document.add(new Paragraph("\n\n"));

            Font mOrderAcNameFont = new Font(urName, mHeadingFontSize, Font.NORMAL, mColorAccent);
            Chunk mOrderAcNameChunk = new Chunk("Thank You", mOrderAcNameFont);
            Paragraph mOrderAcNameParagraph = new Paragraph(mOrderAcNameChunk);
            mOrderAcNameParagraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(mOrderAcNameParagraph);
            Font mOrderAcNameValueFont = new Font(urName, mValueFontSize, Font.NORMAL, BaseColor.BLACK);
            Chunk mOrderAcNameValueChunk = new Chunk(SdkConstants.BRAND_NAME, mOrderAcNameValueFont);
            Paragraph mOrderAcNameValueParagraph = new Paragraph(mOrderAcNameValueChunk);
            mOrderAcNameValueParagraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(mOrderAcNameValueParagraph);


            /*PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell(new Paragraph("Transaction Receipt", font));
            cell.setColspan(2); // colspan
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            table.addCell(new Paragraph("Shop Name", font));
            table.addCell(new Paragraph(Constants.SHOP_NAME, font2));
            table.addCell(new Paragraph("Contact", font));
            table.addCell(new Paragraph(Constants.USER_MOBILE_NO, font2));
            table.addCell(new Paragraph("Account No.", font)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph(bene_acc_no_str, font2)); // how to change cell to have different font and bold and background color
            table.addCell(new Paragraph("Date/Time", font));
            table.addCell(new Paragraph(currentDateandTime, font2));
            table.addCell(new Paragraph("API TID", font));
            table.addCell(new Paragraph(trackingID, font2));
            table.addCell(new Paragraph("Bene Name", font));
            table.addCell(new Paragraph(bene_name_str, font2));
            table.addCell(new Paragraph("Bene Mobile", font));
            table.addCell(new Paragraph(mobile_number.getText().toString(), font2));
            table.addCell(new Paragraph("Bank Name", font));
            table.addCell(new Paragraph(bank_name_str, font2));
            table.addCell(new Paragraph("Transaction Amount", font));
            table.addCell(new Paragraph(transaction_amount_str, font2));
            table.addCell(new Paragraph("Transaction Type", font));
            table.addCell(new Paragraph(transactionMode, font2));
            document.add(table);


            // Title Order Details...
            // Adding Title....
            Font mOrderDetailsTitleFont;
            if (pdfStatus.trim().equalsIgnoreCase("Failed")) {
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.RED);

            } else {
                mOrderDetailsTitleFont = new Font(urName, 40.0f, Font.NORMAL, BaseColor.GREEN);
            }

            Chunk mOrderDetailsTitleChunk = new Chunk(pdfStatus.trim(), mOrderDetailsTitleFont);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk);
            mOrderDetailsTitleParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(mOrderDetailsTitleParagraph);

            Font termsTitleFont;
            termsTitleFont = new Font(urName, 30.0f, Font.NORMAL, BaseColor.RED);
            Chunk termsTitleChunk = new Chunk("*  Terms & Conditions", termsTitleFont);
            Paragraph termsTitleParagraph = new Paragraph(termsTitleChunk);
            termsTitleParagraph.setAlignment(Element.ALIGN_LEFT);
            document.add(termsTitleParagraph);

            Font termsDetailsFont;
            termsDetailsFont = new Font(urName, 22.0f, Font.NORMAL, BaseColor.BLACK);
            Chunk termsDetailsChunk = new Chunk(getResources().getString(R.string.terms), termsDetailsFont);
            Paragraph termsDetailsParagraph = new Paragraph(termsDetailsChunk);
            termsDetailsParagraph.setAlignment(Element.ALIGN_LEFT);
            document.add(termsDetailsParagraph);
*/
            document.close();
            hideLoader();
            Toast.makeText(getActivity(), "PDF saved in the internal storage", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), PreviewPDFActivity.class);
            intent.putExtra("filePath", dest);
            startActivity(intent);

        } catch (IOException | DocumentException ie) {
            Log.e("createPdf: Error ", "" + ie.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(getActivity(), "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }

    private void showBrandSetAlert() {
        try {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
            builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
            builder1.setTitle("Warning!!!");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "GOT IT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        } catch (Exception e) {

        }
    }

    private void callBluetoothFunction(final String bene_name, final String bene_mob, final String bene_acc, final String bank_name, final String amount, final String tid, final String type, BluetoothDevice bluetoothDevice) {


        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {


                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(Constants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.printText("-----Transaction Report-----");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.printText("Bene Name: " + bene_name);
                mPrinter.addNewLine();
                mPrinter.printText("Bene Mobile No.: " + bene_mob);
                mPrinter.addNewLine();
                mPrinter.printText("Bene Account No.: " + bene_acc);
                mPrinter.addNewLine();
                mPrinter.printText("Bank Name.: " + bank_name);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Amount: " + amount);
                mPrinter.addNewLine();
                mPrinter.printText("API Tid: " + tid);
                mPrinter.addNewLine();
                mPrinter.printText("Transaction Type: " + type);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(Constants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");
                Toast.makeText(getActivity(), "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showPaymentFailedDialog(String message) {

        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.transaction_successful_custom_dialog);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;

            dialog.getWindow().setAttributes(lp);

            Window window = dialog.getWindow();

            final TextView transaction_status = (TextView) dialog.findViewById(R.id.transaction_status);
            final TextView trandsaction_id = (TextView) dialog.findViewById(R.id.trandsaction_id);
            final Button dialogButtonConfirm = (Button) dialog.findViewById(R.id.dialogButtonConfirm);

            transaction_status.setText("Transaction failed");
            transaction_status.setBackgroundColor(getResources().getColor(R.color.color_report_red));
            trandsaction_id.setText(message);

            dialogButtonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        } catch (Exception e) {

        }
    }

    private void bulkTransfer(String pipeNo) {

        if (pipeNo.equalsIgnoreCase("1")) {

            showLoader();

            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get(BULK_TRANSFER)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);

                                encriptedBulkTransfer(encodedUrl, pipeNo);

                            } catch (JSONException e) {
                                //e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                // e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        } else {
            //For wallet 3

            showLoader();

            if (this.apiFundTransferService == null) {
                this.apiFundTransferService = new APIFundTransferService();
            }

            AndroidNetworking.get("https://dmt.iserveu.tech/generate/v84")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----" + key);
                                byte[] data = Base64.decode(key, Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                if (encodedUrl.contains("https://itpl.iserveu.tech")) {
                                    encodedUrl = encodedUrl.replace("itpl", "dmt");
                                }

                                System.out.println(">>>>-----" + encodedUrl);

                                encriptedBulkTransfer(encodedUrl, pipeNo);

                            } catch (JSONException e) {
                                //e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                // e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });


        }

    }

    public void encriptedBulkTransfer(String encodedUrl, String pipeNo) {

        if (pipeNo.equalsIgnoreCase("1")) {

            BulkTransactionAPI transactionAPI = this.apiFundTransferService.getClient().create(BulkTransactionAPI.class);

            transactionAPI.getTransectionReport(_token, task, encodedUrl).enqueue(new Callback<BulkTransactionResponse>() {

                @Override
                public void onResponse(Call<BulkTransactionResponse> call, Response<BulkTransactionResponse> response) {
                    try {
                        if (response.body().getTransactionResponse() != null) {

                            if (response.isSuccessful()) {

                                hideLoader();

//                          Log.i("body :: ", "" + response.body().getTransactionResponse());

                                ArrayList<TransactionResponse> BulkList = response.body().getTransactionResponse();

                                String txnId = "";

                                transactionDetailsList = new ArrayList<TransactionDetails>();

                                int amountTemp = amountNumber;

                                for (int i = 0; i < BulkList.size(); i++) {
                                    TransactionDetails transactionDetails = new TransactionDetails();
                                    transactionDetails.setTracking_no(BulkList.get(i).getTxnId());
                                    transactionDetails.setTransaction_type(transactionMode);

                                    if (amountTemp > 5000) {
                                        amountTemp = amountTemp - 5000;
                                        transactionDetails.setTransaction_amount("5000");
                                    } else {
                                        transactionDetails.setTransaction_amount(String.valueOf(amountTemp));
                                    }

                                    transactionDetailsList.add(transactionDetails);

                                    if (i == 0) {
                                        txnId = BulkList.get(i).getTxnId();
                                    } else {
                                        txnId = txnId + "," + BulkList.get(i).getTxnId();
                                    }
                                }

                                transactionSuccessDialog(txnId, BulkList.get(1).getStatusDesc(), transactionDetailsList);
                            } else {

                                try {
                                    hideLoader();
                                    Gson gson = new Gson();
                                    MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);

                                    try {
                                        showPaymentFailedDialog(message.getMessage());
                                    } catch (Exception e) {

                                        showPaymentFailedDialog("Transaction failed");
                                    }
                                } catch (Exception e) {

                                }
                            }
                        } else {
                            hideLoader();
                            showPaymentFailedDialog(" You can see your transaction details on fund transfer report section.");
                        }
                    } catch (Exception exc) {
                        //exc.printStackTrace();
                        hideLoader();
                        showPaymentFailedDialog(" You can see your transaction details on fund transfer report section.");

                    }
                }

                @Override
                public void onFailure(Call<BulkTransactionResponse> call, Throwable t) {
                    hideLoader();
                    showPaymentFailedDialog(" You can see your transaction details on fund transfer report section.");
                }
            });

        } else {

            //For wallet3

            BulkTransfer_Wallet3_TransactionAPi transactionAPI = this.apiFundTransferService.getClient().create(BulkTransfer_Wallet3_TransactionAPi.class);

            transactionAPI.getTransectionReport(_token, wallet3_task, encodedUrl).enqueue(new Callback<BulkTransactionResponse>() {

                @Override
                public void onResponse(Call<BulkTransactionResponse> call, Response<BulkTransactionResponse> response) {
                    try {
                        if (response.body().getTransactionResponse() != null) {

                            if (response.isSuccessful()) {

                                hideLoader();

//                          Log.i("body :: ", "" + response.body().getTransactionResponse());

                                ArrayList<TransactionResponse> BulkList = response.body().getTransactionResponse();

                                String txnId = "";

                                transactionDetailsList = new ArrayList<TransactionDetails>();

                                int amountTemp = amountNumber;

                                for (int i = 0; i < BulkList.size(); i++) {
                                    TransactionDetails transactionDetails = new TransactionDetails();
                                    transactionDetails.setTracking_no(BulkList.get(i).getTxnId());
                                    transactionDetails.setTransaction_type(transactionMode);

                                    if (amountTemp > 5000) {
                                        amountTemp = amountTemp - 5000;
                                        transactionDetails.setTransaction_amount("5000");
                                    } else {
                                        transactionDetails.setTransaction_amount(String.valueOf(amountTemp));
                                    }

                                    transactionDetailsList.add(transactionDetails);

                                    if (i == 0) {
                                        txnId = BulkList.get(i).getTxnId();
                                    } else {
                                        txnId = txnId + "," + BulkList.get(i).getTxnId();
                                    }
                                }

                                transactionSuccessDialog(txnId, BulkList.get(1).getStatusDesc(), transactionDetailsList);
                            } else {

                                try {
                                    hideLoader();
                                    Gson gson = new Gson();
                                    MyErrorMessage message = gson.fromJson(response.errorBody().charStream(), MyErrorMessage.class);

                                    try {
                                        showPaymentFailedDialog(message.getMessage());
                                    } catch (Exception e) {

                                        showPaymentFailedDialog("Transaction failed");
                                    }
                                } catch (Exception e) {

                                }
                            }
                        } else {
                            hideLoader();
                            showPaymentFailedDialog("  You can see your transaction details on fund transfer report section.");
                        }
                    } catch (Exception exc) {
                        //exc.printStackTrace();
                        hideLoader();
                        showPaymentFailedDialog("You can see your transaction details on fund transfer report section.");

                    }
                }

                @Override
                public void onFailure(Call<BulkTransactionResponse> call, Throwable t) {
                    hideLoader();
                    showPaymentFailedDialog(" You can see your transaction details on fund transfer report section.");
                }
            });


        }

    }

    public void clearViewList() {
        //mobile_number.setText("");
        amount_transfer.setText("");
        customer_name.setText("");
        otp.setText("");

        ifc_code.setText("");
        acc_no.setText("");
        ifccode.setText("");
        bene_name.setText("");
        bank_name_autocomplete.setText("");
        ifccode.setText("");
    }

 /*   @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        timer_handler.removeCallbacksAndMessages(null);
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer_handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer_handler.removeCallbacksAndMessages(null);
    }

    public void startTimer() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        timer_handler.removeCallbacksAndMessages(null);

                        if (!isFinishing()) {
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }

    public void showSessionAlert() {

        pd.dismiss();

        AlertDialog.Builder alertbuilderupdate;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new AlertDialog.Builder(this);
        }

        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(getActivity(), LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
         }).show();

    }*/


    public void showAlert(String msg) {
        try {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Alert!!");
            builder.setMessage(msg);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });
            androidx.appcompat.app.AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showTransactionDetails(Activity activity) {
        try {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.transaction_dmt_details_layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView aadhar_number = (TextView) dialog.findViewById(R.id.bene_account_tv);
            TextView rref_num = (TextView) dialog.findViewById(R.id.tracking_no_tv);
            TextView card_transaction_type = (TextView) dialog.findViewById(R.id.transaction_type_tv);
            aadhar_number.setText(bene_acc_no_str);
            rref_num.setText(trackingID);
            card_transaction_type.setText(transactionMode);

            Button dialogBtn_close = (Button) dialog.findViewById(R.id.close_Btn);
            dialogBtn_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();

                }
            });

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getActivity().getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getActivity().getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }
}