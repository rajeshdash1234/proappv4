package com.isuisudmt.fragment;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.isuisudmt.Constants;
import com.isuisudmt.CustomThemes;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.matm.MatmOptionActivity;
import com.isuisudmt.matm.MicroAtmContract;
import com.isuisudmt.matm.MicroAtmPresenter;
import com.isuisudmt.matm.MicroAtmReportActivity;
import com.isuisudmt.matm.MicroAtmRequestModel;
import com.isuisudmt.matm.MicroAtmResponse;
import com.isuisudmt.matm.UserInfoModel;
import com.isuisudmt.utility.ConnectionDetector;
import com.matm.matmsdk.MPOS.PosServiceActivity;
import com.matm.matmsdk.Utils.EnvData;
import com.matm.matmsdk.Utils.SdkConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;


public class MatmbFragment extends Fragment implements View.OnClickListener, MicroAtmContract.View {


    String strTransType = "Cash Withdrawal";

    RadioGroup rg_trans_type;
    RadioButton rb_cw, rb_be;

    FrameLayout matm_cashwithdraw, matm_reqbalance;
    TextInputEditText amnt_txt;

    Button submit, pair, reportBtn;
    SessionManager session;
    TextView enter_text;
    AlertDialog deleteDialog;
    String encData;
    String authentication;
    String _token = "", _admin = "";
    public MicroAtmPresenter microAtmPresenter;
    MicroAtmRequestModel microAtmRequestModel;
    Handler timer_handler;
    String _userName;
    boolean session_logout = false;
    boolean matm_transaction_intent = false;
    AlertDialog deleteDialogg;
    ProgressDialog dialog;
    private FirebaseAnalytics firebaseAnalytics;
    private View MatmView ;
    private Context mContext;


    public MatmbFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static Fragment getInstance() {
        return new MatmbFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        MatmView = inflater.inflate(R.layout.fragment_matmb, container, false);
        mContext = getActivity();
        new CustomThemes(getActivity());

        firebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
        dialog = new ProgressDialog(getActivity());
      //  Toolbar toolbar = findViewById(R.id.toolbar);
      //  toolbar.setTitle("micro ATM");
      //  setSupportActionBar(toolbar);

       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setDisplayShowHomeEnabled(true);
        microAtmPresenter = new MicroAtmPresenter(this);

       /* toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/


        matm_cashwithdraw = MatmView.findViewById(R.id.matm_cashwithdraw);
        matm_reqbalance = MatmView.findViewById(R.id.matm_reqbalance);
        amnt_txt = MatmView.findViewById(R.id.price);
        submit = MatmView.findViewById(R.id.submit);
        pair = MatmView.findViewById(R.id.pair);
        rb_cw =MatmView. findViewById(R.id.rb_cw);
        rb_be = MatmView.findViewById(R.id.rb_be);
        rg_trans_type = MatmView.findViewById(R.id.rg_trans_type);
        enter_text = MatmView.findViewById(R.id.enter_text);
        reportBtn = MatmView.findViewById(R.id.reportBtn);
        session = new SessionManager(getActivity());

        HashMap<String, String> _user = session.getUserDetails();
        _token = _user.get(SessionManager.KEY_TOKEN);
        _admin = _user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);

        rg_trans_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_cw:
                        strTransType = "Cash Withdrawal";
                        amnt_txt.setText("");
                        amnt_txt.setEnabled(true);
                        amnt_txt.setVisibility(View.VISIBLE);
                        enter_text.setVisibility(View.VISIBLE);
                        amnt_txt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case R.id.rb_be:
                        strTransType = "Balance Enquiry";
                        amnt_txt.setText("");
                        amnt_txt.setVisibility(View.GONE);
                        enter_text.setVisibility(View.GONE);
                        amnt_txt.setClickable(false);
                        amnt_txt.setEnabled(false);
                        break;
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                ConnectionDetector cd = new ConnectionDetector(
                        mContext);
                if (cd.isConnectingToInternet()) {
                    if (rb_cw.isChecked()) {
                        String amountString = Objects.requireNonNull(amnt_txt.getText().toString().trim());
                        if (amountString.equalsIgnoreCase("")) {
//                        if (amountString.length() == 0) {
//                            Toast.makeText(MATMActivity.this, "Please enter minimum Rs 100 ", Toast.LENGTH_SHORT).show();
                            amnt_txt.setError("Please enter minimum Rs 100 ");
                        } else if (Double.valueOf(amountString) < 100) {
//                            Toast.makeText(MATMActivity.this, "Please enter minimum Rs 100 ", Toast.LENGTH_SHORT).show();
                            amnt_txt.setError("Please enter minimum Rs 100 ");
                        } else if (amountString.startsWith("0")) {
                            amnt_txt.setError("Please don't enter zero at begining !");
                        } else {
                            //showChooseWalletOption();
                            Intent intent = new Intent(mContext, MatmOptionActivity.class);
                            intent.putExtra("AMOUNT",amnt_txt.getText().toString().trim());
                            intent.putExtra("AEPSTRANS_TYPE",strTransType);
                            startActivity(intent);

                        }
                    } else {
                        //showChooseWalletOption();
                        Intent intent = new Intent(mContext, MatmOptionActivity.class);
                        intent.putExtra("AMOUNT",amnt_txt.getText().toString().trim());
                        intent.putExtra("AEPSTRANS_TYPE",strTransType);
                        startActivity(intent);

                    }
                } else {
                    Toast.makeText(mContext, "No internet connection", Toast.LENGTH_LONG).show();
                }
            }
        });
        pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("MATM_PAIR", _userName);
            }
        });

        reportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFBAnalytics("MATM_REPORT_BUTTON", _userName);
                Intent intent = new Intent(mContext, MicroAtmReportActivity.class);
                startActivity(intent);


            }
        });
        return MatmView;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null & resultCode == RESULT_OK) {
            if (requestCode == SdkConstants.REQUEST_CODE) {
                String response = data.getStringExtra(SdkConstants.responseData);
                System.out.println("Response:" + response);
            } else if (requestCode == SdkConstants.MATM_REQUEST_CODE) {
                String response = data.getStringExtra(SdkConstants.responseData);
                System.out.println("Response:" + response);
            }
        }
    }


    /*public void showChooseWalletOption() {
        try {
            LayoutInflater factory = LayoutInflater.from(this);
            final View deleteDialogView = factory.inflate(R.layout.dialog_matm_info, null);
            deleteDialog = new AlertDialog.Builder(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen).create();
            deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            deleteDialog.setView(deleteDialogView);
            deleteDialog.setCancelable(false);

            RelativeLayout item1 = deleteDialogView.findViewById(R.id.item1);
            RelativeLayout item2 = deleteDialogView.findViewById(R.id.item2);
            RelativeLayout item3 = deleteDialogView.findViewById(R.id.item3);
            TextView service_txt_1 = deleteDialogView.findViewById(R.id.service_txt_1);
            TextView service_txt_2 = deleteDialogView.findViewById(R.id.service_txt_2);

            ImageView back_image = deleteDialogView.findViewById(R.id.close_btn);

            if (getPackageName().equalsIgnoreCase("com.bharatmoney.bharatmoneylite")) {
                item1.setVisibility(View.GONE);
                item2.setVisibility(View.GONE);
            }else{
                item3.setVisibility(View.GONE);
            }



            if (Constants.user_feature_array != null) {
                if (!Constants.user_feature_array.contains("33")) {
                    item1.setBackgroundResource(R.color.service_unavailable_col);
                    service_txt_1.setVisibility(View.VISIBLE);
                    item1.setEnabled(false);
                }


                if (!Constants.user_feature_array.contains("48")) {
                    item2.setBackgroundResource(R.color.service_unavailable_col);
                    service_txt_2.setVisibility(View.VISIBLE);
                    item2.setEnabled(false);
                }

                if (!Constants.user_feature_array.contains("48")) {

                    item3.setEnabled(false);
                }


            }


            item1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();

                    refreshToken("MATM1");

                    item1.setBackground(getResources().getDrawable(R.drawable.ripple_effect));


                }
            });
            item2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    refreshToken("MATM2");

                    item2.setBackground(getResources().getDrawable(R.drawable.ripple_effect));

                }
            });
            item3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    refreshToken("MATM2");

                    item2.setBackground(getResources().getDrawable(R.drawable.ripple_effect));

                }
            });

            back_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });
            deleteDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                        try {
                            if (!isFinishing() && !isDestroyed()) {
                                if (deleteDialog != null && deleteDialog.isShowing()) {
                                    deleteDialog.dismiss();
                                } else {
                                    finish();
                                }
                            }
                        } catch (Exception e) {

                        }
                    return false;
                }
            });
            deleteDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = mContext.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public void redirectToPlayStore() {
//        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.matm.matmservices&hl=en_US");
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.matm.matmservice&hl=en_US");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
//                    Uri.parse("https://play.google.com/store/apps/details?id=com.matm.matmservices&hl=en_US")));
                    Uri.parse("https://play.google.com/store/apps/details?id=com.matm.matmservice&hl=en_US")));
        }
    }

    public void showAlert(Context context) {
        try {

            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(context);
            }
            alertbuilderupdate.setCancelable(false);
            String message = "Please download the MATM service app from the playstore.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            redirectToPlayStore();
                        }
                    })
                    .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                        }
                    });
//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();

        } catch (Exception e) {

        }

    }

    private void callMatm2(String _token) {
        try {
            if (!getActivity().isFinishing() && !getActivity().isDestroyed()) {
                if (deleteDialog != null) {
                    deleteDialog.dismiss();
                }
            }
            //for cash withdrawl------
            if (rb_cw.isChecked()) {
                SdkConstants.transactionType = SdkConstants.cashWithdrawal;
                SdkConstants.transactionAmount = amnt_txt.getText().toString();

                SdkConstants.applicationType = "CORE";
                SdkConstants.tokenFromCoreApp = _token;
                SdkConstants.userNameFromCoreApp = EnvData.UserName;
                SdkConstants.paramB = "";
                SdkConstants.paramC = "";
                SdkConstants.loginID = "";
                matm_transaction_intent = true;
                Intent intent = new Intent(mContext, PosServiceActivity.class);
                startActivity(intent);


            }
            if (rb_be.isChecked()) {


                SdkConstants.transactionType = SdkConstants.balanceEnquiry;
                SdkConstants.transactionAmount = "0";

                SdkConstants.applicationType = "CORE";
                SdkConstants.tokenFromCoreApp = _token;
                SdkConstants.userNameFromCoreApp = EnvData.UserName;
                SdkConstants.paramB = "";
                SdkConstants.paramC = "";
                SdkConstants.loginID = "";
                matm_transaction_intent = true;
                Intent intent = new Intent(mContext, PosServiceActivity.class);
                startActivity(intent);
            }
        } catch (Exception e) {

        }
    }

    private boolean validate() {
        if ((!rb_cw.isChecked()) && (!rb_be.isChecked())) {
            showOneBtnDialog(mContext, "Info", "Please select Transaction Type!", false);
            return false;
        }
        if (rb_cw.isChecked()) {
            if (amnt_txt.getText().toString().trim().length() == 0) {
                String msg = "";
                if (rb_cw.isChecked()) {
                    msg = "Amount";
                }
                showOneBtnDialog(mContext, "Info", "Please enter " + msg + " !", false);
                return false;
            }

            if (amnt_txt.getText().toString().trim().length() != 0) {
                if (Long.valueOf(amnt_txt.getText().toString().trim()) < 100) {


                    String msg = "";
                    if (rb_cw.isChecked()) {
                        msg = "Amount";
                    }
                    showOneBtnDialog(mContext, "Info", "Minimum amount should be Rs.100 for transaction", false);

                    return false;

                }
            }
        }
        return true;
    }

    private void showOneBtnDialog(final Context mContext, String title, String msg,
                                  boolean cancelable) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(title);
            builder.setMessage(msg);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    hideKeyboard();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(cancelable);
            dialog.show();
        } catch (Exception e) {

        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(amnt_txt.getWindowToken(), 0);
    }

    public void apiCalling() {
        microAtmRequestModel = new MicroAtmRequestModel(amnt_txt.getText().toString(), getServiceID(), "mobile");
        microAtmPresenter.performRequestData(_token, microAtmRequestModel);

    }

    public void balanceEnquiryApiCalling() {
        microAtmRequestModel = new MicroAtmRequestModel("0", getServiceID(), "mobile");
        microAtmPresenter.performRequestData(_token, microAtmRequestModel);

    }

    public String getServiceID() {
        String clientRefID = "";

        if (rb_cw.isChecked())
            clientRefID = Constants.SERVICE_MICRO_CW;
        if (rb_be.isChecked())
            clientRefID = Constants.SERVICE_MICRO_BE;

        return clientRefID;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void checkRequestCode(String status, String message, MicroAtmResponse
            microAtmResponse) {
        if (status != null && !status.matches("")) {
            authentication = microAtmResponse.getAuthentication();
            encData = microAtmResponse.getEncData();
            String trans_type = "";
            boolean cash_withdrawal = true;
            if (rb_cw.isChecked()) {
                cash_withdrawal = true;
                trans_type = "cash";
            } else {
                cash_withdrawal = false;
                trans_type = "balance";
            }
            matm_transaction_intent = true;
            Intent intent = new Intent(Intent.ACTION_DATE_CHANGED);
            PackageManager manager = getActivity().getPackageManager();
//            intent = manager.getLaunchIntentForPackage("com.matm.matmservices");
            intent = manager.getLaunchIntentForPackage("com.matm.matmservice");
            intent.putExtra("RequestData", encData);
            intent.putExtra("HeaderData", authentication);
            intent.putExtra("ReturnTime", 5);
            intent.putExtra("IS_PAIR_DEVICE", false);
            intent.putExtra("Flag", "transaction");
            intent.putExtra("TransactionType", trans_type);
            intent.putExtra("client_id", "");
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            startActivityForResult(intent, 1);

      /*      Intent intent = new Intent(getBaseContext(), com.finopaytech.finosdk.activity.MainTransactionActivity.class);
            intent.putExtra("RequestData", encData);
            intent.putExtra("HeaderData", authentication);
            intent.putExtra("ReturnTime", 5);// Application return time in second
            startActivityForResult(intent, 1);*/
        } else {
            if (!getActivity().isFinishing()) {
                Util.showAlert(mContext, getResources().getString(R.string.fail_error), message);
            }
        }
    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {

    }

    @Override
    public void checkEmptyFields() {

    }

    public void showLoader() {
        try {
            if (!getActivity().isFinishing() && !getActivity().isDestroyed()) {
                dialog = new ProgressDialog(mContext);
                dialog.setMessage("please wait..");
                dialog.setCancelable(false);
                dialog.show();
            }
        } catch (Exception e) {

        }
    }

    public void hideLoader() {
        try {
            if (!getActivity().isFinishing() && !getActivity().isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {

        }
    }


    @Override
    public void onResume() {
        super.onResume();
//        startTimer();

        getUserId(_token, "https://mobile.9fin.co.in/user/user_details");
        /*if(_userName!=null) {
            matm_transaction_intent = false;
            if(session_logout==false) {
                checkSessionExistance(_userName, true);
            }
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (!getActivity().isFinishing() && !getActivity().isDestroyed()) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {

        }
    }

    /* @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(!matm_transaction_intent) {
            if (timer_handler != null) {
                timer_handler.removeCallbacksAndMessages(null);
            }
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false && matm_transaction_intent ==false) {
            checkSessionExistance(_userName,false);
        }
        if(matm_transaction_intent){
            startTimer();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(!matm_transaction_intent) {
            if (timer_handler != null) {
                timer_handler.removeCallbacksAndMessages(null);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!matm_transaction_intent) {
            if (timer_handler != null) {
                timer_handler.removeCallbacksAndMessages(null);
            }
        }
        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
    }

    public void startTimer(){
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(BluetoothConnectorActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(MATMActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(MATMActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(MATMActivity.this, user_name, status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(MATMActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/

    public void getUserId(String token, String urlString) {
        AndroidNetworking.get(urlString)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String id = obj.getString("id");
                            EnvData.user_id = id;
                            System.out.println(obj);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        System.out.println("Error  " + anError.getErrorDetail());
                    }
                });
    }

    private void checkAddressStatus() {
        showLoader();
        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v81")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            checkAddressStatus1("http://matmtest.iserveu.website/viewUserAddress");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(mContext, "Service is unavailable for this user, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void checkAddressStatus1(String url) {
        AndroidNetworking.get(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (obj.has("response")) {
                                JSONObject res_obj = obj.getJSONObject("response");
                                String pin = res_obj.getString("pincode");
                                String state = res_obj.getString("state");
                                String city = res_obj.getString("city");
                                // boolean bio_auth_status = res_obj.getBoolean("bioauth");
                                if (pin.equalsIgnoreCase("null") || pin.length() == 0 || pin == null || state.length() == 0 || state.length() > 2) {
                                    //if(!bio_auth_status){
                                    /*JSONObject jsonObject = new JSONObject();

                                    if(postalCode.length()!=0){
                                        pincode = Integer.valueOf(postalCode);
                                    }
                                    jsonObject.put("pin",pincode);
                                    getAddressFromPin(jsonObject);*/
                                    // showFingurePrintDialog();
                                    hideLoader();
                                    showPinCodeDialog();
                                } else {
                                    hideLoader();
                                    callMatm2(_token);
                                }
                            } else {

                               /* JSONObject jsonObject = new JSONObject();
                                if(postalCode.length()!=0){
                                    pincode = Integer.valueOf(postalCode);
                                }
                                jsonObject.put("pin",pincode);
                                getAddressFromPin(jsonObject);*/
                                hideLoader();
                                showPinCodeDialog();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                            Toast.makeText(mContext, "Something went wrong, please contact our help desk for details.", Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(mContext, "Something went wrong, please contact our help desk for details.", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getAddressFromPin(JSONObject obj) {
        showLoader();
        AndroidNetworking.post("https://us-central1-iserveustaging.cloudfunctions.net/pincodeFetch/api/v1/getCitystateAndroid")
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());
                            JSONObject data_obj = obj.getJSONObject("data");
                            String status = data_obj.getString("status");
                            if (status.equalsIgnoreCase("success")) {
                                JSONObject data_pin = data_obj.getJSONObject("data");
                                String pincode = data_pin.getString("pincode");
                                String state = data_pin.getString("state");
                                String city = data_pin.getString("city");
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("state", getShortState(state));
                                jsonObject.put("pincode", pincode);
                                jsonObject.put("city", city);
                                /*String lat="0.0";
                                String lng="0.0";

                                if(mylocation!=null){
                                    lat = String.valueOf(mylocation.getLatitude());
                                    lng = String.valueOf(mylocation.getLongitude());
                                }
                                jsonObject.put("latLong",lat+","+lng);*/
                                setAddress(jsonObject);
                            } else {
                                hideLoader();
                                Toast.makeText(mContext, "Invalid area pin, please try after sometimes", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                        Toast.makeText(mContext, "Invalid area pin, please try again.", Toast.LENGTH_SHORT).show();

                    }
                });
    }

    private void setAddress(JSONObject objj) {

        AndroidNetworking.get("https://vpn.iserveu.tech/generate/v82")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            setAddress1(objj, "http://matmtest.iserveu.website/updateUserAddress");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }


    private void setAddress1(JSONObject obj, String url) {

        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", _token)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        try {
                            // {"statusDesc":"connected","status":"0"}
                            JSONObject obj = new JSONObject(response.toString());

                            String status = obj.getString("status");
                            String statusDesc = obj.getString("statusDesc");
                            if (status.equalsIgnoreCase("0")) {
                                if (deleteDialogg != null) {
                                    deleteDialogg.dismiss();
                                }
                                // showFingurePrintDialog();
                                callMatm2(_token);
                            } else {
                                Toast.makeText(mContext, statusDesc, Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            // e.printStackTrace();
                            hideLoader();
                        } catch (Exception e) {
                            // e.printStackTrace();
                            hideLoader();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        hideLoader();
                    }
                });
    }

    /**
     * AEPS2 Location Auth
     */
    public void showPinCodeDialog() {

        setFBAnalytics("MATM_SHOW_PIN", _userName);

        LayoutInflater factory = LayoutInflater.from(mContext);
        final View deleteDialogView = factory.inflate(R.layout.dialog_otp_info, null);
        deleteDialogg = new AlertDialog.Builder(mContext).create();
        deleteDialogg.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        deleteDialogg.setView(deleteDialogView);
        deleteDialogg.setCancelable(false);
        final EditText otp_edt = deleteDialogView.findViewById(R.id.otp_edt);

        deleteDialogView.findViewById(R.id.process_profile_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic
                try {
                    if (otp_edt.getText().toString().length() != 0 && !otp_edt.getText().toString().contains(".")) {
                        //verifyOTPMobile(mobileno,otp_edt.getText().toString());
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("pin", Integer.valueOf(otp_edt.getText().toString()));
                            getAddressFromPin(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Toast.makeText(mContext, "Please insert area pin or check pin", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }

            }
        });

        deleteDialogView.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialogg.dismiss();
            }
        });
        deleteDialogg.show();

    }

    public String getShortState(String state) {
        try {
            if (state.equalsIgnoreCase("Andhra Pradesh")) {
                return "AP";
            } else if (state.equalsIgnoreCase("Arunachal Pradesh")) {
                return "AR";
            } else if (state.equalsIgnoreCase("Assam")) {
                return "AS";
            } else if (state.equalsIgnoreCase("Bihar")) {
                return "BR";
            } else if (state.equalsIgnoreCase("Chandigarh")) {
                return "CH";
            } else if (state.equalsIgnoreCase("Chhattisgarh")) {
                return "CG";
            } else if (state.equalsIgnoreCase("Dadra and Nagar Haveli")) {
                return "DN";
            } else if (state.equalsIgnoreCase("Daman & Diu")) {
                return "DD";
            } else if (state.equalsIgnoreCase("Delhi")) {
                return "DL";
            } else if (state.equalsIgnoreCase("Goa")) {
                return "GA";
            } else if (state.equalsIgnoreCase("Gujarat")) {
                return "GJ";
            } else if (state.equalsIgnoreCase("Haryana")) {
                return "HR";
            } else if (state.equalsIgnoreCase("Himachal Pradesh")) {
                return "HP";
            } else if (state.equalsIgnoreCase("Jammu & Kashmir")) {
                return "JK";
            } else if (state.equalsIgnoreCase("Karnataka")) {
                return "KA";
            } else if (state.equalsIgnoreCase("Kerala")) {
                return "KL";
            } else if (state.equalsIgnoreCase("Lakshadweep")) {
                return "LD";
            } else if (state.equalsIgnoreCase("Madhya Pradesh")) {
                return "MP";
            } else if (state.equalsIgnoreCase("Maharashtra")) {
                return "MH";
            } else if (state.equalsIgnoreCase("Manipur")) {
                return "MN";
            } else if (state.equalsIgnoreCase("Meghalaya")) {
                return "ML";
            } else if (state.equalsIgnoreCase("Mizoram")) {
                return "MZ";
            } else if (state.equalsIgnoreCase("Nagaland")) {
                return "NL";
            } else if (state.equalsIgnoreCase("Orissa")) {
                return "OR";
            } else if (state.equalsIgnoreCase("Odisha")) {
                return "OD";
            } else if (state.equalsIgnoreCase("Puducherry")) {
                return "PY";
            } else if (state.equalsIgnoreCase("Punjab")) {
                return "PB";
            } else if (state.equalsIgnoreCase("Rajasthan")) {
                return "RJ";
            } else if (state.equalsIgnoreCase("Sikkim")) {
                return "SK";
            } else if (state.equalsIgnoreCase("Tamil Nadu")) {
                return "TN";
            } else if (state.equalsIgnoreCase("Telangana")) {
                return "TG";
            } else if (state.equalsIgnoreCase("Tripura")) {
                return "TR";
            } else if (state.equalsIgnoreCase("Uttar Pradesh")) {
                return "UP";
            } else if (state.equalsIgnoreCase("Uttarakhand (Uttranchal)")) {
                return "UK";
            } else if (state.equalsIgnoreCase("West Bengal")) {
                return "WB";
            } else {
                return "BR";
            }
        } catch (Exception e) {
            return "BR";
        }
    }


    //For refresh token

    private String refreshToken(String type) {
//        showLoader();
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v87")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
//                            hideLoader();
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            refreshTokenEncodedURL(_token, encodedUrl, type);

                        } catch (JSONException e) {
                            e.printStackTrace();
//                            hideLoader();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
//                        hideLoader();
                        Toast.makeText(mContext, "Please try again..check issue regarding token ", Toast.LENGTH_LONG).show();
                    }
                });
        return _token;
    }

    private void refreshTokenEncodedURL(String tokenStr, String encodedUrl, String type) {
        try {
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject respObj = new JSONObject(response.toString());
                                _token = respObj.getString("token");
                                _admin = respObj.getString("adminName");
                                if (!_token.equalsIgnoreCase("")) {

                                    session.createLoginSession(_token, _admin);

                                    HashMap<String, String> _user = session.getUserDetails();
                                    _token = _user.get(SessionManager.KEY_TOKEN);
                                    _admin = _user.get(SessionManager.KEY_ADMIN);

                                    //-------  For MATM1 -------

                                    if (type.equalsIgnoreCase("MATM1")) {

                                        boolean installed = appInstalledOrNot("com.matm.matmservice");
                                        if (installed) {
                                            if (validate()) {
                                                if (rb_cw.isChecked()) {
                                                    apiCalling();
                                                } else if (rb_be.isChecked()) {
                                                    balanceEnquiryApiCalling();
                                                }
                                            }
                                        } else {
                                            try {
                                                showAlert(mContext);
                                                System.out.println("App is not installed on your phone");
                                            } catch (Exception e) {

                                            }

                                        }

                                    } else {

                                        //-------- For MATM2 ----------

                                        callMatm2(_token);


                                    }

                                } else {
                                    Toast.makeText(mContext, "Error in response of token !", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            System.out.println("ERROR: " + anError.getErrorDetail());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void setFBAnalytics(String propertyKey, String propertyValue) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getActivity().getPackageName());
        //Logs an app event.
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        //Sets whether analytics collection is enabled for this app on this device.
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
        firebaseAnalytics.setSessionTimeoutDuration(500);
        firebaseAnalytics.setDefaultEventParameters(bundle);
        //Sets the user ID property.
        firebaseAnalytics.setUserId(getActivity().getPackageName());
        //Sets a user property to a given value.
        firebaseAnalytics.setUserProperty(propertyKey, propertyValue);
    }


}

