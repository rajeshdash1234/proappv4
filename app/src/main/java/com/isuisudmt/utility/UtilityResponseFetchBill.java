package com.isuisudmt.utility;

public class UtilityResponseFetchBill {

    private String statusDesc;

    private String response;

    private String operatorTransactionId;

    private String operatorDesc;

    private String transactionId;

    private String status;

    public String getStatusDesc ()
    {
        return statusDesc;
    }

    public void setStatusDesc (String statusDesc)
    {
        this.statusDesc = statusDesc;
    }

    public String getResponse ()
    {
        return response;
    }

    public void setResponse (String response)
    {
        this.response = response;
    }

    public String getOperatorTransactionId ()
    {
        return operatorTransactionId;
    }

    public void setOperatorTransactionId (String operatorTransactionId)
    {
        this.operatorTransactionId = operatorTransactionId;
    }

    public String getOperatorDesc ()
    {
        return operatorDesc;
    }

    public void setOperatorDesc (String operatorDesc)
    {
        this.operatorDesc = operatorDesc;
    }

    public String getTransactionId ()
    {
        return transactionId;
    }

    public void setTransactionId (String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusDesc = "+statusDesc+", response = "+response+", operatorTransactionId = "+operatorTransactionId+", operatorDesc = "+operatorDesc+", transactionId = "+transactionId+", status = "+status+"]";
    }

}
