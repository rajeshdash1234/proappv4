package com.isuisudmt.utility;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface UtilityApiSubmit {
    @POST()
    Call<UtilityResponseSubmit> getUtilitySubmit(@Header("Authorization") String token, @Body UtilityRequestSubmit utilityRequestSubmit, @Url String url);
}
