package com.isuisudmt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchraisedRequestModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("transferType")
    @Expose
    private String transferType;
    @SerializedName("bankRefId")
    @Expose
    private String bankRefId;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("requestFrom")
    @Expose
    private String requestFrom;
    @SerializedName("requestTo")
    @Expose
    private String requestTo;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("depositedDate")
    @Expose
    private Long depositedDate;
    @SerializedName("requestedTime")
    @Expose
    private Long requestedTime;
    @SerializedName("approveOrDecline_Time")
    @Expose
    private String approveOrDeclineTime;
    @SerializedName("receiptDownloadUrl")
    @Expose
    private Object receiptDownloadUrl;
    @SerializedName("depositedBankName")
    @Expose
    private String depositedBankName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getBankRefId() {
        return bankRefId;
    }

    public void setBankRefId(String bankRefId) {
        this.bankRefId = bankRefId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRequestFrom() {
        return requestFrom;
    }

    public void setRequestFrom(String requestFrom) {
        this.requestFrom = requestFrom;
    }

    public String getRequestTo() {
        return requestTo;
    }

    public void setRequestTo(String requestTo) {
        this.requestTo = requestTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getDepositedDate() {
        return depositedDate;
    }

    public void setDepositedDate(Long depositedDate) {
        this.depositedDate = depositedDate;
    }

    public Long getRequestedTime() {
        return requestedTime;
    }

    public void setRequestedTime(Long requestedTime) {
        this.requestedTime = requestedTime;
    }

    public String getApproveOrDeclineTime() {
        return approveOrDeclineTime;
    }

    public void setApproveOrDeclineTime(String approveOrDeclineTime) {
        this.approveOrDeclineTime = approveOrDeclineTime;
    }

    public Object getReceiptDownloadUrl() {
        return receiptDownloadUrl;
    }

    public void setReceiptDownloadUrl(Object receiptDownloadUrl) {
        this.receiptDownloadUrl = receiptDownloadUrl;
    }

    public String getDepositedBankName() {
        return depositedBankName;
    }

    public void setDepositedBankName(String depositedBankName) {
        this.depositedBankName = depositedBankName;
    }
}
