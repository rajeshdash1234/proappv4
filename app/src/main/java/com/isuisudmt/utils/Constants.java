package com.isuisudmt.utils;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * This class represents the constants used in the app.
 *
 * @author Subhalaxmi Panda
 * @date 23/06/18.
 */
public class Constants {
    /**
     * Base Url of the server
     */
    public static final String BASE_URL = "https://itpl.iserveu.tech/";
    //    public static final String BASE_URL = "http://35.200.175.157:8080";
//    public static final String BASE_URL = "http://35.220.171.29:8080";
    //public static final String recharge = "https://iserveu.xyz/";
    public static final String recharge = "https://itpl.iserveu.tech/";

    public static final String URL_WALLET2 = "https://us-central1-creditapp-29bf2.cloudfunctions.net/wallet_2_latest";
    public static final String URL_UPI = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/all_transaction_report";
    public static final String URL_COMMISION = "https://us-central1-creditapp-29bf2.cloudfunctions.net/commission2_bigquery_report";
    public static final String URL_ADD_MONEY = "https://itpl.iserveu.tech/fetchallpaymentrequests.json";
    public static final String URL_CASHOUT = "https://wallet2-cashout-prod-vn3k2k7q7q-uc.a.run.app/report?jack=RS2fX3wyAwJJ1o2gJqk%2FWEjqAUkxSUbc38tHO7AqMrw%3D&expiry=1611926097938&jill=MTE2MTE5MjYwODc5MzhVaW8x&jojo=U25laGFzb255";

    public static final String BASE_URL_FUND_TRANSFER = "https://habizglobal.in/";
    public static final int REQUEST_FOR_ACTIVITY_BALANCE_ENQUIRY_CODE = 1003;
    public static final int REQUEST_FOR_ACTIVITY_CASH_DEPOSIT_CODE = 1001;
    public static final int REQUEST_FOR_ACTIVITY_CASH_WITHDRAWAL_CODE = 1002;
    public static final int BALANCE_RELOAD = 1004;
    public static final String IIN_KEY = "IIN_KEY";
    public static final String USB_DEVICE = "USB_DEVICE";
    public static final String TRANSACTION_STATUS_KEY = "TRANSACTION_STATUS_KEY";
    public static final String MICRO_ATM_TRANSACTION_STATUS_KEY = "MICRO_ATM_TRANSACTION_STATUS_KEY";
    public static final String AEPS_2_TRANSACTION_STATUS_KEY = "AEPS_2_TRANSACTION_STATUS_KEY";
    public static final String TRANSACTION_STATUS_KEY_SDK = "TRANSACTION_STATUS_KEY_SDK";
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String USER_TOKEN_KEY = "USER_TOKEN_KEY";
    public static final String NEXT_FRESHNESS_FACTOR = "NEXT_FRESHNESS_FACTOR";
    public static final String EASY_AEPS_PREF_KEY = "EASY_AEPS_PREF_KEY";
    public static final String EASY_AEPS_USER_LOGGED_IN_KEY = "EASY_AEPS_USER_LOGGED_IN_KEY";
    public static final String EASY_AEPS_USER_NAME_KEY = "EASY_AEPS_USER_NAME_KEY";
    public static final String EASY_AEPS_PASSWORD_KEY = "EASY_AEPS_PASSWORD_KEY";
    public static final String TEST_USER_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTI5OTkyMDQxNTE1LCJleHAiOjE1MzAwMjgwNDF9.Se0zZhaoemx6eeAVlE_9N_LqkhylDgsJS8f1-5Y4uIYIfFV_7Gst5JhwnA-ogjs4whk9x9VnNvBL8T-AVkctnQ";
    public static final String RETURN_URL = "http://10.15.20.131:8023/PG/TestPage.aspx";
    public static final String VERSION = "1000";
    public static final String SERVICE_AEPS_TS = "AEPStransactionStatus";
    public static final String SERVICE_AEPS_CW = "AEPScashWithdrawal";
    public static final String SERVICE_AEPS_BE = "AEPSbalanceEnquiry";
    public static final String SERVICE_MICRO_TS = "MATMtransactionStatus";
    public static final String SERVICE_MICRO_CW = "MATMcashWithdrawal";
    public static final String SERVICE_MICRO_BE = "MATMbalanceEnquiry";
    public static final String CLIENT_REQUEST_ENCRYPTION_KEY = "11e9903b-7cde-4738-96fe-c8295c8232de";
    //    public static final String CLIENT_REQUEST_ENCRYPTION_KEY = "5c542938-904a-49fa-b981-10203c7db1f1";
    public static final String CLIENT_HEADER_ENCRYPTION_KEY = "982b0d01-b262-4ece-a2a2-45be82212ba1";
    public static final String MERCHANT_ID = "8270629964";
    public static final String AUTHKEY = "333-444-555";
    public static final String CLIENTID = "8";
    public static final int RELOADREPORTS = 1005;
    public static String USER_NAME = "";
    public static String ADMIN_NAME = "";

    public static String USER_SHOP_NAME = "";
    public static String USER_MOBILE_NO = "";

    public static String USER_VERIFICATION_STATUS = "unverified";

    public static String VERIFIED_USERID = "";
    public static String VERIFIED_USERPHONE = "";
    public static String VERIFIED_USER_MPIN = "";
    public static String VERIFIED_USER_PWD = "";
    public static String VERIFIED_USER_EMAIL = "";
    public static String VERIFIED_USER_VIDEO = "";
    public static String VERIFIED_USER_LOC = "";
    public static String VERIFIED_USER_LACT = "";
    public static String VERIFIED_USER_LONG = "";

    public static boolean USER_WALLET_2_ACTIVE_STATUS = false;
    public static boolean USER_WALLET_3_ACTIVE_STATUS = false;

    public static BluetoothDevice bluetoothDevice = null;
    public static String SHOP_NAME = "";
    public static String BRAND_NAME = "";
    public static boolean intentTo = false;
    public static String APP_UNIQUE_ID = "1.0.1";
    public String LIVE_CATEGORY_LIST =
                    "[{\"catId\":\"1\",\"catName\":\"Broadband Postpaid\"}," +
                    "{\"catId\":\"2\",\"catName\":\"DTH\"}," +
                    "{\"catId\":\"3\",\"catName\":\"Electricity\"}," +
                    "{\"catId\":\"4\",\"catName\":\"Gas\"}," +
                    "{\"catId\":\"5\",\"catName\":\"Landline Postpaid\"}," +
                    "{\"catId\": \"6\",\"catName\":\"Mobile Postpaid\"}," +
                    "{\"catId\": \"7\",\"catName\":\"Water\"}," +
                    "{\"catId\": \"8\",\"catName\":\"Loan Repayment\"}," +
                    "{\"catId\": \"10\",\"catName\":\"Life Insurance\"}," +
                    "{\"catId\": \"11\",\"catName\":\"Fastag\"}," +
                    "{\"catId\": \"12\",\"catName\":\"LPG Gas\"}," +
                    "{\"catId\": \"13\",\"catName\":\"Cable TV\"}," +
                    "{\"catId\": \"14\",\"catName\":\"Health Insurance\"}," +
                    "{\"catId\": \"15\",\"catName\":\"Education Fees\"}]";

    //Firestore Collection
    public static String DEFAULT_BILLER_COLLECTION = "DefaultBiller_Collection";
    public static String PREFIX_DEFAULT_BILLER_DOCUMENT = "CategoryId_";

    //catId
    public static String BROADBAND_POSTPAID = "1";
    public static String DTH = "2";
    public static String ELECTRICITY = "3";
    public static String GAS = "4";
    public static String LANDLINE_POSTPAID = "5";
    public static String MOBILE_POSTPAID = "6";
    public static String WATER = "7";
    public static String LOAN_REPAYMENT = "8";
    public static String LIFE_INSURANCE = "10";
    public static String FASTAG = "11";
    public static String LPG_GAS = "12";
    public static String CABLE_TV = "13";
    public static String HEALTH_INSURANCE = "14";
    public static String EDUCATION_FEES = "15";

    //BBPS Required Parameters
    public static String BBPS_PREF = "bbpsPref" ;
    public static String SF_PINCODE = "pincodeKey";
    public static String SF_MOBILE_NUMBER = "mobileNumberKey";
    public static String SF_LAT_LONG = "latlongKey";
    public static String SF_TERMINAL_ID = "terminalIdKey";
    public static String SF_AGENT_ID = "agentIDKey";
    public static String SF_KEYWORD = "keyboardKey";

    public static String PAY_CHANNEL = "BSC";
    public static String PAY_MODE = "Cash";


    public static void updateSession(Context context, String user_name, boolean status) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> map = new HashMap<>();
        map.put("app_unique_id", context.getPackageName());
        map.put("device_id", getDeviceID(context));
        map.put("login_datetime", System.currentTimeMillis());
        map.put("login_status", status);
        db.collection("CoreApp_Session_Manager").document(user_name)
                .set(map)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);

                    }
                });
    }

    private static String getDeviceID(Context activity) {
        return Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }
}
