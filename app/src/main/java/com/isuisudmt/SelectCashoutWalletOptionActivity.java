package com.isuisudmt;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.HashMap;

import static com.isuisudmt.bbps.utils.Const.isCashoutWallet1enabled;
import static com.isuisudmt.bbps.utils.Const.isCashoutWallet2enabled;

public class SelectCashoutWalletOptionActivity extends AppCompatActivity {
    RelativeLayout bank_trans_lay, wallet_trans_lay;
    Toolbar toolbar;
    Handler timer_handler;
    String _userName;
    boolean session_logout = false;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CustomThemes(this);
        setContentView(R.layout.activity_select_wallet_option);

        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);
        RelativeLayout item1 = findViewById(R.id.item1);
        RelativeLayout item2 = findViewById(R.id.item2);

        if (isCashoutWallet1enabled == false) {
            item1.setBackgroundResource(R.color.service_unavailable_col);
            item1.setEnabled(false);
        }
        if (isCashoutWallet2enabled == false) {
            item2.setBackgroundResource(R.color.service_unavailable_col);
            item2.setEnabled(false);
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        item1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SelectCashoutWalletOptionActivity.this, "Wallet1 cashout service is not available for application.", Toast.LENGTH_LONG).show();
            }
        });
        item2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SelectCashoutWalletOptionActivity.this, SelectCashoutOptionActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

 /*   @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        if(_userName!=null) {
            checkSessionExistance(_userName,true);
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
        //startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
    }

    public void startTimer(){

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(SelectCashoutOptionActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(SelectCashoutOptionActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(SelectCashoutOptionActivity.this, user_name, status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(SelectCashoutOptionActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/
}
